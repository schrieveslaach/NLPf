# Version 1.1.0 (not yet released)

- Support Java 9 and above
- Upgrade DKPro Core to 1.10.0
- Upgrade dependencies

## nlp-sonarqube-plugin

- Add SonarQube plugin. Fixes #12.

## nlp-machen-plugin

- Fixes #31

# Version 1.0.1

- Upgrade DKPro Core to 1.9.2

## nlp-maven-plugin

- Add logging information for isolated NLP tool test
- Fix loading of DKPro trainer classes
- Add target/classes as resource path during training NLP tools

## quick-pad-tagger

- Fixes #28

# Version 1.0.0

## io-odt

- Read ODT files as [`JCas` object](https://uima.apache.org/d/uimaj-current/apidocs/org/apache/uima/jcas/JCas.html) objects
- Write [`JCas` object](https://uima.apache.org/d/uimaj-current/apidocs/org/apache/uima/jcas/JCas.html) objects as ODT files
- Limited to paragraphs

## nlp-maven-plugin

- Custom packaging for corpora projects
- Use DKPro Core trainer classes to train custom NLP models (`mvn compile`)
- Evaluate NLP pipelines and determine best-performing NLP pipeline (`mvn test`)
- Package NLP models and configuratin of best-performing NLP pipeline into JAR file (`mvn package`)

## quick-pad-tagger

- Annotate text segmentation
- Annotate Part-of-speech tags
- Annotate named entities
- Suggest annotations based on previous annotations

## plumbing

- Integrate best-performing NLP pipeline into UIMAfit [`runPipeline`](https://uima.apache.org/d/uimafit-current/api/org/apache/uima/fit/pipeline/SimplePipeline.html)
