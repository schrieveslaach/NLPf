[![build status](https://gitlab.com/schrieveslaach/NLPf/badges/master/build.svg)](https://gitlab.com/schrieveslaach/NLPf/commits/master)
[![quality gate](https://sonarcloud.io/api/project_badges/measure?project=de.schrieveslaach.nlpf%3Amaster&metric=alert_status)](https://sonarcloud.io/dashboard?id=de.schrieveslaach.nlpf%3Amaster)
[![License: LGPL v3](https://img.shields.io/badge/License-LGPL%20v3-blue.svg)](https://www.gnu.org/licenses/lgpl-3.0)
[![Maven Central](https://maven-badges.herokuapp.com/maven-central/de.schrieveslaach.nlpf/nlpf-parent/badge.svg)](https://search.maven.org/#search%7Cga%7C1%7Cg%3A%22de.schrieveslaach.nlpf%22)

# NLP Lean Programming framework (NLPf)

![NLP Lean Programming framework (NLPf)](images/NLPf.svg.png)

As a developer, have you ever wondered how you build your domain-specific Natural Language Processing (NLP) models? For example, you need to write on Information Extraction system which requires a custom set of Named Entity Recognition (NER) models but nowhere in the World Wide Web you are able to find any tool or model. The project _“Towards Effective NLP Application Development”_ provides the required tools in a familiar environment to build and use your custom NLP models very efficiently.

This project is based on research results of the collaboration [ETL Quadrat](http://www.softwaresysteme.pt-dlr.de/media/content/Infoblatt_ETL_QUADRAT.pdf).
This project is an Open Source rewrite of the previous implementation and uses [DKPro Core™][1] to leverage the benefits to a wide variaty of
projects using DKPro Core™.

[![NLPf Tutorial](https://img.youtube.com/vi/44UJspVebTA/0.jpg)](https://www.youtube.com/watch?v=44UJspVebTA&autoplay=1&origin=https://gitlab.com/schrieveslaach/NLPf)

See also [the case study of Eyequant](https://www.eyequant.com/hubfs/Whitepapers/Case%20Studies/EyeQuant%20-%20FH%20Aachen%20Case%20Study.pdf) to graps the motivation of NLPf's Quick Pad Tagger.

## Project Goals

- Build your domain-specific corpus more effectively
  * Effective project management through Maven project management
  * Integrated annotation tool support through the Quick Pad Tagger
- Determine your best-performing NLP pipeline through build automation
  * Add NLP tools as Maven dependency and determine the best-performing NLP pipeline by `mvn test`
  * Deploy the best-performing NLP pipeline with its Models as Maven dependency into a Maven repository
- Integrate the best-performing NLP pipeline as Maven dependency

Following sections provide an example setup how you can use Maven and the annotation to build your NLP models more efficiently.
Make sure that you meet the [requirements](#requirements). 

## Create Your Corpus

Create an empty directory and create a `pom.xml` file:

```xml
<project>
    <modelVersion>4.0.0</modelVersion>

    <groupId>de.your.company</groupId>
    <artifactId>test-corpus</artifactId>
    <version>1.0-SNAPSHOT</version>
    <packaging>nlp-models</packaging>

    <build>
        <plugins>
            <plugin>
                <groupId>de.schrieveslaach.nlpf</groupId>
                <artifactId>nlp-maven-plugin</artifactId>
                <version>1.0.0</version>
                <extensions>true</extensions>
            </plugin>
        </plugins>
    </build>
</project>
```

Add your files, which you want to annotate in ordert to build custom NLP models, 
into the directory `src/main/corpus`. We recommend to use ODT documents ([OpenDocument-Text][2]) 
because you can start annotating them from scratch. When you are done with 
annotating documents (c. f. [Annotate Your Corpus](#annotate-your-corpus)),
you can perform multiple Maven goals. Furthermore, add a Maven dependency which
contains the corresponding reader and writer implementations for DKPro Core™.

```xml
<project>
    <!-- ... see above ... -->

    <dependencies>
        <!-- Read write ODT files -->
        <dependency>
            <groupId>de.schrieveslaach.nlpf</groupId>
            <artifactId>io-odt</artifactId>
            <version>1.0.0</version>
        </dependency>
    </dependencies>
</project>
```

Furthermore, you need to place your test documents into `src/test/corpus`.

## Annotate Your Corpus

[Download the Quick Pad Tagger](https://gitlab.com/schrieveslaach/NLPf/-/jobs/53855551/artifacts/browse/quick-pad-tagger/target/). 
After that, open the `quick-pad-tagger-1.0.0.exe` file or execute `java -jar quick-pad-tagger-1.0.0.jar` to start the annotation tool.
The Quick Pad Tagger has been tested on Linux, Windows 8 & 10, and macOS.

Use the [cheat sheet](quick-pad-tagger/Cheat Sheet.md) to look up the controls of the Quick Pad Tagger. Following videos show the capabilities of the annotation tool.

### Text-Segmentation Tagging

![Text-Segmentation Tagging](videos/Text-Segmentation.mp4)

### Part-of-Speech Tagging

![Part-of-speech Tagging](videos/POS-Tagging.mp4)

### Troubleshooting

#### Xbox 360 Controller

On Windows and Linux the Xbox 360 controller should work out-of-the-box. If you want to use the controller on macOS, you need to follow the instructions of [this page](https://github.com/360Controller/360Controller).

## Determine Your Best-performing NLP Pipeline

As you do not know which NLP tool combination works in your domain, the
[nlp-maven-plugin](nlp-maven-plugin/) supports you, your developers, and your
annotators in order to find the best-performing NLP pipeline. Just add DKPro Core™
dependencies to your `pom.xml` and execute the commands below.

```xml
<project>
    <!-- ... see above ... -->

    <dependencies>
        <dependency>
            <groupId>de.tudarmstadt.ukp.dkpro.core</groupId>
            <artifactId>de.tudarmstadt.ukp.dkpro.core.opennlp-asl</artifactId>
            <version>1.9.2</version>
        </dependency>
    </dependencies>
</project>
```

```bash
# Compile the NLP models from the annotated corpus
mvn compile

# Evaluates the compiled NLP models against test documents
mvn test

# Packages the best-performing NLP pipeline configuration with models and deploys
# it to your Maven repository
mvn package
mvn deploy
```

NLPf supports multiple NLP frameworks. Following table provides an overview.

| Tool             | Maven Artifact Id                               | Segmenter Training   | Named Entity Recognition Trainer | POS Tagging Trainer                      |
| ---------------- | ----------------------------------------------- | -------------------- | -------------------------------- | ---------------------------------------- |
| OpenNLP          | `de.tudarmstadt.ukp.dkpro.core.opennlp-asl`     | :white_check_mark:   | :white_check_mark:               | :white_check_mark:                       |
| Stanford CoreNLP | `de.tudarmstadt.ukp.dkpro.core.stanfordnlp-gpl` | :white_large_square: | :white_check_mark:               | :white_check_mark:                       |
| LingPipe         | `de.tudarmstadt.ukp.dkpro.core.lingpipe-gpl`    | :white_large_square: | :white_check_mark:               | :white_large_square:                     |
| ArkTweet         | `de.tudarmstadt.ukp.dkpro.core.arktools-gpl`    | :white_large_square: | :white_large_square:             | :white_check_mark: (since version 1.1.0) |

If you want to add further support for more trainers to DKPro Core™, see this 
[pull request on Github](https://github.com/dkpro/dkpro-core/pull/1080) for 
more information. For example, this [pull request](https://github.com/dkpro/dkpro-core/pull/1114) 
adds LingPipe's NER trainer to DKPro Core™.

## Configure Corpus

The configuration of the project can be controlled by the Maven project. For example, you can define the POS tag set by
properties:

```xml
<project>
    <!-- … -->
    <properties>
        <pos.tag.set>ptb</pos.tag.set>
    </properties>
    <!-- … -->
</project>
```

Here is the list of configuration parameters.

| Property name               | Default                        | Description                                                                                                                                                                                                                                              |
| --------------------------- | ------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| `named.entity.types`        | `location,person,organization` | Comma separated list of named entity types, for example, `person, location, organization`                                                                                                                                                                |
| `pos.tag.set`               | `ud`                           | The POS tag set used for part of speech tagging. The full list of POS tag set can be retrieved [here](https://github.com/dkpro/dkpro-core/tree/master/dkpro-core-api-lexmorph-asl/src/main/resources/de/tudarmstadt/ukp/dkpro/core/api/lexmorph/tagset). |
| `default.document.language` | `en`                           | The language used for documents which do not specify the [document's langauge](https://uima.apache.org/d/uimaj-2.10.1/apidocs/org/apache/uima/jcas/JCas.html#getDocumentLanguage--).                                                                     |

Additionally, you can configure DKPro Core's trainer and annotator classes through the Maven properties.
For example, the [`OpenNlpSentenceTrainer`](https://github.com/dkpro/dkpro-core/blob/master/dkpro-core-opennlp-asl/src/main/java/de/tudarmstadt/ukp/dkpro/core/opennlp/OpenNlpSentenceTrainer.java) provides the parameter `PARAM_ITERATIONS` with default value `100`. You can define a property to change this value to `200`, as shown below:

```xml
<project>
<!-- … -->
  <properties>
    <pos.tag.set>ptb</pos.tag.set>
    <!-- Other properties -->
    <de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSentenceTrainer.iterations>200</de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSentenceTrainer.iterations>
  </properties>
<!-- … -->
</project>
```

Search for [trainer](https://github.com/dkpro/dkpro-core/search?utf8=%E2%9C%93&q=trainer&type=) in the code repository of DKPro Core to find more trainer classes and their parameters.

### Add Custom NLP Steps to Your Pipeline

If you want to add further analysis steps to your NLP pipeline, you can add factory methods to the test classpath. Create a Java
class in `src/test/java` which with a name equal to the pattern `*EngineFactory.java`. For example, the following factory creates 
a configuration of the [`StanfordSegmenter`](https://nlp.stanford.edu/software/segmenter.html) which will be tested during `mvn test`. 
If this segmenter performs better than trained segmenters, it will be added to the configuration of the best-performing NLP pipeline.

Clarification: the `StanfordSegmenter` won't be tested automatically by the maven plugin because it does not provide any training mechanism. 
You can use the factory feature in order to integrate custom NLP analysis, for example, you could integrate a dictionary based named entity recognizer.  

```java
import de.tudarmstadt.ukp.dkpro.core.stanfordnlp.StanfordSegmenter;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

public class CustomAnalysisEngineFactory {
    
    public AnalysisEngineDescription createStanfordSegmenter() throws Exception {
        return createEngineDescription(
                StanfordSegmenter.class,
                StanfordSegmenter.PARAM_LANGUAGE_FALLBACK, "en"
        );
    }
    
}
```

You can add as many factory methods as you want to any factory class with the pattern `*EngineFactory.java`. The maven 
plugin expects methods returning an object of AnalysisEngineDescription without parameters. Additionally, the factory 
class must have a default constructor.   

## Use The Best-Perfroming NLP Pipeline in Your Application

When you deployed your best-performing NLP pipeline to your Maven repository,
you can add your pipeline as Maven dependency to your project which requires
an NLP pipeline:

```xml
<project>
    <dependencies>
        <!-- Library for executing your best-performing NLP pipeline -->
        <dependency>
            <groupId>de.schrieveslaach.nlpf</groupId>
            <artifactId>plumbing</artifactId>
            <version>1.0.0</version>
        </dependency>

        <!-- your corpus -->
        <dependency>
            <groupId>de.your.company</groupId>
            <artifactId>test-corpus</artifactId>
            <version>1.0-SNAPSHOT</version>
        </dependency>
    </dependencies>
</project>
```

Then, in your application configure your project to run your best-performing NLP pipeline:

```java
import de.tudarmstadt.ukp.dkpro.core.io.text.TextReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;

import java.io.File;

import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static de.schrieveslaach.nlpf.plumbing.BestPerformingPipelineFactory.createBestPerformingPipelineEngineDescription;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;

public class App {

    public static void main(String[] args) throws Exception {

        CollectionReaderDescription readerDescription = createReaderDescription(
                TextReader.class,
                TextReader.PARAM_SOURCE_LOCATION, new File("test.txt")
        );

        AnalysisEngineDescription writerDescription = createEngineDescription(
                XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, new File("."),
                XmiWriter.PARAM_OVERWRITE, true
        );

        runPipeline(readerDescription,
                createBestPerformingPipelineEngineDescription(),
                writerDescription
        );
    }
}
```

# Requirements

- Java 8 (Java 9 is not yet supported)
- at least Maven 3.5
- If you are using OpenJDK make sure that you installed OpenJFX as well, for example, on Arch Linux OpenJDK and OpenJFX are separated packages.

# Build Project

The project can be installed via Maven, for example:

```bash
mvn install
```

If you want to skip the unit tests, use following command:

```bash
mvn install -Dskip.unit.tests
```

If you want to skip the integration tests, use following command:

```bash
mvn install -DskipTests
```

If you want to run the acceptance tests, use following command:

```bash
mvn test -P acceptance-tests
```

If you want to run Sonarqube plugin locally, use following commands (requires running Docker environment):

```bash
cd nlp-sonarqube-plugin
mvn clean package
docker run --rm -it \
    -p 9000:9000 -p 9092:9092 \
    -v $(pwd)/target/nlp-sonarqube-plugin-<NLPf version>.jar:/opt/sonarqube/extensions/plugins/nlp-sonarqube-plugin.jar \
    sonarqube:6.7-alpine
```

# How to Cite?

<p>M. Schreiber, B. Kraft, and A. Zündorf. (2018). 
<strong>“NLP Lean Programming Framework: Devel-oping NLP Applications More Effectively”</strong>. In Proceedings of the 2018 Conference of
the North American Chapter of the Association for Computational Linguistics: Demonstrations, p 1-5, New Orleans, Louisiana. 
<a href="http://dx.doi.org/10.18653/v1/N18-5001">(pdf)</a> <a href="https://aclanthology.coli.uni-saarland.de/papers/N18-5001/n18-5001.bib">(bib)</a>
</p>

[1]: https://github.com/dkpro/dkpro-core
[2]: https://en.wikipedia.org/wiki/OpenDocument
