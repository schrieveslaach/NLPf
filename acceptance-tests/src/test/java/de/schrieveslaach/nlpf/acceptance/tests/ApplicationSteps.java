package de.schrieveslaach.nlpf.acceptance.tests;

/*-
 * ========================LICENSE_START=================================
 * acceptance-tests
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import org.apache.commons.io.FileUtils;
import org.apache.uima.jcas.JCas;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.Then;
import org.jbehave.core.annotations.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Random;
import java.util.stream.Collectors;

import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.io.FileMatchers.anExistingFile;
import static org.junit.Assert.assertThat;

public class ApplicationSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(ApplicationSteps.class);

    private File applicationSourceDirectory;

    /**
     * Path to the created JAR application
     */
    private File jarFile;

    @AfterStories
    public void cleanUp() {
        FileUtils.deleteQuietly(applicationSourceDirectory);
    }

    @Given("an example NLP application")
    public void createSampleNlpApplication() throws Exception {
        Random r = new Random(System.currentTimeMillis());
        applicationSourceDirectory = new File(FileUtils.getTempDirectory(), "app-" + r.nextInt());

        FileUtils.copyFile(
                new File(getClass().getResource("pom-app-template.xml").toURI()),
                new File(applicationSourceDirectory, "pom.xml")
        );

        File sourceCodeDirectory = new File(applicationSourceDirectory, "src/main/java");
        FileUtils.copyFileToDirectory(
                new File(getClass().getResource("App.java").toURI()),
                sourceCodeDirectory
        );

        FileUtils.copyFileToDirectory(
                new File(getClass().getResource("test.odt").toURI()),
                applicationSourceDirectory
        );
    }

    @When("the spring-maven plugin builds the NLP application")
    public void packageApplication() throws Exception {
        ProcessBuilder builder = new ProcessBuilder("mvn", "package")
                .directory(applicationSourceDirectory);

        Process process = builder.start();
        try (BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            String s;
            while ((s = stdout.readLine()) != null) {
                LOGGER.info(s);
            }
        } finally {
            process.destroy();
        }

        int exitCode = process.waitFor();
        LOGGER.info("Exit value: " + exitCode);
        assertThat(exitCode, is(equalTo(0)));

        jarFile = new File(applicationSourceDirectory, "target/app.jar");
        assertThat(jarFile, is(anExistingFile()));
    }

    @When("the client application uses the best-performing NLP pipeline during runtime")
    public void runApplication() throws Exception {
        ProcessBuilder builder = new ProcessBuilder("java", "-jar", jarFile.toString())
                .directory(applicationSourceDirectory);

        Process process = builder.start();
        try (BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            String s;
            while ((s = stdout.readLine()) != null) {
                LOGGER.info(s);
            }
        } finally {
            process.destroy();
        }

        int exitCode = process.waitFor();
        LOGGER.info("Exit value: " + exitCode);
        assertThat(exitCode, is(equalTo(0)));

        File resultingFile = new File(applicationSourceDirectory, "test.odt.xmi");
        assertThat(resultingFile, is(anExistingFile()));
    }

    @Then("the resulting XMI file should contain named entities for person and organization")
    public void assertThatResultingFileContainsNamedEntities() throws Exception {
        File resultingFile = new File(applicationSourceDirectory, "test.odt.xmi");

        for (JCas jCas : iteratePipeline(createReaderDescription(XmiReader.class, XmiReader.PARAM_SOURCE_LOCATION, resultingFile))) {
            List<NamedEntity> persons = select(jCas, NamedEntity.class).stream()
                    .filter(ne -> "person".equals(ne.getValue()))
                    .collect(Collectors.toList());

            List<NamedEntity> organizations = select(jCas, NamedEntity.class).stream()
                    .filter(ne -> "organization".equals(ne.getValue()))
                    .collect(Collectors.toList());

            assertThat(persons, is(not(empty())));
            assertThat(organizations, is(not(empty())));
        }
    }
}
