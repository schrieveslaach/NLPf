package de.schrieveslaach.nlpf.acceptance.tests;

/*-
 * ========================LICENSE_START=================================
 * acceptance-tests
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import org.apache.commons.io.FileUtils;
import org.apache.uima.jcas.JCas;
import org.jbehave.core.annotations.AfterStories;
import org.jbehave.core.annotations.Given;
import org.jbehave.core.annotations.When;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.BufferedReader;
import java.io.File;
import java.io.InputStreamReader;
import java.util.Random;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createSatyaNadellaExample;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createTorwaldsExample;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class CorpusSteps {

    private static final Logger LOGGER = LoggerFactory.getLogger(CorpusSteps.class);

    private File corpusDirectory;

    @AfterStories
    public void cleanUp() {
        FileUtils.deleteQuietly(corpusDirectory);
    }

    @Given("an existing annotated corpus")
    public void createAnnotatedCorpusProject() throws Exception {
        Random r = new Random(System.currentTimeMillis());
        corpusDirectory = new File(FileUtils.getTempDirectory(), "corpus-" + r.nextInt());

        FileUtils.copyFile(
                new File(getClass().getResource("pom-corpus-template.xml").toURI()),
                new File(corpusDirectory, "pom.xml")
        );

        File corpusFilesDirectory = new File(corpusDirectory, "src/main/corpus");
        JCas openNlpExample = createOpenNlpExample();
        runPipeline(openNlpExample, createEngineDescription(XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, corpusFilesDirectory,
                XmiWriter.PARAM_OVERWRITE, true));

        JCas torwaldsExample = createTorwaldsExample();
        runPipeline(torwaldsExample, createEngineDescription(XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, corpusFilesDirectory,
                XmiWriter.PARAM_OVERWRITE, true));

        File corpusTestFilesDirectory = new File(corpusDirectory, "src/test/corpus");
        JCas satyaNadellaExample = createSatyaNadellaExample();
        runPipeline(satyaNadellaExample, createEngineDescription(XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, corpusFilesDirectory,
                XmiWriter.PARAM_OVERWRITE, true));
        runPipeline(satyaNadellaExample, createEngineDescription(XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, corpusTestFilesDirectory,
                XmiWriter.PARAM_OVERWRITE, true));
    }

    @When("the nlp-maven plugin installs the NLP models into local Maven repository")
    public void runMavenInstall() throws Exception {
        ProcessBuilder builder = new ProcessBuilder("mvn", "install")
                .directory(corpusDirectory);

        Process process = builder.start();
        try (BufferedReader stdout = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
            String s;
            while ((s = stdout.readLine()) != null) {
                LOGGER.info(s);
            }
        } finally {
            process.destroy();
        }

        int exitCode = process.waitFor();
        LOGGER.info("Exit value: " + exitCode);
        assertThat(exitCode, is(equalTo(0)));
    }
}
