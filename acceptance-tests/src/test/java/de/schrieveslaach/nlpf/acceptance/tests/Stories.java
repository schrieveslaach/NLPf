package de.schrieveslaach.nlpf.acceptance.tests;

/*-
 * ========================LICENSE_START=================================
 * acceptance-tests
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.jbehave.core.configuration.Configuration;
import org.jbehave.core.configuration.MostUsefulConfiguration;
import org.jbehave.core.embedder.Embedder;
import org.jbehave.core.io.CodeLocations;
import org.jbehave.core.io.LoadFromClasspath;
import org.jbehave.core.reporters.CrossReference;
import org.jbehave.core.reporters.StoryReporterBuilder;
import org.jbehave.core.steps.InjectableStepsFactory;
import org.jbehave.core.steps.InstanceStepsFactory;
import org.jbehave.core.steps.SilentStepMonitor;
import org.junit.Test;

import static java.util.Arrays.asList;
import static org.jbehave.core.reporters.Format.CONSOLE;
import static org.jbehave.core.reporters.Format.HTML;
import static org.jbehave.core.reporters.Format.TXT;
import static org.jbehave.core.reporters.Format.XML;

public class Stories {

    @Test
    public void runClasspathLoadedStoriesAsJUnit() {
        Embedder embedder = new MyEmbedder();
        embedder.embedderControls().useStoryTimeouts(String.valueOf(60 * 30));
        embedder.runStoriesAsPaths(asList("de/schrieveslaach/nlpf/acceptance/tests/build-and-use-pipeline.story"));
    }

    private static class MyEmbedder extends Embedder {

        @Override
        public Configuration configuration() {
            Class<?> cls = getClass();
            return new MostUsefulConfiguration()
                    .useStoryLoader(new LoadFromClasspath(cls.getClassLoader()))
                    .useStoryReporterBuilder(new StoryReporterBuilder()
                            .withCodeLocation(CodeLocations.codeLocationFromClass(cls))
                            .withDefaultFormats()
                            .withFormats(CONSOLE, TXT, HTML, XML)
                            .withCrossReference(new CrossReference()))
                    .useStepMonitor(new SilentStepMonitor());
        }

        @Override
        public InjectableStepsFactory stepsFactory() {
            return new InstanceStepsFactory(configuration(),
                    new CorpusSteps(),
                    new ApplicationSteps()
            );
        }
    }

}
