/*-
 * ========================LICENSE_START=================================
 * acceptance-tests
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */
import de.schrieveslaach.nlpf.io.odt.OdtReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;

import java.io.File;

import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static de.schrieveslaach.nlpf.plumbing.BestPerformingPipelineFactory.createBestPerformingPipelineEngineDescription;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;

public class App {

    public static void main(String[] args) throws Exception {

        CollectionReaderDescription readerDescription = createReaderDescription(
                OdtReader.class,
                OdtReader.PARAM_SOURCE_LOCATION, new File("test.odt")
        );

        AnalysisEngineDescription writerDescription = createEngineDescription(
                XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, new File("."),
                XmiWriter.PARAM_OVERWRITE, true
        );

        runPipeline(readerDescription,
                createBestPerformingPipelineEngineDescription(),
                writerDescription
        );
    }
}
