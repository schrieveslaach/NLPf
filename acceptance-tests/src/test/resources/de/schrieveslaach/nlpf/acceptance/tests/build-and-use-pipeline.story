Feature: Build NLP pipeline and use it in NLP application

Narrative:
In order to be able to use the best-performing NLP pipeline
As a development team member who is familiar with Java and Maven
I want to build NLP pipeline with a Maven plugin
and I want to use the best-performing NLP pipeline as Maven dependency

Scenario: Determine and use best-performing NLP through Maven

Given an existing annotated corpus
And an example NLP application
When the nlp-maven plugin installs the NLP models into local Maven repository
And the spring-maven plugin builds the NLP application
And the client application uses the best-performing NLP pipeline during runtime
Then the resulting XMI file should contain named entities for person and organization
