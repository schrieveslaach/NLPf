package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableSet;
import de.schrieveslaach.nlpf.io.odt.types.OfficeAnnotation;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph;
import org.apache.uima.jcas.tcas.Annotation;

class Constants {

    public static final ImmutableSet<Class<? extends Annotation>> ODT_STRUCTURE_CLASSES;

    static {
        ODT_STRUCTURE_CLASSES = ImmutableSet.<Class<? extends Annotation>>builder()
                .add(OfficeAnnotation.class)
                .add(Paragraph.class)
                .add(DocumentMetaData.class)
                .build();
    }

    private Constants() {
        throw new IllegalStateException("Utility class");
    }
}
