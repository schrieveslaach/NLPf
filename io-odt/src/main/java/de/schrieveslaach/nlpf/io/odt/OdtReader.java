package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */


import de.schrieveslaach.nlpf.io.odt.types.OfficeAnnotation;
import de.tudarmstadt.ukp.dkpro.core.api.io.JCasResourceCollectionReader_ImplBase;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import org.apache.commons.io.FileUtils;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.fit.descriptor.MimeTypeCapability;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.CasCopier;
import org.apache.xerces.dom.TextImpl;
import org.odftoolkit.odfdom.dom.element.office.OfficeAnnotationElement;
import org.odftoolkit.odfdom.dom.element.office.OfficeAnnotationEndElement;
import org.odftoolkit.odfdom.dom.element.text.TextLineBreakElement;
import org.odftoolkit.odfdom.dom.element.text.TextPElement;
import org.odftoolkit.odfdom.dom.element.text.TextSpanElement;
import org.odftoolkit.odfdom.pkg.OdfElement;
import org.odftoolkit.simple.TextDocument;
import org.odftoolkit.simple.meta.Meta;
import org.w3c.dom.Node;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;
import static org.apache.uima.fit.util.JCasUtil.select;

@MimeTypeCapability("application/vnd.oasis.opendocument.text")
@TypeCapability(
        outputs = {
                "de.schrieveslaach.effective.nlp.application.development.io.odt.types.OfficeAnnotation",
                "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph"})
@SuppressWarnings("squid:MaximumInheritanceDepth")
public class OdtReader extends JCasResourceCollectionReader_ImplBase {

    private static final String ELEMENT_SEPARATOR = "\n";

    private final Map<Class<? extends OdfElement>, OdfElementConverter> converters;

    public OdtReader() {
        this.converters = new HashMap<>();

        this.converters.put(TextPElement.class, e -> {
            StringBuilder content = new StringBuilder();

            for (int i = 0; i < e.getChildNodes().getLength(); ++i) {
                Node item = e.getChildNodes().item(i);
                if (item instanceof TextImpl) {
                    content.append(((TextImpl) item).getWholeText());
                } else if (item instanceof TextLineBreakElement) {
                    content.append("\n");
                } else if (item instanceof TextSpanElement) {
                    content.append(((TextSpanElement) item).getTextContent());
                }
            }

            return content.toString();
        });

        this.converters.put(OfficeAnnotationElement.class, e -> "");
    }

    @Override
    public void getNext(JCas aJCas) throws CollectionException {
        Resource resource = nextFile();
        initCas(aJCas, resource);

        Map<OdfElement, Annotation> elementAnnotationMap = new HashMap<>();

        try (InputStream is = resource.getInputStream(); TextDocument textDocument = TextDocument.loadDocument(is)) {
            Meta meta = new Meta(textDocument.getMetaDom());
            String language = meta.getLanguage();
            if (language != null) {
                aJCas.setDocumentLanguage(language);
            }

            DocumentMetaData metaData = DocumentMetaData.get(aJCas);
            metaData.setDocumentTitle(meta.getTitle());

            setDocumentText(aJCas, textDocument);
            setOdtStructureAnnotations(aJCas, textDocument, elementAnnotationMap);
            loadUimaAnnotations(aJCas, textDocument);
        } catch (Exception ex) {
            throw new CollectionException(ex);
        }
    }

    private void loadUimaAnnotations(JCas aJCas, TextDocument textDocument) throws IOException, ResourceInitializationException {
        if (textDocument.getPackage().getFilePaths().contains("UIMA/cas.xmi")) {
            Path path = Files.createTempFile("cas", ".xmi");
            Files.copy(textDocument.getPackage().getInputStream("UIMA/cas.xmi"), path, StandardCopyOption.REPLACE_EXISTING);

            // Prepare reading CAS from xml file
            JCas jCasFromCasXmi = iteratePipeline(createReaderDescription(
                    XmiReader.class, PARAM_SOURCE_LOCATION, path.toFile()
            )).iterator().next();

            copyMetaData(jCasFromCasXmi, aJCas);

            // Needed to copy FeatureStructures
            CasCopier copier = new CasCopier(jCasFromCasXmi.getCas(), aJCas.getCas());

            // walk through annotations
            for (Annotation a : select(jCasFromCasXmi, Annotation.class)) {
                // continue if it's an ODT structure class
                if (Constants.ODT_STRUCTURE_CLASSES.stream().anyMatch(cls -> cls.isAssignableFrom(a.getClass()))) {
                    continue;
                }
                aJCas.addFsToIndexes(copier.copyFs(a));
            }

            // cleanup temp dir
            FileUtils.deleteQuietly(path.toFile());
        }
    }

    private void copyMetaData(JCas src, JCas dest) {
        DocumentMetaData srcMetaData = DocumentMetaData.get(src);
        DocumentMetaData destMetaData = DocumentMetaData.get(dest);

        String documentBaseUri = srcMetaData.getDocumentBaseUri();
        if (documentBaseUri != null) {
            destMetaData.setDocumentBaseUri(documentBaseUri);
        }

        String documentUri = srcMetaData.getDocumentUri();
        if (documentUri != null) {
            destMetaData.setDocumentUri(documentUri);
        }
    }

    private void setDocumentText(JCas aJCas, TextDocument textDocument) {
        StringBuilder documentText = new StringBuilder();

        Iterator<OdfElement> it = new TextDocumentIterator(textDocument);
        while (it.hasNext()) {
            OdfElement element = it.next();
            documentText
                    .append(convertToText(element))
                    .append(ELEMENT_SEPARATOR);
        }

        aJCas.setDocumentText(documentText.toString());
    }

    private String convertToText(OdfElement element) {
        Optional<OdfElementConverter> firstConverter = converters.entrySet().stream()
                .filter(e -> e.getKey().isAssignableFrom(element.getClass()))
                .map(Map.Entry::getValue)
                .findFirst();

        if (firstConverter.isPresent()) {
            return firstConverter.get().toString(element);
        }

        return element.getTextContent();
    }

    private void setOdtStructureAnnotations(JCas aJCas, TextDocument textDocument, Map<OdfElement, Annotation> elementAnnotationMap) {
        Iterator<OdfElement> it = new TextDocumentIterator(textDocument);

        int begin = 0;
        OfficeAnnotation officeAnnotation = null;
        Annotation annotation = null;
        while (it.hasNext()) {
            OdfElement element = it.next();
            int length = convertToText(element).length();

            if (element instanceof TextPElement) {
                annotation = new Paragraph(aJCas, begin, begin + length);
            } else if (element instanceof OfficeAnnotationElement) {
                officeAnnotation = new OfficeAnnotation(aJCas);
                officeAnnotation.setCreator(element.getChildNodes().item(0).getTextContent());
                officeAnnotation.setDate(element.getChildNodes().item(1).getTextContent());
                officeAnnotation.setText(element.getChildNodes().item(3).getTextContent());

                officeAnnotation.setBegin(findAnnotationElementIndex(
                        (OdfElement) element.getParentNode(),
                        element,
                        elementAnnotationMap
                ));
            } else if ((element instanceof OfficeAnnotationEndElement) && officeAnnotation != null) {
                annotation = officeAnnotation;

                officeAnnotation.setEnd(findAnnotationElementIndex(
                        (OdfElement) element.getParentNode(),
                        element,
                        elementAnnotationMap
                ));
            }

            if (annotation != null) {
                annotation.addToIndexes();
                elementAnnotationMap.put(element, annotation);
                annotation = null;
            }

            begin += length + ELEMENT_SEPARATOR.length();

        }
    }

    private int findAnnotationElementIndex(OdfElement parent, OdfElement annotationElement, Map<OdfElement, Annotation> elementAnnotationMap) {
        int index = elementAnnotationMap.get(parent).getBegin();

        for (int i = 0; i < parent.getChildNodes().getLength(); ++i) {
            Node item = parent.getChildNodes().item(i);

            if (item == annotationElement) {
                break;
            }

            if (item instanceof TextImpl) {
                index += ((TextImpl) item).getWholeText().length();
            }
        }

        return index;
    }

    @FunctionalInterface
    private interface OdfElementConverter {

        String toString(OdfElement odfElement);

    }
}
