package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */


import de.schrieveslaach.nlpf.io.odt.types.OfficeAnnotation;
import de.tudarmstadt.ukp.dkpro.core.api.io.JCasFileWriter_ImplBase;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Div;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.MimeTypeCapability;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.xerces.dom.TextImpl;
import org.odftoolkit.odfdom.dom.element.office.OfficeAnnotationElement;
import org.odftoolkit.odfdom.dom.element.text.TextPElement;
import org.odftoolkit.odfdom.pkg.OdfElement;
import org.odftoolkit.odfdom.pkg.OdfPackage;
import org.odftoolkit.simple.TextDocument;
import org.odftoolkit.simple.meta.Meta;
import org.w3c.dom.Text;

import java.io.File;
import java.io.OutputStream;
import java.nio.file.Files;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectCovering;

@MimeTypeCapability("application/vnd.oasis.opendocument.text")
@TypeCapability(
        inputs = {
                "de.schrieveslaach.effective.nlp.application.development.io.odt.types.OfficeAnnotation",
                "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph"})
@SuppressWarnings("squid:MaximumInheritanceDepth")
public class OdtWriter extends JCasFileWriter_ImplBase {

    /**
     * Specify the suffix of output files. Default value <code>.odt</code>. If the suffix is not
     * needed, provide an empty string as value.
     */
    public static final String PARAM_FILENAME_EXTENSION = ComponentParameters.PARAM_FILENAME_EXTENSION;
    @ConfigurationParameter(name = PARAM_FILENAME_EXTENSION, mandatory = true, defaultValue = ".odt")
    private String filenameSuffix;

    public void process(JCas jCas) throws AnalysisEngineProcessException {
        try (TextDocument odt = TextDocument.newTextDocument()) {

            Map<Annotation, OdfElement> annotationOdfElementMap = new HashMap<>();

            // Remove the empty paragraph
            odt.removeParagraph(odt.getParagraphByIndex(0, false));

            writeContent(odt, jCas, annotationOdfElementMap);
            writeAnnotations(odt, jCas, annotationOdfElementMap);
            storeUimaAnnotations(odt, jCas);

            Meta meta = new Meta(odt.getMetaDom());
            meta.setLanguage(jCas.getDocumentLanguage());

            DocumentMetaData metaData = DocumentMetaData.get(jCas);
            meta.setTitle(metaData.getDocumentTitle());

            try (OutputStream docOS = getOutputStream(jCas, filenameSuffix)) {
                odt.save(docOS);
            }
        } catch (Exception e) {
            throw new AnalysisEngineProcessException(e);
        }
    }

    private void storeUimaAnnotations(TextDocument odt, JCas jCas) throws Exception {
        File tempDir = Files.createTempDirectory("UIMA").toFile();
        File casFile = new File(tempDir, "cas.xmi");
        File typeSystemFile = new File(tempDir, "TypeSystem.xml");
        runPipeline(jCas, createEngineDescription(XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, casFile,
                XmiWriter.PARAM_SINGULAR_TARGET, true,
                XmiWriter.PARAM_TYPE_SYSTEM_FILE, typeSystemFile
        ));

        OdfPackage pkg = odt.getPackage();
        // insert directory
        pkg.insert((byte[]) null, "UIMA/", null);

        // insert UIMA files
        pkg.insert(casFile.toURI(), "UIMA/cas.xmi", "text/xml");
        pkg.insert(typeSystemFile.toURI(), "UIMA/TypeSystem.xml", "text/xml");

        // cleanup temp dir
        FileUtils.deleteQuietly(tempDir);
    }

    private void writeContent(TextDocument textDocument, JCas jCas, Map<Annotation, OdfElement> annotationOdfElementMap) {
        for (Paragraph paragraph : select(jCas, Paragraph.class)) {
            annotationOdfElementMap.put(paragraph,
                    textDocument.addParagraph(paragraph.getCoveredText()).getOdfElement()
            );
        }
    }

    @SneakyThrows
    private void writeAnnotations(TextDocument textDocument, JCas jCas, Map<Annotation, OdfElement> annotationOdfElementMap) {
        for (OfficeAnnotation officeAnnotation : select(jCas, OfficeAnnotation.class)) {
            textDocument.getContentDom().newOdfElement(OfficeAnnotationElement.class);

            List<Div> divs = selectCovering(jCas, Div.class, officeAnnotation.getBegin(), officeAnnotation.getEnd());
            if (divs.isEmpty()) {
                continue;
            }

            // Should contain just one diff
            Div firstDiv = divs.get(0);
            int annotationBegin = firstDiv.getBegin() - officeAnnotation.getBegin();
            int annotationLength = officeAnnotation.getEnd() - officeAnnotation.getBegin();

            // split text before and after annotation
            TextPElement element = (TextPElement) annotationOdfElementMap.get(firstDiv);
            TextImpl node = (TextImpl) element.getChildNodes().item(0);
            Text textInsideAnnotation = node.splitText(annotationBegin);
            Text textAfterAnnotation = textInsideAnnotation.splitText(annotationLength);

            //
            OfficeAnnotationElement annotationElement = createOfficeAnnotationElement(officeAnnotation, element);
            element.appendChild(textInsideAnnotation);
            element.newOfficeAnnotationEndElement(annotationElement.getOfficeNameAttribute());
            element.appendChild(textAfterAnnotation);
        }

    }

    private OfficeAnnotationElement createOfficeAnnotationElement(OfficeAnnotation officeAnnotation, TextPElement element) {
        OfficeAnnotationElement annotationElement = element.newOfficeAnnotationElement();

        // Dublin core data
        annotationElement.newDcCreatorElement().setTextContent(officeAnnotation.getCreator());
        annotationElement.newDcDateElement().setTextContent(officeAnnotation.getDate());
        annotationElement.newTextPElement().newTextSpanElement().setTextContent(officeAnnotation.getText());

        annotationElement.setOfficeNameAttribute(UUID.randomUUID().toString());
        return annotationElement;
    }
}
