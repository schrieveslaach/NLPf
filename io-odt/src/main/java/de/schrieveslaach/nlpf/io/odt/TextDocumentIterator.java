package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.SneakyThrows;
import org.apache.xerces.dom.DocumentImpl;
import org.odftoolkit.odfdom.dom.element.office.OfficeAnnotationElement;
import org.odftoolkit.odfdom.dom.element.office.OfficeAnnotationEndElement;
import org.odftoolkit.odfdom.dom.element.office.OfficeTextElement;
import org.odftoolkit.odfdom.dom.element.table.TableTableCellElement;
import org.odftoolkit.odfdom.dom.element.text.TextHElement;
import org.odftoolkit.odfdom.dom.element.text.TextPElement;
import org.odftoolkit.odfdom.pkg.OdfElement;
import org.odftoolkit.simple.TextDocument;
import org.w3c.dom.Node;
import org.w3c.dom.traversal.NodeIterator;

import java.util.Iterator;
import java.util.NoSuchElementException;

import static org.w3c.dom.traversal.NodeFilter.FILTER_ACCEPT;
import static org.w3c.dom.traversal.NodeFilter.FILTER_SKIP;

public class TextDocumentIterator implements Iterator<OdfElement> {

    private final NodeIterator nodeIterator;

    private Node currentNode;

    @SneakyThrows
    public TextDocumentIterator(TextDocument document) {
        OfficeTextElement root = document.getContentRoot();
        DocumentImpl documentImpl = (DocumentImpl) root.getOwnerDocument();
        nodeIterator = documentImpl.createNodeIterator(root, (short) 1, TextDocumentIterator::acceptNode);
    }

    private static short acceptNode(Node node) {
        // Do not include the content of an annotation because paragraphs,
        // for example, inside of the would be included twice in the iteration
        if (node.getParentNode() instanceof OfficeAnnotationElement) {
            return FILTER_SKIP;
        }

        if (node instanceof TableTableCellElement
                || node instanceof OfficeAnnotationElement
                || node instanceof OfficeAnnotationEndElement
                || node instanceof TextHElement
                || node instanceof TextPElement) {
            return FILTER_ACCEPT;
        }

        return FILTER_SKIP;
    }

    @Override
    public boolean hasNext() {
        if (currentNode != null) {
            return true;
        }

        currentNode = nodeIterator.nextNode();
        return currentNode != null;
    }

    @Override
    public OdfElement next() {
        if (currentNode == null) {
            throw new NoSuchElementException("No more nodes in the tree");
        }

        try {
            return (OdfElement) currentNode;
        } finally {
            currentNode = null;
        }
    }

}
