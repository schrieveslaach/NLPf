package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import de.tudarmstadt.ukp.dkpro.core.io.text.TextWriter;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.jcas.JCas;
import org.junit.Test;

import java.io.File;
import java.util.List;

import static de.schrieveslaach.nlpf.io.odt.XmiTestRunner.testOneWayToXmi;
import static de.schrieveslaach.nlpf.testing.JCasMatchers.hasDocumentBaseUri;
import static de.schrieveslaach.nlpf.testing.JCasMatchers.hasDocumentUri;
import static de.tudarmstadt.ukp.dkpro.core.testing.IOTestRunner.testOneWay;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.core.StringEndsWith.endsWith;
import static org.junit.Assert.assertThat;

public class OdtReaderTest {

    @Test
    public void shouldReadOdtWithLineBreaks() throws Exception {
        testOneWay(
                OdtReader.class,
                TextWriter.class,
                "txt/Plain-ODT-with-Line-Breaks-created-by-LibreOffice.txt",
                "odt/Plain-ODT-with-Line-Breaks-created-by-LibreOffice.odt"
        );
    }

    @Test
    public void shouldReadOdtWithSpan() throws Exception {
        testOneWay(
                OdtReader.class,
                TextWriter.class,
                "txt/Plain-ODT-with-Span-created-by-LibreOffice.txt",
                "odt/Plain-ODT-with-Span-created-by-LibreOffice.odt"
        );
    }

    @Test
    public void shouldReadPlainOdtContentAndSaveAsTextFile() throws Exception {
        testOneWay(
                OdtReader.class,
                TextWriter.class,
                "txt/5-paragraphs-of-Lorem-Ipsum.txt",
                "odt/5-paragraphs-of-Lorem-Ipsum.odt"
        );
    }

    @Test
    public void shouldReadPlainOdtContentAndSaveAsXmiFile() throws Exception {
        testOneWayToXmi(
                OdtReader.class,
                "xmi/5-paragraphs-of-Lorem-Ipsum.xmi",
                "odt/5-paragraphs-of-Lorem-Ipsum.odt"
        );
    }

    @Test
    public void shouldReadOdtContentWithCommentAndSaveAsXmiFile() throws Exception {
        testOneWayToXmi(
                OdtReader.class,
                "xmi/Fox-with-comments.xmi",
                "odt/Fox-with-comments.odt"
        );
    }

    @Test
    public void shouldReadOdtContentWithUimaXmiPosTokensAndSaveAsXmiFile() throws Exception {
        testOneWayToXmi(
                OdtReader.class,
                "xmi/Fox-with-uima.xmi",
                "odt/Fox-with-uima.odt",
                //these attributes can differ because of id's
                "sofa", "xmi:id", "pos", "members"
        );
    }

    @Test
    public void shouldReadOdtContent_WithDocumentBaseUri_UriIsMissing() throws Exception {
        List<JCas> jCas = Lists.newArrayList(iteratePipeline(createReaderDescription(
                OdtReader.class,
                OdtReader.PARAM_SOURCE_LOCATION, new File(getClass().getResource("/odt/Fox-with-uima.odt").toURI())
        )));

        assertThat(jCas, everyItem(hasDocumentUri(endsWith("odt/Fox-with-uima.odt"))));
    }

    @Test
    public void shouldReadOdtContentWithUimaXmiNamedEntityTokensAndSaveAsXmiFile() throws Exception {
        testOneWayToXmi(
                OdtReader.class,
                "xmi/Torwalds-with-uima.xmi",
                "odt/Torwalds-with-uima.odt",
                //these attributes can differ because of id's
                "sofa", "xmi:id", "members"
        );
    }

    @Test
    public void shouldReadOdtContent_WithDocumentBaseUri_UriIsProvided() throws Exception {
        List<JCas> jCas = Lists.newArrayList(iteratePipeline(createReaderDescription(
                OdtReader.class,
                OdtReader.PARAM_SOURCE_LOCATION, new File(getClass().getResource("/odt/Torwalds-with-uima.odt").toURI())
        )));

        assertThat(jCas, everyItem(hasDocumentBaseUri("https://tempuri.org/")));
        assertThat(jCas, everyItem(hasDocumentUri("https://tempuri.org/torwalds")));
    }

    @Test
    public void shouldReadPlainOdtContentAndSaveAsXmiFile_createdWithLibreOffice() throws Exception {
        testOneWayToXmi(
                OdtReader.class,
                "xmi/Plain-ODT-created-by-LibreOffice.xmi",
                "odt/Plain-ODT-created-by-LibreOffice.odt"
        );
    }

    @Test
    public void shouldOverrideLanguageFromOdtDocument_LanguageNotProvidedInDocumentMetaData() throws Exception {
        JCas jCas = iteratePipeline(createReaderDescription(OdtReader.class,
                OdtReader.PARAM_SOURCE_LOCATION, getClass().getResource("/odt/Plain-ODT-created-by-LibreOffice.odt"),
                OdtReader.PARAM_LANGUAGE, "en")).iterator().next();

        assertThat(jCas.getDocumentLanguage(), is(equalTo("en")));
    }
}
