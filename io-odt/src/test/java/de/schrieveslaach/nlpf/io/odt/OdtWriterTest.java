package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.tokit.RegexSegmenter;
import lombok.SneakyThrows;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.pipeline.SimplePipeline;
import org.apache.uima.jcas.JCas;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.odftoolkit.simple.TextDocument;
import org.xmlunit.builder.Input;

import java.io.File;

import static de.schrieveslaach.nlpf.io.odt.XmiTestRunner.testOneWayFromXmiToOdt;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.junit.Assert.assertThat;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

public class OdtWriterTest {

    @Rule
    public final TemporaryFolder temporaryFolder = new TemporaryFolder();

    @Test
    public void shouldWriteOdtFileWithJCasAnnotations() throws Exception {
        testOneWayFromXmiToOdt(
                "odt/5-paragraphs-of-Lorem-Ipsum.odt",
                "xmi/5-paragraphs-of-Lorem-Ipsum.xmi"
        );
    }

    @Test
    public void shouldWritePlainOdtFileFromLibreOfficeWithJCasAnnotations() throws Exception {
        testOneWayFromXmiToOdt(
                "odt/Plain-ODT-created-by-LibreOffice.odt",
                "xmi/Plain-ODT-created-by-LibreOffice.xmi"
        );
    }

    @Test
    public void shouldWriteOdtFileWith_OfficeAnnotations() throws Exception {
        testOneWayFromXmiToOdt(
                "odt/Fox-with-comments.odt",
                "xmi/Fox-with-comments.xmi"
        );
    }

    @Test
    public void shouldWriteOdtFileWith_PosTokens() throws Exception {
        testOneWayFromXmiToOdt(
                "odt/Fox-with-uima.odt",
                "xmi/Fox-with-uima.xmi"
        );
    }

    @Test
    public void shouldWriteOdtFileWith_NamedEntityTokens() throws Exception {
        testOneWayFromXmiToOdt(
                "odt/Torwalds-with-uima.odt",
                "xmi/Torwalds-with-uima.xmi"
        );
    }

    @Test
    public void shouldWriteOdtFileWith_TokenInformation() throws Exception {
        JCas quickBrownFox = createQuickBrownFox();


        SimplePipeline.runPipeline(
                quickBrownFox,
                createEngineDescription(OdtWriter.class, OdtWriter.PARAM_TARGET_LOCATION, temporaryFolder.getRoot())
        );

        TextDocument textDocument = TextDocument.loadDocument(new File(temporaryFolder.getRoot(), "Fox-with-uima.odt.odt"));

        assertThat(
                Input.fromStream(textDocument.getPackage().getInputStream("UIMA/cas.xmi")),
                isSimilarTo(Input.fromStream(getClass().getClassLoader().getResourceAsStream("xmi/Fox-with-uima.xmi")))
        );
    }

    @SneakyThrows
    private JCas createQuickBrownFox() {
        String text = "The quick brown fox jumps over the lazy dog.\nThe five boxing wizards jump quickly.\n";

        AggregateBuilder builder = new AggregateBuilder();
        builder.add(
                createEngineDescription(RegexSegmenter.class)
        );
        builder.add(createEngineDescription(PseudoPostTagger.class));
        AnalysisEngine engine = builder.createAggregate();
        JCas jcas = engine.newJCas();
        jcas.setDocumentText(text);
        jcas.setDocumentLanguage("en");

        DocumentMetaData documentMetaData = DocumentMetaData.create(jcas);
        documentMetaData.setDocumentId("Fox-with-uima.odt");

        Paragraph paragraph = new Paragraph(jcas, 0, 44);
        paragraph.addToIndexes();

        paragraph = new Paragraph(jcas, 45, text.length() - 1);
        paragraph.addToIndexes();

        engine.process(jcas);

        return jcas;
    }

    public static class PseudoPostTagger extends JCasAnnotator_ImplBase {

        @Override
        public void process(JCas aJCas) throws AnalysisEngineProcessException {
            for (Token token : select(aJCas, Token.class)) {
                POS adp = new POS(aJCas, token.getBegin(), token.getEnd());
                adp.setPosValue("ADP");
                token.setPos(adp);
                adp.addToIndexes();
            }
        }
    }
}
