package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.util.List;
import java.util.Objects;

import static de.schrieveslaach.nlpf.testing.JCasMatchers.hasDocumentBaseUri;
import static de.schrieveslaach.nlpf.testing.JCasMatchers.hasDocumentUri;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createHelloWorld;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.hamcrest.CoreMatchers.everyItem;
import static org.junit.Assert.assertThat;

public class RoundTripTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void shouldWriteAndReadDocumentBaseUri_UrisAreProvided() throws Exception {
        JCas helloWorld = createHelloWorld();

        runPipeline(helloWorld, createEngineDescription(
                OdtWriter.class,
                OdtWriter.PARAM_TARGET_LOCATION, folder.getRoot()
        ));

        List<JCas> jCas = Lists.newArrayList(iteratePipeline(createReaderDescription(
                OdtReader.class,
                OdtReader.PARAM_SOURCE_LOCATION, new File(folder.getRoot(), "Hello_World.odt")
        )));

        assertThat(jCas, everyItem(hasDocumentBaseUri("https://en.wikipedia.org/wiki/")));
        assertThat(jCas, everyItem(hasDocumentUri("https://en.wikipedia.org/wiki/Hello_World")));
    }

    @Test
    public void shouldReadAndWriteDocumentTitle() throws Exception {
        File odt = new File(getClass().getResource("/odt/Plain-ODT-with-Title-created-by-LibreOffice.odt").toURI());

        List<JCas> jCases = Lists.newArrayList(iteratePipeline(createReaderDescription(
                OdtReader.class,
                OdtReader.PARAM_SOURCE_LOCATION, odt
        )));

        for (JCas jCas : jCases) {
            runPipeline(jCas, createEngineDescription(
                    OdtWriter.class,
                    OdtWriter.PARAM_TARGET_LOCATION, folder.getRoot(),
                    OdtWriter.PARAM_STRIP_EXTENSION, true
            ));
        }

        jCases = Lists.newArrayList(iteratePipeline(createReaderDescription(
                OdtReader.class,
                OdtReader.PARAM_SOURCE_LOCATION, new File(folder.getRoot(), "Plain-ODT-with-Title-created-by-LibreOffice.odt")
        )));

        assertThat(jCases, everyItem(hasDocumentTitle("Hello World")));
    }

    private Matcher<JCas> hasDocumentTitle(String title) {
        return new TypeSafeMatcher<JCas>() {
            @Override
            protected boolean matchesSafely(JCas jCas) {
                DocumentMetaData metaData = DocumentMetaData.get(jCas);
                if (metaData == null) {
                    return false;
                }

                return Objects.equals(metaData.getDocumentTitle(), title);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has document title ").appendValue(title);
            }

            @Override
            protected void describeMismatchSafely(JCas jCas, Description mismatchDescription) {
                DocumentMetaData metaData = DocumentMetaData.get(jCas);
                if (metaData == null) {
                    mismatchDescription.appendValue("has no document meta data");
                    return;
                }

                mismatchDescription.appendText("has document title ").appendValue(metaData.getDocumentTitle());
            }
        };
    }
}
