package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import de.schrieveslaach.nlpf.io.odt.TextDocumentIterator;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.odftoolkit.odfdom.pkg.OdfElement;
import org.odftoolkit.simple.TextDocument;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static org.junit.Assert.assertThat;

public class TextDocumentIteratorTest {

    @Test
    public void shouldBeAbleToCallMultipleTimesHasNext() throws Exception {
        // Document with empty paragraph
        TextDocument document = TextDocument.newTextDocument();
        TextDocumentIterator it = new TextDocumentIterator(document);

        for (int i = 0; i < 10; ++i) {
            it.hasNext();
        }

        assertThat(it.next(), hasTextContents(""));
    }

    @Test
    public void shouldBeAbleToCallNextWithoutHasNext_IfDocumentHasElements() throws Exception {
        // Document with empty paragraph
        TextDocument document = TextDocument.newTextDocument();
        TextDocumentIterator it = new TextDocumentIterator(document);

        for (int i = 0; i < 10; ++i) {
            it.hasNext();
        }

        assertThat(it.next(), hasTextContents(""));
    }

    @Test
    public void shouldThrowHasNoSuchElementExceptionWhenIteratorReachedEndOfDocument() throws Exception {
        // Document with empty paragraph
        TextDocument document = TextDocument.newTextDocument();
        TextDocumentIterator it = new TextDocumentIterator(document);

        it.hasNext();

        it.next();
    }

    @Test
    public void shouldIterateOverTextDocumentWithParagraphsOnly() throws Exception {
        TextDocument document = TextDocument.loadDocument(getClass().getResourceAsStream("/odt/5-paragraphs-of-Lorem-Ipsum.odt"));
        TextDocumentIterator it = new TextDocumentIterator(document);

        List<OdfElement> paragraphs = Lists.newArrayList(it);
        assertThat(paragraphs, hasTextContents(
                "Consectetur voluptas aliquam maiores. Non maiores quia eum. Assumenda quisquam sit voluptatem ratione amet. Voluptatem est voluptatem quis ea quidem natus. In molestiae similique dolorem nisi et veniam.",
                "In est doloremque ut sapiente quos nisi blanditiis est. Labore nemo autem rerum assumenda quo in. Illum omnis et dolores veritatis et officia. Ea facere nihil sapiente vero. Praesentium nihil nemo ducimus accusantium optio optio aspernatur excepturi.",
                "Delectus eligendi et assumenda aliquam ut in. Non quasi id totam et ut voluptas voluptatem nesciunt. Necessitatibus laborum reiciendis id consequatur nobis autem ducimus aut. Est impedit totam placeat.",
                "Perferendis eum eius itaque et. Et ullam repudiandae quas modi ipsum. Iusto quia laudantium quia et est. Repellendus ad aliquam in qui expedita excepturi eligendi. Possimus omnis dignissimos culpa aperiam. Sequi sunt sed earum unde.",
                "Temporibus doloribus quibusdam animi. Repellendus quo iure iure occaecati ut consequatur. Ea quis sit quia omnis et est praesentium. Voluptates iste modi vel qui ut velit at ea. Velit consequatur sint eaque non possimus ut nostrum."
        ));
    }

    @Test
    public void shouldIterateOverTextDocumentWithListOnly() throws Exception {
        TextDocument document = TextDocument.loadDocument(getClass().getResourceAsStream("/odt/3-list-items-of-Lorem-Ipsum.odt"));
        TextDocumentIterator it = new TextDocumentIterator(document);

        List<OdfElement> paragraphs = Lists.newArrayList(it);
        assertThat(paragraphs, hasTextContents(
                "Consectetur voluptas aliquam maiores. Non maiores quia eum. Assumenda quisquam sit voluptatem ratione amet. Voluptatem est voluptatem quis ea quidem natus. In molestiae similique dolorem nisi et veniam.",
                "Delectus eligendi et assumenda aliquam ut in. Non quasi id totam et ut voluptas voluptatem nesciunt. Necessitatibus laborum reiciendis id consequatur nobis autem ducimus aut. Est impedit totam placeat.",
                "Temporibus doloribus quibusdam animi. Repellendus quo iure iure occaecati ut consequatur. Ea quis sit quia omnis et est praesentium. Voluptates iste modi vel qui ut velit at ea. Velit consequatur sint eaque non possimus ut nostrum."
        ));
    }

    private Matcher<List<OdfElement>> hasTextContents(String... text) {
        return new TypeSafeMatcher<List<OdfElement>>() {
            @Override
            protected boolean matchesSafely(List<OdfElement> items) {
                List<String> actualText = items.stream().map(paragraph -> paragraph.getTextContent()).collect(Collectors.toList());
                return Lists.newArrayList(text).equals(actualText);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has text contents: ")
                        .appendValueList("", ",\n", "", text);
            }

            @Override
            protected void describeMismatchSafely(List<OdfElement> items, Description mismatchDescription) {
                List<String> actualText = items.stream().map(paragraph -> paragraph.getTextContent()).collect(Collectors.toList());

                mismatchDescription.appendText("has text contents: ")
                        .appendValueList("", ",\n", "", actualText);
            }
        };
    }

    private Matcher<OdfElement> hasTextContents(String text) {
        return new TypeSafeMatcher<OdfElement>() {
            @Override
            protected boolean matchesSafely(OdfElement odfElement) {
                return Objects.equals(text, odfElement.getTextContent());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has text contents: ").appendValue(text);
            }
        };
    }
}
