package de.schrieveslaach.nlpf.io.odt;

/*-
 * ========================LICENSE_START=================================
 * io-odt
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.io.odt.OdtWriter;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import de.tudarmstadt.ukp.dkpro.core.testing.AssertAnnotations;
import de.tudarmstadt.ukp.dkpro.core.testing.DkproTestContext;
import de.tudarmstadt.ukp.dkpro.core.testing.DocumentMetaDataStripper;
import de.tudarmstadt.ukp.dkpro.core.testing.TestOptions;
import org.apache.commons.compress.archivers.ArchiveInputStream;
import org.apache.commons.compress.archivers.ArchiveStreamFactory;
import org.apache.commons.compress.archivers.zip.ZipArchiveEntry;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReader;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.resource.ResourceCreationSpecifier;
import org.apache.xerces.dom.AttrNSImpl;
import org.w3c.dom.Node;
import org.xmlunit.builder.Input;
import org.xmlunit.diff.ComparisonResult;
import org.xmlunit.diff.ComparisonType;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.Arrays;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.factory.ConfigurationParameterFactory.*;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.junit.Assert.assertThat;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

public class XmiTestRunner {

    private static final String RESOURCE_COLLECTION_READER_BASE = "de.tudarmstadt.ukp.dkpro.core.api.io.ResourceCollectionReaderBase";

    public static void testOneWayToXmi(Class<? extends CollectionReader> aReader, String aExpectedFile, String aFile, String... ignoreContentOfAttributes) throws Exception {
        Class<?> dkproReaderBase = Class.forName(RESOURCE_COLLECTION_READER_BASE);
        if (!dkproReaderBase.isAssignableFrom(aReader)) {
            throw new IllegalArgumentException("Reader must be a subclass of ["
                    + RESOURCE_COLLECTION_READER_BASE + "]");
        }

        // We assume that the writer is creating a file with the same extension as is provided as
        // the expected file
        String extension = FilenameUtils.getExtension(aExpectedFile);
        String name = FilenameUtils.getBaseName(aFile);
        Object[] aExtraParams = {};

        testOneWay2(
                createReaderDescription(aReader, aExtraParams),
                createEngineDescription(XmiWriter.class, aExtraParams),
                aExpectedFile, name + "." + extension, aFile,
                (actualXmi, referenceXmi) -> assertThat(
                        Input.fromFile(actualXmi),
                        isSimilarTo(Input.fromFile(referenceXmi)).ignoreWhitespace().withDifferenceEvaluator((comparison, outcome) -> {
                            if (outcome == ComparisonResult.EQUAL || outcome == ComparisonResult.SIMILAR) {
                                return outcome;
                            }

                            if (ComparisonType.ATTR_VALUE.equals(comparison.getType())) {
                                Node controlTarget = comparison.getControlDetails().getTarget();
                                Node testTarget = comparison.getTestDetails().getTarget();
                                if (controlTarget instanceof AttrNSImpl && testTarget instanceof AttrNSImpl) {
                                    AttrNSImpl controlAttr = (AttrNSImpl) controlTarget;
                                    AttrNSImpl testAttr = (AttrNSImpl) testTarget;
                                    
                                    // ignores given attributes from comparison
                                    if (controlAttr.getName().equals(testAttr.getName()) && Arrays.asList(ignoreContentOfAttributes)
                                            .stream()
                                            .filter(elem -> elem.equals(controlAttr.getName()))
                                            .findFirst()
                                            .isPresent()) {
                                        return ComparisonResult.SIMILAR;
                                    }
                                }
                            }

                            return outcome;
                        })
                )
        );
    }

    public static void testOneWayFromXmiToOdt(String aExpectedFile, String aFile) throws Exception {
        // We assume that the writer is creating a file with the same extension as is provided as
        // the expected file
        String extension = FilenameUtils.getExtension(aExpectedFile);
        String name = FilenameUtils.getBaseName(aFile);
        Object[] aExtraParams = {};

        CollectionReaderDescription reader = createReaderDescription(XmiReader.class, aExtraParams);
            AnalysisEngineDescription writer = createEngineDescription(OdtWriter.class, aExtraParams);

        testOneWay2(
                reader, writer,
                aExpectedFile, name + "." + extension, aFile,
                (File actualOdt, File referenceOdt) -> {
                    File actualXml = extractContentXml(writer, aFile, actualOdt);
                    File referenceXml = extractContentXml(reader, aExpectedFile, referenceOdt);

                    assertThat(
                            Input.fromFile(actualXml),
                            isSimilarTo(Input.fromFile(referenceXml))
                                    .ignoreWhitespace()
                                    .withNodeFilter(
                                            node -> !"sender-initials".equals(node.getLocalName())
                                                    && !"font-face-decls".equals(node.getLocalName())
                                                    && !"automatic-styles".equals(node.getLocalName())
                                    )
                                    .withDifferenceEvaluator((comparison, outcome) -> {
                                        if (outcome == ComparisonResult.EQUAL) {
                                            return outcome;
                                        }

                                        final Node controlNode = comparison.getControlDetails().getTarget();
                                        final Node testNode = comparison.getTestDetails().getTarget();
                                        if (controlNode.getAttributes() != null && testNode.getAttributes() != null) {
                                            // ignore style attribute (might have different names)
                                            if (controlNode.getAttributes().getNamedItem("text:style-name") != null
                                                    && testNode.getAttributes().getNamedItem("text:style-name") == null) {
                                                return ComparisonResult.SIMILAR;
                                            }
                                        }

                                        // ignore office:name annotations because they will be generated
                                        if ("office:name".equals(controlNode.getNodeName()) || "office:name".equals(testNode.getNodeName())) {
                                            return ComparisonResult.SIMILAR;
                                        }

                                        return outcome;
                                    })
                    );
                }
        );
    }

    private static File extractContentXml(ResourceCreationSpecifier specifier, String aFile, File odt) throws Exception {
        File output = getOutputFolder(specifier, aFile);

        try (FileInputStream is = new FileInputStream(odt)) {
            ArchiveInputStream ais = new ArchiveStreamFactory().createArchiveInputStream(ArchiveStreamFactory.ZIP, is);
            ZipArchiveEntry entry;
            while ((entry = (ZipArchiveEntry) ais.getNextEntry()) != null) {
                if ("content.xml".equals(entry.getName())) {
                    File contentXml = new File(output, FilenameUtils.getBaseName(odt.getAbsolutePath()) + ".xml");
                    contentXml.getParentFile().mkdirs();
                    try (OutputStream os = new FileOutputStream(contentXml)) {
                        IOUtils.copy(ais, os);
                    }
                    return contentXml;
                }
            }
        }
        return null;
    }

    private static void testOneWay2(CollectionReaderDescription aReader,
                                    AnalysisEngineDescription aWriter, String aExpectedFile, String aOutputFile,
                                    String aInputFile, Assert ass)
            throws Exception {

        File reference = new File("src/test/resources/" + aExpectedFile);
        File input = new File("src/test/resources/" + aInputFile);
        File output = getOutputFolder(aReader, aInputFile);
        File outputFile = new File(output, aOutputFile);

        setParameter(aReader, ComponentParameters.PARAM_SOURCE_LOCATION, input);

        if (canParameterBeSet(aWriter, ComponentParameters.PARAM_STRIP_EXTENSION)) {
            setParameter(aWriter, ComponentParameters.PARAM_STRIP_EXTENSION, true);
        }

        if (canParameterBeSet(aWriter, "overwrite")) {
            setParameter(aWriter, "overwrite", true);
        }

        if (!getParameterSettings(aWriter).containsKey(ComponentParameters.PARAM_TARGET_LOCATION)) {
            setParameter(aWriter, ComponentParameters.PARAM_TARGET_LOCATION, output);
        }

        AnalysisEngineDescription metadataStripper = createEngineDescription(
                DocumentMetaDataStripper.class);

        AnalysisEngineDescription validator = createEngineDescription(
                de.tudarmstadt.ukp.dkpro.core.testing.IOTestRunner.Validator.class);

        de.tudarmstadt.ukp.dkpro.core.testing.IOTestRunner.Validator.options = new TestOptions();

        runPipeline(aReader, validator, metadataStripper, aWriter);

        AssertAnnotations.assertValid(de.tudarmstadt.ukp.dkpro.core.testing.IOTestRunner.Validator.messages);

        ass.assertThat(outputFile, reference);
    }

    private static File getOutputFolder(ResourceCreationSpecifier aReader, String aInputFile) {
        String outputFolder = StringUtils.substringAfterLast(aReader.getImplementationName(), ".")
                + "-" + FilenameUtils.getBaseName(aInputFile);
        if (DkproTestContext.get() != null) {
            outputFolder = DkproTestContext.get().getTestOutputFolderName();
        }
        return new File("target/test-output/" + outputFolder);
    }

    @FunctionalInterface
    private interface Assert {

        void assertThat(File actual, File reference) throws Exception;

    }
}
