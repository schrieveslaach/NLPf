package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import lombok.SneakyThrows;
import org.apache.uima.jcas.JCas;

import java.io.File;
import java.net.URISyntaxException;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createSatyaNadellaExample;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createTorwaldsExample;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;

public abstract class BaseMojoIT {

    protected void storeExampleCasFiles(File testProjectBaseDir) throws Exception {
        File corpusDir = new File(testProjectBaseDir, "src/main/corpus");
        corpusDir.mkdirs();

        JCas openNlpExample = createOpenNlpExample();
        runPipeline(openNlpExample, createEngineDescription(XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, corpusDir,
                XmiWriter.PARAM_OVERWRITE, true));

        JCas torwaldsExample = createTorwaldsExample();
        runPipeline(torwaldsExample, createEngineDescription(XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, corpusDir,
                XmiWriter.PARAM_OVERWRITE, true));

        File corpusTestDir = new File(testProjectBaseDir, "src/test/corpus");
        corpusTestDir.mkdirs();

        JCas satyaNadellaExample = createSatyaNadellaExample();
        runPipeline(satyaNadellaExample, createEngineDescription(XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, corpusTestDir,
                XmiWriter.PARAM_OVERWRITE, true));
    }

    @SneakyThrows(URISyntaxException.class)
    protected File getTestProjectBaseDir(String path) {
        return new File(getClass().getResource(path).toURI());
    }
}
