package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import de.schrieveslaach.nlpf.maven.plugin.models.TestResult;
import de.schrieveslaach.nlpf.testing.annotators.MyPosTagger;
import org.apache.maven.plugin.testing.MojoRule;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.io.FileMatchers.anExistingFile;
import static org.junit.Assert.assertThat;

public class IsolatedToolsTestMojoIT extends BaseMojoIT {

    @Rule
    public MojoRule rule = new MojoRule();

    @Test
    public void shouldTestNlpModels() throws Exception {
        File testProjectBaseDir = getTestProjectBaseDir("/sample-project");
        storeExampleCasFiles(testProjectBaseDir);

        rule.executeMojo(testProjectBaseDir, "train");
        rule.executeMojo(testProjectBaseDir, "tools-test");

        File testDataFile = new File(testProjectBaseDir, "target/isolated-tools-test-data/" + MyPosTagger.DEFAULT_HASH_CODE + ".json");
        assertThat(testDataFile, is(anExistingFile()));

        ObjectMapper mapper = new ObjectMapper();
        TestResult testResult = mapper.readValue(testDataFile, TestResult.class);
        assertThat(testResult, is(notNullValue()));
    }
}
