package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.SneakyThrows;
import org.apache.maven.plugin.testing.MojoRule;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;
import java.io.IOException;
import java.util.Optional;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import static org.junit.Assert.assertThat;

public class PackageMojoIT extends BaseMojoIT {

    @Rule
    public MojoRule rule = new MojoRule();

    @Test
    public void shouldPackageNlpModels() throws Exception {
        File testProjectBaseDir = getTestProjectBaseDir("/sample-project");
        storeExampleCasFiles(testProjectBaseDir);

        rule.executeMojo(testProjectBaseDir, "train");
        rule.executeMojo(testProjectBaseDir, "pipelines-test");
        rule.executeMojo(testProjectBaseDir, "package");

        File packageFile = new File(testProjectBaseDir, "target/domain-specific-corpus-1.0-SNAPSHOT.jar");
        assertThat(packageFile, containsNlpModel(
                "de.company",
                "domain-specific-corpus",
                "de.tudarmstadt.ukp.dkpro.core.opennlp",
                "x.org.dkpro.core.opennlp.ner",
                "en",
                "person")
        );
        packageFile = new File(testProjectBaseDir, "target/domain-specific-corpus-1.0-SNAPSHOT.jar");
        assertThat(packageFile, containsNlpModel(
                "de.company",
                "domain-specific-corpus",
                "de.tudarmstadt.ukp.dkpro.core.opennlp",
                "x.org.dkpro.core.opennlp.ner",
                "en",
                "organization")
        );

        assertThat(packageFile, containsNlpModel(
                "de.company",
                "domain-specific-corpus",
                "de.tudarmstadt.ukp.dkpro.core.opennlp",
                "x.org.dkpro.core.opennlp.token",
                "en",
                "default")
        );

        assertThat(packageFile, containsNlpModel(
                "de.company",
                "domain-specific-corpus",
                "de.tudarmstadt.ukp.dkpro.core.opennlp",
                "x.org.dkpro.core.opennlp.sent",
                "en",
                "default")
        );
    }

    private Matcher<File> containsNlpModel(String groupId, String artifactId, String framework, String type, String language, String variant) {
        return new JarEntryMatcher(type, language, variant, groupId, artifactId, framework);
    }

    private static class JarEntryMatcher extends TypeSafeMatcher<File> {

        private final String type;
        private final String language;
        private final String variant;
        private final String groupId;
        private final String artifactId;
        private final String framework;

        public JarEntryMatcher(String type, String language, String variant, String groupId, String artifactId, String framework) {
            this.type = type;
            this.language = language;
            this.variant = variant;
            this.groupId = groupId;
            this.artifactId = artifactId;
            this.framework = framework;
        }

        @SneakyThrows(IOException.class)
        private Optional<JarEntry> findJarEntry(File file) {
            try (JarFile jarFile = new JarFile(file)) {
                return jarFile.stream()
                        .filter(entry -> getModelJarEntryName().equals(entry.getName()))
                        .findFirst();
            }
        }

        private String getModelJarEntryName() {
            return String.format("%s/%s/%s/%s-%s-%s.model",
                    groupId,
                    artifactId,
                    framework,
                    type,
                    language,
                    variant
            );
        }

        @Override
        protected boolean matchesSafely(File file) {
            if (!file.isFile()) {
                return false;
            }

            return findJarEntry(file).isPresent();
        }

        @Override
        public void describeTo(Description description) {
            description.appendText("contains ner model: ").appendValue(getModelJarEntryName());
        }

        @Override
        @SneakyThrows(IOException.class)
        protected void describeMismatchSafely(File file, Description mismatchDescription) {
            if (!file.isFile()) {
                mismatchDescription.appendText("jar file does not exists");
                return;
            }

            try (JarFile jarFile = new JarFile(file)) {
                mismatchDescription.appendText("has entries: ");
                jarFile.stream().forEach(
                        entry -> mismatchDescription.appendValue(entry.getName()).appendText(", ")
                );
            }
        }
    }
}
