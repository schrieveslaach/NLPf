package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.apache.maven.plugin.testing.MojoRule;
import org.junit.Rule;
import org.junit.Test;

import java.io.File;

import static org.hamcrest.Matchers.is;
import static org.hamcrest.io.FileMatchers.anExistingFile;
import static org.junit.Assert.assertThat;

public class TrainMojoIT extends BaseMojoIT {

    @Rule
    public MojoRule rule = new MojoRule();

    @Test
    public void shouldCreateNerModel() throws Exception {
        File testProjectBaseDir = getTestProjectBaseDir("/sample-project");
        storeExampleCasFiles(testProjectBaseDir);

        rule.executeMojo(testProjectBaseDir, "train");

        File modelFile = new File(testProjectBaseDir, "target/models/de.company/domain-specific-corpus/de.tudarmstadt.ukp.dkpro.core.opennlp/x.org.dkpro.core.opennlp.ner-en-person.model");
        assertThat(modelFile, is(anExistingFile()));
        modelFile = new File(testProjectBaseDir, "target/models/de.company/domain-specific-corpus/de.tudarmstadt.ukp.dkpro.core.opennlp/x.org.dkpro.core.opennlp.ner-en-organization.model");
        assertThat(modelFile, is(anExistingFile()));

        modelFile = new File(testProjectBaseDir, "target/models/de.company/domain-specific-corpus/de.tudarmstadt.ukp.dkpro.core.opennlp/x.org.dkpro.core.opennlp.sent-en-default.model");
        assertThat(modelFile, is(anExistingFile()));

        modelFile = new File(testProjectBaseDir, "target/models/de.company/domain-specific-corpus/de.tudarmstadt.ukp.dkpro.core.opennlp/x.org.dkpro.core.opennlp.token-en-default.model");
        assertThat(modelFile, is(anExistingFile()));
    }

}
