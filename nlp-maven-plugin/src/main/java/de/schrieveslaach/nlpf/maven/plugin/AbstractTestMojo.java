package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableMap;
import com.google.common.collect.Sets;
import de.schrieveslaach.nlpf.maven.plugin.models.TestResult;
import de.schrieveslaach.nlpf.maven.plugin.service.AnalysisEngineDescriptionService;
import de.schrieveslaach.nlpf.maven.plugin.service.ClassLoaderService;
import de.schrieveslaach.nlpf.maven.plugin.service.CollectionReaderDescriptionService;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import de.schrieveslaach.nlpf.plumbing.util.Uima;
import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.eval.measure.FMeasure;
import de.tudarmstadt.ukp.dkpro.core.eval.model.Span;
import lombok.Cleanup;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.apache.uima.util.XMLParser;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.BiPredicate;
import java.util.function.Function;

import static org.apache.uima.fit.util.JCasUtil.select;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;

public abstract class AbstractTestMojo extends AbstractMojo {

    protected static final ImmutableMap<Class<? extends Annotation>, Function<? extends Annotation, String>> LABEL_FUNCTIONS =
            ImmutableMap.of(
                    NamedEntity.class, (NamedEntity ne) -> ne.getValue(),
                    POS.class, (POS pos) -> pos.getPosValue()
            );

    protected static final ImmutableMap<Class<? extends Annotation>, BiPredicate<? extends Annotation, String>> FILTER_FUNTIONS =
            ImmutableMap.of(
                    NamedEntity.class, (NamedEntity ne, String variant) -> Objects.equals(ne.getValue(), variant)
            );

    @Inject
    protected ClassLoaderService classLoaderService;

    @Inject
    protected DirectoryService directoryService;

    @Inject
    protected AnalysisEngineDescriptionService analysisEngineService;

    @Inject
    protected CollectionReaderDescriptionService collectionReaderDescriptionService;

    @Getter
    private ClassLoader urlClassLoader;


    /**
     * Loads all {@link AnalysisEngineDescription engine descriptions} from the
     * {@link DirectoryService#getDescriptorsDirectory() descriptors directory}.
     *
     * @return
     * @throws MojoExecutionException
     */
    protected List<AnalysisEngineDescription> loadEngineDescriptions() throws MojoExecutionException {
        List<AnalysisEngineDescription> descriptions = new ArrayList<>();

        XMLParser xmlParser = UIMAFramework.getXMLParser();

        for (File descriptorFile : directoryService.getDescriptorsDirectory().listFiles()) {
            try {
                @Cleanup
                XMLInputSource s = new XMLInputSource(descriptorFile);
                AnalysisEngineDescription engineDescription = xmlParser.parseAnalysisEngineDescription(s);
                if (filterEngineDescriptionWithoutTypeCapability(engineDescription)) {
                    descriptions.add(engineDescription);
                }
            } catch (IOException | InvalidXMLException e) {
                throw new MojoExecutionException("Could not read engine description from " + descriptorFile, e);
            }
        }

        return descriptions;
    }

    private boolean filterEngineDescriptionWithoutTypeCapability(AnalysisEngineDescription engineDescription){
        Class<?> implementationClass = analysisEngineService.loadAnalysisEngineImplementation(engineDescription);
        return findAnnotation(implementationClass, TypeCapability.class) != null;
    }

    @Override
    public void execute() throws MojoExecutionException {
        Uima.swallowLogging();
        ClassLoader previousContextClassLoader = initContextClassLoader();

        executeWithUimaClassLoader();

        // restore context classloader
        Thread.currentThread().setContextClassLoader(previousContextClassLoader);
    }


    /**
     * The descriptor files of the {@link AnalysisEngineDescription engine descriptions} reference
     * the models through the classpath. As DKPro Core need to resolve the models to test from the
     * {@link Thread#getContextClassLoader()} of the {@link Thread#currentThread() current thread}
     * the class loader needs to contain the {@link DirectoryService#getModelsBaseDirectory()} as
     * resource.
     */
    @SneakyThrows({MalformedURLException.class, NoSuchMethodException.class, IllegalAccessException.class, InvocationTargetException.class})
    private ClassLoader initContextClassLoader() {
        ClassLoader baseContextClassLoader = Thread.currentThread().getContextClassLoader();

        URL url = directoryService.getModelsBaseDirectory().toURI().toURL();
        urlClassLoader = URLClassLoader.newInstance(
                new URL[]{directoryService.getModelsBaseDirectory().toURI().toURL()},
                classLoaderService.getDependencyClassLoader()
        );

        Thread.currentThread().setContextClassLoader(urlClassLoader);

        // Hack to add the models directory as resource on the classpath
        // https://stackoverflow.com/a/60766/5088458
        URLClassLoader classLoader = (URLClassLoader) ClassLoader.getSystemClassLoader();
        Method method = URLClassLoader.class.getDeclaredMethod("addURL", URL.class);
        method.setAccessible(true);
        method.invoke(classLoader, url);

        return baseContextClassLoader;
    }

    protected abstract void executeWithUimaClassLoader() throws MojoExecutionException;

    protected static <T extends Annotation> List<Span<String>> loadSamples(JCas jCas, Class<T> aType, String variant) {
        List<Span<String>> samples = new ArrayList<>();

        BiPredicate<T, String> filter = (BiPredicate<T, String>) FILTER_FUNTIONS.getOrDefault(aType, (a, v) -> true);
        Function<T, String> labelFunction = (Function<T, String>) LABEL_FUNCTIONS.getOrDefault(aType, x -> null);


        DocumentMetaData dmd = DocumentMetaData.get(jCas);
        for (T t : select(jCas, aType)) {
            if (filter.test(t, variant)) {
                samples.add(new Span<>(dmd.getDocumentUri(), t.getBegin(), t.getEnd(), labelFunction.apply(t)));
            }
        }

        return samples;
    }

    @SneakyThrows(ClassNotFoundException.class)
    @SuppressWarnings("squid:S2445")
    protected void measureAndAddToTestResult(List<JCas> testData, List<JCas> pipelineData, TestResult testResult, AnalysisEngineDescription analysisEngineDescription) {
        PipelinesTestMojo.EngineDescriptionElement ede = new PipelinesTestMojo.EngineDescriptionElement(analysisEngineDescription);
        String analysisEngineName = analysisEngineService.getAnalysisEngineName(ede.engineDescription);
        for (String outputType : ede.outputs) {
            Class<? extends Annotation> outputClassType = (Class<? extends Annotation>) Class.forName(outputType);

            List<Span<String>> testDataSpans = new ArrayList<>();
            List<Span<String>> pipelineDataSpans = new ArrayList<>();

            Iterator<JCas> testDataIterator = testData.iterator();
            Iterator<JCas> pipelineDataIterator = pipelineData.iterator();

            while (testDataIterator.hasNext()) {
                JCas testDataJCas = testDataIterator.next();
                JCas pipelineDataJCas = pipelineDataIterator.next();

                // The lock is required here because multiple threads iterate over the test data JCas object
                // but JCas is not thread safe iterable
                synchronized (testDataJCas) {
                    if (select(testDataJCas, outputClassType).isEmpty()) {
                        continue;
                    }
                    testDataSpans.addAll(loadSamples(testDataJCas, outputClassType, ede.variant));
                }

                synchronized (pipelineDataJCas) {
                    pipelineDataSpans.addAll(loadSamples(pipelineDataJCas, outputClassType, ede.variant));
                }
            }

            FMeasure fMeasure = new FMeasure();
            fMeasure.process(testDataSpans, pipelineDataSpans);

            testResult.add(analysisEngineName, outputType, ede.variant, fMeasure);
        }
    }

    @EqualsAndHashCode(exclude = "engineDescription")
    protected class EngineDescriptionElement {

        private final Set<String> outputs;

        private final String variant;

        @Getter
        private final AnalysisEngineDescription engineDescription;

        protected EngineDescriptionElement(AnalysisEngineDescription engineDescription) {
            this.engineDescription = engineDescription;

            Class<?> cls = analysisEngineService.loadAnalysisEngineImplementation(engineDescription);

            TypeCapability typeCapability = findAnnotation(cls, TypeCapability.class);
            this.outputs = Sets.newHashSet(typeCapability.outputs());

            Object parameterVariant = engineDescription.getAnalysisEngineMetaData().getConfigurationParameterSettings().getParameterValue(ComponentParameters.PARAM_VARIANT);
            if (parameterVariant != null) {
                this.variant = parameterVariant.toString();
            } else {
                this.variant = null;
            }
        }

    }
}
