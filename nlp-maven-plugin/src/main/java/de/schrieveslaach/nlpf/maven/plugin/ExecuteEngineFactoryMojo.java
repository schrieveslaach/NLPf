package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.reflect.ClassPath;
import de.schrieveslaach.nlpf.maven.plugin.service.ClassLoaderService;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import de.schrieveslaach.nlpf.plumbing.util.Uima;
import lombok.SneakyThrows;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.project.MavenProject;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

@Mojo(name = "execute-engine-factories", requiresDependencyResolution = ResolutionScope.TEST)
public class ExecuteEngineFactoryMojo extends AbstractMojo {

    @Inject
    private MavenProject project;

    @Inject
    private DirectoryService directoryService;

    @Inject
    private ClassLoaderService classLoaderService;

    @Override
    @SneakyThrows({IOException.class})
    public void execute() throws MojoExecutionException, MojoFailureException {
        Uima.swallowLogging();


        ClassLoader classLoader = getTestClasspathLoader();
        Thread.currentThread().setContextClassLoader(classLoader);

        Set<? extends Class<?>> engineFactoryClasses = ClassPath.from(classLoader).getAllClasses().stream()
                .filter(ci -> ci.getName().endsWith("EngineFactory"))
                .map(ClassPath.ClassInfo::load)
                .collect(Collectors.toSet());

        getLog().debug("found " + engineFactoryClasses.size() + " engine factories");

        List<AnalysisEngineDescription> engineDescriptions = createAnalysisEngineDescriptions(engineFactoryClasses);
        getLog().info("Found " + engineDescriptions.size() + " additional engine descriptions created by engine factories.");

        for (AnalysisEngineDescription aed : engineDescriptions) {
            directoryService.store(aed);
        }
    }

    private List<AnalysisEngineDescription> createAnalysisEngineDescriptions(Collection<? extends Class<?>> engineFactoryClasses) {
        List<AnalysisEngineDescription> engineDescriptions = new ArrayList<>();

        for (Class<?> engineFactoryClass : engineFactoryClasses) {

            Object factory;
            try {
                Constructor constructor = engineFactoryClass.getConstructor();
                factory = constructor.newInstance();
            } catch (NoSuchMethodException | InstantiationException | IllegalAccessException | InvocationTargetException e) {
                getLog().debug(engineFactoryClass + " could not create engine factory.", e);
                continue;
            }

            callFactoryMethods(engineDescriptions, engineFactoryClass, factory);
        }

        return engineDescriptions;
    }

    private void callFactoryMethods(List<AnalysisEngineDescription> engineDescriptions, Class<?> engineFactoryClass, Object factory) {
        for (Method method : engineFactoryClass.getMethods()) {
            if (method.getParameterCount() != 0 || !method.getReturnType().equals(AnalysisEngineDescription.class)) {
                continue;
            }

            try {
                getLog().info("Create analysis engine description by factory: " + engineFactoryClass.getSimpleName() + "#" + method.getName());

                AnalysisEngineDescription engineDescription = (AnalysisEngineDescription) method.invoke(factory);
                engineDescriptions.add(engineDescription);
            } catch (IllegalAccessException | InvocationTargetException e) {
                getLog().debug(engineFactoryClass + " could not create factory", e);
            }
        }
    }

    @SneakyThrows(MalformedURLException.class)
    private ClassLoader getTestClasspathLoader() throws MojoExecutionException {
        try {
            ClassLoader classLoader = classLoaderService.getDependencyClassLoader();

            List<String> testClasspathElements = project.getTestClasspathElements();
            URL[] urls = new URL[testClasspathElements.size()];
            int i = 0;
            for (String entry : testClasspathElements) {
                getLog().debug("use classPath entry " + entry);
                urls[i] = new File(entry).toURI().toURL();
                i++;
            }

            return new URLClassLoader(urls, classLoader);
        } catch (DependencyResolutionRequiredException e) {
            throw new MojoExecutionException("Could not resolve test classpath elements", e);
        }
    }


}
