package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import de.schrieveslaach.nlpf.maven.plugin.models.TestResult;
import de.schrieveslaach.nlpf.maven.plugin.reader.ClearTypesCombinationReader;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import lombok.AllArgsConstructor;
import lombok.SneakyThrows;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.descriptor.ResourceMetaData;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;
import java.util.Objects;

import static de.schrieveslaach.nlpf.maven.plugin.JCasDataUtil.loadJCas;
import static de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionUtil.hash;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;

@Mojo(name = "tools-test", requiresDependencyResolution = ResolutionScope.COMPILE)
public class IsolatedToolsTestMojo extends AbstractTestMojo {

    @Override
    public void executeWithUimaClassLoader() throws MojoExecutionException {

        List<JCas> testData = loadJCas(collectionReaderDescriptionService.createTestReaderDescriptions());

        loadEngineDescriptions().parallelStream()
                .map(engineDescription -> testNlpTool(engineDescription, testData))
                .filter(Objects::nonNull)
                .forEach(TestResultWrapper::store);
    }

    @SneakyThrows({MojoExecutionException.class})
    private TestResultWrapper testNlpTool(AnalysisEngineDescription engineDescription, List<JCas> testData) {
        logNlpToolsInfo(engineDescription);

        String[] outputTypes = getOutputTypesOfDescription(engineDescription);

        CollectionReaderDescription testReaderDescription = collectionReaderDescriptionService.createTestReaderDescriptions(
                ClearTypesCombinationReader.class,
                ClearTypesCombinationReader.PARAM_TYPES_TO_CLEAR, outputTypes
        );

        List<JCas> plainTestData = loadJCas(testReaderDescription);
        for (JCas jCas : plainTestData) {
            try {
                runPipeline(jCas, engineDescription);
            } catch (UIMAException e) {
                getLog().warn("Cannot test NLP tool " + analysisEngineService.getAnalysisEngineName(engineDescription) + " in isolation", e);
                return null;
            }
        }

        TestResult testResult = new TestResult(engineDescription);
        measureAndAddToTestResult(testData, plainTestData, testResult, engineDescription);
        return new TestResultWrapper(testResult, engineDescription);
    }

    private void logNlpToolsInfo(AnalysisEngineDescription engineDescription) {
        Class<?> implClass = analysisEngineService.loadAnalysisEngineImplementation(engineDescription);

        String name;
        ResourceMetaData metaData = findAnnotation(implClass, ResourceMetaData.class);
        if (metaData != null) {
            name = metaData.name();
        } else {
            name = engineDescription.getAnnotatorImplementationName();
        }

        String variant = null;
        Object parameterVariant = engineDescription.getAnalysisEngineMetaData().getConfigurationParameterSettings().getParameterValue(ComponentParameters.PARAM_VARIANT);
        if (parameterVariant != null) {
            variant = parameterVariant.toString();
        }

        getLog().info(String.format("Testing %s with variant %s in isolation", name, variant));
    }

    private String[] getOutputTypesOfDescription(AnalysisEngineDescription engineDescription) {
        Class<?> implementationClass = analysisEngineService.loadAnalysisEngineImplementation(engineDescription);
        TypeCapability typeCapability = findAnnotation(implementationClass, TypeCapability.class);
        return typeCapability != null ? typeCapability.outputs() : new String[]{};
    }

    @AllArgsConstructor
    private class TestResultWrapper {

        private TestResult testResult;

        private AnalysisEngineDescription engineDescription;

        @SneakyThrows(IOException.class)
        private void store() {
            File dataDirectory = directoryService.getIsolatedToolsTestDataDirectory();
            File testDataFile = new File(dataDirectory, hash(engineDescription) + ".json");

            ObjectMapper mapper = new ObjectMapper();
            mapper.enable(SerializationFeature.INDENT_OUTPUT);
            try (FileOutputStream fos = new FileOutputStream(testDataFile)) {
                mapper.writeValue(fos, testResult);
            }
        }
    }
}
