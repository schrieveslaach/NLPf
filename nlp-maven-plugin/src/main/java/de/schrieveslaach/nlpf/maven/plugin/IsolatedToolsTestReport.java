package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import de.schrieveslaach.nlpf.maven.plugin.models.Measure;
import de.schrieveslaach.nlpf.maven.plugin.models.TestResult;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import lombok.Cleanup;
import org.apache.maven.doxia.sink.Sink;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.maven.reporting.AbstractMavenReport;
import org.apache.maven.reporting.MavenReportException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.metadata.NameValuePair;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.apache.uima.util.XMLParser;

import javax.inject.Inject;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

@Mojo(name = "tools-report",
        defaultPhase = LifecyclePhase.SITE,
        requiresDependencyResolution = ResolutionScope.COMPILE,
        threadSafe = true)
public class IsolatedToolsTestReport extends AbstractMavenReport {


    @Inject
    protected DirectoryService directoryService;

    @Override
    protected void executeReport(Locale locale) throws MavenReportException {
        Sink sink = getSink();

        // Page title
        sink.head();
        sink.title();
        sink.text("Isolated Tools Test Report for " + project.getName() + " " + project.getVersion());
        sink.title_();
        sink.head_();

        sink.body();

        Map<TestResult, AnalysisEngineDescription> resultsAndEngineDescriptions = loadTestResultsAndEngineDescriptions();

        for (Map.Entry<TestResult, AnalysisEngineDescription> e : resultsAndEngineDescriptions.entrySet()) {
            sink.section1();

            TestResult testResult = e.getKey();

            sink.sectionTitle1();
            sink.text(testResult.toString());
            sink.sectionTitle1_();

            writeMeasureSection(sink, testResult);
            writeParametersSections(sink, e);

            sink.section1_();
        }

        sink.body_();
    }

    private void writeParametersSections(Sink sink, Map.Entry<TestResult, AnalysisEngineDescription> e) {
        sink.section2();
        sink.sectionTitle2();
        sink.text("Parameters");
        sink.sectionTitle2_();

        sink.table();
        sink.tableCaption();
        sink.tableHeaderCell();
        sink.text("Parameter Name");
        sink.tableHeaderCell_();

        sink.tableHeaderCell();
        sink.text("Value");
        sink.tableHeaderCell_();
        sink.tableCaption_();

        AnalysisEngineDescription engineDescription = e.getValue();
        for (NameValuePair nvp : engineDescription.getAnalysisEngineMetaData()
                .getConfigurationParameterSettings()
                .getParameterSettings()) {

            sink.tableRow();

            sink.tableCell();
            sink.text(nvp.getName());
            sink.tableCell_();

            sink.tableCell();
            sink.text(nvp.getValue().toString());
            sink.tableCell_();

            sink.tableRow_();
        }

        sink.table_();
        sink.section2_();
    }

    private void writeMeasureSection(Sink sink, TestResult testResult) {
        sink.section2();
        sink.sectionTitle2();
        sink.text("Measure");
        sink.sectionTitle2_();
        sink.table();

        sink.tableCaption();
        sink.tableHeaderCell();
        sink.text("NLP Tool");
        sink.tableHeaderCell_();

        sink.tableHeaderCell();
        sink.text("Output Type");
        sink.tableHeaderCell_();

        sink.tableHeaderCell();
        sink.text("Variant");
        sink.tableHeaderCell_();

        sink.tableHeaderCell();
        sink.text("F-Measure");
        sink.tableHeaderCell_();
        sink.tableCaption_();

        for (Measure m : testResult.getMeasures()) {
            sink.tableRow();

            sink.tableCell();
            sink.text(m.getAnalysisEngineName());
            sink.tableCell_();

            sink.tableCell();
            sink.text(m.getOutputType());
            sink.tableCell_();

            sink.tableCell();
            sink.text(m.getVariant());
            sink.tableCell_();

            sink.tableCell();
            sink.text(String.format("%.3f", m.getFScore().getFMeasure()));
            sink.tableCell_();

            sink.tableRow_();
        }

        sink.table_();
        sink.section2_();
    }

    private Map<TestResult, AnalysisEngineDescription> loadTestResultsAndEngineDescriptions() throws MavenReportException {
        ObjectMapper mapper = new ObjectMapper();
        XMLParser xmlParser = UIMAFramework.getXMLParser();

        Map<TestResult, AnalysisEngineDescription> resultsAndDescriptions = new HashMap<>();
        for (File testDataFile : directoryService.getIsolatedToolsTestDataDirectory().listFiles()) {
            TestResult testResult;
            try {
                testResult = mapper.readValue(testDataFile, TestResult.class);
            } catch (IOException e) {
                getLog().warn("Cannot load test data of " + testDataFile);
                continue;
            }

            File descriptorFile = new File(directoryService.getDescriptorsDirectory(),
                    testResult.getPipelineDescriptorNames().iterator().next());

            try {
                @Cleanup
                XMLInputSource s = new XMLInputSource(descriptorFile);
                resultsAndDescriptions.put(testResult, xmlParser.parseAnalysisEngineDescription(s));
            } catch (IOException | InvalidXMLException e) {
                throw new MavenReportException("Could not read engine description from " + descriptorFile, e);
            }
        }

        return resultsAndDescriptions;
    }

    @Override
    public String getOutputName() {
        return "isolated-tools-test-report";
    }

    @Override
    public String getName(Locale locale) {
        return "Isolated Tools Test Report";
    }

    @Override
    public String getDescription(Locale locale) {
        return "This report summarizes the results of the isolated tools test goal";
    }
}
