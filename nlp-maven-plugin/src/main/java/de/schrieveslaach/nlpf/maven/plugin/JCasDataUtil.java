package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import lombok.SneakyThrows;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.util.CasCopier;

import java.util.ArrayList;
import java.util.List;

import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;
import static org.apache.uima.fit.util.JCasUtil.select;

class JCasDataUtil {

    private JCasDataUtil() {
    }


    static List<JCas> loadJCas(CollectionReaderDescription collectionReaderDescription, AnalysisEngineDescription... engineDescriptions) {
        List<JCas> jCasList = new ArrayList<>();
        for (JCas originalJCas : iteratePipeline(collectionReaderDescription, engineDescriptions)) {
            jCasList.add(copyJCas(originalJCas));
        }

        return jCasList;
    }

    @SneakyThrows(UIMAException.class)
    @SuppressWarnings("squid:S2445")
    static JCas copyJCas(JCas originalJCas) {
        // The lock is required here because multiple threads during test might copy the JCas object
        // but JCas is not thread safe iterable
        synchronized (originalJCas) {
            JCas copiedJCas = JCasFactory.createJCas();

            // Use copier because iterate pipeline uses a singleton for each returned JCas object.
            CasCopier copier = new CasCopier(originalJCas.getCas(), copiedJCas.getCas());
            for (Annotation a : select(originalJCas, Annotation.class)) {
                if (a instanceof DocumentMetaData) {
                    continue;
                }

                copiedJCas.addFsToIndexes(copier.copyFs(a));
            }

            DocumentMetaData copiedMetaData = DocumentMetaData.create(copiedJCas);
            DocumentMetaData originalMetaData = DocumentMetaData.get(originalJCas);

            copiedMetaData.setDocumentBaseUri(originalMetaData.getDocumentBaseUri());
            copiedMetaData.setDocumentUri(originalMetaData.getDocumentUri());
            copiedMetaData.setDocumentId(originalMetaData.getDocumentId());
            copiedMetaData.setDocumentTitle(originalMetaData.getDocumentTitle());

            copiedJCas.setDocumentText(originalJCas.getDocumentText());
            copiedJCas.setDocumentLanguage(originalJCas.getDocumentLanguage());

            return copiedJCas;
        }
    }
}
