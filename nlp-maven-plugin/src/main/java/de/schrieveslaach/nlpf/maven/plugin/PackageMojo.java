package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import de.schrieveslaach.nlpf.maven.plugin.models.TestResult;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import de.schrieveslaach.nlpf.plumbing.util.Uima;
import org.apache.commons.io.FileUtils;
import org.apache.maven.archiver.MavenArchiveConfiguration;
import org.apache.maven.archiver.MavenArchiver;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.LifecyclePhase;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.archiver.ArchiverException;
import org.codehaus.plexus.archiver.jar.JarArchiver;
import org.codehaus.plexus.archiver.jar.ManifestException;

import javax.inject.Inject;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;

@Mojo(name = "package", defaultPhase = LifecyclePhase.PACKAGE)
public class PackageMojo extends AbstractMojo {

    public static final String PIPELINE_TXT = "pipeline.txt";
    /**
     * The maven project.
     */
    @Parameter(readonly = true, defaultValue = "${project}")
    private MavenProject project;

    @Inject
    private DirectoryService directoryService;

    /**
     * The Jar archiver needed for archiving.
     */
    @Parameter(readonly = true, defaultValue = "${component.org.codehaus.plexus.archiver.Archiver#jar}")
    private JarArchiver jarArchiver;

    /**
     * The language of the corpus
     */
    @Parameter(property = "default.document.language", defaultValue = "en")
    private String language;

    @Parameter
    private MavenSession session;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        Uima.swallowLogging();

        directoryService.setLanguage(language);
        getLog().debug("Archive for language: " + language);

        File jarFile = directoryService.getJarFile();
        getLog().debug("Archive NLP models and pipeline configuration into " + jarFile);

        TestResult bestPipelineResult = getBestPipelineResult();

        MavenArchiver archiver = new MavenArchiver();
        archiver.setArchiver(jarArchiver);
        archiver.setOutputFile(jarFile);

        try {
            createPipelinesManifestEntry(bestPipelineResult);

            // add all NLP models to archive
            archiver.getArchiver().addDirectory(
                    directoryService.getModelsBaseDirectory()
            );

            // Add manifest entry
            archiver.getArchiver().addFile(
                    directoryService.getPipelineTxtFile(),
                    "META-INF/de.schrieveslaach.effective.nlp.application.development/pipeline.txt"
            );

            // add all Analysis engine descriptions which have been the best f-measure
            for (String pipelineDescriptorFile : bestPipelineResult.getPipelineDescriptorNames()) {
                archiver.getArchiver().addFile(
                        new File(directoryService.getDescriptorsDirectory(), pipelineDescriptorFile),
                        getJarFilePrefixForResources() + pipelineDescriptorFile
                );
            }

            MavenArchiveConfiguration archiveConfiguration = new MavenArchiveConfiguration();
            archiver.createArchive(session, project, archiveConfiguration);
            project.getArtifact().setFile(jarFile);
        } catch (ArchiverException | IOException | ManifestException | DependencyResolutionRequiredException e) {
            throw new MojoExecutionException("Exception while packaging", e);
        }
    }

    private TestResult getBestPipelineResult() throws MojoExecutionException {
        File testDataFile = new File(directoryService.getPipelinesTestDataDirectory(), "0.json");

        try (FileInputStream fis = new FileInputStream(testDataFile)) {
            ObjectMapper mapper = new ObjectMapper();
            return mapper.readValue(fis, TestResult.class);
        } catch (IOException e) {
            throw new MojoExecutionException("Could not read test data of best-performing NLP pipeline", e);
        }
    }

    private String getJarFilePrefixForResources() {
        return project.getGroupId() + "/" + project.getArtifactId() + "/";
    }

    private void createPipelinesManifestEntry(TestResult bestPipelineTestResult) throws MojoExecutionException {
        File descriptors = directoryService.getDescriptorsDirectory();
        if (!descriptors.isDirectory()) {
            return;
        }

        File pipeline = directoryService.getPipelineTxtFile();

        try (FileOutputStream fos = new FileOutputStream(pipeline); PrintWriter pw = new PrintWriter(fos)) {
            for (File f : FileUtils.listFiles(descriptors, new String[]{"xml"}, false)) {
                if (!bestPipelineTestResult.getPipelineDescriptorNames().contains(f.getName())) {
                    continue;
                }

                pw.printf("classpath*:/%s/%s/%s", project.getGroupId(), project.getArtifactId(), f.getName());
                pw.println();
            }
            pw.flush();
        } catch (IOException e) {
            throw new MojoExecutionException("Could not create pipelines.txt", e);
        }
    }
}
