package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Lists;
import com.google.common.collect.Sets;
import com.google.common.collect.Table;
import de.schrieveslaach.nlpf.maven.plugin.models.TestResult;
import de.schrieveslaach.nlpf.maven.plugin.reader.ClearTypesCombinationReader;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionNode;
import de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineGraph;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.CasCopier;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Deque;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.BiConsumer;
import java.util.function.BiPredicate;
import java.util.function.BinaryOperator;
import java.util.function.Function;
import java.util.function.Supplier;
import java.util.stream.Collector;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.maven.plugin.JCasDataUtil.copyJCas;
import static de.schrieveslaach.nlpf.maven.plugin.JCasDataUtil.loadJCas;
import static de.schrieveslaach.nlpf.plumbing.BestPerformingPipelineFactory.sortByTopologicalDependencyOrder;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;

@Mojo(name = "pipelines-test", requiresDependencyResolution = ResolutionScope.COMPILE)
public class PipelinesTestMojo extends AbstractTestMojo {

    @Override
    public void executeWithUimaClassLoader() throws MojoExecutionException {
        List<List<AnalysisEngineDescription>> pipelines;
        List<NlpPipelineGroup> pipelineGroups;
        try {
            pipelines = buildPipelineCombinations();
            pipelineGroups = buildGroups(pipelines);
        } catch (ResourceInitializationException e) {
            throw new MojoExecutionException("Could not build NLP pipelines", e);
        }

        getLog().info("Testing " + pipelines.size() + " NLP pipelines in " + pipelineGroups.size() + " groups");

        List<JCas> testData = loadJCas(collectionReaderDescriptionService.createTestReaderDescriptions());

        // Create a reader description which removes all output types of the pipeline.
        // The pipeline annotates these types and they will be compared to the original
        // JCas instances.
        CollectionReaderDescription testReaderDescription = collectionReaderDescriptionService.createTestReaderDescriptions(
                ClearTypesCombinationReader.class,
                ClearTypesCombinationReader.PARAM_TYPES_TO_CLEAR, getOutputTypesOfPipeline(pipelines)
        );
        List<JCas> plainTestData = loadJCas(testReaderDescription);

        List<TestResult> testResults = new ArrayList<>();
        for (NlpPipelineGroup group : pipelineGroups) {
            try {
                testResults.addAll(group.runPipelines(testData, plainTestData));
            } catch (UIMAException e) {
                throw new MojoExecutionException("Could not test NLP pipeline", e);
            }
        }

        storeBestTestResults(testResults);
    }

    private List<NlpPipelineGroup> buildGroups(List<List<AnalysisEngineDescription>> pipelines) {
        return pipelines.stream()
                .map(pipeline -> {
                    AnalysisEngineGraph graph = new AnalysisEngineGraph();
                    graph.init(pipeline, Thread.currentThread().getContextClassLoader());
                    return graph;
                })
                .collect(new NlpPipelineGroupCollector());
    }


    public void storeBestTestResults(List<TestResult> testResults) throws MojoExecutionException {
        Collections.sort(testResults);

        ObjectMapper mapper = new ObjectMapper();
        mapper.enable(SerializationFeature.INDENT_OUTPUT);

        int i = 0;
        for (TestResult testResult : testResults) {

            File testResultFile = new File(directoryService.getPipelinesTestDataDirectory(), i + ".json");
            try {
                try (FileOutputStream fos = new FileOutputStream(testResultFile)) {
                    mapper.writeValue(fos, testResult);
                }
            } catch (IOException e) {
                throw new MojoExecutionException("Could not save test data.", e);
            }

            if (++i >= 5) {
                break;
            }
        }
    }

    private String[] getOutputTypesOfPipeline(List<List<AnalysisEngineDescription>> pipelines) {
        Set<String> outputTypes = pipelines.stream()
                .flatMap(Collection::stream)
                .map(analysisEngineService::loadAnalysisEngineImplementation)
                .map(cls -> findAnnotation(cls, TypeCapability.class))
                .filter(Objects::nonNull)
                .map(TypeCapability::outputs)
                .flatMap(Arrays::stream)
                .collect(Collectors.toSet());

        return outputTypes.toArray(new String[outputTypes.size()]);
    }

    /**
     * Builds all pipeline combinations from the available
     * {@link AnalysisEngineDescription annotators} in the
     * {@link DirectoryService#getDescriptorsDirectory() descriptors directory}.
     *
     * @return
     */
    List<List<AnalysisEngineDescription>> buildPipelineCombinations() throws ResourceInitializationException, MojoExecutionException {
        List<AnalysisEngineDescription> annotatorDescriptions = loadEngineDescriptions();

        Map<EngineDescriptionElement, List<EngineDescriptionElement>> m = annotatorDescriptions.stream()
                .map(EngineDescriptionElement::new)
                .collect(Collectors.groupingBy(
                        Function.identity(),
                        Collectors.<EngineDescriptionElement>toList()
                ));

        // Groups all engine description by the output value of @TypeCapability
        List<List<EngineDescriptionElement>> groupingByOutputCapability = new ArrayList<>(m.values());

        List<List<AnalysisEngineDescription>> pipelines = new ArrayList<>();
        buildAllPipelineCombinations(groupingByOutputCapability, 0, pipelines, new LinkedList<>());
        return pipelines;
    }

    private void buildAllPipelineCombinations(List<List<EngineDescriptionElement>> groupingByOutputCapability, int depth, List<List<AnalysisEngineDescription>> pipelines, Deque<AnalysisEngineDescription> currentPipeline) throws ResourceInitializationException {
        if (depth == groupingByOutputCapability.size()) {
            List<AnalysisEngineDescription> pipeline = new ArrayList<>(currentPipeline);
            sortByTopologicalDependencyOrder(pipeline, classLoaderService.getDependencyClassLoader());
            pipelines.add(pipeline);
            return;
        }

        for (EngineDescriptionElement element : groupingByOutputCapability.get(depth)) {
            currentPipeline.push(element.getEngineDescription());
            buildAllPipelineCombinations(groupingByOutputCapability, depth + 1, pipelines, currentPipeline);
            currentPipeline.pop();
        }
    }

    @AllArgsConstructor
    private static class RunResult {

        @Getter
        private final TestResult result;

        @Getter
        private final UIMAException exception;

    }

    /**
     * <p></p>
     * Helps to execute {@link #commonPipelineHead common parts of a NLP  pipeline} with the remaining
     * {@link #tails remaining NLP pipeline tails}. For example, two NLP pipelines can have following common
     * pipeline head:
     * </p>
     * <p>
     * <pre><code>
     * Segmenter (OpenNlp) -> PosTagger (OpenNlp)
     * </code></pre>
     * <p>
     * <p>
     * Then, the remain tails of the pipelines are:
     * </p>
     * <p>
     * <pre><code>
     * NamedEntityRecognizer (OpenNlp, person)
     * </code></pre>
     * <p>
     * <p>
     * and
     * </p>
     * <p>
     * <pre><code>
     * NamedEntityRecognizer (StanfordCoreNlp, person)
     * </code></pre>
     * <p>
     * <p>
     * All pipelines together are:
     * </p>
     * <p>
     * <pre><code>
     * Segmenter (OpenNlp) -> PosTagger (OpenNlp) -> NamedEntityRecognizer (OpenNlp, person)
     * </code></pre>
     * <p>
     * <p>
     * and
     * </p>
     * <p>
     * <pre><code>
     * Segmenter (OpenNlp) -> PosTagger (OpenNlp) -> NamedEntityRecognizer (StanfordCoreNlp, person)
     * </code></pre>
     */
    private class NlpPipelineGroup {

        private final AnalysisEngineGraph commonPipelineHead;

        private final NlpPipelineTails tails = new NlpPipelineTails(this);

        public NlpPipelineGroup(AnalysisEngineGraph commonPipelineHead) {
            this.commonPipelineHead = commonPipelineHead;
        }

        public List<TestResult> runPipelines(List<JCas> testData, List<JCas> plainTestData) throws UIMAException {
            getLog().info("Starting to test pipeline with common head: " + commonPipelineHead);

            List<JCas> copiedJCases = new ArrayList<>();
            for (JCas jCas : plainTestData) {
                JCas copiedJCas = copyJCas(jCas);
                commonPipelineHead.runOn(copiedJCas);
                copiedJCases.add(copiedJCas);
            }

            return tails.run(testData, copiedJCases);
        }

        public void splitAndAddTail(int splitAt, AnalysisEngineGraph tail) {
            List<AnalysisEngineDescription> newTail = tail.subList(splitAt);

            if (tails.isEmpty()) {
                List<AnalysisEngineDescription> headDescriptions = Lists.newArrayList(commonPipelineHead.iterator());

                tails.addTail(headDescriptions.subList(splitAt, headDescriptions.size()), getUrlClassLoader());

                commonPipelineHead.clear();
                commonPipelineHead.init(headDescriptions.subList(0, splitAt), getUrlClassLoader());
            } else {
                if (commonPipelineHead.size() != splitAt) {
                    throw new IllegalArgumentException();
                }
            }

            tails.addTail(newTail, getUrlClassLoader());
        }

        public boolean haveCommonHead(NlpPipelineGroup group) {
            return commonPipelineHead.equals(group.commonPipelineHead);
        }

        public void add(NlpPipelineGroup group) {
            if (!haveCommonHead(group)) {
                throw new IllegalArgumentException();
            }

            tails.addTailsOf(group);
        }

    }

    private class NlpPipelineGroupCollector implements Collector<AnalysisEngineGraph, List<NlpPipelineGroup>, List<NlpPipelineGroup>> {

        @Override
        public Supplier<List<NlpPipelineGroup>> supplier() {
            return ArrayList::new;
        }

        @Override
        public BiConsumer<List<NlpPipelineGroup>, AnalysisEngineGraph> accumulator() {
            return (nlpPipelineGroups, analysisEngineGraph) -> {
                if (nlpPipelineGroups.isEmpty()) {
                    nlpPipelineGroups.add(new NlpPipelineGroup(analysisEngineGraph));
                    return;
                }

                NlpPipelineGroup grp = nlpPipelineGroups.get(0);
                int maxHeadCount = analysisEngineGraph.countCommonPipelineHeadElements(grp.commonPipelineHead);
                for (int i = 1; i < nlpPipelineGroups.size(); i++) {
                    NlpPipelineGroup tempGroup = nlpPipelineGroups.get(i);
                    int cnt = analysisEngineGraph.countCommonPipelineHeadElements(tempGroup.commonPipelineHead);

                    if (cnt > maxHeadCount) {
                        maxHeadCount = cnt;
                        grp = tempGroup;
                    }
                }

                if (maxHeadCount == 0) {
                    nlpPipelineGroups.add(new NlpPipelineGroup(analysisEngineGraph));
                } else {
                    grp.splitAndAddTail(maxHeadCount, analysisEngineGraph);
                }
            };
        }

        @Override
        public BinaryOperator<List<NlpPipelineGroup>> combiner() {
            return (nlpPipelineGroups, nlpPipelineGroups2) -> {
                for (NlpPipelineGroup grp1 : nlpPipelineGroups) {
                    for (int i = nlpPipelineGroups2.size() - 1; i >= 0; i--) {
                        NlpPipelineGroup grp2 = nlpPipelineGroups2.get(i);

                        if (grp1.haveCommonHead(grp2)) {
                            grp1.add(grp2);
                            nlpPipelineGroups2.remove(i);
                        }
                    }
                }
                nlpPipelineGroups.addAll(nlpPipelineGroups2);
                return nlpPipelineGroups;
            };
        }

        @Override
        public Function<List<NlpPipelineGroup>, List<NlpPipelineGroup>> finisher() {
            return nlpPipelineGroups -> nlpPipelineGroups;
        }

        @Override
        public Set<Characteristics> characteristics() {
            return Sets.newHashSet(Characteristics.UNORDERED);
        }
    }

    private class NlpPipelineTails {

        private final List<AnalysisEngineGraph> pipelineTails = new ArrayList<>();

        private final Table<AnalysisEngineDescriptionNode, String, JCas> commonTailResults = HashBasedTable.create();

        private final TailBarrier barrier = new TailBarrier();

        private final NlpPipelineGroup group;

        private NlpPipelineTails(NlpPipelineGroup group) {
            this.group = group;
        }

        /**
         * @param testData
         * @param pipelineData The data annotated by the {@link #group pipeline head}.
         * @return
         * @throws UIMAException
         */
        private List<TestResult> run(List<JCas> testData, List<JCas> pipelineData) throws UIMAException {
            if (pipelineTails.isEmpty()) {
                return Lists.newArrayList(determineMeasures(group.commonPipelineHead, testData, pipelineData));
            }

            List<RunResult> runResults = pipelineTails.parallelStream()
                    .peek(pipelineTail -> getLog().info("Starting to test remaining pipeline with head " + group.commonPipelineHead + " and tail " + pipelineTail))
                    .map(pipelineTail -> run(pipelineTail, testData, pipelineData))
                    .collect(Collectors.toList());

            Optional<UIMAException> uimaException = runResults.stream()
                    .map(RunResult::getException)
                    .filter(Objects::nonNull)
                    .findAny();

            if (uimaException.isPresent()) {
                throw uimaException.get();
            }

            return runResults.stream()
                    .map(RunResult::getResult)
                    .collect(Collectors.toList());
        }

        @SuppressWarnings("squid:S2445")
        private RunResult run(AnalysisEngineGraph pipelineTail, List<JCas> testData, List<JCas> pipelineData) {
            List<JCas> fullPipelineTestData = new ArrayList<>();
            for (JCas jCas : pipelineData) {
                JCas copiedJCas = copyJCas(jCas);

                try {
                    List<AnalysisEngineDescriptionNode> nodes = pipelineTail.stream().collect(Collectors.toList());

                    for (AnalysisEngineDescriptionNode node : nodes) {
                        if (node.isStartOfPipeline()) {
                            runOrCopyPipelineData(copiedJCas, node);
                        } else {
                            synchronized (copiedJCas) {
                                runPipeline(copiedJCas, node.getDescription());
                            }
                        }
                    }
                } catch (UIMAException e) {
                    return new RunResult(null, e);
                }
                fullPipelineTestData.add(copiedJCas);
            }

            List<AnalysisEngineDescription> fullPipeline = Lists.newArrayList(group.commonPipelineHead);
            fullPipeline.addAll(Lists.newArrayList(pipelineTail));

            return new RunResult(determineMeasures(fullPipeline, testData, fullPipelineTestData), null);
        }

        @SuppressWarnings("squid:S2445")
        private void runOrCopyPipelineData(JCas copiedJCas, AnalysisEngineDescriptionNode node) throws UIMAException {
            DocumentMetaData documentMetaData = DocumentMetaData.get(copiedJCas);
            String documentUri = documentMetaData.getDocumentUri();

            if (!barrier.waitUntilFirstNodeProvidedCommonTailResult(node, documentMetaData)) {
                synchronized (copiedJCas) {
                    runPipeline(copiedJCas, node.getDescription());
                }

                synchronized (commonTailResults) {
                    synchronized (copiedJCas) {
                        commonTailResults.put(node, documentUri, copiedJCas);
                    }
                }

                barrier.unlock(node, documentMetaData);
                return;
            }

            synchronized (commonTailResults) {
                synchronized (copiedJCas) {
                    JCas commonResult = commonTailResults.get(node, documentUri);
                    copyPipelineData(copiedJCas, node, commonResult);
                }
            }
        }

        @SuppressWarnings("squid:S2445")
        private void copyPipelineData(JCas copiedJCas, AnalysisEngineDescriptionNode node, JCas commonResult) {
            // The lock is required here because multiple threads iterate over the common JCas object
            // but JCas is not thread safe iterable
            synchronized (commonResult) {
                CasCopier copier = new CasCopier(commonResult.getCas(), copiedJCas.getCas());

                for (Class<? extends Annotation> outputType : node.getOutputTypes()) {
                    BiPredicate<Annotation, String> filter = (BiPredicate<Annotation, String>) FILTER_FUNTIONS
                            .getOrDefault(outputType, (a, v) -> true);

                    for (Annotation annotation : select(commonResult, outputType)) {
                        if (filter.test(annotation, node.getVariant())) {
                            copiedJCas.addFsToIndexes(copier.copyFs(annotation));
                        }
                    }
                }
            }
        }

        private TestResult determineMeasures(Iterable<AnalysisEngineDescription> pipeline, List<JCas> testData, List<JCas> pipelineData) {
            TestResult testResult = new TestResult(pipeline);

            for (AnalysisEngineDescription analysisEngineDescription : pipeline) {
                measureAndAddToTestResult(testData, pipelineData, testResult, analysisEngineDescription);
            }

            return testResult;
        }

        public void addTailsOf(NlpPipelineGroup group) {
            pipelineTails.addAll(group.tails.pipelineTails);
        }

        public void addTail(List<AnalysisEngineDescription> tail, ClassLoader classLoader) {
            AnalysisEngineGraph graph = new AnalysisEngineGraph();
            graph.init(tail, classLoader);
            pipelineTails.add(graph);
        }

        public boolean isEmpty() {
            return pipelineTails.isEmpty();
        }
    }


    private static class TailBarrier {

        private Table<AnalysisEngineDescriptionNode, String, Boolean> barrierStatus = HashBasedTable.create();

        @SneakyThrows(InterruptedException.class)
        public boolean waitUntilFirstNodeProvidedCommonTailResult(AnalysisEngineDescriptionNode node, DocumentMetaData metaData) {
            String documentUri = metaData.getDocumentUri();

            synchronized (barrierStatus) {
                if (!barrierStatus.contains(node, documentUri)) {
                    barrierStatus.put(node, documentUri, false);
                    return false;
                }

                while (!barrierStatus.get(node, documentUri)) {
                    barrierStatus.wait();
                }
            }

            return true;
        }

        public void unlock(AnalysisEngineDescriptionNode node, DocumentMetaData metaData) {
            String documentUri = metaData.getDocumentUri();

            synchronized (barrierStatus) {
                barrierStatus.put(node, documentUri, true);
                barrierStatus.notifyAll();
            }
        }

    }
}
