package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.service.AnalysisEngineDescriptionService;
import de.schrieveslaach.nlpf.maven.plugin.service.ClassLoaderService;
import de.schrieveslaach.nlpf.maven.plugin.service.CollectionReaderDescriptionService;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import de.schrieveslaach.nlpf.plumbing.util.Uima;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.fit.internal.ResourceManagerFactory;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.fit.util.LifeCycleUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceManager;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.maven.plugin.JCasDataUtil.copyJCas;
import static de.schrieveslaach.nlpf.maven.plugin.JCasDataUtil.loadJCas;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

@Mojo(name = "train", requiresDependencyResolution = ResolutionScope.COMPILE)
public class TrainMojo extends AbstractMojo {

    /**
     * The language of the corpus
     */
    @Parameter(property = "default.document.language", defaultValue = "en")
    private String language;

    @Inject
    private AnalysisEngineDescriptionService analysisEngineService;

    @Inject
    private CollectionReaderDescriptionService collectionReaderService;

    @Inject
    private DirectoryService directoryService;

    @Inject
    private ClassLoaderService classLoaderService;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        Uima.swallowLogging();

        analysisEngineService.setLanguage(language);
        collectionReaderService.setLanguage(language);
        directoryService.setLanguage(language);

        Thread.currentThread().setContextClassLoader(classLoaderService.getDependencyClassLoader());

        List<AnalysisEngineDescription> trainerDescriptions = trainNlpModels();
        for (AnalysisEngineDescription aed : analysisEngineService.findAnnotatorDescriptions(trainerDescriptions)) {
            directoryService.store(aed);
        }
    }

    private List<AnalysisEngineDescription> trainNlpModels() throws MojoExecutionException {
        List<AnalysisEngineDescription> trainerDescriptions;

        getLog().debug("Create DKPro Core reader description for all files");

        CollectionReaderDescription trainReader = collectionReaderService.createReaderDescriptions();
        List<JCas> trainingData = loadJCas(trainReader);

        trainerDescriptions = runTrainerPipeline(trainingData);

        getLog().info("NLP model training succeeded.");

        return trainerDescriptions;
    }

    private List<AnalysisEngineDescription> runTrainerPipeline(List<JCas> trainingData) {
        return analysisEngineService.findTrainerDescriptions()
                .parallelStream()
                .filter(aed -> train(trainingData, aed))
                .collect(Collectors.toList());
    }

    private boolean train(List<JCas> trainingData, AnalysisEngineDescription aed) {
        String analysisEngineName = analysisEngineService.getAnalysisEngineName(aed);
        getLog().info("Starting to train with " + analysisEngineName);

        AnalysisEngine aae = null;
        try {
            ResourceManager resMgr = ResourceManagerFactory.newResourceManager();

            // Instantiate AAE
            AnalysisEngineDescription aaeDesc = createEngineDescription(aed);
            aae = UIMAFramework.produceAnalysisEngine(aaeDesc, resMgr, null);

            // Process
            boolean hasCollectedTrainingData = false;
            for (JCas jCas : trainingData) {
                if (hasRequiredAnnotations(jCas.getCas(), aed)) {
                    aae.process(copyJCas(jCas).getCas());
                    hasCollectedTrainingData = true;
                }
            }

            if (hasCollectedTrainingData) {
                aae.collectionProcessComplete();

                getLog().info("Finished training of " + analysisEngineName);

                return true;
            } else {
                getLog().warn("Could not train with " + analysisEngineName + ". No training data available.");
            }
        } catch (AnalysisEngineProcessException | ResourceInitializationException e) {
            getLog().error("Can not train with " + analysisEngineName, e);
        } finally {
            // Destroy
            LifeCycleUtil.destroy(aae);
        }

        return false;
    }

    private boolean hasRequiredAnnotations(CAS cas, AnalysisEngineDescription aed) {
        Set<String> mimetypes = analysisEngineService.getMimetypes(aed);

        for (String type : mimetypes) {
            if (TypeCapability.NO_DEFAULT_VALUE.equals(type)) {
                continue;
            }

            Collection<AnnotationFS> select = CasUtil.select(cas, CasUtil.getType(cas, type));

            if (select.isEmpty()) {
                return false;
            }
        }

        return true;
    }
}
