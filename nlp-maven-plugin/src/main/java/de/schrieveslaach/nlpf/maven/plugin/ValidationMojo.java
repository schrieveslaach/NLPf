package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.service.ClassLoaderService;
import de.schrieveslaach.nlpf.maven.plugin.service.CollectionReaderDescriptionService;
import de.schrieveslaach.nlpf.plumbing.util.Uima;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import org.apache.maven.plugin.AbstractMojo;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.plugin.MojoFailureException;
import org.apache.maven.plugins.annotations.Mojo;
import org.apache.maven.plugins.annotations.Parameter;
import org.apache.maven.plugins.annotations.ResolutionScope;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.jcas.JCas;

import javax.inject.Inject;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectCovering;

@Mojo(name = "validate-annotations", requiresDependencyResolution = ResolutionScope.COMPILE)
public class ValidationMojo extends AbstractMojo {

    /**
     * The language of the corpus
     */
    @Parameter(property = "default.document.language", defaultValue = "en")
    private String language;

    @Inject
    private CollectionReaderDescriptionService collectionReaderService;

    @Inject
    private ClassLoaderService classLoaderService;

    @Override
    public void execute() throws MojoExecutionException, MojoFailureException {
        Uima.swallowLogging();

        collectionReaderService.setLanguage(language);

        Thread.currentThread().setContextClassLoader(classLoaderService.getDependencyClassLoader());

        CollectionReaderDescription trainReader = collectionReaderService.createReaderDescriptions();
        for (JCas jCas : iteratePipeline(trainReader)) {
            verifyAllOrNoneTokenContainsPosTags(jCas);
        }
    }

    private void verifyAllOrNoneTokenContainsPosTags(JCas jCas) throws MojoExecutionException {
        Collection<Token> tokens = select(jCas, Token.class);

        List<Token> tokensWithoutPosTag = tokens.stream()
                .filter(t -> t.getPos() == null)
                .collect(Collectors.toList());

        if (!tokensWithoutPosTag.isEmpty() && tokensWithoutPosTag.size() != tokens.size()) {
            Token token = tokensWithoutPosTag.get(0);

            List<Sentence> sentences = selectCovering(jCas, Sentence.class, token);

            DocumentMetaData documentMetaData = DocumentMetaData.get(jCas);

            String errorMsg = "The token “" + token.getText() + "” in the sentence “" + sentences.get(0).getCoveredText()
                    + "” must be annotated with a pos tag because all other tokens are annotated with pos tags, c.f. document: "
                    + documentMetaData.getDocumentUri();
            getLog().error(errorMsg);
            throw new MojoExecutionException(errorMsg);
        }
    }
}
