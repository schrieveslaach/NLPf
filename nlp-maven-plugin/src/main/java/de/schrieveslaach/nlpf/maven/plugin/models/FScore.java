package de.schrieveslaach.nlpf.maven.plugin.models;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.eval.measure.FMeasure;
import lombok.AllArgsConstructor;
import lombok.Getter;

@AllArgsConstructor
public class FScore {

    @Getter
    private final double precision;
    @Getter
    private final double recall;
    @Getter
    private final double fMeasure;

    FScore(FMeasure fMeasure) {
        this.fMeasure = fMeasure.getFMeasure();
        this.recall = fMeasure.getRecall();
        this.precision = fMeasure.getPrecision();
    }
}
