package de.schrieveslaach.nlpf.maven.plugin.models;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;

@AllArgsConstructor
@EqualsAndHashCode(exclude = "fScore")
public class Measure implements Comparable<Measure> {

    @Getter
    @NonNull
    private final String analysisEngineName;

    @Getter
    @NonNull
    private final String outputType;

    @Getter
    private final String variant;

    @Getter
    @NonNull
    private FScore fScore;

    @Override
    public int compareTo(Measure measure) {
        int cmp = outputType.compareTo(measure.outputType);

        if (cmp == 0) {
            if (variant != null && measure.variant != null) {
                cmp = variant.compareTo(measure.variant);
            } else if (variant == null && measure.variant != null) {
                cmp = -1;
            } else if (variant != null && measure.variant == null) {
                cmp = 1;
            }
        }

        if (cmp == 0) {
            cmp = analysisEngineName.compareTo(measure.analysisEngineName);
        }

        return cmp;
    }
}
