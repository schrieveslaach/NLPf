package de.schrieveslaach.nlpf.maven.plugin.models;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;
import com.google.common.collect.ImmutableList;
import de.tudarmstadt.ukp.dkpro.core.eval.measure.FMeasure;
import lombok.EqualsAndHashCode;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionUtil.hash;
import static java.util.Arrays.asList;

@JsonDeserialize(using = TestResult.TestResultDeserializer.class)
@JsonSerialize(using = TestResult.TestResultSerializer.class)
@EqualsAndHashCode
public class TestResult implements Comparable<TestResult> {

    private final SortedSet<Measure> measures = new TreeSet<>();

    private final List<String> pipelineDescriptors = new ArrayList<>();

    private TestResult() {
    }

    public TestResult(AnalysisEngineDescription engineDescription) {
        this(asList(engineDescription));
    }

    public TestResult(Iterable<AnalysisEngineDescription> pipeline) {
        for (AnalysisEngineDescription aed : pipeline) {
            pipelineDescriptors.add(hash(aed) + ".xml");
        }
    }

    public void add(String analysisEngineName, String outputType, String variant, FMeasure measure) {
        measures.add(new Measure(analysisEngineName, outputType, variant, new FScore(measure)));
    }

    public ImmutableList<String> getPipelineDescriptorNames() {
        return ImmutableList.copyOf(pipelineDescriptors);
    }

    public ImmutableList<Measure> getMeasures() {
        return ImmutableList.copyOf(measures);
    }

    @Override
    public String toString() {
        return measures.stream()
                .map(Measure::getAnalysisEngineName)
                .distinct()
                .collect(Collectors.joining(", "));
    }

    @Override
    public int compareTo(TestResult other) {
        int compare = Integer.compare(measures.size(), other.measures.size());
        if (compare != 0) {
            return compare;
        }

        // reverse order so that higher harmonic means will be sorted first
        return -1 * Double.compare(calculateHarmonicMean(), other.calculateHarmonicMean());
    }

    public double calculateHarmonicMean() {
        double sum = 0.0;
        int cnt = 0;

        for (Measure measure : measures) {
            double fMeasure = measure.getFScore().getFMeasure();
            if (fMeasure < 0) {
                // punish measures without any hits
                fMeasure = 1 / 1_000_000.0;
            }

            sum += 1.0 / fMeasure;
            ++cnt;
        }

        if (sum <= 0.0) {
            return 0.0;
        }
        return cnt / sum;
    }

    public static class TestResultSerializer extends JsonSerializer<TestResult> {

        @Override
        public void serialize(TestResult testResult, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
            jsonGenerator.writeStartObject();

            jsonGenerator.writeFieldName("descriptors");
            jsonGenerator.writeStartArray();
            for (String aed : testResult.pipelineDescriptors) {
                jsonGenerator.writeString(aed);
            }
            jsonGenerator.writeEndArray();

            jsonGenerator.writeFieldName("measures");
            jsonGenerator.writeStartArray();
            for (Measure measureEntry : testResult.measures) {
                jsonGenerator.writeStartObject();

                jsonGenerator.writeFieldName("analysisEngineName");
                jsonGenerator.writeString(measureEntry.getAnalysisEngineName());

                jsonGenerator.writeFieldName("outputType");
                jsonGenerator.writeString(measureEntry.getOutputType());
                jsonGenerator.writeFieldName("variant");
                jsonGenerator.writeString(measureEntry.getVariant());

                jsonGenerator.writeFieldName("measure");
                jsonGenerator.writeStartObject();

                jsonGenerator.writeFieldName("f-measure");
                jsonGenerator.writeNumber(measureEntry.getFScore().getFMeasure());
                jsonGenerator.writeFieldName("precision");
                jsonGenerator.writeNumber(measureEntry.getFScore().getPrecision());
                jsonGenerator.writeFieldName("recall");
                jsonGenerator.writeNumber(measureEntry.getFScore().getRecall());
                jsonGenerator.writeEndObject();

                jsonGenerator.writeEndObject();
            }
            jsonGenerator.writeEndArray();

            jsonGenerator.writeEndObject();
        }
    }

    public static class TestResultDeserializer extends JsonDeserializer<TestResult> {

        @Override
        public TestResult deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            TestResult testResult = new TestResult();

            TreeNode treeNode = jsonParser.readValueAsTree();

            ArrayNode descriptorsNode = (ArrayNode) treeNode.get("descriptors");
            for (JsonNode node : descriptorsNode) {
                testResult.pipelineDescriptors.add(node.textValue());
            }

            ArrayNode measuresNode = (ArrayNode) treeNode.get("measures");
            for (JsonNode node : measuresNode) {
                JsonNode variant = node.get("variant");

                testResult.measures.add(new Measure(
                        node.get("analysisEngineName").asText(),
                        node.get("outputType").asText(),
                        variant.isNull() ? null : variant.asText(),
                        createFScore((ObjectNode) node.get("measure"))
                ));
            }

            return testResult;
        }

        private FScore createFScore(ObjectNode measureNode) {
            return new FScore(
                    measureNode.get("precision").doubleValue(),
                    measureNode.get("recall").doubleValue(),
                    measureNode.get("f-measure").doubleValue()
            );
        }

    }
}
