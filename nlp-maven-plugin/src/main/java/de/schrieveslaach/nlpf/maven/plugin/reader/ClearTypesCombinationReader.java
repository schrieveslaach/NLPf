package de.schrieveslaach.nlpf.maven.plugin.reader;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.io.combination.CombinationReader;
import org.apache.uima.cas.CAS;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.CollectionException;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.cas.TOP;

import java.io.IOException;
import java.util.Collection;

@SuppressWarnings("squid:MaximumInheritanceDepth")
public class ClearTypesCombinationReader extends CombinationReader {

    public static final String PARAM_TYPES_TO_CLEAR = "types-to-clear";
    @ConfigurationParameter(name = PARAM_TYPES_TO_CLEAR)
    private String[] typesToClear;

    @Override
    public void getNext(CAS aCAS) throws IOException, CollectionException {
        super.getNext(aCAS);

        for (String typeToClear : typesToClear) {
            Type type = aCAS.getTypeSystem().getType(typeToClear);
            Collection<AnnotationFS> annotationsToClear = CasUtil.select(aCAS, type);
            annotationsToClear.stream()
                    .filter(annotationFS -> annotationFS instanceof TOP)
                    .map(annotationFS -> (TOP) annotationFS)
                    .forEach(TOP::removeFromIndexes);
        }
    }
}
