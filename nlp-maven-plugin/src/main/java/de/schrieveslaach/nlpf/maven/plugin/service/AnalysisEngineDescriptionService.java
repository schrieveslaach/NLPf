package de.schrieveslaach.nlpf.maven.plugin.service;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.base.CharMatcher;
import com.google.common.base.Splitter;
import com.google.common.collect.HashMultimap;
import com.google.common.collect.Lists;
import com.google.common.collect.Multimap;
import de.schrieveslaach.nlpf.plumbing.ClassMimeTypeMapping;
import de.schrieveslaach.nlpf.plumbing.ClassMimeTypeMappingPair;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ResourceParameter;
import lombok.EqualsAndHashCode;
import lombok.Setter;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.project.MavenProject;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.component.JCasConsumer_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.MimeTypeCapability;
import org.apache.uima.fit.descriptor.ResourceMetaData;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.resource.ResourceInitializationException;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.lang.reflect.Field;
import java.util.AbstractMap.SimpleEntry;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;
import java.util.stream.Stream;

import static de.schrieveslaach.nlpf.plumbing.DKProCoreReflection.convertToFlatStreamOfMimeTypes;
import static de.schrieveslaach.nlpf.plumbing.DKProCoreReflection.findClassMimeTypePairs;
import static de.schrieveslaach.nlpf.plumbing.DKProCoreReflection.lookUpClasses;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static java.util.stream.Collectors.toList;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

@Singleton
public class AnalysisEngineDescriptionService {

    public static final String NAMED_ENTITY_PROPERTY_KEY = "named.entity.types";

    private final Splitter namedEntitySplitter = Splitter.on(CharMatcher.anyOf(","))
            .trimResults()
            .omitEmptyStrings();

    private final MavenProject mavenProject;

    private final ClassLoaderService classLoaderService;

    private final DirectoryService directoryService;

    private final Log logger = new SystemStreamLog();

    @Setter
    private String language;

    @Inject
    public AnalysisEngineDescriptionService(MavenProject mavenProject, ClassLoaderService classLoaderService, DirectoryService directoryService) {
        this.mavenProject = mavenProject;
        this.classLoaderService = classLoaderService;
        this.directoryService = directoryService;
    }

    /**
     * Looks up the {@link ResourceMetaData} to get a readable annotator name.
     *
     * @param aed
     * @return
     */
    public String getAnalysisEngineName(AnalysisEngineDescription aed) {
        Class<?> trainerClass = loadAnalysisEngineImplementation(aed);

        ResourceMetaData metaData = trainerClass.getAnnotation(ResourceMetaData.class);
        if (metaData != null) {
            return metaData.name();
        }

        return aed.getAnnotatorImplementationName();
    }

    @SneakyThrows(ClassNotFoundException.class)
    public Class<?> loadAnalysisEngineImplementation(AnalysisEngineDescription aed) {
        return Class.forName(aed.getAnnotatorImplementationName(), true, classLoaderService.getDependencyClassLoader());
    }

    @SuppressWarnings("squid:S1872")
    public Set<String> getMimetypes(AnalysisEngineDescription trainerDescription) {
        Optional<TypeCapability> capability = findAllTrainerAnnotatorPairs().stream()
                .filter(pair -> trainerDescription.getAnnotatorImplementationName().equals(pair.getFirst().getImplementationClass().getName()))
                .map(pair -> pair.getSecond().getImplementationClass().getAnnotation(TypeCapability.class))
                .findFirst();

        Set<String> inputsAndOutput = new HashSet<>();

        if (capability.isPresent()) {
            inputsAndOutput.addAll(asList(capability.get().inputs()));
            inputsAndOutput.addAll(asList(capability.get().outputs()));
        }

        return inputsAndOutput;
    }

    /**
     * Finds all {@link AnalysisEngineDescription engine descriptions} from the classpath which correspond to a
     * {@link org.apache.uima.fit.component.JCasAnnotator_ImplBase annotator}.
     *
     * @return
     */
    public List<AnalysisEngineDescription> findAnnotatorDescriptions(List<AnalysisEngineDescription> trainerDescriptions) {
        Multimap<Class, ModelParameter> modelParameterMappings = HashMultimap.create();
        findAllTrainerAnnotatorPairs().stream()
                // filter by given trainer descriptions
                .filter(pair -> trainerDescriptions.stream()
                        .map(AnalysisEngineDescription::getAnnotatorImplementationName)
                        .anyMatch(implementationName -> implementationName.equals(pair.getFirst().getImplementationClass().getName())))
                // Search for ResourceParameter/ConfigurationParameter field to find the model location
                .forEach(pair ->
                        stream(pair.getSecond().getImplementationClass().getDeclaredFields())
                                .filter(field -> field.getAnnotation(ResourceParameter.class) != null)
                                .filter(field -> field.getAnnotation(ConfigurationParameter.class) != null)
                                .map(ModelParameter::new)
                                .forEach(modelParameter ->
                                        modelParameterMappings.put(pair.getSecond().getImplementationClass(), modelParameter)
                                ));

        return modelParameterMappings
                .asMap()
                .entrySet()
                .stream()
                .map(entry -> multiplyByVariant(
                        entry,
                        (modelParameters, variant) -> modelParameters.stream()
                                .map(p -> p.cloneWithVariant(variant))
                                .collect(toList()))
                )
                .flatMap(List::stream)
                .map(classModelParameterMapping -> {
                    Class annotatorClass = classModelParameterMapping.getKey();

                    try {
                        List<Object> params = new ArrayList<>();

                        ModelParameter firstModelParameter = null;
                        for (ModelParameter modelParameter : classModelParameterMapping.getValue()) {
                            params.add(modelParameter.name);
                            params.add(createAnnotatorModelLocation(language,
                                    annotatorClass.getPackage().getName(),
                                    modelParameter));

                            if (firstModelParameter == null) {
                                firstModelParameter = modelParameter;
                            }
                        }

                        if (firstModelParameter != null && firstModelParameter.variant != null) {
                            params.add(ComponentParameters.PARAM_VARIANT);
                            params.add(firstModelParameter.variant);
                        }

                        return createEngineDescription(annotatorClass, getConfigurationsFromPropertiesForClass(annotatorClass, params));
                    } catch (ResourceInitializationException e) {
                        logger.info("Could not create annotator description for " + annotatorClass + ".", e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(toList());
    }

    private <T> List<Map.Entry<Class, T>> multiplyByVariant(Map.Entry<Class, T> modelParameter, ModelConverterFunction<T> parameterConverter) {
        // TODO check if class has variant parameter

        Optional<TypeCapability> capability = findAllTrainerAnnotatorPairs().stream()
                .filter(pair -> modelParameter.getKey().equals(pair.getFirst().getImplementationClass())
                        || modelParameter.getKey().equals(pair.getSecond().getImplementationClass()))
                .map(pair -> pair.getSecond().getImplementationClass().getAnnotation(TypeCapability.class))
                .findFirst();

        List<String> namedEntityVariants = getNamedEntityVariants();

        if (capability.isPresent() && !namedEntityVariants.isEmpty() && outputsNamedEntity(capability.get())) {
            List<Map.Entry<Class, T>> multipliedModelParameters = new ArrayList<>();

            for (String neVariant : namedEntityVariants) {
                Map.Entry<Class, T> cloneWithVariant = new SimpleEntry<>(
                        modelParameter.getKey(),
                        parameterConverter.apply(modelParameter.getValue(), neVariant)
                );
                multipliedModelParameters.add(cloneWithVariant);
            }
            return multipliedModelParameters;
        }

        return asList(modelParameter);
    }

    private List<String> getNamedEntityVariants() {
        String property = mavenProject.getProperties().getProperty(NAMED_ENTITY_PROPERTY_KEY, "");
        return Lists.newArrayList(namedEntitySplitter.split(property));
    }

    @SuppressWarnings("squid:S1872")
    private boolean outputsNamedEntity(TypeCapability typeCapability) {
        for (String output : typeCapability.outputs()) {
            if (output.equals(NamedEntity.class.getName())) {
                return true;
            }
        }

        return false;
    }

    /**
     * Creates the model classpath name according to the DKPro Core naming schema.
     * <p>
     * {@code ${type}-${language}-${variant}.model}
     * </p>
     *
     * @return
     */
    private String createAnnotatorModelLocation(String language, String packageName, ModelParameter modelParameter) {
        return String.format("classpath:/%s/%s/%s/%s-%s-%s.model",
                mavenProject.getGroupId(),
                mavenProject.getArtifactId(),
                packageName,
                modelParameter.type,
                language,
                StringUtils.defaultIfBlank(modelParameter.variant, "default")
        );
    }

    /**
     * Searches for {@link ConfigurationParameter} matching {@link Properties} in the {@code mavenProject}.
     *
     * @param annotatorClass
     * @return array of params.
     */
    private Object[] getConfigurationsFromPropertiesForClass(Class annotatorClass, List<Object> additionalParams) {
        Properties properties = mavenProject.getProperties();
        if (properties != null) {
            Pattern classNameAndFieldPattern = Pattern.compile("\\b(?<className>" + annotatorClass.getName() + ")" + "\\.?(?<parameterName>[0-z]*)?");
            Arrays.stream(annotatorClass.getDeclaredFields())
                    .map(field -> field.getDeclaredAnnotation(ConfigurationParameter.class))
                    .filter(Objects::nonNull)
                    .forEach(configurationParameter -> properties.entrySet().stream()
                            .map(entry -> new SimpleEntry<>(classNameAndFieldPattern.matcher((String) entry.getKey()),
                                    entry.getValue()))
                            .filter(entry -> entry.getKey().matches() &&
                                    Objects.equals(entry.getKey().group("parameterName"), configurationParameter.name()))
                            .forEach(entry -> {
                                additionalParams.add(configurationParameter.name());
                                additionalParams.add(entry.getValue());
                            })
                    );
        }

        return additionalParams.toArray();
    }

    /**
     * Finds all {@link AnalysisEngineDescription engine descriptions} from the classpath which correspond to a
     * trainers.
     *
     * @return
     */
    public List<AnalysisEngineDescription> findTrainerDescriptions() {
        Map<Class, ModelParameter> modelParameterMappings = new HashMap<>();
        findAllTrainerAnnotatorPairs().forEach(pair ->
                stream(pair.getSecond().getImplementationClass().getDeclaredFields())
                        .filter(field -> field.getAnnotation(ResourceParameter.class) != null)
                        .filter(field -> field.getAnnotation(ConfigurationParameter.class) != null)
                        .map(ModelParameter::new)
                        .filter(modelParameter -> {
                            Class<?> implementationClass = pair.getFirst().getImplementationClass();
                            MimeTypeCapability typeCapability = implementationClass.getAnnotation(MimeTypeCapability.class);
                            return modelParameter.matches(typeCapability);
                        })
                        .forEach(modelParameter ->
                                modelParameterMappings.put(pair.getFirst().getImplementationClass(), modelParameter)
                        ));


        return modelParameterMappings.entrySet().stream()
                .map(entry -> multiplyByVariant(
                        entry, (modelParameter, variant) -> modelParameter.cloneWithVariant(variant)
                ))
                .flatMap(List::stream)
                .map(classModelParameterMapping -> {
                    try {
                        Class trainerClass = classModelParameterMapping.getKey();
                        ModelParameter modelParameter = classModelParameterMapping.getValue();

                        List<Object> params = new ArrayList<>();

                        params.add(ComponentParameters.PARAM_TARGET_LOCATION);
                        params.add(directoryService.getModelTargetLocation(trainerClass.getPackage(), modelParameter.type, modelParameter.variant));
                        params.add(ComponentParameters.PARAM_LANGUAGE);
                        params.add(language);

                        if (modelParameter.variant != null) {
                            params.add(ComponentParameters.PARAM_ACCEPTED_TAGS_REGEX);
                            params.add(modelParameter.variant);
                        }

                        return createEngineDescription(trainerClass, getConfigurationsFromPropertiesForClass(trainerClass, params));
                    } catch (ResourceInitializationException e) {
                        logger.info("Could not create trainer description for " + classModelParameterMapping + ".", e);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .collect(toList());
    }

    private Set<ClassMimeTypeMappingPair> findAllTrainerAnnotatorPairs() {
        List<Class> classes = lookUpClasses(
                classLoaderService.getDependencyClassLoader(),
                c -> c.getName().startsWith("de.tudarmstadt."),
                JCasConsumer_ImplBase.class,
                JCasAnnotator_ImplBase.class
        );

        Stream<ClassMimeTypeMapping> trainerClasses = convertToFlatStreamOfMimeTypes(classes, JCasConsumer_ImplBase.class);
        Stream<ClassMimeTypeMapping> annotatorClasses = convertToFlatStreamOfMimeTypes(classes, JCasAnnotator_ImplBase.class,
                c -> stream(c.getDeclaredFields()).anyMatch(f -> f.getAnnotation(ResourceParameter.class) != null),
                c -> {
                    List<ClassMimeTypeMapping> mappings = new ArrayList<>();

                    stream(c.getDeclaredFields())
                            .map(f -> f.getAnnotation(ResourceParameter.class))
                            .filter(Objects::nonNull)
                            .forEach(rp ->
                                    stream(rp.value()).forEach(
                                            mimetype -> mappings.add(new ClassMimeTypeMapping(c, mimetype))
                                    )
                            );

                    return mappings.stream();
                }
        );

        return findClassMimeTypePairs(trainerClasses, annotatorClasses);
    }

    @EqualsAndHashCode
    private static class ModelParameter {

        private final String name;

        private final String type;

        private String variant;

        private ModelParameter(ModelParameter other) {
            this.name = other.name;
            this.type = other.type;
            this.variant = other.variant;
        }

        public ModelParameter(Field field) {
            this.name = field.getAnnotation(ConfigurationParameter.class).name();
            this.type = extractType(field.getAnnotation(ResourceParameter.class));
        }

        private String extractType(ResourceParameter resourceParameter) {
            StringBuilder str = new StringBuilder();

            for (String mimeType : resourceParameter.value()) {
                str.append(mimeType.split("/")[1]);
            }

            return str.toString();
        }

        private boolean matches(MimeTypeCapability mimeTypeCapability) {
            return stream(mimeTypeCapability.value())
                    .anyMatch(s -> s.endsWith(type));
        }

        private ModelParameter cloneWithVariant(String variant) {
            ModelParameter clone = new ModelParameter(this);
            clone.variant = variant;
            return clone;
        }
    }

    @FunctionalInterface
    private interface ModelConverterFunction<T> {

        T apply(T modelParameter, String variant);

    }
}
