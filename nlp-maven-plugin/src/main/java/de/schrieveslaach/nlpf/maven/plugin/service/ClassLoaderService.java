package de.schrieveslaach.nlpf.maven.plugin.service;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.maven.plugin.logging.Log;
import org.apache.maven.plugin.logging.SystemStreamLog;
import org.apache.maven.project.MavenProject;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;

@Singleton
public class ClassLoaderService {

    private final Log logger = new SystemStreamLog();

    private final MavenProject mavenProject;

    @Getter(lazy = true)
    private final ClassLoader dependencyClassLoader = initDependencyClassLoader();

    @Inject
    public ClassLoaderService(MavenProject mavenProject) {
        this.mavenProject = mavenProject;
    }

    /**
     * Returns a {@link ClassLoader} which can resolve classes from the declared dependencies in
     * the {@link MavenProject}.
     *
     * @return
     */
    @SneakyThrows(MalformedURLException.class)
    private ClassLoader initDependencyClassLoader() {
        List<URL> paths = new ArrayList<>();

        mavenProject.getArtifacts().stream()
                .filter(artifact -> "compile".equals(artifact.getScope()) || "provided".equals(artifact.getScope()))
                .forEach(artifact -> {
                    try {
                        paths.add(artifact.getFile().toURI().toURL());
                    } catch (MalformedURLException e) {
                        logger.error("Could not convert artifact file to URL.", e);
                    }
                });

        paths.add(new File(mavenProject.getBuild().getOutputDirectory()).toURI().toURL());

        return new URLClassLoader(paths.toArray(new URL[paths.size()]), getClass().getClassLoader());
    }

}
