package de.schrieveslaach.nlpf.maven.plugin.service;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.io.combination.CombinationReader;
import lombok.Setter;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static de.schrieveslaach.nlpf.plumbing.CollectionReaderFactory.findReaderDescriptionByFileExtension;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;

@Singleton
public class CollectionReaderDescriptionService {

    private final DirectoryService directoryService;

    private final ClassLoaderService classLoaderService;

    @Setter
    private String language;

    @Inject
    public CollectionReaderDescriptionService(DirectoryService directoryService, ClassLoaderService classLoaderService) {
        this.directoryService = directoryService;
        this.classLoaderService = classLoaderService;
    }

    /**
     * Reads the reader descriptions from the {@link DirectoryService#getCorpusDirectory() main corpus directory}.
     *
     * @return
     * @throws MojoExecutionException
     */
    public CollectionReaderDescription createReaderDescriptions() throws MojoExecutionException {
        File corpusDirectory = directoryService.getCorpusDirectory();
        if (!corpusDirectory.isDirectory()) {
            throw new MojoExecutionException("The corpus directory is missing. Create the directory src/main/corpus");
        }

        return createCollectionReaderDescription(corpusDirectory, CombinationReader.class);
    }

    private CollectionReaderDescription createCollectionReaderDescription(File corpusDirectory, Class<? extends CombinationReader> combinationReaderClass, Object... configurationData) throws MojoExecutionException {
        List<String> readerFiles = new ArrayList<>();

        try {
            for (File f : FileUtils.listFiles(corpusDirectory, null, true)) {
                String extension = FilenameUtils.getExtension(f.getName());
                List<CollectionReaderDescription> descriptions = findReaderDescriptionByFileExtension(
                        classLoaderService.getDependencyClassLoader(),
                        "." + extension,
                        ComponentParameters.PARAM_SOURCE_LOCATION, f.toString(),
                        ComponentParameters.PARAM_LANGUAGE, language);

                if (!descriptions.isEmpty()) {
                    CollectionReaderDescription readerDescription = descriptions.get(0);

                    File temp = File.createTempFile(extension, ".xml");
                    temp.deleteOnExit();

                    try (FileOutputStream fos = new FileOutputStream(temp)) {
                        readerDescription.toXML(fos);
                    }
                    readerFiles.add(temp.toString());
                }
            }

            List<Object> parameters = Lists.newArrayList(configurationData);
            parameters.add(CombinationReader.PARAM_READERS);
            parameters.add(readerFiles.toArray(new String[readerFiles.size()]));

            return createReaderDescription(combinationReaderClass, parameters.toArray());
        } catch (ResourceInitializationException | IOException | SAXException e) {
            throw new MojoExecutionException("Could not create reader description for files of corpus", e);
        }
    }

    /**
     * Reads the reader descriptions from the {@link DirectoryService#getCorpusDirectory() main corpus directory}.
     *
     * @return
     * @throws MojoExecutionException
     */
    public CollectionReaderDescription createTestReaderDescriptions() throws MojoExecutionException {
        return createTestReaderDescriptions(CombinationReader.class);

    }

    public CollectionReaderDescription createTestReaderDescriptions(Class<? extends CombinationReader> combinationReaderClass, Object... configurationData) throws MojoExecutionException {
        File corpusDirectory = directoryService.getTestCorpusDirectory();
        if (!corpusDirectory.isDirectory()) {
            throw new MojoExecutionException("The corpus test directory is missing. Create the directory src/test/corpus");
        }

        return createCollectionReaderDescription(corpusDirectory, combinationReaderClass, configurationData);
    }
}
