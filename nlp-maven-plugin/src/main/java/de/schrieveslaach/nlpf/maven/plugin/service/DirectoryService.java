package de.schrieveslaach.nlpf.maven.plugin.service;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.Setter;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.xml.sax.SAXException;

import javax.inject.Inject;
import javax.inject.Singleton;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;

import static de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionUtil.hash;

@Singleton
public class DirectoryService {

    private final MavenProject mavenProject;

    @Setter
    private String language;

    @Inject
    public DirectoryService(MavenProject mavenProject) {
        this.mavenProject = mavenProject;
    }

    public File getCorpusDirectory() {
        // TODO: read from pom.xml
        return new File(mavenProject.getBasedir(), "src/main/corpus");
    }

    public File getTestCorpusDirectory() {
        // TODO: read from pom.xml
        return new File(mavenProject.getBasedir(), "src/test/corpus");
    }

    /**
     * Returns the directory in the {@link MavenProject#getBuild() build directory} which contains the derived NLP
     * models.
     *
     * @return
     */
    public File getModelsBaseDirectory() {
        File modelsDirectory = new File(mavenProject.getBuild().getDirectory(), "models");
        modelsDirectory.mkdirs();
        return modelsDirectory;
    }

    public File getModelTargetLocation(Package pkg, String type, String variant) {
        File modelFile = new File(
                new File(new File(new File(getModelsBaseDirectory(), mavenProject.getGroupId()), mavenProject.getArtifactId()), pkg.getName()),
                String.format("%s-%s-%s.model", type, language, StringUtils.defaultIfBlank(variant, "default"))
        );

        modelFile.getParentFile().mkdirs();

        return modelFile;
    }

    /**
     * Returns the directory in the {@link MavenProject#getBuild() build directory} which contains the analysis engine
     * descriptor files.
     *
     * @return
     */
    public File getDescriptorsDirectory() {
        File descriptorsDirectory = new File(mavenProject.getBuild().getDirectory(), "descriptors");
        descriptorsDirectory.mkdirs();
        return descriptorsDirectory;
    }

    /**
     * Returns the directory in the {@link MavenProject#getBuild() build directory} which contains the
     * measured f-scores for the NLP pipelines, determined during
     * {@code mvn de.schrieveslaach.nlpf:nlp-maven-plugin:pipelines-test}.
     *
     * @return
     */
    public File getPipelinesTestDataDirectory() {
        File descriptorsDirectory = new File(mavenProject.getBuild().getDirectory(), "nlp-pipelines-test-data");
        descriptorsDirectory.mkdirs();
        return descriptorsDirectory;
    }

    /**
     * Returns the directory in the {@link MavenProject#getBuild() build directory} which contains the
     * measured f-scores for the test of isolated NLP tool, determined during
     * {@code mvn de.schrieveslaach.nlpf:nlp-maven-plugin:tools-test}.
     *
     * @return
     */
    public File getIsolatedToolsTestDataDirectory() {
        File descriptorsDirectory = new File(mavenProject.getBuild().getDirectory(), "isolated-tools-test-data");
        descriptorsDirectory.mkdirs();
        return descriptorsDirectory;
    }

    /**
     * Returns the path to the JAR file which contains all resources (NLP models and pipeline descriptors)
     *
     * @return
     */
    public File getJarFile() {
        String archiveName = mavenProject.getBuild().getFinalName() + ".jar";
        return new File(mavenProject.getBuild().getDirectory(), archiveName);
    }

    /**
     * Stores the given {@link AnalysisEngineDescription} in
     * {@link #getDescriptorsDirectory() descriptors directory}.
     *
     * @param aed
     */
    public void store(AnalysisEngineDescription aed) throws MojoExecutionException {
        File descriptorsDirectory = getDescriptorsDirectory();
        descriptorsDirectory.mkdirs();

        File nerDescriptor = new File(descriptorsDirectory, hash(aed) + ".xml");
        try (FileOutputStream fos = new FileOutputStream(nerDescriptor)) {
            aed.toXML(fos);
        } catch (IOException | SAXException e) {
            throw new MojoExecutionException("Could not store analysis engine descriptors", e);
        }
    }

    public File getPipelineTxtFile() {
        return new File(mavenProject.getBuild().getDirectory(), "pipeline.txt");
    }

}
