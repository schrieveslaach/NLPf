package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.annotators.EmptyAnalysisComponent;
import de.schrieveslaach.nlpf.maven.plugin.reader.ClearTypesCombinationReader;
import de.schrieveslaach.nlpf.maven.plugin.service.AnalysisEngineDescriptionService;
import de.schrieveslaach.nlpf.maven.plugin.service.ClassLoaderService;
import de.schrieveslaach.nlpf.maven.plugin.service.CollectionReaderDescriptionService;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import lombok.SneakyThrows;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_component.AnalysisComponent;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.jcas.JCas;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.mockito.Mock;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.List;

import static de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionUtil.hash;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createSatyaNadellaExample;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createTorwaldsExampleWithPosTags;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public abstract class AbstractTestMojoTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Mock
    protected DirectoryService directoryService;

    @Mock
    private AnalysisEngineDescriptionService analysisEngineDescriptionService;

    @Mock
    private CollectionReaderDescriptionService collectionReaderDescriptionService;

    @Mock
    private ClassLoaderService classLoaderService;

    @Before
    public void mockClassLoaderService() {
        when(classLoaderService.getDependencyClassLoader()).thenReturn(getClass().getClassLoader());
    }

    @Before
    public void mockDirectoryService() {
        when(directoryService.getPipelinesTestDataDirectory()).then(invocation -> {
            File directory = new File(new File(folder.getRoot(), "target"), "nlp-pipelines-test-data");
            directory.mkdirs();
            return directory;
        });


        when(directoryService.getIsolatedToolsTestDataDirectory()).then(invocation -> {
            File directory = new File(new File(folder.getRoot(), "target"), "isolated-tools-test-data");
            directory.mkdirs();
            return directory;
        });

        when(directoryService.getModelsBaseDirectory()).then(invocation -> {
            File directory = new File(new File(folder.getRoot(), "target"), "models");
            directory.mkdirs();
            return directory;
        });
    }

    @Before
    public void mockAnalysisEngineDescriptionService() {
        when(analysisEngineDescriptionService.loadAnalysisEngineImplementation(any(AnalysisEngineDescription.class))).then(invocation -> {
            AnalysisEngineDescription engineDescription = invocation.getArgument(0);
            return Class.forName(engineDescription.getImplementationName());
        });
        when(analysisEngineDescriptionService.getAnalysisEngineName(any(AnalysisEngineDescription.class))).then(invocation -> {
            AnalysisEngineDescription engineDescription = invocation.getArgument(0);
            return Class.forName(engineDescription.getImplementationName()).getSimpleName();
        });
    }

    @Test
    public void shouldFilterAnalysisEngineDescriptionWithoutTypeCapability() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(createEngineDescription(
                EmptyAnalysisComponent.class
        ));

        provideTestJCas();

        List<AnalysisEngineDescription> analysisEngineDescriptions = getMojoUnderTest().loadEngineDescriptions();

        assertThat(analysisEngineDescriptions, is(empty()));
    }

    @SneakyThrows({IOException.class, UIMAException.class})
    public void mockEngineDescriptionsInDescriptorsDirectory(Class<? extends AnalysisComponent>... engineDescriptions) {
        File descriptorsDirectory = folder.newFolder("target", "descriptors");
        when(directoryService.getDescriptorsDirectory()).thenReturn(descriptorsDirectory);

        for (Class<? extends AnalysisComponent> cls : engineDescriptions) {
            AnalysisEngineDescription aed = createEngineDescription(cls);

            saveEngineDescription(descriptorsDirectory, aed);
        }
    }


    @SneakyThrows(IOException.class)
    public void mockEngineDescriptionsInDescriptorsDirectory(AnalysisEngineDescription... descriptions) {
        File descriptorsDirectory = folder.newFolder("target", "descriptors");
        when(directoryService.getDescriptorsDirectory()).thenReturn(descriptorsDirectory);

        for (AnalysisEngineDescription aed : descriptions) {
            saveEngineDescription(descriptorsDirectory, aed);
        }
    }

    @SneakyThrows({IOException.class, SAXException.class})
    public void saveEngineDescription(File descriptorsDirectory, AnalysisEngineDescription aed) {

        File nerDescriptor = new File(descriptorsDirectory, hash(aed) + ".xml");
        try (FileOutputStream fos = new FileOutputStream(nerDescriptor)) {
            aed.toXML(fos);
        }
    }

    @SneakyThrows({IOException.class, UIMAException.class, MojoExecutionException.class, SAXException.class})
    public void provideTestJCas() {
        JCas satyaNadellaExample = createSatyaNadellaExample();
        JCas openNlpExample = createOpenNlpExample();
        JCas torwaldsExample = createTorwaldsExampleWithPosTags();

        File testDirectory = folder.newFolder("src", "test", "corpus");

        runPipeline(satyaNadellaExample,
                createEngineDescription(
                        XmiWriter.class,
                        XmiWriter.PARAM_TARGET_LOCATION, testDirectory,
                        XmiWriter.PARAM_OVERWRITE, true
                )
        );
        runPipeline(openNlpExample,
                createEngineDescription(
                        XmiWriter.class,
                        XmiWriter.PARAM_TARGET_LOCATION, testDirectory,
                        XmiWriter.PARAM_OVERWRITE, true
                )
        );
        runPipeline(torwaldsExample,
                createEngineDescription(
                        XmiWriter.class,
                        XmiWriter.PARAM_TARGET_LOCATION, testDirectory,
                        XmiWriter.PARAM_OVERWRITE, true
                )
        );

        CollectionReaderDescription readerDescription = createReaderDescription(
                XmiReader.class,
                XmiReader.PARAM_PATTERNS, new File(testDirectory, "*.xmi")
        );

        when(collectionReaderDescriptionService.createTestReaderDescriptions()).thenReturn(readerDescription);

        File configurationFile = folder.newFile();
        try (FileOutputStream fos = new FileOutputStream(configurationFile)) {
            readerDescription.toXML(fos);
        }

        when(collectionReaderDescriptionService.createTestReaderDescriptions(any(), any())).then(invocation -> {
            String[] params = invocation.getArgument(2);

            return createReaderDescription(
                    ClearTypesCombinationReader.class,
                    ClearTypesCombinationReader.PARAM_READERS, new String[]{
                            configurationFile.toString()
                    },
                    ClearTypesCombinationReader.PARAM_TYPES_TO_CLEAR, params
            );
        });
    }

    protected abstract AbstractTestMojo getMojoUnderTest();

}
