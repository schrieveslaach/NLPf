package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.apache.uima.analysis_component.Annotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.mockito.ArgumentMatcher;

public class ArgumentMatchers {

    public static ArgumentMatcher<AnalysisEngineDescription> hasImplementationClass(Class<? extends Annotator_ImplBase> cls) {
        return argument -> {
            try {
                return Class.forName(argument.getImplementationName()).equals(cls);
            } catch (ClassNotFoundException e) {
                e.printStackTrace();
                return false;
            }
        };
    }
}
