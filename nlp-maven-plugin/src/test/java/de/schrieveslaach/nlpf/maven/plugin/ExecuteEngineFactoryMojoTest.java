package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.factories.CustomEngineFactory;
import de.schrieveslaach.nlpf.maven.plugin.service.ClassLoaderService;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import de.schrieveslaach.nlpf.testing.annotators.MyPosTagger;
import de.schrieveslaach.nlpf.testing.annotators.MySegmenter;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.artifact.DependencyResolutionRequiredException;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.net.URL;
import java.security.CodeSource;
import java.security.ProtectionDomain;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.maven.plugin.ArgumentMatchers.hasImplementationClass;
import static java.util.Arrays.stream;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ExecuteEngineFactoryMojoTest {

    @InjectMocks
    private ExecuteEngineFactoryMojo mojo;

    @Mock
    private MavenProject project;

    @Mock
    private DirectoryService directoryService;

    @Mock
    private ClassLoaderService classLoaderService;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Before
    public void setUpClassLoaderService() {
        when(classLoaderService.getDependencyClassLoader()).thenReturn(getClass().getClassLoader());
    }

    @Test
    public void shouldExecuteEngineFactories_FromMavenTestClassPathElements() throws Exception {
        mockTestClasspathElements(CustomEngineFactory.class);
        mojo.execute();

        verify(directoryService).store(argThat(hasImplementationClass(MySegmenter.class)));
        verify(directoryService).store(argThat(hasImplementationClass(MyPosTagger.class)));
    }

    @Test
    public void shouldExecuteEngineFactories_CouldNotLoadClassPathElements() throws Exception {
        expectedException.expect(MojoExecutionException.class);
        expectedException.expectMessage("Could not resolve test classpath elements");

        Artifact artifact = mock(Artifact.class);
        doThrow(new DependencyResolutionRequiredException(artifact))
                .when(project).getTestClasspathElements();

        mojo.execute();
    }

    private void mockTestClasspathElements(Class<?>... classes) throws Exception {
        when(project.getTestClasspathElements())
                .thenReturn(stream(classes)
                        .map(Class::getProtectionDomain)
                        .map(ProtectionDomain::getCodeSource)
                        .map(CodeSource::getLocation)
                        .map(URL::toString)
                        .distinct()
                        .collect(Collectors.toList()));
    }

}
