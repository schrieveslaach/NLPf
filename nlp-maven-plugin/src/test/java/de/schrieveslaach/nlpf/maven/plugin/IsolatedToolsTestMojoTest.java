package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.testing.annotators.MyNamedEntityRecognizer;
import de.schrieveslaach.nlpf.testing.annotators.MyPosTagger;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import org.apache.commons.io.FileUtils;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.nio.charset.Charset;

import static de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionUtil.hash;
import static java.util.Arrays.asList;
import static net.javacrumbs.jsonunit.JsonMatchers.jsonEquals;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.nullValue;
import static org.hamcrest.io.FileMatchers.anExistingFile;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class IsolatedToolsTestMojoTest extends AbstractTestMojoTest {

    private static final AnalysisEngineDescription OPEN_NLP_PERSON_DESCRIPTION;
    private static final AnalysisEngineDescription MY_PERSON_DESCRIPTION;
    private static final AnalysisEngineDescription OPEN_NLP_ORGANIZATION_DESCRIPTION;
    private static final AnalysisEngineDescription MY_ORGANIZATION_DESCRIPTION;

    static {
        try {
            OPEN_NLP_PERSON_DESCRIPTION = createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "person");
            MY_PERSON_DESCRIPTION = createEngineDescription(MyNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "person");
            OPEN_NLP_ORGANIZATION_DESCRIPTION = createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "organization");
            MY_ORGANIZATION_DESCRIPTION = createEngineDescription(MyNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "organization");
        } catch (ResourceInitializationException e) {
            throw new AssertionError(e);
        }
    }

    @InjectMocks
    private IsolatedToolsTestMojo mojo;

    public static String jsonTestResult() {
        return new JSONObject()
                .put("descriptors", asList(
                        MyPosTagger.DEFAULT_HASH_CODE + ".xml"
                ))
                .put("measures", asList(
                        new JSONObject()
                                .put("analysisEngineName", "MyPosTagger")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.352)
                                        .put("precision", 0.352)
                                        .put("recall", 0.352))
                ))
                .toString();
    }


    public static String jsonOpenNlpPersonTestResult() {
        return new JSONObject()
                .put("descriptors", asList(
                        hash(OPEN_NLP_PERSON_DESCRIPTION) + ".xml"
                ))
                .put("measures", asList(
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpNamedEntityRecognizer")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity")
                                .put("variant", "person")
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.5)
                                        .put("precision", 0.75)
                                        .put("recall", 0.375))
                ))
                .toString();
    }

    @Before
    public void mockDirectoryService() {
        when(directoryService.getIsolatedToolsTestDataDirectory()).then(invocation -> {
            File directory = new File(new File(folder.getRoot(), "target"), "isolated-tools-test-data");
            directory.mkdirs();
            return directory;
        });

        when(directoryService.getModelsBaseDirectory()).then(invocation -> {
            File directory = new File(new File(folder.getRoot(), "target"), "models");
            directory.mkdirs();
            return directory;
        });
    }

    @Test
    public void shouldExecutePOSTaggerIsolated_MyPosTagger() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(MyPosTagger.class);
        provideTestJCas();

        mojo.execute();

        File testResult = assertThatIsolatedTestDataFileExists(MyPosTagger.DEFAULT_HASH_CODE);

        String json = FileUtils.readFileToString(testResult, Charset.defaultCharset());
        assertThat(json, jsonEquals(jsonTestResult()).withTolerance(0.001));
    }

    @Test
    public void shouldExecutePOSTaggerIsolated_OpenNlpNER() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(
                OPEN_NLP_PERSON_DESCRIPTION,
                MY_PERSON_DESCRIPTION,
                OPEN_NLP_ORGANIZATION_DESCRIPTION,
                MY_ORGANIZATION_DESCRIPTION
        );
        provideTestJCas();

        mojo.execute();

        assertThatIsolatedTestDataFileExists(hash(OPEN_NLP_ORGANIZATION_DESCRIPTION));
        assertThatIsolatedTestDataFileExists(hash(MY_ORGANIZATION_DESCRIPTION));
        assertThatIsolatedTestDataFileExists(hash(MY_PERSON_DESCRIPTION));
        File openNerPersonTestResult = assertThatIsolatedTestDataFileExists(hash(OPEN_NLP_PERSON_DESCRIPTION));

        String json = FileUtils.readFileToString(openNerPersonTestResult, Charset.defaultCharset());
        assertThat(json, jsonEquals(jsonOpenNlpPersonTestResult()).withTolerance(0.001));
    }

    @Test
    public void shouldContinueTestingIfSingleEngineDescriptionCannotBeTested() throws Exception {
        AnalysisEngineDescription failingEngineDescription = createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "abcdef-non-existing");
        mockEngineDescriptionsInDescriptorsDirectory(failingEngineDescription);
        provideTestJCas();

        mojo.execute();

        File directory = new File(folder.getRoot(), "target/isolated-tools-test-data/");
        assertThat(directory.listFiles(), is(nullValue()));
    }

    @Override
    protected AbstractTestMojo getMojoUnderTest() {
        return mojo;
    }

    private File assertThatIsolatedTestDataFileExists(String hash) {
        File testResult = new File(folder.getRoot(), "target/isolated-tools-test-data/" + hash + ".json");
        assertThat(testResult, is(anExistingFile()));
        return testResult;
    }
}
