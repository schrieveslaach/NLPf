package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.maven.artifact.Artifact;
import org.apache.maven.execution.MavenSession;
import org.apache.maven.model.Build;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.codehaus.plexus.archiver.ArchiverException;
import org.codehaus.plexus.archiver.jar.JarArchiver;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.io.IOException;
import java.nio.charset.Charset;
import java.util.Properties;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.matchesPattern;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PackageMojoTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private PackageMojo mojo;

    @Mock
    private DirectoryService directoryService;

    @Mock
    private MavenProject project;

    @Mock
    private MavenSession session;

    @Mock
    private JarArchiver jarArchiver;

    @Before
    public void mockDirectoryService() throws Exception {
        File target = folder.newFolder("target");
        when(directoryService.getJarFile()).thenReturn(new File(target, "domain-specific-corpus-1.0.jar"));
        when(directoryService.getDescriptorsDirectory()).thenReturn(new File(target, "descriptors"));
        when(directoryService.getPipelineTxtFile()).thenReturn(new File(target, "pipeline.txt"));
        when(directoryService.getModelsBaseDirectory()).thenReturn(new File(target, "models"));
        when(directoryService.getPipelinesTestDataDirectory()).thenReturn(new File(target, "nlp-pipelines-test-data"));
    }

    @Before
    @SneakyThrows(IOException.class)
    public void initMavenProject() {
        Build build = mock(Build.class);
        when(build.getDirectory()).thenReturn(new File(folder.getRoot(), "target").toString());
        when(project.getBuild()).thenReturn(build);

        when(project.getFile()).thenReturn(folder.newFile("pom.xml"));

        Artifact artifact = mock(Artifact.class);
        when(project.getArtifact()).thenReturn(artifact);

        when(project.getGroupId()).thenReturn("de.company");
        when(project.getArtifactId()).thenReturn("domain-specific-corpus");
        when(project.getVersion()).thenReturn("1.0");

        when(project.clone()).thenReturn(project);
    }

    @Before
    public void initMavenSession() {
        when(session.getSystemProperties()).thenReturn(new Properties());
    }

    @Test
    public void shouldArchiveModelsDirectoryWithGroupIdPrefix() throws Exception {
        initTestDataAndDescriptorFiles();

        mojo.execute();

        verify(jarArchiver).addDirectory(
                new File(folder.getRoot(), "target/models")
        );
    }

    @Test
    public void shouldArchiveAnalysisEngineDescriptors() throws Exception {
        initTestDataAndDescriptorFiles();

        mojo.execute();

        verify(jarArchiver).addFile(
                new File(folder.getRoot(), "target/descriptors/opennlp-1.xml"),
                "de.company/domain-specific-corpus/opennlp-1.xml"
        );
        verify(jarArchiver).addFile(
                new File(folder.getRoot(), "target/descriptors/opennlp-2.xml"),
                "de.company/domain-specific-corpus/opennlp-2.xml"
        );
    }

    private void initTestDataAndDescriptorFiles() throws IOException {
        folder.newFolder("target", "descriptors");
        folder.newFile("target/descriptors/opennlp-1.xml");
        folder.newFile("target/descriptors/opennlp-2.xml");
        folder.newFile("target/descriptors/opennlp-3.xml");

        storeTestResult();
    }

    @Test
    public void shouldArchivePipelinesTextFile() throws Exception {
        initTestDataAndDescriptorFiles();

        mojo.execute();

        verify(jarArchiver).addFile(
                new File(folder.getRoot(), "target/pipeline.txt"),
                "META-INF/de.schrieveslaach.effective.nlp.application.development/pipeline.txt"
        );
    }

    @Test
    public void shouldNotCreateManifestEntry_TestDataIsMissing() throws Exception {
        expectedException.expectMessage("Could not read test data of best-performing NLP pipeline");

        mojo.execute();
    }

    @Test
    public void shouldCreateManifestEntry() throws Exception {
        initTestDataAndDescriptorFiles();

        mojo.execute();

        File pipelineTxt = new File(new File(folder.getRoot(), "target"), "pipeline.txt");
        String content = FileUtils.readFileToString(pipelineTxt, Charset.defaultCharset());

        assertThat(asList(content.split("\n")), hasSize(2));
        assertThat(content, matchesPattern("^(?is).*classpath\\*:/de\\.company/domain-specific-corpus/opennlp-2.xml.*$"));
        assertThat(content, matchesPattern("^(?is).*classpath\\*:/de\\.company/domain-specific-corpus/opennlp-1.xml.*$"));
    }

    @Test(expected = MojoExecutionException.class)
    public void shouldFailWithMojoExecutionException_CouldNotAddDirectory() throws Exception {
        doThrow(ArchiverException.class).when(jarArchiver).addFile(any(File.class), anyString());

        initTestDataAndDescriptorFiles();

        mojo.execute();
    }

    @SneakyThrows(IOException.class)
    private void storeTestResult() {
        File target = new File(folder.getRoot(), "target");
        File testDataFile = new File(new File(target, "nlp-pipelines-test-data"), "0.json");

        testDataFile.getParentFile().mkdirs();

        String testDataJson = new JSONObject()
                .put("descriptors", asList(
                        // hash of MySegmenter
                        "opennlp-1.xml",
                        // hash of MyPosTagger
                        "opennlp-2.xml"
                ))
                .put("measures", asList(
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpPosTagger")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 1.0)
                                        .put("precision", 1.0)
                                        .put("recall", 1.0)
                                ),
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpSegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 1.0)
                                        .put("precision", 1.0)
                                        .put("recall", 1.0)
                                ),
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpSegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 1.0)
                                        .put("precision", 1.0)
                                        .put("recall", 1.0)
                                )
                ))
                .toString();

        FileUtils.write(testDataFile, testDataJson, Charset.defaultCharset());
    }
}
