package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.annotators.RelationExtractor;
import de.schrieveslaach.nlpf.testing.annotators.FailingSegmenter;
import de.schrieveslaach.nlpf.testing.annotators.MyNamedEntityRecognizer;
import de.schrieveslaach.nlpf.testing.annotators.MyPosTagger;
import de.schrieveslaach.nlpf.testing.annotators.MySegmenter;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.json.JSONObject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.nio.charset.Charset;
import java.util.List;

import static de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionUtil.hash;
import static de.schrieveslaach.nlpf.testing.AnalysisEngineDescriptionMatchers.hasConfigurationParameter;
import static de.schrieveslaach.nlpf.testing.JsonDataFactory.createNlpToolTestResultOfMySegmenterAndMyPosTagger;
import static de.schrieveslaach.nlpf.testing.JsonDataFactory.createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant;
import static de.schrieveslaach.nlpf.testing.ResourceCreationSpecifierMatchers.hasImplementationName;
import static java.util.Arrays.asList;
import static net.javacrumbs.jsonunit.JsonMatchers.jsonEquals;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.io.FileMatchers.anExistingFile;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class PipelinesTestMojoPipelinesTest extends AbstractTestMojoTest {

    private static final AnalysisEngineDescription OPEN_NLP_SEGMENTER_DESCRIPTION;

    private static final AnalysisEngineDescription OPEN_NLP_PERSON_DESCRIPTION;
    private static final AnalysisEngineDescription OPEN_NLP_ORGANIZATION_DESCRIPTION;

    private static final AnalysisEngineDescription MY_PERSON_DESCRIPTION;
    private static final AnalysisEngineDescription MY_ORGANIZATION_DESCRIPTION;

    static {
        try {
            OPEN_NLP_SEGMENTER_DESCRIPTION = createEngineDescription(OpenNlpSegmenter.class);
            OPEN_NLP_PERSON_DESCRIPTION = createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "person");
            OPEN_NLP_ORGANIZATION_DESCRIPTION = createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "organization");
            MY_PERSON_DESCRIPTION = createEngineDescription(MyNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "person");
            MY_ORGANIZATION_DESCRIPTION = createEngineDescription(MyNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "organization");
        } catch (ResourceInitializationException e) {
            throw new AssertionError(e);
        }
    }

    @InjectMocks
    private PipelinesTestMojo mojo;

    @SneakyThrows(ResourceInitializationException.class)
    public static String jsonTestResult() {
        return new JSONObject()
                .put("descriptors", asList(
                        hash(createEngineDescription(MySegmenter.class)) + ".xml",
                        hash(createEngineDescription(MyPosTagger.class)) + ".xml"
                ))
                .put("measures", asList(
                        new JSONObject()
                                .put("analysisEngineName", "MyPosTagger")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.2857)
                                        .put("precision", 0.3103)
                                        .put("recall", 0.2647)),
                        new JSONObject()
                                .put("analysisEngineName", "MySegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", -1.0)
                                        .put("precision", 0.0)
                                        .put("recall", 0.0)),
                        new JSONObject()
                                .put("analysisEngineName", "MySegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.8057)
                                        .put("precision", 0.8615)
                                        .put("recall", 0.7567))
                ))
                .toString();
    }

    public static String jsonTestResultWithNerVariant() {
        return new JSONObject()
                .put("descriptors", asList(
                        hash(OPEN_NLP_SEGMENTER_DESCRIPTION) + ".xml",
                        hash(OPEN_NLP_PERSON_DESCRIPTION) + ".xml",
                        hash(OPEN_NLP_ORGANIZATION_DESCRIPTION) + ".xml"
                ))
                .put("measures", asList(
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpNamedEntityRecognizer")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity")
                                .put("variant", "organization")
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.5714)
                                        .put("precision", 1.0)
                                        .put("recall", 0.4)),
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpNamedEntityRecognizer")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity")
                                .put("variant", "person")
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.5)
                                        .put("precision", 0.75)
                                        .put("recall", 0.375)),
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpSegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 1.0)
                                        .put("precision", 1.0)
                                        .put("recall", 1.0)),
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpSegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 1.0)
                                        .put("precision", 1.0)
                                        .put("recall", 1.0))
                ))
                .toString();
    }

    @Before
    public void mockDirectoryService() {
        when(directoryService.getPipelinesTestDataDirectory()).then(invocation -> {
            File directory = new File(new File(folder.getRoot(), "target"), "nlp-pipelines-test-data");
            directory.mkdirs();
            return directory;
        });

        when(directoryService.getModelsBaseDirectory()).then(invocation -> {
            File directory = new File(new File(folder.getRoot(), "target"), "models");
            directory.mkdirs();
            return directory;
        });
    }

    @Test(expected = MojoExecutionException.class)
    public void shouldNotTestNlpModel_FailingSegmenter() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(FailingSegmenter.class);
        provideTestJCas();

        mojo.execute();
    }

    @Test
    public void shouldTestNlpModels_WithMyPosTaggerAndMySegmenter_SinglePipelineWithSingleResult() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(MySegmenter.class, MyPosTagger.class);
        provideTestJCas();

        mojo.execute();

        File testResult = new File(folder.getRoot(), "target/nlp-pipelines-test-data/0.json");
        assertThat(testResult, is(anExistingFile()));

        String json = FileUtils.readFileToString(testResult, Charset.defaultCharset());
        /* The test pipeline should produce:
         * - no sentence
         * - whitespace tokens
         * - nine NOUN pos tags
         */
        assertThat(json, jsonEquals(createNlpToolTestResultOfMySegmenterAndMyPosTagger())
                .withTolerance(0.001));
    }

    @Test
    public void shouldTestNlpModels_WithOpenNlpNamedEntityRecognizerWithDifferentVariants_SinglePipelineWithSingleResult() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(
                OPEN_NLP_SEGMENTER_DESCRIPTION,
                OPEN_NLP_PERSON_DESCRIPTION,
                MY_PERSON_DESCRIPTION,
                OPEN_NLP_ORGANIZATION_DESCRIPTION,
                MY_ORGANIZATION_DESCRIPTION
        );
        provideTestJCas();

        mojo.execute();

        File testResult = new File(folder.getRoot(), "target/nlp-pipelines-test-data/0.json");
        assertThat(testResult, is(anExistingFile()));

        String json = FileUtils.readFileToString(testResult, Charset.defaultCharset());
        assertThat(json, jsonEquals(createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant())
                .withTolerance(0.001));
    }

    @Test
    public void shouldTestNlpModels_ReuseResultsOfStartingdNlpToolsInTheTailOfEachGroup() throws Exception {
        MySegmenter.resetRuns();
        MyNamedEntityRecognizer.resetRuns();

        mockEngineDescriptionsInDescriptorsDirectory(
                OPEN_NLP_SEGMENTER_DESCRIPTION,
                OPEN_NLP_PERSON_DESCRIPTION,
                MY_PERSON_DESCRIPTION,
                OPEN_NLP_ORGANIZATION_DESCRIPTION,
                MY_ORGANIZATION_DESCRIPTION
        );
        provideTestJCas();

        mojo.execute();

        assertThat(MyNamedEntityRecognizer.class.getSimpleName() + " has been executed six times", MyNamedEntityRecognizer.getRuns(), is(equalTo(6)));
    }

    @Test
    public void shouldTestNlpModels_ReuseResultsOfStartingNlpToolsInTheTailOfEachGroup_WithFollowingAnalysis() throws Exception {
        MySegmenter.resetRuns();
        MyNamedEntityRecognizer.resetRuns();

        mockEngineDescriptionsInDescriptorsDirectory(
                OPEN_NLP_SEGMENTER_DESCRIPTION,
                OPEN_NLP_PERSON_DESCRIPTION,
                MY_PERSON_DESCRIPTION,
                OPEN_NLP_ORGANIZATION_DESCRIPTION,
                MY_ORGANIZATION_DESCRIPTION,
                // relies on named entities but results of ner should be reused
                createEngineDescription(RelationExtractor.class)
        );
        provideTestJCas();

        mojo.execute();

        assertThat(MyNamedEntityRecognizer.class.getSimpleName() + " has been executed six times", MyNamedEntityRecognizer.getRuns(), is(equalTo(6)));
        assertThat(RelationExtractor.class.getSimpleName() + " has been executed twelve times", RelationExtractor.getRuns(), is(equalTo(12)));
    }

    @Test
    public void shouldTestNlpModels_RunMySegmenterJustOnceForEachDocument_MySegmenterIsCommonHeadOfAllNlpPipelines() throws Exception {
        MySegmenter.resetRuns();
        MyNamedEntityRecognizer.resetRuns();

        mockEngineDescriptionsInDescriptorsDirectory(
                OPEN_NLP_SEGMENTER_DESCRIPTION,
                createEngineDescription(MySegmenter.class),
                OPEN_NLP_PERSON_DESCRIPTION,
                MY_PERSON_DESCRIPTION
        );
        provideTestJCas();

        mojo.execute();

        // MySegmenter should be execute just three times because for each document (three) it just be executed just once
        assertThat(MySegmenter.class.getSimpleName() + " has been executed three times", MySegmenter.getRuns(), is(equalTo(3)));
        assertThat(MyNamedEntityRecognizer.class.getSimpleName() + " has been executed six times", MyNamedEntityRecognizer.getRuns(), is(equalTo(6)));
    }

    @Test
    public void shouldBuildAllPipelineCombinations_OpenNlpSegmenterAndTagger() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(OpenNlpSegmenter.class, OpenNlpPosTagger.class);

        List<List<AnalysisEngineDescription>> pipelines = mojo.buildPipelineCombinations();

        assertThat(pipelines, hasSize(1));
        assertThat(pipelines.get(0), contains(
                hasImplementationName(OpenNlpSegmenter.class),
                hasImplementationName(OpenNlpPosTagger.class)
        ));
    }

    @Test
    public void shouldBuildAllPipelineCombinations_OpenNlpSegmenterAndTagger_AndMyPosTagger() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(OpenNlpSegmenter.class, OpenNlpPosTagger.class, MyPosTagger.class);

        List<List<AnalysisEngineDescription>> pipelines = mojo.buildPipelineCombinations();

        assertThat(pipelines, hasSize(2));
        assertThat(pipelines, containsInAnyOrder(
                contains(
                        hasImplementationName(OpenNlpSegmenter.class),
                        hasImplementationName(OpenNlpPosTagger.class)
                ),
                contains(
                        hasImplementationName(OpenNlpSegmenter.class),
                        hasImplementationName(MyPosTagger.class)
                )
        ));
    }

    @Test
    public void shouldBuildAllPipelineCombinations_OpenNlpSegmenterAndTagger_AndMyPosTagger_AndMySegmenter() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(MySegmenter.class, OpenNlpSegmenter.class, OpenNlpPosTagger.class, MyPosTagger.class);

        List<List<AnalysisEngineDescription>> pipelines = mojo.buildPipelineCombinations();

        assertThat(pipelines, hasSize(4));
        assertThat(pipelines, containsInAnyOrder(
                contains(
                        hasImplementationName(OpenNlpSegmenter.class),
                        hasImplementationName(OpenNlpPosTagger.class)
                ),
                contains(
                        hasImplementationName(OpenNlpSegmenter.class),
                        hasImplementationName(MyPosTagger.class)
                ),
                contains(
                        hasImplementationName(MySegmenter.class),
                        hasImplementationName(OpenNlpPosTagger.class)
                ),
                contains(
                        hasImplementationName(MySegmenter.class),
                        hasImplementationName(MyPosTagger.class)
                )
        ));
    }

    @Test
    public void shouldBuildAllPipelineCombinations_WithOpenNlpNamedEntityRecognizerWithDifferentVariants() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(
                OPEN_NLP_SEGMENTER_DESCRIPTION,
                OPEN_NLP_PERSON_DESCRIPTION,
                createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "location")
        );

        List<List<AnalysisEngineDescription>> pipelines = mojo.buildPipelineCombinations();
        assertThat(pipelines, hasSize(1));
        assertThat(pipelines, containsInAnyOrder(
                contains(
                        hasImplementationName(OpenNlpSegmenter.class),
                        allOf(
                                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                                hasConfigurationParameter(ComponentParameters.PARAM_VARIANT, equalTo("person"))
                        ),
                        allOf(
                                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                                hasConfigurationParameter(ComponentParameters.PARAM_VARIANT, equalTo("location"))
                        )
                )
        ));
    }

    @Test
    public void shouldBuildAllPipelineCombinations_WithNamedEntityRecognizerWithMultipleVariants() throws Exception {
        mockEngineDescriptionsInDescriptorsDirectory(
                OPEN_NLP_SEGMENTER_DESCRIPTION,
                OPEN_NLP_PERSON_DESCRIPTION,
                MY_PERSON_DESCRIPTION
        );

        List<List<AnalysisEngineDescription>> pipelines = mojo.buildPipelineCombinations();
        assertThat(pipelines, hasSize(2));
        assertThat(pipelines, containsInAnyOrder(
                contains(
                        hasImplementationName(OpenNlpSegmenter.class),
                        allOf(
                                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                                hasConfigurationParameter(ComponentParameters.PARAM_VARIANT, equalTo("person"))
                        )
                ),
                contains(
                        hasImplementationName(OpenNlpSegmenter.class),
                        allOf(
                                hasImplementationName(MyNamedEntityRecognizer.class),
                                hasConfigurationParameter(ComponentParameters.PARAM_VARIANT, equalTo("person"))
                        )
                )
        ));
    }

    @Override
    protected AbstractTestMojo getMojoUnderTest() {
        return mojo;
    }
}
