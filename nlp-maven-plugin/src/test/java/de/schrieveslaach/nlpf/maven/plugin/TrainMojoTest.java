package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.service.AnalysisEngineDescriptionService;
import de.schrieveslaach.nlpf.maven.plugin.service.ClassLoaderService;
import de.schrieveslaach.nlpf.maven.plugin.service.CollectionReaderDescriptionService;
import de.schrieveslaach.nlpf.maven.plugin.service.DirectoryService;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ResourceParameter;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.maven.project.MavenProject;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasConsumer_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.MimeTypeCapability;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.junit.After;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import static de.schrieveslaach.nlpf.maven.plugin.ArgumentMatchers.hasImplementationClass;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static java.util.Arrays.asList;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.argThat;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class TrainMojoTest {

    private static JCas jCasOfPosTaggerTrainer;
    private static JCas jCasOfNamedEntityTrainer;

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private TrainMojo mojo;

    @Mock
    private MavenProject project;

    @Mock
    private CollectionReaderDescriptionService collectionReaderService;

    @Mock
    private AnalysisEngineDescriptionService analysisEngineService;

    @Mock
    private DirectoryService directoryService;

    @Mock
    private ClassLoaderService classLoaderService;

    @After
    public void cleanUpJCas() {
        jCasOfPosTaggerTrainer = null;
        jCasOfNamedEntityTrainer = null;
    }

    @Before
    public void injectLanguage() throws Exception {
        Field language = mojo.getClass().getDeclaredField("language");
        language.setAccessible(true);
        language.set(mojo, "de");
    }

    @Before
    public void mockAnalysisEngineService() {
        when(analysisEngineService.getMimetypes(any(AnalysisEngineDescription.class))).then(invocationOnMock -> {
            AnalysisEngineDescription description = invocationOnMock.getArgument(0);

            Set<String> mimetypes = new HashSet<>();

            mimetypes.add("de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token");
            mimetypes.add("de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence");

            switch (description.getImplementationName()) {
                case "de.schrieveslaach.nlpf.maven.plugin.TrainMojoTest$TestNerTrainer":
                    mimetypes.add("de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity");
                    break;
                case "de.schrieveslaach.nlpf.maven.plugin.TrainMojoTest$TestPosTaggerTrainer":
                    mimetypes.add("de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS");
                    break;
            }

            return mimetypes;
        });

        when(analysisEngineService.getAnalysisEngineName(any())).then(invocationOnMock -> {
            AnalysisEngineDescription description = invocationOnMock.getArgument(0);
            return description.getImplementationName();
        });
    }

    @Test
    public void shouldFailBecauseSrcMainCorpusIsMissing() throws Exception {
        expectedException.expectMessage("The corpus directory is missing. Create the directory src/main/corpus");

        when(collectionReaderService.createReaderDescriptions()).thenThrow(
                new MojoExecutionException("The corpus directory is missing. Create the directory src/main/corpus")
        );
        mojo.execute();
    }

    @Test
    public void shouldStoreEngineDescriptions() throws Exception {
        mockTrainingPipeline();

        AnalysisEngineDescription aed = createEngineDescription(TestPosTagger.class);
        when(analysisEngineService.findAnnotatorDescriptions(any())).thenReturn(asList(aed));

        mojo.execute();

        verify(directoryService).store(argThat(hasImplementationClass(TestPosTagger.class)));
    }

    @Test
    public void shouldPerformingTraining() throws Exception {
        mockTrainingPipeline(TestPosTaggerTrainer.class, TestNerTrainer.class);

        mojo.execute();

        assertThat(jCasOfNamedEntityTrainer, is(notNullValue()));
        assertThat(jCasOfPosTaggerTrainer, is(nullValue()));
    }

    @Test
    public void shouldContinueTrainingIfSingleTrainerCannotBeCreated() throws Exception {
        mockTrainingPipeline(TestPosTaggerTrainerWithMandatoryParameter.class, TestNerTrainer.class);

        mojo.execute();

        assertThat(jCasOfNamedEntityTrainer, is(notNullValue()));
    }

    private void mockTrainingPipeline(Class... trainerClasses) throws Exception {
        List<AnalysisEngineDescription> descriptions = new ArrayList<>();
        for (Class trainerClass : trainerClasses) {
            descriptions.add(createEngineDescription(trainerClass));
        }

        JCas openNlpExample = createOpenNlpExample();
        runPipeline(openNlpExample, createEngineDescription(
                XmiWriter.class, XmiWriter.PARAM_TARGET_LOCATION, folder.getRoot())
        );

        when(collectionReaderService.createReaderDescriptions()).thenReturn(
                createReaderDescription(
                        XmiReader.class,
                        XmiReader.PARAM_SOURCE_LOCATION, new File(folder.getRoot(), "opennlp.xmi")
                )
        );
        when(analysisEngineService.findTrainerDescriptions()).thenReturn(descriptions);
    }

    @MimeTypeCapability("nlp/test-pos")
    public static class TestPosTaggerTrainer extends JCasConsumer_ImplBase {

        @Override
        public void process(JCas aJCas) throws AnalysisEngineProcessException {
            jCasOfPosTaggerTrainer = aJCas;
        }
    }

    @MimeTypeCapability("nlp/test-pos")
    public static class TestPosTaggerTrainerWithMandatoryParameter extends JCasConsumer_ImplBase {

        public static final String PARAM_PARAMETER_FILE = "trainFile";
        @ConfigurationParameter(name = PARAM_PARAMETER_FILE, mandatory = true)
        private File parameterFile;

        @Override
        public void process(JCas aJCas) throws AnalysisEngineProcessException {
            jCasOfPosTaggerTrainer = aJCas;
        }
    }

    @MimeTypeCapability("nlp/test-pos")
    @TypeCapability(
            inputs = {"de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence"},
            outputs = {"de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS"}
    )
    public static class TestPosTagger extends JCasConsumer_ImplBase {

        @ConfigurationParameter(name = ComponentParameters.PARAM_MODEL_LOCATION, mandatory = false)
        @ResourceParameter("nlp/test-pos")
        private File modelFile;

        @Override
        public void process(JCas aJCas) throws AnalysisEngineProcessException {

        }
    }

    @MimeTypeCapability("nlp/test-ner")
    public static class TestNerTrainer extends JCasConsumer_ImplBase {

        @Override
        public void process(JCas aJCas) throws AnalysisEngineProcessException {
            jCasOfNamedEntityTrainer = aJCas;
        }
    }

    @MimeTypeCapability("nlp/test-ner")
    @TypeCapability(
            inputs = {"de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token"},
            outputs = {"de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity"}
    )
    public static class TestNer extends JCasConsumer_ImplBase {

        @ConfigurationParameter(name = ComponentParameters.PARAM_MODEL_LOCATION, mandatory = false)
        @ResourceParameter("nlp/test-pos")
        private File modelFile;

        @Override
        public void process(JCas aJCas) throws AnalysisEngineProcessException {

        }
    }

}
