package de.schrieveslaach.nlpf.maven.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.service.ClassLoaderService;
import de.schrieveslaach.nlpf.maven.plugin.service.CollectionReaderDescriptionService;
import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import org.apache.maven.plugin.MojoExecutionException;
import org.apache.uima.jcas.JCas;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ValidationMojoTest {

    @InjectMocks
    private ValidationMojo mojo;

    @Mock
    private ClassLoaderService classLoaderService;

    @Mock
    private CollectionReaderDescriptionService collectionReaderService;

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void shouldNotFail_JCasDoesNotContainAnyPosValues() throws Exception {
        JCas openNlpExample = createOpenNlpExample();
        mockReaderWith(openNlpExample, "opennlp.xmi");

        mojo.execute();
    }

    @Test
    public void shouldNotFail_JCasContainsForAllTokensPosValues() throws Exception {
        JCas openNlpExample = createOpenNlpExample();
        for (Token token : select(openNlpExample, Token.class)) {
            POS pos = new POS(openNlpExample, token.getBegin(), token.getEnd());
            pos.setPosValue("X");
            pos.addToIndexes();
            token.setPos(pos);
        }

        mockReaderWith(openNlpExample, "opennlp.xmi");

        mojo.execute();
    }

    @Test
    public void shouldFail_JCasContainsPosValuesForJustOneToken() throws Exception {
        expectedException.expect(MojoExecutionException.class);
        expectedException.expectMessage("The token “Vinken” in the sentence “Pierre Vinken, 61 years old, will join the board as a nonexecutive director on Nov. 29.” must be annotated with a pos tag because all other tokens are annotated with pos tags, c.f. document: https://opennlp.apache.org/opennlp");

        JCas openNlpExample = createOpenNlpExample();
        select(openNlpExample, Token.class).stream().limit(1).forEach(token -> {
            POS pos = new POS(openNlpExample, token.getBegin(), token.getEnd());
            pos.setPosValue("X");
            pos.addToIndexes();
            token.setPos(pos);
        });

        mockReaderWith(openNlpExample, "opennlp.xmi");

        mojo.execute();
    }

    private void mockReaderWith(JCas jCas, String fileName) throws Exception {
        runPipeline(jCas, createEngineDescription(
                XmiWriter.class, XmiWriter.PARAM_TARGET_LOCATION, folder.getRoot())
        );

        when(collectionReaderService.createReaderDescriptions()).thenReturn(
                createReaderDescription(
                        XmiReader.class,
                        XmiReader.PARAM_SOURCE_LOCATION, new File(folder.getRoot(), fileName)
                )
        );
    }
}
