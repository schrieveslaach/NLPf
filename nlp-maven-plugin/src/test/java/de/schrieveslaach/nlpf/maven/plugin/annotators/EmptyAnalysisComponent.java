package de.schrieveslaach.nlpf.maven.plugin.annotators;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.apache.uima.analysis_component.AnalysisComponent_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.AbstractCas;

public class EmptyAnalysisComponent extends AnalysisComponent_ImplBase {
    @Override
    public void process(AbstractCas aCAS) throws AnalysisEngineProcessException {

    }

    @Override
    public boolean hasNext() throws AnalysisEngineProcessException {
        return false;
    }

    @Override
    public AbstractCas next() throws AnalysisEngineProcessException {
        return null;
    }

    @Override
    public Class<? extends AbstractCas> getRequiredCasInterface() {
        return null;
    }

    @Override
    public int getCasInstancesRequired() {
        return 0;
    }
}
