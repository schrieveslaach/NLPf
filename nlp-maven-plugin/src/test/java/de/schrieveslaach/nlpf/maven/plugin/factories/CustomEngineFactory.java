package de.schrieveslaach.nlpf.maven.plugin.factories;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.maven.plugin.PipelinesTestMojoPipelinesTest;
import de.schrieveslaach.nlpf.testing.annotators.MyPosTagger;
import de.schrieveslaach.nlpf.testing.annotators.MySegmenter;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

/**
 * Test factory required by {@link PipelinesTestMojoPipelinesTest}.
 */
public class CustomEngineFactory {

    public AnalysisEngineDescription createMySegmenter() throws Exception {
        return createEngineDescription(
                MySegmenter.class
        );
    }

    public AnalysisEngineDescription createMyPosTagger() throws Exception {
        return createEngineDescription(
                MyPosTagger.class
        );
    }
}
