package de.schrieveslaach.nlpf.maven.plugin.models;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import de.schrieveslaach.nlpf.testing.annotators.MyNamedEntityRecognizer;
import de.schrieveslaach.nlpf.testing.annotators.MyPosTagger;
import de.schrieveslaach.nlpf.testing.annotators.MySegmenter;
import de.tudarmstadt.ukp.dkpro.core.eval.measure.FMeasure;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import lombok.SneakyThrows;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.json.JSONObject;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static java.util.Arrays.asList;
import static net.javacrumbs.jsonunit.JsonMatchers.jsonEquals;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.hamcrest.Matchers.closeTo;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasEntry;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.sameInstance;
import static org.hamcrest.junit.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class TestResultTest {

    public static String jsonTestResult() {
        return new JSONObject()
                .put("descriptors", asList(
                        MySegmenter.DEFAULT_HASH_CODE + ".xml",
                        MyPosTagger.DEFAULT_HASH_CODE + ".xml"
                ))
                .put("measures", asList(new JSONObject()
                        .put("analysisEngineName", MySegmenter.class.getCanonicalName())
                        .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS")
                        .put("variant", JSONObject.NULL)
                        .put("measure", new JSONObject()
                                .put("f-measure", 0.5)
                                .put("precision", 0.5)
                                .put("recall", 0.5))
                ))
                .toString();
    }

    @Test
    public void shouldDeserializeFromJson() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        TestResult testResult = mapper.readValue(jsonTestResult(), TestResult.class);

        assertThat(testResult, is(equalTo(createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(MyPosTagger.class)
                ),
                0.5,
                "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS",
                null
        ))));
    }

    @Test
    public void shouldSerializeAsJson() throws Exception {
        TestResult testResult = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(MyPosTagger.class)
                ),
                0.5,
                "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS",
                null
        );

        ObjectMapper mapper = new ObjectMapper();
        String json = mapper.writeValueAsString(testResult);

        assertThat(json, jsonEquals(jsonTestResult()));
    }

    @Test
    public void shouldComplyWithHashCodeContract_ShouldContainTestResultWithDifferentFMeasureValue() throws Exception {
        Map<TestResult, String> map = createTestResultStringMap();

        TestResult anotherTestResult = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(MyPosTagger.class)
                ),
                0.3,
                "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS",
                null
        );

        assertThat(map, hasKey(anotherTestResult));
    }

    @Test
    public void shouldComplyWithHashCodeContract_ShouldNotContainTestResultWithDifferentPipeline_WithoutPosTagger() throws Exception {
        Map<TestResult, String> map = createTestResultStringMap();

        TestResult anotherTestResult = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class)
                ),
                0.5,
                "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS",
                null
        );

        assertThat(map, not(hasKey(anotherTestResult)));
    }

    @Test
    public void shouldComplyWithHashCodeContract_ShouldNotContainTestResultWithDifferentPipeline_WithoutOpenNLP() throws Exception {
        Map<TestResult, String> map = createTestResultStringMap();

        TestResult anotherTestResult = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(OpenNlpPosTagger.class)
                ),
                0.5,
                "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS",
                null
        );

        assertThat(map, not(hasKey(anotherTestResult)));
    }

    @Test
    public void shouldComplyWithHashCodeContract_ShouldNotContainTestResultWithDifferentType() throws Exception {
        Map<TestResult, String> map = createTestResultStringMap();

        TestResult anotherTestResult = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(MyPosTagger.class)
                ),
                0.5,
                "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token",
                null
        );

        assertThat(map, not(hasKey(anotherTestResult)));
    }

    @Test
    public void shouldCompareTestResults_ThroughCollectionSort() throws Exception {
        List<TestResult> testResults = new ArrayList<>();

        TestResult testResult1 = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(MyPosTagger.class)
                ),
                0.5,
                "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token",
                null
        );
        TestResult testResult2 = createTestResult(
                asList(
                        createEngineDescription(OpenNlpSegmenter.class),
                        createEngineDescription(OpenNlpPosTagger.class)
                ),
                1.0,
                "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token",
                null
        );
        TestResult testResult3 = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(OpenNlpPosTagger.class)
                ),
                0.8,
                "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token",
                null
        );

        testResults.add(testResult1);
        testResults.add(testResult2);
        testResults.add(testResult3);

        Collections.sort(testResults);

        assertThat(testResults, contains(
                sameInstance(testResult2),
                sameInstance(testResult3),
                sameInstance(testResult1)
        ));
    }

    @Test
    public void shouldCompareTestResults_ThroughCollectionSort_WithVariants() throws Exception {
        List<TestResult> testResults = new ArrayList<>();

        TestResult testResult1 = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(MyNamedEntityRecognizer.class, MyNamedEntityRecognizer.PARAM_VARIANT, "person")
                ),
                0.5,
                "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity",
                "person"
        );
        TestResult testResult2 = createOpenNlpNerPersonTestResult();
        TestResult testResult3 = createOpenNlpNerTestResult();

        testResults.add(testResult1);
        testResults.add(testResult2);
        testResults.add(testResult3);

        Collections.sort(testResults);

        assertThat(testResults, contains(
                sameInstance(testResult3),
                sameInstance(testResult2),
                sameInstance(testResult1)
        ));
    }

    @Test
    public void shouldCompareTestResults_ThroughCollectionSort_WithoutAnyHits() throws Exception {
        List<TestResult> testResults = new ArrayList<>();

        TestResult testResult1 = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(MyNamedEntityRecognizer.class, MyNamedEntityRecognizer.PARAM_VARIANT, "person")
                ),
                -1.0,
                "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity",
                "person"
        );
        TestResult testResult2 = createOpenNlpNerPersonTestResult();
        TestResult testResult3 = createOpenNlpNerTestResult();

        testResults.add(testResult1);
        testResults.add(testResult2);
        testResults.add(testResult3);

        Collections.sort(testResults);

        assertThat(testResults, contains(
                sameInstance(testResult3),
                sameInstance(testResult2),
                sameInstance(testResult1)
        ));
    }


    @Test
    public void shouldComplyWithHashCodeContract_ValuePresent() {
        Map<TestResult, String> map = new HashMap<>();
        map.put(createOpenNlpNerTestResult(), "hello world");
        assertThat(map, hasKey(createOpenNlpNerTestResult()));
    }

    @Test
    public void shouldComplyWithHashCodeContract_ValueNotPresent() {
        Map<TestResult, String> map = new HashMap<>();
        map.put(createOpenNlpNerTestResult(), "hello world");
        assertThat(map, not(hasKey(createOpenNlpNerPersonTestResult())));
    }

    @Test
    public void shouldCalculateHarmonicMean_WithoutMeasures() throws Exception {
        TestResult testResult = new TestResult(asList(
                createEngineDescription(MySegmenter.class),
                createEngineDescription(OpenNlpNamedEntityRecognizer.class, OpenNlpNamedEntityRecognizer.PARAM_VARIANT, "person")
        ));

        assertThat(testResult.calculateHarmonicMean(), is(equalTo(0.0)));
    }

    @Test
    public void shouldCalculateHarmonicMean_WithMeasures() throws Exception {
        TestResult testResult = createOpenNlpNerPersonTestResult();

        assertThat(testResult.calculateHarmonicMean(), is(closeTo(0.9, 0.0001)));
    }

    @SneakyThrows(UIMAException.class)
    private TestResult createOpenNlpNerTestResult() {
        return createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(OpenNlpNamedEntityRecognizer.class, OpenNlpNamedEntityRecognizer.PARAM_VARIANT, "person")
                ),
                1.0,
                "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity",
                null
        );
    }

    @SneakyThrows(UIMAException.class)
    private TestResult createOpenNlpNerPersonTestResult() {
        return createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(OpenNlpNamedEntityRecognizer.class, OpenNlpNamedEntityRecognizer.PARAM_VARIANT, "person")
                ),
                0.9,
                "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity",
                "person"
        );
    }

    private TestResult createTestResult(List<AnalysisEngineDescription> pipeline, double fScore, String outputType, String variant) throws ResourceInitializationException {
        TestResult anotherTestResult = new TestResult(pipeline);
        FMeasure fMeasure = mock(FMeasure.class);
        when(fMeasure.getFMeasure()).thenReturn(fScore);
        when(fMeasure.getPrecision()).thenReturn(fScore);
        when(fMeasure.getRecall()).thenReturn(fScore);
        anotherTestResult.add(pipeline.get(0).getImplementationName(), outputType, variant, fMeasure);
        return anotherTestResult;
    }

    private Map<TestResult, String> createTestResultStringMap() throws ResourceInitializationException {
        TestResult testResult = createTestResult(
                asList(
                        createEngineDescription(MySegmenter.class),
                        createEngineDescription(MyPosTagger.class)
                ),
                0.5,
                "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS",
                null
        );

        Map<TestResult, String> map = new HashMap<>();
        map.put(testResult, "Hello World");
        assertThat(map, hasEntry(testResult, "Hello World"));
        return map;
    }

}
