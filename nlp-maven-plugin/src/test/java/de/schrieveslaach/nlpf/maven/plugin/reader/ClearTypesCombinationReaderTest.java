package de.schrieveslaach.nlpf.maven.plugin.reader;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiWriter;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Collection;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createTorwaldsExampleWithPosTags;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class ClearTypesCombinationReaderTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Test
    public void shouldCreatePosTags() throws Exception {
        File readerConfiguration = storeCasAndProvideConfigurationFile(createTorwaldsExampleWithPosTags(), "torwalds.xmi");

        CollectionReaderDescription readerDescription = createReaderDescription(
                ClearTypesCombinationReader.class,
                ClearTypesCombinationReader.PARAM_READERS, new String[]{
                        readerConfiguration.toString()
                },
                ClearTypesCombinationReader.PARAM_TYPES_TO_CLEAR, new String[]{
                        POS.class.getName()
                }
        );

        for (JCas jCas : iteratePipeline(readerDescription)) {
            assertThat(jCas, not(hasAnnotations(POS.class)));
        }
    }

    private File storeCasAndProvideConfigurationFile(JCas jCas, String filename) throws Exception {
        runPipeline(jCas, createEngineDescription(
                XmiWriter.class,
                XmiWriter.PARAM_TARGET_LOCATION, folder.getRoot()
        ));

        CollectionReaderDescription readerDescription = createReaderDescription(
                XmiReader.class,
                XmiReader.PARAM_SOURCE_LOCATION, new File(folder.getRoot(), filename)
        );

        File configurationFile = folder.newFile();
        try (FileOutputStream fos = new FileOutputStream(configurationFile)) {
            readerDescription.toXML(fos);
        }

        return configurationFile;
    }

    public Matcher<JCas> hasAnnotations(final Class<? extends Annotation> annotationClass) {
        return new TypeSafeMatcher<JCas>() {
            @Override
            protected boolean matchesSafely(JCas jCas) {
                return !select(jCas, annotationClass).isEmpty();
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has annotations of ").appendValue(annotationClass.getSimpleName());
            }

            @Override
            protected void describeMismatchSafely(JCas jCas, Description mismatchDescription) {
                Collection<? extends Annotation> annotations = select(jCas, annotationClass);

                mismatchDescription.appendText("has ")
                        .appendValue(annotations.size())
                        .appendText(" annotations of ")
                        .appendValue(annotationClass.getSimpleName());
            }
        };
    }
}
