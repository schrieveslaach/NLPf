package de.schrieveslaach.nlpf.maven.plugin.service;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpChunker;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpChunkerTrainer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpLemmatizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpLemmatizerTrainer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizerTrainer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTaggerTrainer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSentenceTrainer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpTokenTrainer;
import lombok.SneakyThrows;
import org.apache.commons.lang3.StringUtils;
import org.apache.maven.project.MavenProject;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasConsumer_ImplBase;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.regex.Pattern;

import static de.schrieveslaach.nlpf.maven.plugin.service.AnalysisEngineDescriptionService.NAMED_ENTITY_PROPERTY_KEY;
import static de.schrieveslaach.nlpf.testing.AnalysisEngineDescriptionMatchers.hasConfigurationParameter;
import static de.schrieveslaach.nlpf.testing.ResourceCreationSpecifierMatchers.hasImplementationName;
import static java.util.Arrays.asList;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.text.MatchesPattern.matchesPattern;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class AnalysisEngineDescriptionServiceTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @InjectMocks
    private AnalysisEngineDescriptionService service;

    @Mock
    private MavenProject mavenProject;

    @Mock
    private ClassLoaderService classLoaderService;

    @Mock
    private DirectoryService directoryService;

    @Before
    @SneakyThrows
    public void setLanguage() {
        service.setLanguage("en");
    }

    @Before
    public void mockMavenProject() {
        when(mavenProject.getGroupId()).thenReturn("de.company");
        when(mavenProject.getArtifactId()).thenReturn("domain-specific-corpus");
        when(mavenProject.getProperties()).thenReturn(new Properties());
    }

    @Before
    public void mockClassLoaderService() {
        when(classLoaderService.getDependencyClassLoader()).thenReturn(getClass().getClassLoader());
    }

    @Before
    public void mockDirectoryService() {
        when(directoryService.getModelTargetLocation(any(Package.class), any(), any())).then(invocationOnMock -> {
            Package pkg = invocationOnMock.getArgument(0);
            String type = invocationOnMock.getArgument(1);
            String variant = invocationOnMock.getArgument(2);
            File folder = new File(this.folder.getRoot(), "target/models/" + pkg.getName());
            folder.mkdirs();
            return new File(folder, String.format("%s-en-%s.model", type, StringUtils.defaultIfBlank(variant, "default")));
        });
    }

    @Test
    public void shouldLookUpMimetypesOfTrainerDescriptions() {
        AnalysisEngineDescription description = service.findTrainerDescriptions().stream()
                .filter(aed -> aed.getImplementationName().equals(OpenNlpChunkerTrainer.class.getName()))
                .findFirst()
                .get();

        Set<String> mimetypes = service.getMimetypes(description);

        assertThat(mimetypes, hasItem("de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS"));
        assertThat(mimetypes, hasItem("de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token"));
        assertThat(mimetypes, hasItem("de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence"));
        assertThat(mimetypes, hasItem("de.tudarmstadt.ukp.dkpro.core.api.syntax.type.chunk.Chunk"));
    }

    @Test
    public void shouldNotLookUpMimetypesOfTrainerDescriptions_NotATrainerClass() throws Exception {
        Set<String> mimetypes = service.getMimetypes(createEngineDescription(
                TestPosTagger.class
        ));

        assertThat(mimetypes, is(empty()));
    }

    @Test
    public void shouldFindTrainerDescriptionFromClassPath() {
        List<AnalysisEngineDescription> descriptions = service.findTrainerDescriptions();

        assertThat(descriptions, containsInAnyOrder(
                hasImplementationName(OpenNlpChunkerTrainer.class),
                hasImplementationName(OpenNlpLemmatizerTrainer.class),
                hasImplementationName(OpenNlpNamedEntityRecognizerTrainer.class),
                hasImplementationName(OpenNlpPosTaggerTrainer.class),
                hasImplementationName(OpenNlpSentenceTrainer.class),
                hasImplementationName(OpenNlpTokenTrainer.class)
        ));
    }

    @Test
    public void shouldFindTrainerDescriptionFromClassPath_ConfigureWithTrainerParamModelLocation() {
        List<AnalysisEngineDescription> descriptions = service.findTrainerDescriptions();

        assertThat(descriptions, containsInAnyOrder(
                hasConfigurationParameter(
                        ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                                Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.chunk-en-default.model$")
                        )
                ),
                hasConfigurationParameter(
                        ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                                Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.lemma-en-default.model$")
                        )
                ),
                hasConfigurationParameter(
                        ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                                Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.ner-en-default.model$")
                        )
                ),
                hasConfigurationParameter(
                        ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                                Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.tagger-en-default.model$")
                        )
                ),
                hasConfigurationParameter(
                        ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                                Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.sent-en-default.model$")
                        )
                ),
                hasConfigurationParameter(
                        ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                                Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.token-en-default.model$")
                        )
                )
        ));
    }

    @Test
    public void shouldFindTrainerDescriptionFromClassPath_ConfigureWithTrainerParamModelLocation_WithNamedEntityVariant() {
        Properties properties = new Properties();
        properties.put(NAMED_ENTITY_PROPERTY_KEY, "person,location,organization");
        when(mavenProject.getProperties()).thenReturn(properties);

        List<AnalysisEngineDescription> descriptions = service.findTrainerDescriptions();
        assertThat(descriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizerTrainer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                        Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.ner-en-person.model$")
                ))
        )));
        assertThat(descriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizerTrainer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                        Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.ner-en-location.model$")
                ))
        )));
        assertThat(descriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizerTrainer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_TARGET_LOCATION, matchesPattern(
                        Pattern.compile("^.*/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.ner-en-organization.model$")
                ))
        )));
    }

    @Test
    public void shouldFindTrainerDescriptionFromClassPath_ConfigureWithLanguage() {
        List<AnalysisEngineDescription> descriptions = service.findTrainerDescriptions();

        assertThat(descriptions, everyItem(
                hasConfigurationParameter(ComponentParameters.PARAM_LANGUAGE, hasToString("en"))
        ));
    }

    @Test
    public void shouldFindAnnotatorDescriptionFromClassPath_OnlyChunker() throws Exception {
        List<AnalysisEngineDescription> descriptions = service.findAnnotatorDescriptions(asList(
                createEngineDescription(OpenNlpChunkerTrainer.class)
        ));

        assertThat(descriptions, containsInAnyOrder(
                hasImplementationName(OpenNlpChunker.class)
        ));
    }

    @Test
    public void shouldFindAnnotatorDescriptionFromClassPath_AllOpenNlpClasses() throws Exception {
        List<AnalysisEngineDescription> descriptions = service.findAnnotatorDescriptions(asList(
                createEngineDescription(OpenNlpChunkerTrainer.class),
                createEngineDescription(OpenNlpLemmatizerTrainer.class),
                createEngineDescription(OpenNlpNamedEntityRecognizerTrainer.class),
                createEngineDescription(OpenNlpPosTaggerTrainer.class),
                createEngineDescription(OpenNlpSentenceTrainer.class),
                createEngineDescription(OpenNlpTokenTrainer.class)
        ));

        assertThat(descriptions, containsInAnyOrder(
                hasImplementationName(OpenNlpChunker.class),
                hasImplementationName(OpenNlpLemmatizer.class),
                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                hasImplementationName(OpenNlpPosTagger.class),
                hasImplementationName(OpenNlpSegmenter.class)
        ));
    }

    @Test
    public void shouldFindAnnotatorDescriptionFromClassPath_ConfigureWithAnnotatorParamModelLocation() throws Exception {
        List<AnalysisEngineDescription> descriptions = service.findAnnotatorDescriptions(asList(
                createEngineDescription(OpenNlpChunkerTrainer.class),
                createEngineDescription(OpenNlpLemmatizerTrainer.class),
                createEngineDescription(OpenNlpNamedEntityRecognizerTrainer.class),
                createEngineDescription(OpenNlpPosTaggerTrainer.class),
                createEngineDescription(OpenNlpSentenceTrainer.class),
                createEngineDescription(OpenNlpTokenTrainer.class)
        ));

        assertThat(descriptions, hasItem(hasConfigurationParameter(
                ComponentParameters.PARAM_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.chunk-en-default.model$")
                )
        )));
        assertThat(descriptions, hasItem(hasConfigurationParameter(
                ComponentParameters.PARAM_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.lemma-en-default.model$")
                )
        )));
        assertThat(descriptions, hasItem(hasConfigurationParameter(
                ComponentParameters.PARAM_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.ner-en-default.model$")
                )
        )));
        assertThat(descriptions, hasItem(hasConfigurationParameter(
                ComponentParameters.PARAM_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.tagger-en-default.model$")
                )
        )));
        assertThat(descriptions, hasItem(hasConfigurationParameter(
                OpenNlpSegmenter.PARAM_SEGMENTATION_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.sent-en-default.model$")
                )
        )));
        assertThat(descriptions, hasItem(hasConfigurationParameter(
                OpenNlpSegmenter.PARAM_TOKENIZATION_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.token-en-default.model$")
                )
        )));
    }

    @Test
    public void shouldFindAnnotatorDescriptionFromClassPath_ConfigureWithAnnotatorParamModelLocation_WithNamedEntityVariant() throws Exception {
        Properties properties = new Properties();
        properties.put(NAMED_ENTITY_PROPERTY_KEY, "person,location,organization");
        when(mavenProject.getProperties()).thenReturn(properties);

        List<AnalysisEngineDescription> annotatorDescriptions = service.findAnnotatorDescriptions(service.findTrainerDescriptions());
        assertThat(annotatorDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.ner-en-person.model$")
                ))
        )));
        assertThat(annotatorDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.ner-en-location.model$")
                ))
        )));
        assertThat(annotatorDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_MODEL_LOCATION, matchesPattern(
                        Pattern.compile("^classpath:/de\\.company/domain-specific-corpus/de\\.tudarmstadt\\.ukp\\.dkpro\\.core\\.opennlp/x.org.dkpro.core.opennlp.ner-en-organization.model$")
                ))
        )));
    }

    @Test
    public void shouldFindTrainerParameter_FromProperties() throws ResourceInitializationException {
        Properties properties = new Properties();
        properties.put("de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpChunkerTrainer.iterations", 999);
        when(mavenProject.getProperties()).thenReturn(properties);

        List<AnalysisEngineDescription> trainerDescriptions = service.findTrainerDescriptions();
        assertThat(trainerDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpChunkerTrainer.class),
                hasConfigurationParameter(OpenNlpChunkerTrainer.PARAM_ITERATIONS, is("999"))
        )));
    }

    @Test
    public void shouldFindAnnotatorParameter_FromProperties() throws ResourceInitializationException {
        Properties properties = new Properties();
        properties.put("de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpChunker.printTagSet", true);
        when(mavenProject.getProperties()).thenReturn(properties);

        List<AnalysisEngineDescription> annotatorDescriptions = service.findAnnotatorDescriptions(service.findTrainerDescriptions());
        assertThat(annotatorDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpChunker.class),
                hasConfigurationParameter(OpenNlpChunker.PARAM_PRINT_TAGSET, is("true"))
        )));
    }

    @Test
    public void shouldMultipleTrainerEngineDescriptions_ByNamedEntityVariant() {
        Properties properties = new Properties();
        properties.put(NAMED_ENTITY_PROPERTY_KEY, "person,location,organization");
        when(mavenProject.getProperties()).thenReturn(properties);

        List<AnalysisEngineDescription> trainerDescriptions = service.findTrainerDescriptions();
        assertThat(trainerDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizerTrainer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_ACCEPTED_TAGS_REGEX, is("person"))
        )));
        assertThat(trainerDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizerTrainer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_ACCEPTED_TAGS_REGEX, is("location"))
        )));
        assertThat(trainerDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizerTrainer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_ACCEPTED_TAGS_REGEX, is("organization"))
        )));
    }

    @Test
    public void shouldMultipleAnnotatorEngineDescriptions_ByNamedEntityVariant() {
        Properties properties = new Properties();
        properties.put(NAMED_ENTITY_PROPERTY_KEY, "person,location,organization");
        when(mavenProject.getProperties()).thenReturn(properties);

        List<AnalysisEngineDescription> annotatorDescriptions = service.findAnnotatorDescriptions(service.findTrainerDescriptions());
        assertThat(annotatorDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_VARIANT, is("person"))
        )));
        assertThat(annotatorDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_VARIANT, is("location"))
        )));
        assertThat(annotatorDescriptions, hasItem(allOf(
                hasImplementationName(OpenNlpNamedEntityRecognizer.class),
                hasConfigurationParameter(ComponentParameters.PARAM_VARIANT, is("organization"))
        )));
    }

    public static class TestPosTagger extends JCasConsumer_ImplBase {

        @Override
        public void process(JCas aJCas) throws AnalysisEngineProcessException {

        }
    }

}
