package de.schrieveslaach.nlpf.maven.plugin.service;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.apache.maven.artifact.Artifact;
import org.apache.maven.model.Build;
import org.apache.maven.project.MavenProject;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.HashSet;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.instanceOf;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class ClassLoaderServiceTest {

    @InjectMocks
    private ClassLoaderService service;

    @Mock
    private MavenProject mavenProject;

    @Before
    public void setUpMavenProject() {
        Build build = mock(Build.class);
        when(build.getOutputDirectory()).thenReturn(new File(".").getAbsolutePath());
        when(mavenProject.getBuild()).thenReturn(build);
    }

    @Test
    public void shouldReturnClassLoader_WithCompileArtifacts() throws Exception {
        mockArtifact("compile");

        ClassLoader dependencyClassLoader = service.getDependencyClassLoader();
        assertThat(dependencyClassLoader, instanceOf(URLClassLoader.class));

        URLClassLoader classLoader = (URLClassLoader) dependencyClassLoader;
        assertThat(asList(classLoader.getURLs()), hasItem(new URL("file:/tmp/test.jar")));
    }

    @Test
    public void shouldReturnClassLoader_WithSystemArtifacts() throws Exception {
        mockArtifact("system");

        ClassLoader dependencyClassLoader = service.getDependencyClassLoader();
        assertThat(dependencyClassLoader, instanceOf(URLClassLoader.class));

        URLClassLoader classLoader = (URLClassLoader) dependencyClassLoader;
        assertThat("has only build directory as resource", asList(classLoader.getURLs()), hasSize(1));
    }

    @Test
    public void shouldReturnClassLoader_WithProvidedArtifact() throws Exception {
        mockArtifact("provided");

        ClassLoader dependencyClassLoader = service.getDependencyClassLoader();
        assertThat(dependencyClassLoader, instanceOf(URLClassLoader.class));

        URLClassLoader classLoader = (URLClassLoader) dependencyClassLoader;
        assertThat(asList(classLoader.getURLs()), hasItem(new URL("file:/tmp/test.jar")));
    }

    private void mockArtifact(String scope) {
        Set<Artifact> artifacts = new HashSet<>();
        Artifact artifact = mock(Artifact.class);
        when(artifact.getScope()).thenReturn(scope);
        when(artifact.getFile()).thenReturn(new File("/tmp/test.jar"));
        artifacts.add(artifact);
        when(mavenProject.getArtifacts()).thenReturn(artifacts);
    }
}
