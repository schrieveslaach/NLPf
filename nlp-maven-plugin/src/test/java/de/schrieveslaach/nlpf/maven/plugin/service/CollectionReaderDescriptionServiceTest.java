package de.schrieveslaach.nlpf.maven.plugin.service;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.io.combination.CombinationReader;
import lombok.SneakyThrows;
import org.apache.uima.collection.CollectionReaderDescription;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class CollectionReaderDescriptionServiceTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @InjectMocks
    private CollectionReaderDescriptionService service;

    @Mock
    private DirectoryService directoryService;

    @Mock
    private ClassLoaderService classLoaderService;

    @Before
    public void mockClassLoaderService() {
        when(classLoaderService.getDependencyClassLoader()).thenReturn(getClass().getClassLoader());
    }

    @Before
    @SneakyThrows
    public void mockDirectoryService() {
        when(directoryService.getCorpusDirectory()).thenReturn(folder.newFolder("src", "main", "corpus"));
        when(directoryService.getTestCorpusDirectory()).thenReturn(folder.newFolder("src", "test", "corpus"));
    }

    @Test
    public void shouldNotCreateReaderDescription_CorpusDirectoryIsMissing() throws Exception {
        File corpusDirectory = new File(folder.getRoot(), "src/main/corpus");
        corpusDirectory.delete();

        expectedException.expectMessage("The corpus directory is missing. Create the directory src/main/corpus");
        service.createReaderDescriptions();
    }

    @Test
    public void shouldCreateReaderDescription() throws Exception {
        folder.newFile("src/main/corpus/Test1.xmi");
        folder.newFile("src/main/corpus/Test2.xmi");

        CollectionReaderDescription readerDescriptions = service.createReaderDescriptions();

        assertThat(readerDescriptions, isCombinationReader());
        assertThat(readerDescriptions, hasReaderFiles(2));
    }

    @Test
    public void shouldNotCreateTestReaderDescription_TestCorpusDirectoryIsMissing() throws Exception {
        File corpusDirectory = new File(folder.getRoot(), "src/test/corpus");
        corpusDirectory.delete();

        expectedException.expectMessage("The corpus test directory is missing. Create the directory src/test/corpus");
        service.createTestReaderDescriptions();
    }

    @Test
    public void shouldCreateTestReaderDescription() throws Exception {
        folder.newFile("src/test/corpus/Test1.xmi");
        folder.newFile("src/test/corpus/Test2.xmi");

        CollectionReaderDescription readerDescriptions = service.createTestReaderDescriptions();

        assertThat(readerDescriptions, isCombinationReader());
        assertThat(readerDescriptions, hasReaderFiles(2));
    }

    @Test
    public void shouldNotCreateReaderDescriptions_NoReadersForExtensions() throws Exception {
        folder.newFile("src/main/corpus/Test1.abc");
        folder.newFile("src/main/corpus/Test2.abc");

        CollectionReaderDescription readerDescriptions = service.createReaderDescriptions();

        assertThat(readerDescriptions, isCombinationReader());
        assertThat(readerDescriptions, hasReaderFiles(0));
    }

    @Test
    public void shouldNotCreateReaderDescriptions_NoFiles() throws Exception {
        CollectionReaderDescription readerDescriptions = service.createReaderDescriptions();

        assertThat(readerDescriptions, isCombinationReader());
        assertThat(readerDescriptions, hasReaderFiles(0));
    }

    public Matcher<CollectionReaderDescription> isCombinationReader() {
        return new TypeSafeMatcher<CollectionReaderDescription>() {

            @SneakyThrows
            @Override
            protected boolean matchesSafely(CollectionReaderDescription collectionReaderDescription) {
                Class<?> cls = Class.forName(collectionReaderDescription.getImplementationName());
                return CombinationReader.class.isAssignableFrom(cls);
            }

            @Override
            public void describeTo(Description description) {

            }
        };
    }

    public Matcher<CollectionReaderDescription> hasReaderFiles(final int cnt) {
        return new TypeSafeMatcher<CollectionReaderDescription>() {
            @Override
            protected boolean matchesSafely(CollectionReaderDescription collectionReaderDescription) {
                Object readers = collectionReaderDescription.getMetaData().getConfigurationParameterSettings().getParameterValue("readers");
                return readers instanceof String[] && ((String[]) readers).length == cnt;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has ").appendValue(cnt).appendText(" internal readers");
            }

            @Override
            protected void describeMismatchSafely(CollectionReaderDescription collectionReaderDescription, Description mismatchDescription) {
                Object readers = collectionReaderDescription.getMetaData().getConfigurationParameterSettings().getParameterValue("readers");
                if (!(readers instanceof String[])) {
                    mismatchDescription.appendText("readers are not properly set: ").appendValue(readers);
                } else {
                    mismatchDescription.appendText("has ").appendValue(((String[]) readers).length).appendText(" internal readers");
                }
            }
        };
    }
}
