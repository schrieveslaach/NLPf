package de.schrieveslaach.nlpf.maven.plugin.service;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.testing.annotators.MySegmenter;
import org.apache.maven.model.Build;
import org.apache.maven.project.MavenProject;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.MockitoJUnitRunner;

import java.io.File;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.io.FileMatchers.aFileWithAbsolutePath;
import static org.hamcrest.io.FileMatchers.anExistingFile;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class DirectoryServiceTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    @InjectMocks
    private DirectoryService directoryService;

    @Mock
    private MavenProject mavenProject;

    private Build build;

    @Before
    public void mockMavenBuild() {
        build = mock(Build.class);
        when(mavenProject.getBuild()).thenReturn(build);
    }

    @Test
    public void shouldReturnCorpusDirectory() {
        when(mavenProject.getBasedir()).thenReturn(folder.getRoot());

        File corpusDirectory = directoryService.getCorpusDirectory();

        assertThat(corpusDirectory, is(aFileWithAbsolutePath(endsWith("src/main/corpus"))));
    }

    @Test
    public void shouldReturnTestCorpusDirectory() {
        when(mavenProject.getBasedir()).thenReturn(folder.getRoot());

        File corpusDirectory = directoryService.getTestCorpusDirectory();

        assertThat(corpusDirectory, is(aFileWithAbsolutePath(endsWith("src/test/corpus"))));
    }

    @Test
    public void shouldReturnModelTargetLocation_DefaultVariant() throws Exception {
        when(build.getDirectory()).thenReturn(folder.newFolder("target").toString());
        directoryService.setLanguage("de");

        when(mavenProject.getGroupId()).thenReturn("de.schrieveslaach");
        when(mavenProject.getArtifactId()).thenReturn("test-corpus");

        File modelLocation = directoryService.getModelTargetLocation(getClass().getPackage(), "sent", null);

        assertThat(modelLocation, is(aFileWithAbsolutePath(endsWith(
                "target/models/de.schrieveslaach/test-corpus/de.schrieveslaach.nlpf.maven.plugin.service/sent-de-default.model"))
        ));
    }

    @Test
    public void shouldReturnModelTargetLocation_NoneDefaultVariant() throws Exception {
        when(build.getDirectory()).thenReturn(folder.newFolder("target").toString());
        directoryService.setLanguage("de");

        when(mavenProject.getGroupId()).thenReturn("de.schrieveslaach");
        when(mavenProject.getArtifactId()).thenReturn("test-corpus");

        File modelLocation = directoryService.getModelTargetLocation(getClass().getPackage(), "sent", "variant");

        assertThat(modelLocation, is(aFileWithAbsolutePath(endsWith(
                "target/models/de.schrieveslaach/test-corpus/de.schrieveslaach.nlpf.maven.plugin.service/sent-de-variant.model"))
        ));
    }

    @Test
    public void shouldReturnJarFile() throws Exception {
        when(build.getFinalName()).thenReturn("test-corpus-1.0");
        when(build.getDirectory()).thenReturn(folder.newFolder("target").toString());

        File jarFile = directoryService.getJarFile();

        assertThat(jarFile, is(aFileWithAbsolutePath(endsWith("target/test-corpus-1.0.jar"))));
    }

    @Test
    public void shouldReturnDescriptorsDirectory() throws Exception {
        when(build.getDirectory()).thenReturn(folder.newFolder("target").toString());

        File descriptorsDirectory = directoryService.getDescriptorsDirectory();

        assertThat(descriptorsDirectory, is(aFileWithAbsolutePath(endsWith("target/descriptors"))));
    }

    @Test
    public void shouldReturnPipelineTestDataDirectory() throws Exception {
        when(build.getDirectory()).thenReturn(folder.newFolder("target").toString());

        File testDataDirectory = directoryService.getPipelinesTestDataDirectory();

        assertThat(testDataDirectory, is(aFileWithAbsolutePath(endsWith("target/nlp-pipelines-test-data"))));
    }

    @Test
    public void shouldReturnIsolatedToolsTestDataDirectory() throws Exception {
        when(build.getDirectory()).thenReturn(folder.newFolder("target").toString());

        File testDataDirectory = directoryService.getIsolatedToolsTestDataDirectory();

        assertThat(testDataDirectory, is(aFileWithAbsolutePath(endsWith("target/isolated-tools-test-data"))));
    }

    @Test
    public void shouldStoreEngineDescription() throws Exception {
        when(build.getDirectory()).thenReturn(folder.newFolder("target").toString());

        AnalysisEngineDescription engineDescription = createEngineDescription(MySegmenter.class);
        directoryService.store(engineDescription);

        File descriptorFile = new File(folder.getRoot(), String.format("target/descriptors/%s.xml", MySegmenter.DEFAULT_HASH_CODE));
        assertThat(descriptorFile, is(anExistingFile()));
    }
}
