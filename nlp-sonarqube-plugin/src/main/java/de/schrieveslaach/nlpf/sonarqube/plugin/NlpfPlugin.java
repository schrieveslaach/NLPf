package de.schrieveslaach.nlpf.sonarqube.plugin;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotationsComputer;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotationsSensor;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.IsolatedToolsTestSensor;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.NerHeatmapComputer;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.NerHeatmapSensor;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.NlpToolMetrics;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.NumbersMeasureComputer;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.NumbersSensor;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.PipelineDescriptionSensor;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.PipelineMeasuresSensor;
import de.schrieveslaach.nlpf.sonarqube.plugin.measures.PipelineMetrics;
import de.schrieveslaach.nlpf.sonarqube.plugin.web.NaturalLanguageProcessingPageDefinition;
import org.sonar.api.Plugin;

public class NlpfPlugin implements Plugin {

    @Override
    public void define(Context context) {
        // Corpus related measures
        context.addExtension(AnnotatedCorpusMetrics.class);
        context.addExtensions(NumbersSensor.class, NumbersMeasureComputer.class);
        context.addExtensions(AnnotationsSensor.class, AnnotationsComputer.class);
        context.addExtensions(NerHeatmapSensor.class, NerHeatmapComputer.class);
        context.addExtensions(NlpToolMetrics.class, IsolatedToolsTestSensor.class);

        // Pipeline related measures
        context.addExtensions(PipelineMetrics.class, PipelineMeasuresSensor.class, PipelineDescriptionSensor.class);

        // Web
        context.addExtension(NaturalLanguageProcessingPageDefinition.class);
    }
}
