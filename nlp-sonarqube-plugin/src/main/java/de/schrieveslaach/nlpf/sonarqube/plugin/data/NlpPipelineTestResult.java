package de.schrieveslaach.nlpf.sonarqube.plugin.data;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.google.common.collect.LinkedHashMultimap;
import com.google.common.collect.Multimap;
import lombok.Cleanup;
import lombok.Getter;
import org.apache.uima.UIMAException;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.apache.uima.util.XMLParser;
import org.sonar.api.measures.Metric;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class NlpPipelineTestResult {

    @Getter
    @JsonProperty
    private List<String> descriptors;

    @JsonProperty
    private List<NlpToolMeasure> measures;

    public Multimap<Metric, NlpToolMeasure> mapNlpToolMeasuresToMetrics(List<Metric> metrics) {
        Multimap<Metric, NlpToolMeasure> mapping = LinkedHashMultimap.create();

        for (NlpToolMeasure measure : measures) {
            for (Metric m : metrics) {
                if (measure.matches(m)) {
                    mapping.put(m, measure);
                }
            }
        }

        return mapping;
    }

    public NlpPipelineDescription createPipelineDescription(File descriptorsDirectory) throws UIMAException {
        List<AnalysisEngineDescription> aeds = new ArrayList<>();

        for (String engineHash : descriptors) {
            File engineDescriptionFile = new File(descriptorsDirectory, engineHash);
            aeds.add(readEngineDescription(engineDescriptionFile));
        }

        return new NlpPipelineDescription(aeds);
    }

    private AnalysisEngineDescription readEngineDescription(File engineDescriptionFile) throws UIMAException {
        XMLParser xmlParser = UIMAFramework.getXMLParser();

        try {
            @Cleanup
            XMLInputSource s = new XMLInputSource(engineDescriptionFile);
            return xmlParser.parseAnalysisEngineDescription(s);
        } catch (IOException | InvalidXMLException e) {
            throw new ResourceInitializationException("Could not read engine description from " + engineDescriptionFile, new Object[]{}, e);
        }
    }
}
