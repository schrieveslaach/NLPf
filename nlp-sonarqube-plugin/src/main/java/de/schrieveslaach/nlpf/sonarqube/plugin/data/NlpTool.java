package de.schrieveslaach.nlpf.sonarqube.plugin.data;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.metadata.NameValuePair;

import java.util.ArrayList;
import java.util.List;

public class NlpTool {

    @JsonProperty
    private final String name;

    @JsonProperty
    private final String variant;

    @JsonProperty
    private final List<String> models = new ArrayList<>();

    public NlpTool(AnalysisEngineDescription aed) {
        this.name = aed.getMetaData().getName();

        Object variantParameter = aed.getMetaData().getConfigurationParameterSettings().getParameterValue(ComponentParameters.PARAM_VARIANT);
        if (variantParameter != null) {
            this.variant = variantParameter.toString();
        } else {
            this.variant = null;
        }

        for (NameValuePair pair : aed.getMetaData().getConfigurationParameterSettings().getParameterSettings()) {
            if (pair.getName().toLowerCase().endsWith(ComponentParameters.PARAM_MODEL_LOCATION.toLowerCase())) {
                models.add(pair.getValue().toString());
            }
        }
    }
}
