package de.schrieveslaach.nlpf.sonarqube.plugin.data;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.core.TreeNode;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonDeserializer;
import com.fasterxml.jackson.databind.annotation.JsonDeserialize;
import com.fasterxml.jackson.databind.node.NumericNode;
import com.fasterxml.jackson.databind.node.TextNode;
import lombok.AllArgsConstructor;
import org.sonar.api.measures.Metric;

import java.io.IOException;
import java.util.Objects;

@AllArgsConstructor
@JsonDeserialize(using = NlpToolMeasure.Deserializer.class)
public class NlpToolMeasure {

    @JsonProperty
    private final String outputType;

    @JsonProperty
    private final String variant;

    @JsonProperty("f-measure")
    private final double fMeasure;

    @JsonProperty
    private final String analysisEngineName;

    public boolean matches(Metric metric) {
        return Objects.equals(outputType, metric.key());
    }

    public static class Deserializer extends JsonDeserializer<NlpToolMeasure> {

        @Override
        public NlpToolMeasure deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {
            TreeNode treeNode = jsonParser.readValueAsTree();

            String outputType = ((TextNode) treeNode.at("/outputType")).asText();
            String variant;
            String analysisEngineName;

            TreeNode variantNode = treeNode.at("/variant");
            if (variantNode instanceof TextNode) {
                variant = ((TextNode) variantNode).asText();
            } else {
                variant = null;
            }

            TreeNode engineNameNode = treeNode.at("/analysisEngineName");
            if (engineNameNode instanceof TextNode) {
                analysisEngineName = ((TextNode) engineNameNode).asText();
            } else {
                analysisEngineName = null;
            }

            double fMeasure = ((NumericNode) treeNode.at("/measure/f-measure")).doubleValue();

            return new NlpToolMeasure(outputType, variant, fMeasure, analysisEngineName);
        }
    }
}
