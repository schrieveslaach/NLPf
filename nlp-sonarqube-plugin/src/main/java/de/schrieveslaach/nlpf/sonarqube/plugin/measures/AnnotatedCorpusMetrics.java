package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

import java.util.List;

import static java.util.Arrays.asList;

public class AnnotatedCorpusMetrics implements Metrics {

    public static final String ANNOTATED_CORPUS_DOMAIN = "Annotated Corpus";

    public static final Metric<Integer> NUMBER_OF_TOKENS = new Metric.Builder("tokens", "Number of Tokens", Metric.ValueType.INT)
            .setDescription("Numbers of tokens contained in annotated files")
            .setDirection(Metric.DIRECTION_BETTER)
            .setQualitative(true)
            .setDomain(ANNOTATED_CORPUS_DOMAIN)
            .create();

    public static final Metric<String> ANNOTATIONS = new Metric.Builder("annotations", "Annotations", Metric.ValueType.DATA)
            .setDescription("Annotations contained in a document")
            .setDomain(ANNOTATED_CORPUS_DOMAIN)
            .create();

    public static final Metric<String> NUMBER_OF_NAMED_ENTITIES_PER_VARIANT = new Metric.Builder("named-entity-variants", "Number of Named Entities per variant", Metric.ValueType.DATA)
            .setDescription("Number of Named Entities per variant")
            .setDomain(ANNOTATED_CORPUS_DOMAIN)
            .create();

    @Override
    public List<Metric> getMetrics() {
        return asList(NUMBER_OF_TOKENS, ANNOTATIONS, NUMBER_OF_NAMED_ENTITIES_PER_VARIANT);
    }
}
