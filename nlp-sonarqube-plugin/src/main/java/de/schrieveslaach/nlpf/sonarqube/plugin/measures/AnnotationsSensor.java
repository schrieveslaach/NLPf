package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.sonar.api.batch.fs.InputFile;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.SensorDescriptor;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics.ANNOTATIONS;
import static org.apache.uima.fit.util.JCasUtil.select;

public class AnnotationsSensor extends JCasSensor {

    private final ObjectMapper mapper = new ObjectMapper();

    @Override
    public void describe(SensorDescriptor descriptor) {
        descriptor.name("Annotations Sensor");
    }

    @Override
    @SneakyThrows(IOException.class)
    protected void execute(SensorContext context, InputFile inputFile, JCas jCas) {
        List<String> documentAnnotations = select(jCas, Annotation.class)
                .stream()
                .map(this::getAnnotationBaseClass)
                .map(Class::getCanonicalName)
                .distinct()
                .sorted()
                .collect(Collectors.toList());

        context.<String>newMeasure()
                .forMetric(ANNOTATIONS)
                .on(inputFile)
                .withValue(mapper.writeValueAsString(documentAnnotations))
                .save();
    }

    private Class<? extends Annotation> getAnnotationBaseClass(Annotation a) {
        Class<? extends Annotation> superCls = a.getClass();
        while (!superCls.getSuperclass().equals(Annotation.class)) {
            superCls = (Class<? extends Annotation>) superCls.getSuperclass();
        }
        return superCls;
    }

}
