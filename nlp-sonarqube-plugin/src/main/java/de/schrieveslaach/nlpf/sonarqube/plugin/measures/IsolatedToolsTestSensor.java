package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import de.schrieveslaach.nlpf.sonarqube.plugin.data.NlpPipelineTestResult;
import lombok.SneakyThrows;
import org.sonar.api.batch.sensor.Sensor;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.SensorDescriptor;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.NlpToolMetrics.ISOLATED_TOOLS_TEST;
import static java.util.Arrays.sort;

public class IsolatedToolsTestSensor implements Sensor {

    @Override
    public void describe(SensorDescriptor descriptor) {
        descriptor.name("isolated NLP tool tests");
    }

    @Override
    @SneakyThrows(IOException.class)
    public void execute(SensorContext context) {
        File toolsTestDataDirectory = new File(new File(context.fileSystem().baseDir(), "target"), "isolated-tools-test-data");
        if (!toolsTestDataDirectory.isDirectory()) {
            return;
        }

        File[] files = toolsTestDataDirectory.listFiles();
        sort(files);

        List<NlpPipelineTestResult> testResults = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        for (File testDataFile : files) {
            testResults.add(mapper.readValue(testDataFile, NlpPipelineTestResult.class));
        }

        context.<String>newMeasure()
                .forMetric(ISOLATED_TOOLS_TEST)
                .withValue(mapper.writeValueAsString(testResults))
                .on(context.module())
                .save();
    }
}
