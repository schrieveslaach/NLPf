package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.io.odt.OdtReader;
import de.schrieveslaach.nlpf.plumbing.util.Uima;
import lombok.SneakyThrows;
import org.apache.uima.UIMAException;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.jcas.JCas;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.fs.InputFile;
import org.sonar.api.batch.sensor.Sensor;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.utils.log.Logger;
import org.sonar.api.utils.log.Loggers;

import static org.apache.uima.fit.factory.CollectionReaderFactory.createReaderDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;

public abstract class JCasSensor implements Sensor {

    private static final Logger LOGGER = Loggers.get(JCasSensor.class);

    @Override
    @SneakyThrows({UIMAException.class})
    public void execute(SensorContext context) {
        Uima.swallowLogging();

        FileSystem fs = context.fileSystem();

        ClassLoader previousContextClassLoader = Thread.currentThread().getContextClassLoader();

        try {
            Thread.currentThread().setContextClassLoader(getClass().getClassLoader());

            for (InputFile i : fs.inputFiles(fs.predicates().hasType(InputFile.Type.MAIN))) {

                if (!i.filename().endsWith(".odt")) {
                    continue;
                }

                LOGGER.debug("Determine JCas annotations for {}", i);

                CollectionReaderDescription readerDescription = createReaderDescription(
                        OdtReader.class,
                        OdtReader.PARAM_SOURCE_LOCATION, i.toString()
                );

                for (JCas jCas : iteratePipeline(readerDescription)) {
                    execute(context, i, jCas);
                }
            }
        } finally {
            Thread.currentThread().setContextClassLoader(previousContextClassLoader);
        }
    }

    protected abstract void execute(SensorContext context, InputFile inputFile, JCas jCas);
}
