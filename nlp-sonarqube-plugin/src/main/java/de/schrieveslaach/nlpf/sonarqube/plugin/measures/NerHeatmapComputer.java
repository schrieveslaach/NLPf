package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.schrieveslaach.nlpf.sonarqube.plugin.data.NerHeatmapData;
import lombok.SneakyThrows;
import org.sonar.api.ce.measure.Component;
import org.sonar.api.ce.measure.Measure;
import org.sonar.api.ce.measure.MeasureComputer;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics.NUMBER_OF_NAMED_ENTITIES_PER_VARIANT;

public class NerHeatmapComputer implements MeasureComputer {

    @Override
    public MeasureComputerDefinition define(MeasureComputerDefinitionContext defContext) {
        return defContext.newDefinitionBuilder()
                .setOutputMetrics(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT.key())
                .build();
    }

    @Override
    @SneakyThrows(IOException.class)
    public void compute(MeasureComputerContext context) {
        if (context.getComponent().getType() != Component.Type.FILE) {
            ObjectMapper mapper = new ObjectMapper();

            Map<String, NerHeatmapData> heatmapData = new HashMap<>();
            for (Measure child : context.getChildrenMeasures(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT.key())) {
                List<NerHeatmapData> datas = mapper.readValue(child.getStringValue(), new TypeReference<List<NerHeatmapData>>() {
                });

                for (NerHeatmapData data : datas) {
                    if (heatmapData.computeIfPresent(data.getVariant(), (s, nerHeatmapData) -> nerHeatmapData.join(data)) == null) {
                        heatmapData.put(data.getVariant(), data);
                    }
                }
            }

            context.addMeasure(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT.key(),
                    mapper.writeValueAsString(heatmapData.values()));
        }
    }
}
