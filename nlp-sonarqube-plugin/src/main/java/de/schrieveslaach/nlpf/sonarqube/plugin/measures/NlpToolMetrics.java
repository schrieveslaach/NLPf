package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

import java.util.List;

import static java.util.Arrays.asList;

public class NlpToolMetrics implements Metrics {

    private static final String DOMAIN = "NLP Tools";

    public static final Metric<String> ISOLATED_TOOLS_TEST = new Metric.Builder("isolated-tools-test-data", "Tests of Isolated Tools", Metric.ValueType.DATA)
            .setDescription("Test results of isolated NLP tools execution")
            .setDomain(DOMAIN)
            .create();

    @Override
    public List<Metric> getMetrics() {
        return asList(ISOLATED_TOOLS_TEST);
    }
}
