package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */


import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import org.apache.uima.jcas.JCas;
import org.sonar.api.batch.fs.InputFile;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.SensorDescriptor;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics.NUMBER_OF_TOKENS;
import static org.apache.uima.fit.util.JCasUtil.select;

public class NumbersSensor extends JCasSensor {

    @Override
    public void describe(SensorDescriptor descriptor) {
        descriptor.name("Sensor Corpus Size");
    }

    @Override
    public void execute(SensorContext context, InputFile i, JCas jCas) {
        int cnt = select(jCas, Token.class).size();

        context.<Integer>newMeasure()
                .forMetric(NUMBER_OF_TOKENS)
                .on(i)
                .withValue(cnt)
                .save();
    }

}
