package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import de.schrieveslaach.nlpf.sonarqube.plugin.data.NlpPipelineDescription;
import de.schrieveslaach.nlpf.sonarqube.plugin.data.NlpPipelineTestResult;
import lombok.SneakyThrows;
import org.apache.uima.UIMAException;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.SensorDescriptor;
import org.sonar.api.utils.log.Logger;
import org.sonar.api.utils.log.Loggers;

import java.io.File;
import java.io.IOException;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.PipelineMetrics.BEST_PERFORMING_NLP_PIPELINE_DESCRIPTION;

public class PipelineDescriptionSensor extends PipelineSensor {

    private static final Logger LOGGER = Loggers.get(PipelineDescriptionSensor.class);

    @Override
    public void describe(SensorDescriptor descriptor) {
        descriptor.name("Pipeline Name Sensor");
    }

    @Override
    @SneakyThrows(IOException.class)
    protected void execute(SensorContext context, File[] testDataFiles) {
        File descriptorsDirectory = new File(new File(context.fileSystem().baseDir(), "target"), "descriptors");
        if (!descriptorsDirectory.isDirectory()) {
            return;
        }

        ObjectMapper mapper = new ObjectMapper();
        NlpPipelineTestResult testResult = mapper.readValue(testDataFiles[0], NlpPipelineTestResult.class);
        try {
            NlpPipelineDescription description  = testResult.createPipelineDescription(descriptorsDirectory);

            context.<String>newMeasure()
                    .forMetric(BEST_PERFORMING_NLP_PIPELINE_DESCRIPTION)
                    .on(context.module())
                    .withValue(mapper.writeValueAsString(description))
                    .save();
        } catch (UIMAException e) {
            LOGGER.error("Could not read pipeline description", e);
        }
    }
}
