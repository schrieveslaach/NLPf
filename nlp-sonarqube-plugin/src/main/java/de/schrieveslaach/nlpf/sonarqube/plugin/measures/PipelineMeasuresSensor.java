package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.LinkedListMultimap;
import com.google.common.collect.Multimap;
import de.schrieveslaach.nlpf.sonarqube.plugin.data.NlpPipelineTestResult;
import de.schrieveslaach.nlpf.sonarqube.plugin.data.NlpToolMeasure;
import lombok.SneakyThrows;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.SensorDescriptor;
import org.sonar.api.measures.Metric;

import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.List;

public class PipelineMeasuresSensor extends PipelineSensor {

    private final PipelineMetrics metrics;

    public PipelineMeasuresSensor(PipelineMetrics metrics) {
        this.metrics = metrics;
    }

    @Override
    public void describe(SensorDescriptor descriptor) {
        descriptor.name("Pipeline Measures Sensor");
    }

    @SneakyThrows(IOException.class)
    protected void execute(SensorContext context, File[] testDataFiles) {

        Multimap<Metric, NlpToolMeasure> metricNlpToolMeasureMultimap = LinkedListMultimap.create();
        ObjectMapper mapper = new ObjectMapper();
        List<Metric> pipelineMetrics = metrics.getMetrics();

        for (File testDataFile : testDataFiles) {
            NlpPipelineTestResult testResult = mapper.readValue(testDataFile, NlpPipelineTestResult.class);
            metricNlpToolMeasureMultimap.putAll(testResult.mapNlpToolMeasuresToMetrics(pipelineMetrics));
        }

        for (Metric m : metricNlpToolMeasureMultimap.keySet()) {

            Collection<NlpToolMeasure> measures = metricNlpToolMeasureMultimap.get(m);
            String metricData = mapper.writeValueAsString(measures);

            context.<String>newMeasure()
                    .forMetric(m)
                    .on(context.module())
                    .withValue(metricData)
                    .save();
        }
    }

}
