package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.reflect.ClassPath;
import lombok.SneakyThrows;
import org.apache.uima.jcas.tcas.Annotation;
import org.sonar.api.measures.Metric;
import org.sonar.api.measures.Metrics;

import java.io.IOException;
import java.util.List;
import java.util.stream.Collectors;

public class PipelineMetrics implements Metrics {

    private static final String DOMAIN = "NLP Pipelines";

    public static final Metric<String> BEST_PERFORMING_NLP_PIPELINE_DESCRIPTION = new Metric.Builder("best-performing-pipeline-description", "Name of Best-performing NLP Pipeline", Metric.ValueType.DATA)
            .setDescription("the description data, e. g. name, model, etc., of the best-performing nlp pipeline")
            .setDomain(DOMAIN)
            .create();

    @Override
    @SneakyThrows(IOException.class)
    public List<Metric> getMetrics() {
        List<Metric> metrics = ClassPath.from(getClass().getClassLoader())
                .getTopLevelClasses().stream()
                .filter(ci -> ci.getName().startsWith("de.tudarmstadt.ukp.dkpro.core.api"))
                .filter(ci -> ci.getName().length() <= 64)
                .map(ClassPath.ClassInfo::load)
                .filter(Annotation.class::isAssignableFrom)
                .filter(cls -> cls.getSuperclass() == Annotation.class)
                .map(cls -> new Metric.Builder(cls.getCanonicalName(), cls.getSimpleName(), Metric.ValueType.DATA)
                        .setDomain(DOMAIN)
                        .create())
                .collect(Collectors.toList());

        metrics.add(BEST_PERFORMING_NLP_PIPELINE_DESCRIPTION);

        return metrics;
    }
}
