package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.sonar.api.batch.sensor.Sensor;
import org.sonar.api.batch.sensor.SensorContext;

import java.io.File;

import static java.util.Arrays.sort;

public abstract class PipelineSensor implements Sensor {

    @Override
    public void execute(SensorContext context) {
        File nlpTestDataDirectory = new File(new File(context.fileSystem().baseDir(), "target"), "nlp-pipelines-test-data");
        if (!nlpTestDataDirectory.isDirectory()) {
            return;
        }

        File[] files = nlpTestDataDirectory.listFiles();
        sort(files);
        execute(context, files);
    }

    protected abstract void execute(SensorContext context, File[] testDataFiles);
}
