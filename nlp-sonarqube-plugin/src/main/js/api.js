/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import {getJSON} from 'sonar-request';

const Color = require('color');

export function stringToColor(str) {
    let hash = 0;
    for (let i = 0; i < str.length; i++) {
        hash = str.charCodeAt(i) + ((hash << 17) - hash);
    }
    let color = '#';
    for (let i = 0; i < 3; i++) {
        let value = (hash >> (i * 8)) & 0xFF;
        color += ('00' + value.toString(16)).substr(-2);
    }
    return Color(color);
}

function abbreviateClassname(className) {
    return className.substring(className.lastIndexOf('.') + 1);
}

function paintDataSetBasedOnLabelString(dataSet) {
    let color = stringToColor(dataSet.label);
    dataSet.backgroundColor = color.hsl().string();
    dataSet.borderColor = color.lighten(0.2).hsl().string();
    dataSet.pointBorderColor = color.darken(0.2).hsl().string();
    dataSet.pointHoverBackgroundColor = color.hsl().string();
    dataSet.pointHoverBorderColor = color.hsl().string();
}

export function findTokenHistory(project) {
    return getJSON('/api/measures/search_history?component=' + project.key + '&metrics=tokens').then(function (response) {
        const tokenHistory = response.measures[0].history;

        const tokenHistoryChartJsData = [];
        for (let i = 0; i < tokenHistory.length; i++) {
            tokenHistoryChartJsData.push({
                x: tokenHistory[i].date,
                y: tokenHistory[i].value,
            });
        }

        //History date format: YYYY-MM-DDTHH:mm:ssZ
        tokenHistoryChartJsData.sort(function (a, b) {
            return new Date(b.x) - new Date(a.x);
        });

        return tokenHistoryChartJsData;
    });
}

export function findNlpPipelinePerformanceDataSets(project, dataSetConfiguration) {
    return getJSON(`/api/measures/search_history?component=${project.key}&metrics=annotations`).then(function (response) {
        const annotationsHistory = response.measures[0].history;

        const annotations = new Set();
        for (let i = 0; i < annotationsHistory.length; i++) {
            JSON.parse(annotationsHistory[i].value).forEach(function (annotation) {
                annotations.add(annotation);
            });
        }

        const dataSets = [];
        annotations.forEach(annotation => {

            let dataSet = {
                ...dataSetConfiguration,
                label: abbreviateClassname(annotation),
                annotationClassName: annotation,
                yAxisID: 'measures',
                data: [],
                fill: false
            };

            paintDataSetBasedOnLabelString(dataSet);

            dataSets.push(dataSet);
        });

        return dataSets;
    }).then((dataSets) => {
        let promises = [];

        // Loads for each dataSet the time series of F-measures
        dataSets.forEach(dataSet => {
            promises.push(findBestPerformingPipelineFMeasure(project, dataSet.annotationClassName)
                .then(fMeasureHistoryData => {
                    dataSet.data = fMeasureHistoryData;
                    return dataSet;
                }).catch(err => {
                    console.log(err);
                }));
        });

        return Promise.all(promises)
            .then(values => values.filter(data => data !== undefined));
    });
}

function findBestPerformingPipelineFMeasure(project, annotation) {
    return getJSON(`/api/measures/search_history?component=${project.key}&metrics=${annotation}`)
        .then(response => {
            if (response.errors !== undefined) {
                return [];
            }

            const fMeasureHistory = response.measures[0].history;

            let fMeasureHistoryChartJsData = [];
            for (let i = 0; i < fMeasureHistory.length; i++) {
                let value = fMeasureHistory[i].value;

                if (value === undefined) {
                    continue;
                }

                let data = JSON.parse(value);

                fMeasureHistoryChartJsData.push({
                    x: fMeasureHistory[i].date,
                    // TODO currently returns only data for best-performing NLP pipeline
                    y: data[0]["f-measure"],
                });
            }

            //History date format: YYYY-MM-DDTHH:mm:ssZ
            fMeasureHistoryChartJsData.sort((a, b) => new Date(b.x) - new Date(a.x));

            return fMeasureHistoryChartJsData;
        });
}

export function findNerHeatmapData(project) {
    return getJSON(`/api/measures/component?component=${project.key}&metricKeys=named-entity-variants`)
        .then(response => {
            if (response.errors !== undefined) {
                return [];
            }

            return JSON.parse(response.component.measures[0].value);
        }).then((namedEntityQuantities) => {
            return getJSON(`/api/measures/component?component=${project.key}&metricKeys=de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity`)
                .then(response => {
                    let fScores = JSON.parse(response.component.measures[0].value);

                    namedEntityQuantities.forEach(ne => {
                        fScores.forEach(fScore => {
                            if (ne["f-measure"] === undefined && fScore.variant === ne.variant) {
                                ne["f-measure"] = fScore["f-measure"];
                            }
                        })
                    });

                    return {
                        name: "NER Heatmap",
                        children: namedEntityQuantities
                    };
                });
        });
}

export function findIsolatedToolsTestData(project) {
    return getJSON(`/api/measures/component?component=${project.key}&metricKeys=isolated-tools-test-data`)
        .then(response => {
            if (response.errors !== undefined) {
                return [];
            }

            let toolsTestData = JSON.parse(response.component.measures[0].value);

            const toLabel = function (nlpToolMeasure) {
                if (nlpToolMeasure.variant === null) {
                    return abbreviateClassname(nlpToolMeasure.outputType);
                }
                return `${abbreviateClassname(nlpToolMeasure.outputType)} – ${nlpToolMeasure.variant}`;
            };

            const data = {};
            const annotations = new Set();
            toolsTestData.forEach(d => {
                d.measures.forEach(m => {
                    annotations.add(toLabel(m));
                });
            });

            data.labels = Array.from(annotations);

            let dataSets = {};
            toolsTestData.forEach(d => {
                d.measures.forEach(m => {

                    let dataSet = dataSets[m.analysisEngineName];
                    if (dataSet === undefined) {
                        dataSet = {
                            label: m.analysisEngineName,
                            backgroundColor: stringToColor(m.analysisEngineName).hsl().string(),
                            data: new Array(data.labels.length)
                        }
                        paintDataSetBasedOnLabelString(dataSet);
                        dataSets[m.analysisEngineName] = dataSet;
                    }

                    let i = data.labels.indexOf(toLabel(m));
                    dataSet.data[i] = m["f-measure"];
                });
            });

            data.datasets = Object.keys(dataSets).map(k => dataSets[k]);

            return data;
        });
}
