/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import React from 'react';
import {Bar} from 'react-chartjs-2';
import {findIsolatedToolsTestData} from "../api";

const options = {
    scales: {
        yAxes: [{
            type: 'linear',
            ticks: {
                max: 1,
                min: 0
            }
        }]
    }
};

export default class IsolatedToolsGraph extends React.PureComponent {

    state = {};

    componentWillMount() {
    }

    componentDidMount() {
        findIsolatedToolsTestData(this.props.project).then(isolatedToolsTestData => {
            this.setState(isolatedToolsTestData);
        });
    };

    /*
     * Display the information.
     */
    render() {
        return (
            <div>
                <div className="header">
                    <h1>NLP Tool Test Score</h1>
                    <h6>Tested NLP Tools on Perfect Input Data</h6>
                </div>
                <div>
                    <Bar data={this.state} options={options}/>
                </div>
            </div>
        );
    }
}
