/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */
import React from 'react';
import {hierarchy, treemap} from 'd3-hierarchy';
import {select} from 'd3-selection'
import {findNerHeatmapData} from '../api.js';

const Color = require('color');

function backgroundColor(fScore) {
    if (fScore < 0.4) {
        return Color('#B71C1C');
    }
    else if (fScore < 0.6) {
        return Color('#FF9800');
    }
    else if (fScore < 0.7) {
        return Color('#FFEB3B');
    }
    else if (fScore < 0.9) {
        return Color('#4CAF50');
    }
    else {
        return Color('#1B5E20');
    }
}

export default class NERHeatmap extends React.PureComponent {

    state = {};

    componentWillMount() {
    }

    componentDidMount() {
        findNerHeatmapData(this.props.project).then((d) => {
            let root = hierarchy(d)
                .sum((d) => {
                    return d.quantity;
                });

            let layout = treemap()
                .size([800, 400])
                .paddingOuter(10);

            let nodes = layout(root)
                .sum((d) => {
                    return d.quantity
                })
                .sort((a, b) => {
                    return b.height - a.height || b.value - a.value;
                })
                .leaves();

            let cells = select(this.node)
                .selectAll('g')
                .data(nodes)
                .enter()
                .append('g')
                .attr("transform", (d) => {
                    return "translate(" + d.x0 + "," + d.y0 + ")";
                });

            cells.append("svg:rect")
                .attr('width', (d) => {
                    return d.x1 - d.x0;
                })
                .attr('height', (d) => {
                    return d.y1 - d.y0;
                })
                .style("fill", (d) => {
                    return backgroundColor(d.data["f-measure"]).hsl().string();
                });

            cells.append("svg:text")
                .attr("x", function (d) {
                    return (d.x1 - d.x0) / 2;
                })
                .attr("y", function (d) {
                    return (d.y1 - d.y0) / 2;
                })
                .attr("dy", "10px")
                .attr("text-anchor", "middle")
                .attr("font-size", "22px")
                .text((d) => {
                    return d.data.variant;
                })
                .style("fill", (d) => {
                    return backgroundColor(d.data["f-measure"]).darken(0.8).hsl().string();
                })
                .attr('transform', (d) => {
                    let dx = d.x1 - d.x0;
                    let dy = d.y1 - d.y0;
                    return dx < dy ? "rotate(270 " + dx / 2 + " " + dy / 2 + ")" : "";
                })
                .style("opacity", (d) => {
                    let dx = d.x1 - d.x0;
                    let dy = d.y1 - d.y0;
                    d.w = this.getComputedTextLength();
                    if (dx < dy) {
                        return dy > d.w ? 1 : 0;
                    } else {
                        return dx > d.w ? 1 : 0;
                    }
                })

        });
    };

    /*
     * Display the information.
     */
    render() {
        return (
            <div>
                <div className="header">
                    <h1>NER Heatmap</h1>
                    <h6>Displays F-Score of each named entity in relation to the number of occurances</h6>
                </div>
                <div>
                    <svg width={800} height={400} ref={node => this.node = node}></svg>
                </div>
            </div>
        );
    }
}
