/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import React from 'react';
import {findNlpPipelinePerformanceDataSets, findTokenHistory} from "../api";
import {Line} from 'react-chartjs-2';

const defaultDataSetConfiguration = {
    fill: false,
    lineTension: 0.1,
    backgroundColor: 'rgba(75,192,192,0.4)',
    borderColor: 'rgba(75,192,192,1)',
    borderCapStyle: 'butt',
    borderDash: [],
    borderDashOffset: 0.0,
    borderJoinStyle: 'miter',
    pointBorderColor: 'rgba(75,192,192,1)',
    pointBackgroundColor: '#fff',
    pointBorderWidth: 1,
    pointHoverRadius: 5,
    pointHoverBackgroundColor: 'rgba(75,192,192,1)',
    pointHoverBorderColor: 'rgba(220,220,220,1)',
    pointHoverBorderWidth: 2,
    pointRadius: 2,
    pointHitRadius: 10,
}

const initialState = {
    datasets: [{
        ...defaultDataSetConfiguration,
        label: '# of Tokens',
        yAxisID: 'size',
        data: [],
    }],
};

const options = {
    scales: {
        xAxes: [{
            type: 'time',
            time: {
                displayFormats: {
                    day: 'YYYY-MM-DDTHH:mm:ssZ'
                }
            }
        }],
        yAxes: [{
            id: 'size',
            type: 'linear',
            position: 'left',
        }, {
            id: 'measures',
            type: 'linear',
            position: 'right',
            ticks: {
                max: 1,
                min: 0
            }
        }]
    }
};

export default class PerformanceGraph extends React.PureComponent {

    state = {};

    componentWillMount() {
    }

    /*
     * Retrieve all the method calls to get data from SonarQube.
     * Methods are defined in the api.js file.
     */
    componentDidMount() {
        findTokenHistory(this.props.project).then((tokenHistoryChartJsData) => {
            const oldDataSet = initialState.datasets[0];

            const newDataSet = {
                ...oldDataSet
            };
            newDataSet.data = tokenHistoryChartJsData;

            this.setState({
                datasets: [newDataSet]
            });
        }).then(() => {
            findNlpPipelinePerformanceDataSets(this.props.project)
                .then(performanceDataSets => {
                    const oldDataSet = this.state.datasets[0];
                    const newDataSets = [{
                        ...oldDataSet
                    }].concat(performanceDataSets.filter(dataSet => dataSet.data.length > 0));

                    this.setState({
                        datasets: newDataSets
                    });
                });
        })
    };

    /*
     * Display the information.
     */
    render() {
        return (
            <div>
                <div className="header">
                    <h1>Development of Corpus Size and NLP Pipeline Performance</h1>
                    <h6>Relation between corpus' growth and the NLP Pipeline performance</h6>
                </div>
                <div>
                    <Line data={this.state} options={options}/>
                </div>
            </div>
        );
    }
}
