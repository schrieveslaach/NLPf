/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import React from 'react';
import {render, unmountComponentAtNode} from 'react-dom';
import './styles.css';
import IsolatedToolsGraph from "./components/IsolatedToolsGraph";
import PerformanceGraph from "./components/PerformanceGraph";
import NERHeatmap from "./components/NERHeatmap";

window.registerExtension('nlpsonarqubeplugin/nlp_page_react', options => {

    const {el} = options;

    render(
        <div className="page page-limited">
            <div id="nlp-page">
                <PerformanceGraph project={options.component}/>
                <IsolatedToolsGraph project={options.component}/>
                <NERHeatmap project={options.component}/>
            </div>
        </div>
        , el
    );

    return () => unmountComponentAtNode(el);
});
