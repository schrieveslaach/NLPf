package de.schrieveslaach.nlpf.sonarqube.plugin.data;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.collect.Multimap;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.Person;
import lombok.SneakyThrows;
import org.junit.Test;
import org.sonar.api.measures.Metric;

import java.io.IOException;

import static de.schrieveslaach.nlpf.testing.JsonDataFactory.createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant;
import static java.util.Arrays.asList;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NlpPipelineTestResultTest {

    @SneakyThrows(IOException.class)
    public NlpPipelineTestResult load(String json) {
        ObjectMapper mapper = new ObjectMapper();
        return mapper.readValue(json, NlpPipelineTestResult.class);
    }

    @Test
    public void shouldMapMetricsToNlpToolMeasures() {
        Metric namedEntityMetric = mock(Metric.class);
        when(namedEntityMetric.key()).thenReturn(NamedEntity.class.getCanonicalName());
        Metric personMetric = mock(Metric.class);
        when(personMetric.key()).thenReturn(Person.class.getCanonicalName());

        NlpPipelineTestResult testResult = load(createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant());
        Multimap<Metric, NlpToolMeasure> measures = testResult.mapNlpToolMeasuresToMetrics(asList(namedEntityMetric, personMetric));

        assertThat(measures.asMap(), hasKey(namedEntityMetric));
        assertThat(measures.asMap(), not(hasKey(personMetric)));
        assertThat(measures.get(namedEntityMetric), hasSize(2));
    }


}
