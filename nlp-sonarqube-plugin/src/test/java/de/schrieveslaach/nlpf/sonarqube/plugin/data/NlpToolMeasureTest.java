package de.schrieveslaach.nlpf.sonarqube.plugin.data;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.sonar.api.measures.Metric;


import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class NlpToolMeasureTest {

    @Test
    public void shouldMatchToMetric_MetricKeyEqualsOutputType() {
        NlpToolMeasure measureForPerson = new NlpToolMeasure(NamedEntity.class.getCanonicalName(), "person", 0.8, "My NER");

        Metric metric = mock(Metric.class);
        when(metric.key()).thenReturn(NamedEntity.class.getCanonicalName());

        assertThat(measureForPerson, matches(metric));
    }

    @Test
    public void shouldNotMatchToMetric_MetricKeyNotEqualsOutputType() {
        NlpToolMeasure measureForPerson = new NlpToolMeasure(NamedEntity.class.getCanonicalName(), "person", 0.8, "My NER");

        Metric metric = mock(Metric.class);
        when(metric.key()).thenReturn(POS.class.getCanonicalName());

        assertThat(measureForPerson, not(matches(metric)));
    }

    private Matcher<NlpToolMeasure> matches(Metric metric) {
        return new TypeSafeMatcher<NlpToolMeasure>() {
            @Override
            protected boolean matchesSafely(NlpToolMeasure nlpToolMeasure) {
                return nlpToolMeasure.matches(metric);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("matches ").appendValue(metric.key());
            }
        };
    }
}
