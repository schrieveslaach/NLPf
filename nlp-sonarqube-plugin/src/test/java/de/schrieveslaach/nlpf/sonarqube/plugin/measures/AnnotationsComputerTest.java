package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.junit.Test;
import org.sonar.api.ce.measure.Component;
import org.sonar.api.ce.measure.Measure;
import org.sonar.api.ce.measure.MeasureComputer;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics.ANNOTATIONS;
import static java.util.Arrays.asList;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AnnotationsComputerTest {

    @Test
    public void shouldComputeMeasure_AggregateMultipleAnnotations() {
        AnnotationsComputer computer = new AnnotationsComputer();
        MeasureComputer.MeasureComputerContext context = mockMeasureComputerContext(Component.Type.PROJECT,
                asList("person", "organization"), asList("person"), asList("person"));

        computer.compute(context);

        verify(context).addMeasure(ANNOTATIONS.key(), "[\"organization\",\"person\"]");
    }

    @Test
    public void shouldSumUpNumberOfTokens_WithoutAnnotationsData() {
        AnnotationsComputer computer = new AnnotationsComputer();
        MeasureComputer.MeasureComputerContext context = mockMeasureComputerContext(Component.Type.PROJECT);

        computer.compute(context);

        verify(context).addMeasure(ANNOTATIONS.key(), "[]");
    }

    @Test
    public void shouldNotSumUpNumberOfTokens_MeauresOnFile() {
        AnnotationsComputer computer = new AnnotationsComputer();
        MeasureComputer.MeasureComputerContext context = mockMeasureComputerContext(Component.Type.FILE, asList("person"));

        computer.compute(context);

        verify(context, never()).addMeasure(eq(ANNOTATIONS.key()), anyString());
    }

    @SneakyThrows(IOException.class)
    private MeasureComputer.MeasureComputerContext mockMeasureComputerContext(Component.Type type, List<String>... types) {
        MeasureComputer.MeasureComputerContext context = mock(MeasureComputer.MeasureComputerContext.class);

        Component component = mock(Component.class);
        when(component.getType()).thenReturn(type);
        when(context.getComponent()).thenReturn(component);

        ObjectMapper mapper = new ObjectMapper();

        if (types.length > 0) {
            List<List<Measure>> measures = new ArrayList<>();
            for (List<String> t : types) {
                Measure measure = mock(Measure.class);
                when(measure.getStringValue()).thenReturn(mapper.writeValueAsString(t));
                measures.add(asList(measure));
            }
            when(context.getChildrenMeasures(ANNOTATIONS.key())).thenReturn(
                    measures.get(0),
                    measures.subList(1, measures.size()).toArray(new Iterable[measures.size()])
            );
        }

        return context;
    }
}
