package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.measure.NewMeasure;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics.ANNOTATIONS;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class AnnotationsSensorTest extends SensorTest {

    @Test
    public void shouldDetermineAnnotations_ForOpenNlpExample() {
        FileSystem fileSystem = storeOpenNlpExample();
        SensorContext context = mock(SensorContext.class);
        when(context.fileSystem()).thenReturn(fileSystem);

        NewMeasure newMeasure = mockMeasure(context);

        AnnotationsSensor sensor = new AnnotationsSensor();
        sensor.execute(context);

        verify(context).newMeasure();
        verify(newMeasure).forMetric(ANNOTATIONS);
        verify(newMeasure).withValue("[\"de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity\",\"de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence\",\"de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token\",\"org.apache.uima.jcas.tcas.DocumentAnnotation\"]");
    }

}
