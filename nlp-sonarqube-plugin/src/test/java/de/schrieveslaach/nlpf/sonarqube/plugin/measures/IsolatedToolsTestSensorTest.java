package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.uima.jcas.tcas.Annotation;
import org.apache.uima.resource.ResourceInitializationException;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Test;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.fs.InputModule;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.measure.NewMeasure;

import java.io.File;
import java.nio.charset.Charset;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.NlpToolMetrics.ISOLATED_TOOLS_TEST;
import static de.schrieveslaach.nlpf.testing.EngineDescriptionHashing.hash;
import static de.schrieveslaach.nlpf.testing.JsonDataFactory.createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant;
import static net.javacrumbs.jsonunit.JsonMatchers.jsonEquals;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

public class IsolatedToolsTestSensorTest extends SensorTest {

    @Test
    public void shouldDetermineAnnotations_ForOpenNlpExample() throws Exception {
        File testDataDirectory = folder.newFolder("target", "isolated-tools-test-data");

        FileUtils.writeStringToFile(new File(testDataDirectory, "0.json"), createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant(), Charset.defaultCharset());

        FileSystem fileSystem = mock(FileSystem.class);
        when(fileSystem.baseDir()).thenReturn(folder.getRoot());
        SensorContext context = mock(SensorContext.class);
        InputModule inputModule = mock(InputModule.class);
        when(context.module()).thenReturn(inputModule);
        when(context.fileSystem()).thenReturn(fileSystem);
        NewMeasure newMeasure = mockMeasure(context);

        IsolatedToolsTestSensor sensor = new IsolatedToolsTestSensor();
        sensor.execute(context);

        verify(context).newMeasure();
        verify(newMeasure).forMetric(ISOLATED_TOOLS_TEST);
        verify(newMeasure).on(inputModule);
        verify(newMeasure).withValue(argThat(jsonEquals(createIsolatedToolTestValue())));
    }

    @SneakyThrows(ResourceInitializationException.class)
    private static JSONArray createIsolatedToolTestValue() {
        return new JSONArray()
                .put(new JSONObject()
                                .put("descriptors", new JSONArray()
                                        .put(hash(createEngineDescription(OpenNlpSegmenter.class)) + ".xml")
                                        .put(hash(createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "person")) + ".xml")
                                        .put(hash(createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "organization")) + ".xml")
                                )
                                .put("measures", new JSONArray()
                                        .put(createMeasure(NamedEntity.class, "organization", 0.5714, "OpenNlpNamedEntityRecognizer"))
                                        .put(createMeasure(NamedEntity.class, "person", 0.5, "OpenNlpNamedEntityRecognizer"))
                                        .put(createMeasure(Sentence.class, null, 1.0, "OpenNlpSegmenter"))
                                        .put(createMeasure(Token.class, null, 1.0, "OpenNlpSegmenter"))
                                )
                );
    }

    private static JSONObject createMeasure(Class<? extends Annotation> annotationClass, String variant, Double fMeasure, String analysisEngineName) {
        return new JSONObject()
                .put("outputType", annotationClass.getCanonicalName())
                .put("variant", variant != null ? variant : JSONObject.NULL)
                .put("f-measure", fMeasure)
                .put("analysisEngineName", analysisEngineName);
    }
}
