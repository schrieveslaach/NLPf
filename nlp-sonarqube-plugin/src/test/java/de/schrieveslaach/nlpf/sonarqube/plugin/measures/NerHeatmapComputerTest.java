package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import de.schrieveslaach.nlpf.sonarqube.plugin.data.NerHeatmapData;
import org.junit.Test;
import org.sonar.api.ce.measure.Component;
import org.sonar.api.ce.measure.Measure;
import org.sonar.api.ce.measure.MeasureComputer;

import java.util.List;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics.NUMBER_OF_NAMED_ENTITIES_PER_VARIANT;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NerHeatmapComputerTest {

    @Test
    public void shouldComputeMeasure() {
        NerHeatmapComputer computer = new NerHeatmapComputer();
        MeasureComputer.MeasureComputerContext context = mockMeasureComputerContext(Component.Type.PROJECT, "person", "organization");

        computer.compute(context);

        verify(context).addMeasure(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT.key(), "[{\"variant\":\"person\",\"quantity\":6},{\"variant\":\"organization\",\"quantity\":12}]");
    }

    @Test
    public void shouldComputeMeasure_AggregateMultipleNERs() {
        NerHeatmapComputer computer = new NerHeatmapComputer();
        MeasureComputer.MeasureComputerContext context = mockMeasureComputerContext(Component.Type.PROJECT, "person", "organization", "person", "person");

        computer.compute(context);

        verify(context).addMeasure(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT.key(), "[{\"variant\":\"person\",\"quantity\":18},{\"variant\":\"organization\",\"quantity\":12}]");
    }

    @Test
    public void shouldSumUpNumberOfTokens_WithoutMeasuredNerHeatmapData() {
        NerHeatmapComputer computer = new NerHeatmapComputer();
        MeasureComputer.MeasureComputerContext context = mockMeasureComputerContext(Component.Type.PROJECT);

        computer.compute(context);

        verify(context).addMeasure(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT.key(), "[]");
    }

    @Test
    public void shouldNotSumUpNumberOfTokens_MeauresOnFile() {
        NerHeatmapComputer computer = new NerHeatmapComputer();
        MeasureComputer.MeasureComputerContext context = mockMeasureComputerContext(Component.Type.FILE, "person");

        computer.compute(context);

        verify(context, never()).addMeasure(eq(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT.key()), anyInt());
    }

    private MeasureComputer.MeasureComputerContext mockMeasureComputerContext(Component.Type type, String... variants) {
        MeasureComputer.MeasureComputerContext context = mock(MeasureComputer.MeasureComputerContext.class);

        Component component = mock(Component.class);
        when(component.getType()).thenReturn(type);
        when(context.getComponent()).thenReturn(component);

        ObjectMapper mapper = new ObjectMapper();

        List<Measure> measures = stream(variants)
                .map(variant -> {
                    Measure measure = mock(Measure.class);
                    NerHeatmapData nerHeatmapData = new NerHeatmapData(variant);
                    for (int i = 0; i < variant.length(); ++i) {
                        nerHeatmapData.increment();
                    }

                    try {
                        when(measure.getStringValue()).thenReturn(mapper.writeValueAsString(asList(nerHeatmapData)));
                    } catch (JsonProcessingException e) {
                        throw new RuntimeException(e);
                    }
                    return measure;
                })
                .collect(Collectors.toList());
        when(context.getChildrenMeasures(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT.key())).thenReturn(measures);

        return context;
    }

}
