package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.measure.NewMeasure;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics.NUMBER_OF_NAMED_ENTITIES_PER_VARIANT;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NerHeatmapSensorTest extends SensorTest {

    @Test
    public void shouldDetermineHeatmapMeasure() {
        FileSystem fileSystem = storeOpenNlpExample();
        SensorContext context = mock(SensorContext.class);
        when(context.fileSystem()).thenReturn(fileSystem);

        NewMeasure newMeasure = mockMeasure(context);

        NerHeatmapSensor sensor = new NerHeatmapSensor();
        sensor.execute(context);

        verify(context).newMeasure();
        verify(newMeasure).forMetric(NUMBER_OF_NAMED_ENTITIES_PER_VARIANT);
        verify(newMeasure).withValue("[{\"variant\":\"person\",\"quantity\":3},{\"variant\":\"organization\",\"quantity\":2}]");
    }

}
