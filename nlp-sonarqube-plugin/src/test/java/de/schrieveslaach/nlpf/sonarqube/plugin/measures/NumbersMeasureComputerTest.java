package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.junit.Test;
import org.sonar.api.ce.measure.Component;
import org.sonar.api.ce.measure.Measure;
import org.sonar.api.ce.measure.MeasureComputer.MeasureComputerContext;

import java.util.List;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.AnnotatedCorpusMetrics.NUMBER_OF_TOKENS;
import static java.util.Arrays.stream;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class NumbersMeasureComputerTest {

    @Test
    public void shouldSumUpNumberOfTokens_WithMeasuredTokens() {
        NumbersMeasureComputer computer = new NumbersMeasureComputer();
        MeasureComputerContext context = mockMeasureComputerContext(Component.Type.PROJECT, 11, 22, 33);

        computer.compute(context);

        verify(context).addMeasure(NUMBER_OF_TOKENS.key(), 66);
    }

    @Test
    public void shouldSumUpNumberOfTokens_WithoutMeasuredTokens() {
        NumbersMeasureComputer computer = new NumbersMeasureComputer();
        MeasureComputerContext context = mockMeasureComputerContext(Component.Type.PROJECT);

        computer.compute(context);

        verify(context).addMeasure(NUMBER_OF_TOKENS.key(), 0);
    }

    @Test
    public void shouldNotSumUpNumberOfTokens_MeauresOnFile() {
        NumbersMeasureComputer computer = new NumbersMeasureComputer();
        MeasureComputerContext context = mockMeasureComputerContext(Component.Type.FILE, 11);

        computer.compute(context);

        verify(context, never()).addMeasure(eq(NUMBER_OF_TOKENS.key()), anyInt());
    }

    private MeasureComputerContext mockMeasureComputerContext(Component.Type type, int... numberOfTokens) {
        MeasureComputerContext context = mock(MeasureComputerContext.class);

        Component component = mock(Component.class);
        when(component.getType()).thenReturn(type);
        when(context.getComponent()).thenReturn(component);


        List<Measure> measures = stream(numberOfTokens)
                .mapToObj(tokens -> {
                    Measure measure = mock(Measure.class);
                    when(measure.getIntValue()).thenReturn(tokens);
                    return measure;
                })
                .collect(Collectors.toList());
        when(context.getChildrenMeasures(NUMBER_OF_TOKENS.key())).thenReturn(measures);

        return context;
    }

}
