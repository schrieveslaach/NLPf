package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.junit.Test;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.measure.NewMeasure;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.charset.Charset;

import static de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionUtil.hash;
import static de.schrieveslaach.nlpf.sonarqube.plugin.measures.PipelineMetrics.BEST_PERFORMING_NLP_PIPELINE_DESCRIPTION;
import static de.schrieveslaach.nlpf.testing.JsonDataFactory.createNlpToolTestResultOfMySegmenterAndMyPosTagger;
import static de.schrieveslaach.nlpf.testing.JsonDataFactory.createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

public class PipelineDescriptionSensorTest extends SensorTest {

    @Test
    public void shouldStoreBestPerformingPipelineDescription() throws Exception {
        File testDataDirectory = folder.newFolder("target", "nlp-pipelines-test-data");


        FileUtils.writeStringToFile(new File(testDataDirectory, "0.json"), createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant(), Charset.defaultCharset());
        FileUtils.writeStringToFile(new File(testDataDirectory, "1.json"), createNlpToolTestResultOfMySegmenterAndMyPosTagger(), Charset.defaultCharset());

        saveEngineDescription(OpenNlpSegmenter.class);
        saveEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "person");
        saveEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "organization");

        FileSystem fileSystem = mock(FileSystem.class);
        when(fileSystem.baseDir()).thenReturn(folder.getRoot());
        SensorContext context = mock(SensorContext.class);
        when(context.fileSystem()).thenReturn(fileSystem);

        NewMeasure newMeasure = mockMeasure(context);

        PipelineDescriptionSensor sensor = new PipelineDescriptionSensor();
        sensor.execute(context);

        verify(context).newMeasure();
        verify(newMeasure).forMetric(BEST_PERFORMING_NLP_PIPELINE_DESCRIPTION);
        verify(newMeasure).withValue("{\"tools\":[{\"name\":\"OpenNLP Segmenter\",\"variant\":null,\"models\":[]}," +
                "{\"name\":\"OpenNLP Named Entity Recognizer\",\"variant\":\"person\",\"models\":[]}," +
                "{\"name\":\"OpenNLP Named Entity Recognizer\",\"variant\":\"organization\",\"models\":[]}]}");
    }

    @Test
    public void shouldNotStoreBestPerformingPipelineDescription_DescriptorsDirectoryDoesNotExists() throws Exception {
        File testDataDirectory = folder.newFolder("target", "nlp-pipelines-test-data");

        FileUtils.writeStringToFile(new File(testDataDirectory, "0.json"), createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant(), Charset.defaultCharset());
        FileUtils.writeStringToFile(new File(testDataDirectory, "1.json"), createNlpToolTestResultOfMySegmenterAndMyPosTagger(), Charset.defaultCharset());

        FileSystem fileSystem = mock(FileSystem.class);
        when(fileSystem.baseDir()).thenReturn(folder.getRoot());
        SensorContext context = mock(SensorContext.class);
        when(context.fileSystem()).thenReturn(fileSystem);

        mockMeasure(context);

        PipelineDescriptionSensor sensor = new PipelineDescriptionSensor();
        sensor.execute(context);

        verify(context, never()).newMeasure();
    }

    @SneakyThrows({UIMAException.class, IOException.class, SAXException.class})
    private void saveEngineDescription(Class<? extends JCasAnnotator_ImplBase> cls, Object... params) {
        File descriptorsDirectory = new File(folder.getRoot(), "target/descriptors");
        descriptorsDirectory.mkdirs();

        AnalysisEngineDescription aed = createEngineDescription(cls, params);

        File nerDescriptor = new File(descriptorsDirectory, hash(aed) + ".xml");
        try (FileOutputStream fos = new FileOutputStream(nerDescriptor)) {
            aed.toXML(fos);
        }
    }
}
