package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import org.apache.commons.io.FileUtils;
import org.apache.uima.jcas.tcas.Annotation;
import org.json.JSONArray;
import org.json.JSONObject;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.TemporaryFolder;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.measure.NewMeasure;
import org.sonar.api.measures.Metric;

import java.io.File;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

import static de.schrieveslaach.nlpf.testing.JsonDataFactory.createNlpToolTestResultOfMySegmenterAndMyPosTagger;
import static de.schrieveslaach.nlpf.testing.JsonDataFactory.createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant;
import static net.javacrumbs.jsonunit.JsonMatchers.jsonEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.hamcrest.MockitoHamcrest.argThat;

public class PipelineMeasuresSensorTest extends SensorTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    private PipelineMeasuresSensor createPipelineMeasuresSensor(Class<? extends Annotation>... annotations) {
        List<Metric> sonarMetrics = new ArrayList<>();
        for (Class<? extends Annotation> cls : annotations) {
            Metric metric = mock(Metric.class);
            when(metric.key()).thenReturn(cls.getCanonicalName());
            sonarMetrics.add(metric);
        }

        PipelineMetrics metrics = mock(PipelineMetrics.class);
        when(metrics.getMetrics()).thenReturn(sonarMetrics);
        return new PipelineMeasuresSensor(metrics);
    }

    @Test
    public void shouldStoreMultipleTokenizationMetrics() throws Exception {
        File testDataDirectory = folder.newFolder("target", "nlp-pipelines-test-data");

        FileUtils.writeStringToFile(new File(testDataDirectory, "0.json"), createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant(), Charset.defaultCharset());
        FileUtils.writeStringToFile(new File(testDataDirectory, "1.json"), createNlpToolTestResultOfMySegmenterAndMyPosTagger(), Charset.defaultCharset());

        FileSystem fileSystem = mock(FileSystem.class);
        when(fileSystem.baseDir()).thenReturn(folder.getRoot());
        SensorContext context = mock(SensorContext.class);
        when(context.fileSystem()).thenReturn(fileSystem);

        NewMeasure newMeasure = mockMeasure(context);

        PipelineMeasuresSensor sensor = createPipelineMeasuresSensor(Token.class);
        sensor.execute(context);

        verify(context).newMeasure();
        verify(newMeasure).withValue(argThat(jsonEquals(createTokenizationMetrics())));
    }

    @Test
    public void shouldNotStoreAnyMetric_NoData() {
        FileSystem fileSystem = mock(FileSystem.class);
        when(fileSystem.baseDir()).thenReturn(folder.getRoot());
        SensorContext context = mock(SensorContext.class);
        when(context.fileSystem()).thenReturn(fileSystem);

        PipelineMeasuresSensor sensor = createPipelineMeasuresSensor(Token.class);
        sensor.execute(context);

        verify(context, never()).newMeasure();
    }

    private static JSONArray createTokenizationMetrics() {
        return new JSONArray()
                .put(createMeasure(Token.class, 1.0, "OpenNlpSegmenter"))
                .put(createMeasure(Token.class, 0.8057, "MySegmenter"));
    }

    private static JSONObject createMeasure(Class<? extends Annotation> annotationClass, Double fMeasure, String analysisEngineName) {
        return new JSONObject()
                .put("outputType", annotationClass.getCanonicalName())
                .put("variant", JSONObject.NULL)
                .put("f-measure", fMeasure)
                .put("analysisEngineName", analysisEngineName);
    }
}
