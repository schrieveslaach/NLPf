package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import org.apache.uima.jcas.tcas.Annotation;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.sonar.api.measures.Metric;

import java.util.List;
import java.util.Objects;

import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class PipelineMetricsTest {

    @Test
    public void shouldContainNamedEntityMetric() {
        List<Metric> metrics = new PipelineMetrics().getMetrics();

        assertThat(metrics, hasItem(metricFor(NamedEntity.class)));
    }

    @Test
    public void shouldContainTokenMetric() {
        List<Metric> metrics = new PipelineMetrics().getMetrics();

        assertThat(metrics, hasItem(metricFor(Token.class)));
    }

    @Test
    public void shouldContainSentenceMetric() {
        List<Metric> metrics = new PipelineMetrics().getMetrics();

        assertThat(metrics, hasItem(metricFor(Sentence.class)));
    }

    @Test
    public void shouldContainPosMetric() {
        List<Metric> metrics = new PipelineMetrics().getMetrics();

        assertThat(metrics, hasItem(metricFor(POS.class)));
    }

    @Test
    public void shouldContainMetricsWithKeyLongerThan64Chars() {
        List<Metric> metrics = new PipelineMetrics().getMetrics();

        assertThat(metrics, not(hasItem(metricWithKeyLengthLongerThan64Chars())));
    }

    private <T extends Annotation> Matcher<Metric> metricFor(Class<T> type) {
        return new TypeSafeMatcher<Metric>() {
            @Override
            protected boolean matchesSafely(Metric metric) {
                return Objects.equals(metric.key(), type.getCanonicalName());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("metric for ").appendValue(type.getCanonicalName());
            }

            @Override
            protected void describeMismatchSafely(Metric item, Description mismatchDescription) {
                mismatchDescription.appendText("metric with key ").appendValue(item.key());
            }
        };
    }

    private <T extends Annotation> Matcher<Metric> metricWithKeyLengthLongerThan64Chars() {
        return new TypeSafeMatcher<Metric>() {
            @Override
            protected boolean matchesSafely(Metric metric) {
                return metric.key().length() > 64;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("metric with key length longer than 64 chars");
            }

            @Override
            protected void describeMismatchSafely(Metric item, Description mismatchDescription) {
                mismatchDescription.appendText("length of metric key ").appendValue(item.key()).appendText(" is ").appendValue(item.key().length());
            }
        };
    }
}
