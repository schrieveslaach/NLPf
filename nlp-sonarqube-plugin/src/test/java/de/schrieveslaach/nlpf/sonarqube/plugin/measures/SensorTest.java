package de.schrieveslaach.nlpf.sonarqube.plugin.measures;

/*-
 * ========================LICENSE_START=================================
 * nlp-sonarqube-plugin
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.io.odt.OdtWriter;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import lombok.SneakyThrows;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.junit.Rule;
import org.junit.rules.TemporaryFolder;
import org.sonar.api.batch.fs.FilePredicate;
import org.sonar.api.batch.fs.FilePredicates;
import org.sonar.api.batch.fs.FileSystem;
import org.sonar.api.batch.fs.InputFile;
import org.sonar.api.batch.sensor.SensorContext;
import org.sonar.api.batch.sensor.measure.NewMeasure;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public abstract class SensorTest {

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    protected NewMeasure mockMeasure(SensorContext context) {
        NewMeasure measure = mock(NewMeasure.class);
        when(measure.forMetric(any())).thenReturn(measure);
        when(measure.on(any())).thenReturn(measure);
        when(measure.withValue(anyInt())).thenReturn(measure);
        when(measure.withValue(anyString())).thenReturn(measure);
        when(context.newMeasure()).then(invocation -> measure);
        return measure;
    }

    protected FileSystem storeOpenNlpExample() {
        return storeJCas(createOpenNlpExample());
    }

    @SneakyThrows(UIMAException.class)
    protected FileSystem storeJCas(JCas... jCases) {
        AnalysisEngineDescription engineDescription = createEngineDescription(
                OdtWriter.class,
                OdtWriter.PARAM_TARGET_LOCATION, folder.getRoot()
        );

        List<InputFile> inputFiles = new ArrayList<>();
        for (JCas jCas : jCases) {
            runPipeline(jCas, engineDescription);

            DocumentMetaData metaData = DocumentMetaData.get(jCas);
            String filename = metaData.getDocumentUri().replace(metaData.getDocumentBaseUri(), "") + ".odt";

            InputFile inputFile = mock(InputFile.class);
            when(inputFile.filename()).thenReturn(filename);
            when(inputFile.toString()).thenReturn(new File(folder.getRoot(), filename).toString());
            inputFiles.add(inputFile);
        }

        FileSystem fileSystem = mock(FileSystem.class);

        FilePredicates predicates = mock(FilePredicates.class);
        FilePredicate predicate = mock(FilePredicate.class);
        when(predicates.hasType(InputFile.Type.MAIN)).thenReturn(predicate);
        when(fileSystem.predicates()).thenReturn(predicates);
        when(fileSystem.inputFiles(predicate)).thenReturn(inputFiles);

        return fileSystem;
    }
}
