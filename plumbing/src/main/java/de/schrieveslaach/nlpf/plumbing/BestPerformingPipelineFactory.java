package de.schrieveslaach.nlpf.plumbing;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionNode;
import de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineGraph;
import lombok.Cleanup;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.apache.uima.util.XMLInputSource;
import org.apache.uima.util.XMLParser;
import org.jgrapht.alg.CycleDetector;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

/**
 * A collection of static methods for creating UIMA {@link AnalysisEngineDescription}s which describe the
 * best-performing NLP pipeline. The pipeline has to be embedded into the classpath, (c.f.
 * {@link BestPerformingPipelineFactory#PIPELINES_TXT}).
 */
public class BestPerformingPipelineFactory {

    /**
     * Classpath to the file which contains the analysis engine description pointers to the best-performing NLP pipeline.
     */
    public static final String PIPELINES_TXT = "/META-INF/de.schrieveslaach.effective.nlp.application.development/pipeline.txt";

    private static final Pattern CLASSPATH_ENGINE_DESCRIPTION_PATTERN = Pattern.compile("^classpath\\*:(.*)$");

    private BestPerformingPipelineFactory() {
    }

    private static List<String> readEngineDescriptionPaths(ClassLoader classLoader) throws ResourceInitializationException {
        List<String> classpaths = new ArrayList<>();

        if (!containsClasspathPipelinesTxt(classLoader)) {
            throw new ResourceInitializationException(PIPELINES_TXT + " is not available on the classpath.", new Object[]{});
        }

        try (
                InputStream is = getPipelinesTxt(classLoader);
                BufferedReader br = new BufferedReader(new InputStreamReader(is))
        ) {
            String line;
            while ((line = br.readLine()) != null) {
                Matcher matcher = CLASSPATH_ENGINE_DESCRIPTION_PATTERN.matcher(line);
                if (matcher.matches()) {
                    String classpath = matcher.group(1);
                    if (!classpath.startsWith("/")) {
                        classpath = "/" + classpath;
                    }
                    classpaths.add(classpath);
                }
            }
        } catch (IOException e) {
            throw new ResourceInitializationException("Could not read " + PIPELINES_TXT + " from class path.", new Object[]{}, e);
        }

        return classpaths;
    }

    private static InputStream getPipelinesTxt(ClassLoader classLoader) throws IOException {
        return getResource(classLoader, PIPELINES_TXT).openStream();
    }

    private static URL getResource(ClassLoader classLoader, String resourceName) {
        URL resource = classLoader.getResource(resourceName);
        if (resource == null) {
            resource = classLoader.getResource(resourceName.substring(1));
        }
        return resource;
    }

    private static AnalysisEngineDescription loadAnalysisEngineDescription(String classpath, ClassLoader classLoader) throws ResourceInitializationException {
        XMLParser xmlParser = UIMAFramework.getXMLParser();

        try {
            @Cleanup
            XMLInputSource s = new XMLInputSource(getResource(classLoader, classpath));
            return xmlParser.parseAnalysisEngineDescription(s);
        } catch (IOException | InvalidXMLException e) {
            throw new ResourceInitializationException("Could not read engine description from classpath*:" + classpath, new Object[]{}, e);
        }
    }

    /**
     * Checks if the given {@link ClassLoader} contains {@link #PIPELINES_TXT} files which points to the descriptors
     * of the best-performing NLP pipeline.
     *
     * @param classLoader
     * @return
     */
    public static boolean containsClasspathPipelinesTxt(ClassLoader classLoader) {
        return getResource(classLoader, PIPELINES_TXT) != null;
    }

    /**
     * Creates an {@link AnalysisEngineDescription} which contains an aggregated step of the NLP tools which build the
     * best-performing NLP pipeline, derived by the nlp-maven-plugin.
     *
     * @return
     * @throws ResourceInitializationException
     */
    public static AnalysisEngineDescription createBestPerformingPipelineEngineDescription() throws ResourceInitializationException {
        return createBestPerformingPipelineEngineDescription(BestPerformingPipelineFactory.class.getClassLoader());
    }

    /**
     * Creates an {@link AnalysisEngineDescription} which contains an aggregated step of the NLP tools which build the
     * best-performing NLP pipeline, derived by the nlp-maven-plugin.
     *
     * @param classLoader The {@link ClassLoader} used to look up {@link #PIPELINES_TXT}
     * @return
     * @throws ResourceInitializationException
     */
    public static AnalysisEngineDescription createBestPerformingPipelineEngineDescription(ClassLoader classLoader) throws ResourceInitializationException {
        List<AnalysisEngineDescription> engineDescriptions = new ArrayList<>();

        for (String classpath : readEngineDescriptionPaths(classLoader)) {
            engineDescriptions.add(loadAnalysisEngineDescription(classpath, classLoader));
        }

        sortByTopologicalDependencyOrder(engineDescriptions, classLoader);

        AggregateBuilder aggregateBuilder = new AggregateBuilder();
        for (AnalysisEngineDescription aed : engineDescriptions) {
            aggregateBuilder.add(aed);
        }
        return aggregateBuilder.createAggregateDescription();
    }

    /**
     * <p>
     * Sorts the given {@link AnalysisEngineDescription}s by looking at the
     * {@link org.apache.uima.fit.descriptor.TypeCapability input and output} relation.
     * </p>
     * <p>
     * It builds a directed graph were each node is a {@link AnalysisEngineDescription} and the outgoing edges point to
     * the {@link AnalysisEngineDescription}s which require as input the output of the former description. Through
     * {@link TopologicalOrderIterator topological order} the {@link AnalysisEngineDescription}s will sorted
     * </p>
     *
     * @param descriptions
     * @return
     */
    public static void sortByTopologicalDependencyOrder(List<AnalysisEngineDescription> descriptions, ClassLoader classLoader) throws ResourceInitializationException {
        AnalysisEngineGraph dependencyGraph = new AnalysisEngineGraph();
        dependencyGraph.init(descriptions, classLoader);

        CycleDetector<AnalysisEngineDescriptionNode, DefaultEdge> detector = new CycleDetector<>(dependencyGraph);
        if (detector.detectCycles()) {
            throw new ResourceInitializationException("Could not read engine description: detected circular dependency", new Object[]{});
        }

        List<AnalysisEngineDescriptionNode> nodesWithoutIncomingConnection = dependencyGraph.vertexSet().stream()
                .filter(AnalysisEngineDescriptionNode::hasInputTypes)
                .filter(vertex -> dependencyGraph.inDegreeOf(vertex) == 0)
                .collect(Collectors.toList());

        if (!nodesWithoutIncomingConnection.isEmpty()) {
            throw new ResourceInitializationException(
                    "Some analysis components require input types which are not generated through other analysis components: " +
                            nodesWithoutIncomingConnection.stream()
                                    .map(d -> d.getDescription().getAnnotatorImplementationName())
                                    .collect(Collectors.joining(", ")), new Object[]{});
        }

        Map<AnalysisEngineDescription, Integer> indices = new HashMap<>(descriptions.size());
        TopologicalOrderIterator<AnalysisEngineDescriptionNode, DefaultEdge> it = new TopologicalOrderIterator<>(dependencyGraph);
        int i = 0;
        while (it.hasNext()) {
            AnalysisEngineDescriptionNode node = it.next();
            indices.put(node.getDescription(), i);
            ++i;
        }

        Collections.sort(descriptions, Comparator.comparingInt(indices::get));
    }
}
