package de.schrieveslaach.nlpf.plumbing;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;

import java.util.Objects;

/**
 * Maps a {@link org.apache.uima.fit.descriptor.MimeTypeCapability} to the implementing class
 */
@AllArgsConstructor
@EqualsAndHashCode(exclude = "mimeType")
public class ClassMimeTypeMapping {

    @Getter
    private final Class<?> implementationClass;

    @Getter
    private final String mimeType;

    public boolean mapToSameMimeType(ClassMimeTypeMapping classMimeTypeMapping) {
        return Objects.equals(mimeType, classMimeTypeMapping.mimeType);
    }
}
