package de.schrieveslaach.nlpf.plumbing;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.reflect.ClassPath;
import de.tudarmstadt.ukp.dkpro.core.api.io.JCasFileWriter_ImplBase;
import de.tudarmstadt.ukp.dkpro.core.api.io.ResourceCollectionReaderBase;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import lombok.SneakyThrows;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.MimeTypeCapability;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.lang.reflect.Field;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;

public class DKProCoreReflection {

    private static final Logger LOGGER = LoggerFactory.getLogger(DKProCoreReflection.class);

    private DKProCoreReflection() {
    }

    /**
     * Scan the classpath of the given {@link ClassLoader} in order to find classes which are assignable to the first or
     * second class
     *
     * @param <T>
     * @param <S>
     * @param classLoader
     * @param classInfoFilter
     * @param t
     * @param s
     * @return
     */
    @SneakyThrows(IOException.class)
    public static <T, S> List<Class> lookUpClasses(ClassLoader classLoader, Predicate<ClassPath.ClassInfo> classInfoFilter, Class<T> t, Class<S> s) {
        return ClassPath.from(classLoader).getAllClasses().stream()
                .filter(classInfoFilter)
                .map(cp -> {
                    try {
                        return cp.load();
                    } catch (NoClassDefFoundError ex) {
                        LOGGER.trace("Could not load class", ex);
                        return null;
                    }
                })
                .filter(Objects::nonNull)
                .filter(c -> t.isAssignableFrom(c) || s.isAssignableFrom(c))
                .collect(Collectors.toList());
    }

    public static <T> Stream<ClassMimeTypeMapping> convertToFlatStreamOfMimeTypes(List<Class> classes, Class<T> cls) {
        return convertToFlatStreamOfMimeTypes(
                classes,
                cls,
                c -> findAnnotation(c, MimeTypeCapability.class) != null,
                c -> Arrays.stream(findAnnotation(c, MimeTypeCapability.class).value())
                        .map(a -> new ClassMimeTypeMapping(c, a))
        );
    }

    public static <T> Stream<ClassMimeTypeMapping> convertToFlatStreamOfMimeTypes(List<Class> classes, Class<T> cls, Predicate<Class<T>> mimetypeFilter, Function<Class<T>, Stream<ClassMimeTypeMapping>> mimetypeMapper) {
        return classes.stream()
                .filter(cls::isAssignableFrom)
                .map(c -> (Class<T>) c)
                .filter(mimetypeFilter)
                .map(mimetypeMapper)
                .flatMap(Function.identity());
    }

    public static Set<ClassMimeTypeMappingPair> findClassMimeTypePairs(Stream<ClassMimeTypeMapping> first, Stream<ClassMimeTypeMapping> second) {
        Set<ClassMimeTypeMappingPair> pairs = new HashSet<>();

        List<ClassMimeTypeMapping> secondMappings = second.collect(Collectors.toList());

        first.forEach(cmtmF -> {
            for (ClassMimeTypeMapping cmtmS : secondMappings) {
                if (cmtmF.mapToSameMimeType(cmtmS)) {
                    pairs.add(new ClassMimeTypeMappingPair(cmtmF, cmtmS));
                }
            }
        });

        return pairs;
    }

    static boolean hasWriterClassFileNameExtensionParameter(ClassMimeTypeMappingPair pair, String extension) {
        for (Field field : pair.getSecond().getImplementationClass().getDeclaredFields()) {
            ConfigurationParameter annotation = findAnnotation(field, ConfigurationParameter.class);
            if (annotation != null && annotation.name().equals(ComponentParameters.PARAM_FILENAME_EXTENSION)) {
                for (String suffix : annotation.defaultValue()) {
                    if (suffix.equals(extension)) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    static Set<ClassMimeTypeMappingPair> findAllReaderWriterPairs(ClassLoader classLoader) {
        List<Class> classes = lookUpClasses(
                classLoader,
                c -> !c.getPackageName().startsWith("org.springframework")
                        && (c.getName().endsWith("Writer") || c.getName().endsWith("Reader")),
                ResourceCollectionReaderBase.class,
                JCasFileWriter_ImplBase.class
        );
        Stream<ClassMimeTypeMapping> readerClasses = convertToFlatStreamOfMimeTypes(classes, ResourceCollectionReaderBase.class);
        Stream<ClassMimeTypeMapping> writerClasses = convertToFlatStreamOfMimeTypes(classes, JCasFileWriter_ImplBase.class);
        return findClassMimeTypePairs(readerClasses, writerClasses);
    }
}
