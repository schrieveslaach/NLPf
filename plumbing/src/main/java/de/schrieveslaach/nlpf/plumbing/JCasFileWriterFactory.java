package de.schrieveslaach.nlpf.plumbing;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.io.JCasFileWriter_ImplBase;
import lombok.SneakyThrows;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceInitializationException;

import java.util.List;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.plumbing.DKProCoreReflection.findAllReaderWriterPairs;
import static de.schrieveslaach.nlpf.plumbing.DKProCoreReflection.hasWriterClassFileNameExtensionParameter;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

public class JCasFileWriterFactory {

    private JCasFileWriterFactory() {

    }

    /**
     * Scans the given {@link ClassLoader} for a {@link AnalysisEngineDescription writer descriptions}
     * which will handles files with the given extension.
     *
     * @param classLoader
     * @param extension
     * @param configurationParameters
     * @return
     */
    public static List<AnalysisEngineDescription> findWriterDescriptionByFileExtension(ClassLoader classLoader, String extension, Object... configurationParameters) {
        return findAllReaderWriterPairs(classLoader).stream()
                .filter(pair -> hasWriterClassFileNameExtensionParameter(pair, extension))
                .map(pair -> toEngineDescription(pair, configurationParameters))
                .collect(Collectors.toList());
    }

    @SneakyThrows(ResourceInitializationException.class)
    private static AnalysisEngineDescription toEngineDescription(ClassMimeTypeMappingPair pair, Object[] configurationParameters) {
        return createEngineDescription(
                (Class<? extends JCasFileWriter_ImplBase>) pair.getSecond().getImplementationClass(),
                configurationParameters
        );
    }
}
