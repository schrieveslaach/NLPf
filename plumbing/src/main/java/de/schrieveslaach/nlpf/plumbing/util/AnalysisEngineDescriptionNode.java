package de.schrieveslaach.nlpf.plumbing.util;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Sets;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.fit.descriptor.ResourceMetaData;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.tcas.Annotation;

import java.util.HashSet;

import static org.springframework.core.annotation.AnnotationUtils.findAnnotation;

@EqualsAndHashCode(exclude = {"typeCapability", "implClass", "outputType", "graph"})
public class AnalysisEngineDescriptionNode {

    @Getter
    private final AnalysisEngineDescription description;

    private final TypeCapability typeCapability;

    @Getter
    private final Class<?> implClass;

    @Getter
    private final ImmutableList<Class<? extends Annotation>> outputTypes;

    private final AnalysisEngineGraph graph;

    @SneakyThrows(ClassNotFoundException.class)
    AnalysisEngineDescriptionNode(AnalysisEngineGraph graph, AnalysisEngineDescription description, ClassLoader classLoader) {
        this.description = description;
        this.graph = graph;

        this.implClass = classLoader.loadClass(description.getAnnotatorImplementationName());

        this.typeCapability = findAnnotation(implClass, TypeCapability.class);

        ImmutableList.Builder<Class<? extends Annotation>> builder = ImmutableList.builder();
        if (this.typeCapability != null) {
            for (String outputType : typeCapability.outputs()) {
                builder.add((Class<? extends Annotation>) classLoader.loadClass(outputType));
            }
        }
        this.outputTypes = builder.build();
    }

    public String getVariant() {
        Object parameterVariant = description.getAnalysisEngineMetaData().getConfigurationParameterSettings().getParameterValue(ComponentParameters.PARAM_VARIANT);
        if (parameterVariant != null) {
            return parameterVariant.toString();
        } else {
            return null;
        }
    }

    public boolean requires(AnalysisEngineDescriptionNode other) {
        if (this == other) {
            return false;
        }

        HashSet<String> inputs = Sets.newHashSet(typeCapability.inputs());
        HashSet<String> outputs = Sets.newHashSet(other.typeCapability.outputs());

        return !Sets.intersection(inputs, outputs).isEmpty();
    }

    public boolean hasInputTypes() {
        return typeCapability.inputs().length > 0 && !typeCapability.inputs()[0].equals(TypeCapability.NO_DEFAULT_VALUE);
    }

    @Override
    public String toString() {
        String name;
        ResourceMetaData metaData = findAnnotation(implClass, ResourceMetaData.class);
        if (metaData != null) {
            name = metaData.name();
        } else {
            name = description.getAnnotatorImplementationName();
        }

        String variant = getVariant();
        if (variant == null) {
            return name;
        }
        return String.format("%s (variant = %s)", name, variant);
    }

    public boolean isStartOfPipeline() {
        return graph.inDegreeOf(this) == 0;
    }

}
