package de.schrieveslaach.nlpf.plumbing.util;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.jcas.JCas;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultEdge;
import org.jgrapht.traverse.TopologicalOrderIterator;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;

public class AnalysisEngineGraph extends DefaultDirectedGraph<AnalysisEngineDescriptionNode, DefaultEdge> implements Iterable<AnalysisEngineDescription> {

    public AnalysisEngineGraph() {
        super(DefaultEdge.class);
    }

    public void init(List<AnalysisEngineDescription> descriptions, ClassLoader classLoader) {

        for (AnalysisEngineDescription aed : descriptions) {
            addVertex(new AnalysisEngineDescriptionNode(this, aed, classLoader));
        }

        for (AnalysisEngineDescriptionNode a : vertexSet()) {
            for (AnalysisEngineDescriptionNode b : vertexSet()) {
                if (a.requires(b)) {
                    addEdge(b, a);
                }
            }
        }
    }

    public int size() {
        return vertexSet().size();
    }

    public void clear() {
        Set<DefaultEdge> edges = edgeSet();
        removeAllEdges(edges.toArray(new DefaultEdge[edges.size()]));
        removeAllVertices(Lists.newArrayList(vertexSet()));
    }

    public int countCommonPipelineHeadElements(AnalysisEngineGraph aeg) {
        int headCount = 0;

        TopologicalOrderIterator<AnalysisEngineDescriptionNode, DefaultEdge> itThis = new TopologicalOrderIterator<>(this);
        TopologicalOrderIterator<AnalysisEngineDescriptionNode, DefaultEdge> itOther = new TopologicalOrderIterator<>(aeg);

        while (itThis.hasNext() && itOther.hasNext()) {
            AnalysisEngineDescription aed1 = itThis.next().getDescription();
            AnalysisEngineDescription aed2 = itOther.next().getDescription();

            if (!aed1.equals(aed2)) {
                break;
            }

            ++headCount;
        }

        return headCount;
    }

    public void runOn(JCas jCas) throws UIMAException {
        Iterator<AnalysisEngineDescription> it = iterator();

        while (it.hasNext()) {
            runPipeline(jCas, it.next());
        }
    }

    public Stream<AnalysisEngineDescriptionNode> stream() {
        TopologicalOrderIterator<AnalysisEngineDescriptionNode, DefaultEdge> it = new TopologicalOrderIterator<>(this);
        Iterable<AnalysisEngineDescriptionNode> i = () -> it;
        return StreamSupport.stream(i.spliterator(), false);
    }

    @Override
    public Iterator<AnalysisEngineDescription> iterator() {
        TopologicalOrderIterator<AnalysisEngineDescriptionNode, DefaultEdge> it = new TopologicalOrderIterator<>(this);

        return new Iterator<AnalysisEngineDescription>() {
            @Override
            public boolean hasNext() {
                return it.hasNext();
            }

            @Override
            public AnalysisEngineDescription next() {
                return it.next().getDescription();
            }
        };
    }

    @Override
    public String toString() {
        TopologicalOrderIterator<AnalysisEngineDescriptionNode, DefaultEdge> it = new TopologicalOrderIterator<>(this);

        StringBuilder str = new StringBuilder();
        str.append("[");

        while (it.hasNext()) {
            AnalysisEngineDescriptionNode node = it.next();
            str.append(node.toString());

            if (it.hasNext()) {
                str.append(" → ");
            }
        }

        str.append("]");
        return str.toString();
    }

    public List<AnalysisEngineDescription> subList(int index) {
        List<AnalysisEngineDescription> descriptions = new ArrayList<>();

        Iterator<AnalysisEngineDescription> it = iterator();

        int i = 0;
        while (it.hasNext() && i < index) {
            it.next();
            ++i;
        }

        while (it.hasNext()) {
            descriptions.add(it.next());
        }

        return descriptions;
    }

}
