package de.schrieveslaach.nlpf.plumbing.util;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import java.io.IOException;
import java.io.InputStream;
import java.util.logging.LogManager;

public class Uima {

    private Uima() {
    }

    /**
     * Configures, e.g. {@link LogManager}, to swallow log messages which provide only information from DKProCore/UIMA.
     */
    public static void swallowLogging() {
        LogManager logManager = LogManager.getLogManager();
        try (final InputStream is = Uima.class.getResourceAsStream("/logging.properties")) {
            logManager.readConfiguration(is);
        } catch (IOException e) {
            throw new AssertionError("Failed to read log configuration. This should never happen.", e);
        }
    }

}
