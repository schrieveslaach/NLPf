package de.schrieveslaach.nlpf.plumbing;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */


import de.schrieveslaach.nlpf.plumbing.annotators.MyNerRequiringPosTags;
import de.schrieveslaach.nlpf.plumbing.annotators.MyPosTagger;
import de.schrieveslaach.nlpf.plumbing.annotators.MySentenceSplitter;
import de.schrieveslaach.nlpf.plumbing.annotators.MyTokenizer;
import de.schrieveslaach.nlpf.plumbing.annotators.NoneSenseAnnotator;
import de.schrieveslaach.nlpf.testing.TempClassLoaderRule;
import de.tudarmstadt.ukp.dkpro.core.tokit.RegexSegmenter;
import lombok.SneakyThrows;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceCreationSpecifier;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.util.InvalidXMLException;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;

import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.plumbing.BestPerformingPipelineFactory.createBestPerformingPipelineEngineDescription;
import static org.hamcrest.Matchers.contains;
import static org.junit.Assert.assertThat;

public class BestPerformingPipelineFactoryTest {

    @Rule
    public final TempClassLoaderRule tempClassLoaderRule = new TempClassLoaderRule(getClass().getClassLoader());

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Test
    public void shouldCreateBestPerformingPipeline_RegexSegmenterAndMyPosTagger() throws Exception {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class);

        AnalysisEngineDescription pipeline = createBestPerformingPipelineEngineDescription(tempClassLoaderRule.getTempClassLoader());

        assertThat(pipeline, hasEngineSpecifiers(
                engineDescriptionWithClass(RegexSegmenter.class),
                engineDescriptionWithClass(MyPosTagger.class)
        ));
    }

    @Test
    public void shouldCreateBestPerformingPipeline_MyTokenizerAndMySentenceSplitterAndMyPosTagger() throws Exception {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(
                MyPosTagger.class, MySentenceSplitter.class, MyTokenizer.class
        );

        AnalysisEngineDescription pipeline = createBestPerformingPipelineEngineDescription(tempClassLoaderRule.getTempClassLoader());

        assertThat(pipeline, hasEngineSpecifiers(
                engineDescriptionWithClass(MySentenceSplitter.class),
                engineDescriptionWithClass(MyTokenizer.class),
                engineDescriptionWithClass(MyPosTagger.class)
        ));
    }

    @Test
    public void shouldNotCreateBestPerformingPipeline_InvalidDescriptorFile() throws Exception {
        expectedException.expectMessage("Could not read engine description from classpath*:/invalid-descriptor.xml");
        expectedException.expect(ResourceInitializationException.class);

        tempClassLoaderRule.addResource("", "invalid-descriptor.xml", "<xml/>");
        tempClassLoaderRule.addMetaInfEntry(
                "de.schrieveslaach.effective.nlp.application.development",
                "pipeline.txt",
                "classpath*:/invalid-descriptor.xml");
        createBestPerformingPipelineEngineDescription(tempClassLoaderRule.getTempClassLoader());
    }

    @Test
    public void shouldNotCreateBestPerformingPipeline_MissingBestPerformingManifestEntry() throws Exception {
        expectedException.expectMessage(BestPerformingPipelineFactory.PIPELINES_TXT + " is not available on the classpath.");
        expectedException.expect(ResourceInitializationException.class);

        createBestPerformingPipelineEngineDescription(tempClassLoaderRule.getTempClassLoader());
    }

    @Test
    public void shouldNotCreateBestPerformingPipeline_MissingIntermediateAnnotator() throws Exception {
        expectedException.expectMessage("Some analysis components require input types which are not generated through other analysis components: " + MyNerRequiringPosTags.class.getName());

        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(
                MySentenceSplitter.class, MyNerRequiringPosTags.class
        );

        createBestPerformingPipelineEngineDescription(tempClassLoaderRule.getTempClassLoader());
    }

    @Test
    public void shouldNotCreateBestPerformingPipeline_PipelineContainsCycle() throws Exception {
        expectedException.expectMessage("Could not read engine description: detected circular dependency");
        expectedException.expect(ResourceInitializationException.class);

        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(
                MyPosTagger.class, MySentenceSplitter.class, MyTokenizer.class, NoneSenseAnnotator.class
        );
        createBestPerformingPipelineEngineDescription(tempClassLoaderRule.getTempClassLoader());
    }

    public <T extends JCasAnnotator_ImplBase> Matcher<AnalysisEngineDescription> engineDescriptionWithClass(Class<T> cls) {
        return new TypeSafeMatcher<AnalysisEngineDescription>() {
            @Override
            protected boolean matchesSafely(AnalysisEngineDescription analysisEngineDescription) {
                return Objects.equals(analysisEngineDescription.getAnnotatorImplementationName(), cls.getName());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("engine description with class: ").appendValue(cls);
            }
        };
    }

    public <D extends ResourceCreationSpecifier> Matcher<AnalysisEngineDescription> hasEngineSpecifiers(Matcher<D>... matchers) {
        return new TypeSafeMatcher<AnalysisEngineDescription>() {
            @Override
            @SneakyThrows(InvalidXMLException.class)
            protected boolean matchesSafely(AnalysisEngineDescription aed) {
                List<AnalysisEngineDescription> descriptions = aed.getDelegateAnalysisEngineSpecifiers()
                        .values()
                        .stream()
                        .map(rs -> ((AnalysisEngineDescription) rs))
                        .collect(Collectors.toList());

                return contains(matchers).matches(descriptions);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has engine specifiers ");
                for (Matcher<D> m : matchers) {
                    description.appendDescriptionOf(m).appendText(" ");
                }
            }
        };
    }
}
