package de.schrieveslaach.nlpf.plumbing;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.io.xmi.XmiReader;
import org.apache.uima.collection.CollectionReaderDescription;
import org.junit.Test;

import java.util.List;

import static de.schrieveslaach.nlpf.plumbing.CollectionReaderFactory.findReaderDescriptionByFileExtension;
import static de.schrieveslaach.nlpf.testing.ResourceCreationSpecifierMatchers.hasImplementationName;
import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertThat;

public class CollectionReaderFactoryTest {

    @Test
    public void shouldLoadXmiReader() {
        List<CollectionReaderDescription> descriptions = findReaderDescriptionByFileExtension(
                getClass().getClassLoader(),
                ".xmi");
        assertThat(descriptions, hasSize(1));
        assertThat(descriptions.get(0), hasImplementationName(XmiReader.class));
    }

}
