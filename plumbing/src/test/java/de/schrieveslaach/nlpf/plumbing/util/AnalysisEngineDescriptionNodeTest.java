package de.schrieveslaach.nlpf.plumbing.util;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.testing.TempClassLoaderRule;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import de.tudarmstadt.ukp.dkpro.core.tokit.RegexSegmenter;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.resource.ResourceInitializationException;
import org.junit.Rule;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class AnalysisEngineDescriptionNodeTest {

    @Rule
    public final TempClassLoaderRule tempClassLoaderRule = new TempClassLoaderRule(getClass().getClassLoader());

    @Test
    public void shouldComplyWithHashCodeContract_ValuePresent() {
        Map<AnalysisEngineDescriptionNode, String> map = new HashMap<>();
        map.put(create(RegexSegmenter.class), "hello world");

        assertThat(map, hasKey(create(RegexSegmenter.class)));
    }

    @Test
    public void shouldComplyWithHashCodeContract_ValueNotPresent() {
        Map<AnalysisEngineDescriptionNode, String> map = new HashMap<>();
        map.put(create(OpenNlpSegmenter.class), "hello world");

        assertThat(map, not(hasKey(create(RegexSegmenter.class))));
    }

    @Test
    public void shouldBeStartOfGraph() {
        AnalysisEngineDescriptionNode node = create(RegexSegmenter.class, OpenNlpNamedEntityRecognizer.class);

        assertThat(node.isStartOfPipeline(), is(true));
    }


    @Test
    public void shouldNotBeStartOfGraph() {
        AnalysisEngineDescriptionNode node = create(OpenNlpNamedEntityRecognizer.class, RegexSegmenter.class);

        assertThat(node.isStartOfPipeline(), is(false));
    }

    private AnalysisEngineDescriptionNode create(Class<? extends JCasAnnotator_ImplBase>... cls) {
        AnalysisEngineGraph graph = new AnalysisEngineGraph();
        graph.init(stream(cls).map(c -> {
            try {
                return createEngineDescription(c);
            } catch (ResourceInitializationException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList()), tempClassLoaderRule.getTempClassLoader());

        return graph.stream()
                .filter(node -> node.getImplClass().equals(cls[0]))
                .findFirst()
                .get();
    }

}
