package de.schrieveslaach.nlpf.plumbing.util;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.plumbing.annotators.MyPosTagger;
import org.junit.Test;

import static de.schrieveslaach.nlpf.plumbing.util.AnalysisEngineDescriptionUtil.hash;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;

public class AnalysisEngineDescriptionUtilTest {

    @Test
    public void shouldComputeHashOfEngineDescription() throws Exception {
        String hash = hash(createEngineDescription(MyPosTagger.class));
        assertThat(hash, is(equalTo("A00DCBA0E58B335778B9156E14CBAB8673FAC16AFAAC2D5277C0BEB81025E8802F0AACEC98D4177A6196EEEEEEB771535AE6E94DA6B86654C2FB07CAABCC854C")));
    }

}
