package de.schrieveslaach.nlpf.plumbing.util;

/*-
 * ========================LICENSE_START=================================
 * plumbing
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.plumbing.annotators.MyNerRequiringPosTags;
import de.schrieveslaach.nlpf.plumbing.annotators.MyPosTagger;
import de.schrieveslaach.nlpf.testing.TempClassLoaderRule;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpPosTagger;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import de.tudarmstadt.ukp.dkpro.core.tokit.RegexSegmenter;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.junit.Rule;
import org.junit.Test;

import java.util.Collection;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.testing.ResourceCreationSpecifierMatchers.hasImplementationName;
import static java.util.Arrays.asList;
import static java.util.Arrays.stream;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class AnalysisEngineGraphTest {

    @Rule
    public final TempClassLoaderRule tempClassLoaderRule = new TempClassLoaderRule(getClass().getClassLoader());

    @Test
    public void shouldCountNumberOfCommonElementsInTheHead_NoCommonElements() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        AnalysisEngineGraph graph1 = createGraph(MyPosTagger.class, RegexSegmenter.class);
        AnalysisEngineGraph graph2 = createGraph(OpenNlpPosTagger.class, OpenNlpSegmenter.class);

        assertThat(graph1.countCommonPipelineHeadElements(graph2), is(equalTo(0)));
    }

    @Test
    public void shouldCountNumberOfCommonElementsInTheHead_ContainsDifferentElements() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        AnalysisEngineGraph graph1 = createGraph(MyPosTagger.class, RegexSegmenter.class);
        AnalysisEngineGraph graph2 = createGraph(OpenNlpPosTagger.class, RegexSegmenter.class);

        assertThat(graph1.countCommonPipelineHeadElements(graph2), is(equalTo(1)));
    }

    @Test
    public void shouldCountNumberOfCommonElementsInTheHead_FirstContainsMoreElements() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        AnalysisEngineGraph graph1 = createGraph(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);
        AnalysisEngineGraph graph2 = createGraph(MyPosTagger.class, RegexSegmenter.class);

        assertThat(graph1.countCommonPipelineHeadElements(graph2), is(equalTo(2)));
    }

    @Test
    public void shouldCountNumberOfCommonElementsInTheHead_SecondContainsMoreElements() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        AnalysisEngineGraph graph1 = createGraph(MyPosTagger.class, RegexSegmenter.class);
        AnalysisEngineGraph graph2 = createGraph(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        assertThat(graph1.countCommonPipelineHeadElements(graph2), is(equalTo(2)));
    }

    @Test
    public void shouldRunOnGraph() throws Exception {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(RegexSegmenter.class);

        JCas jCas = JCasFactory.createText("The quick brown fox jumps over the lazy dog.", "en");

        AnalysisEngineGraph graph = createGraph(RegexSegmenter.class);
        graph.runOn(jCas);

        Collection<Token> tokens = select(jCas, Token.class);
        assertThat(tokens, is(not(empty())));
    }

    @Test
    public void shouldHasToString_WithoutVariant() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        AnalysisEngineGraph graph = createGraph(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);
        assertThat(graph, hasToString("[Regex Segmenter → de.schrieveslaach.nlpf.plumbing.annotators.MyPosTagger → de.schrieveslaach.nlpf.plumbing.annotators.MyNerRequiringPosTags]"));
    }

    @Test
    public void shouldHasToString_WithVariant() throws Exception {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(OpenNlpNamedEntityRecognizer.class);

        AnalysisEngineGraph graph = new AnalysisEngineGraph();
        graph.init(asList(createEngineDescription(
                OpenNlpNamedEntityRecognizer.class,
                OpenNlpNamedEntityRecognizer.PARAM_VARIANT, "person"
        )), tempClassLoaderRule.getTempClassLoader());
        assertThat(graph, hasToString("[OpenNLP Named Entity Recognizer (variant = person)]"));
    }

    @Test
    public void shouldClearGraph() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class);
        AnalysisEngineGraph graph = createGraph(MyPosTagger.class, RegexSegmenter.class);

        graph.clear();

        assertThat(graph.size(), is(0));
    }

    @Test
    public void shouldCreateSubList_EmptyGraph() {
        AnalysisEngineGraph graph = new AnalysisEngineGraph();

        assertThat(graph.subList(0), is(empty()));
    }

    @Test
    public void shouldCreateSubList_FullGraph() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);
        AnalysisEngineGraph graph = createGraph(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        assertThat(graph.subList(0), contains(
                hasImplementationName(RegexSegmenter.class),
                hasImplementationName(MyPosTagger.class),
                hasImplementationName(MyNerRequiringPosTags.class)
        ));
    }

    @Test
    public void shouldCreateSubList_PartialGraph() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);
        AnalysisEngineGraph graph = createGraph(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        assertThat(graph.subList(1), contains(
                hasImplementationName(MyPosTagger.class),
                hasImplementationName(MyNerRequiringPosTags.class)
        ));
    }

    @Test
    public void shouldCreateSubList_IndexAfterGraph() {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);
        AnalysisEngineGraph graph = createGraph(MyPosTagger.class, RegexSegmenter.class, MyNerRequiringPosTags.class);

        assertThat(graph.subList(20), is(empty()));
    }

    private AnalysisEngineGraph createGraph(Class<? extends JCasAnnotator_ImplBase>... cls) {
        AnalysisEngineGraph graph = new AnalysisEngineGraph();
        graph.init(stream(cls).map(c -> {
            try {
                return createEngineDescription(c);
            } catch (ResourceInitializationException e) {
                throw new RuntimeException(e);
            }
        }).collect(Collectors.toList()), tempClassLoaderRule.getTempClassLoader());
        return graph;
    }

}
