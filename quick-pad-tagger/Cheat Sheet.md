# QPT-Cheatsheet

#### Table of Contents

- [Global](#global)
- [Text Segmentation](#text-segmentation)
- [Part-of-Speech Tagging](#part-of-speech-tagging)
- [Named-Entity Tagging](#named-entity-tagging)

## Global

| Action                    | Gamepad                   | Keyboard        |
|---------------------------|---------------------------|-----------------|
| Open Menu                 | Guide                     | M               |
| Information               | Y                         | I               |
| Save                      | Start                     | Ctrl + S        |
| Next Token                | Stick right               | Arrow right     |
| Previous Token            | Stick left                | Arrow left      |
| First Token               | DPad up                   | Alt + HOME      |
| Last Token                | DPad down                 | Alt + END       |
| Toogle Fullscreen         |                           | F11             |

## Text Segmentation
| Action                    | Gamepad                   | Keyboard        |
|---------------------------|---------------------------|-----------------|
| Open Tag                  | Hold left trigger         | Hold Ctrl       |
| Split Sentence            | A                         | Enter           |
| Split Token               | Left Trigger + A          | Ctrl + Enter    |
| Merge Sentence            | B                         | End             |
| Merge Token               | X                         | Delete          |

## Part-of-Speech Tagging
| Action                    | Gamepad                   | Keyboard        |
|---------------------------|---------------------------|-----------------|
| Confirm Tag               | A                         | Enter           |
| Next Tag                  | Stick up                  | Arrow up        |
| Previous Tag              | Stick down                | Arrow down      |

## Named-Entity Tagging
| Action                    | Gamepad                         | Keyboard                |
|---------------------------|---------------------------------|-------------------------|
| Expand selection          | Left Trigger + Stick left/right | Crtl + Arrow left/right |
| Confirm named entity      | A                               | Enter                   |
| Remove  named entity      | X                               | Delete                  |
| Next named entity tag     | Stick up                        | Arrow up                |
| Previous named entity tag | Stick down                      | Arrow down              |

![quick-pad-tagger](src/main/resources/splash.png)
