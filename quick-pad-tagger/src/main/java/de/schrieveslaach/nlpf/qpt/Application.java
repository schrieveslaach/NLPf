package de.schrieveslaach.nlpf.qpt;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.felixroske.jfxsupport.AbstractJavaFxApplicationSupport;
import de.felixroske.jfxsupport.SplashScreen;
import de.schrieveslaach.nlpf.qpt.view.RootView;
import javafx.stage.Stage;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.annotation.EnableScheduling;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@EnableScheduling
@SpringBootApplication
public class Application extends AbstractJavaFxApplicationSupport {

    @Bean
    public static ExecutorService executorService() {
        return Executors.newFixedThreadPool(1);
    }

    public static void main(String[] args) {
        launch(Application.class, RootView.class, new QptSplashScreen(), args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        stage.setResizable(true);
        super.start(stage);
    }

    private static class QptSplashScreen extends SplashScreen {

        @Override
        public String getImagePath() {
            return "/splash.png";
        }

    }
}
