package de.schrieveslaach.nlpf.qpt.annotation;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.controller.ActionConsumer;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.view.control.TokenLabel;
import de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.Region;
import lombok.Getter;
import org.apache.uima.jcas.JCas;

import java.util.List;

import static java.util.Arrays.asList;

/**
 * Defines the behavior during an annotation task.
 */
public abstract class AnnotationBehavior implements ActionConsumer {

    protected final JCasService jCasService;

    @Getter
    protected final ObjectProperty<Selection> selectionProperty = new SimpleObjectProperty<>();

    protected AnnotationBehavior(JCasService jCasService) {
        this.jCasService = jCasService;
    }

    public String getName() {
        return getClass().getSimpleName().replace("Behavior", "");
    }

    /**
     * If the behavior {@link AnnotationBehavior#consume(Action) consumes an action} containing an
     * {@link Action#isAcceptButtonPressed() accept}, the selection will be forwarded to the next token.
     *
     * @return
     */
    public boolean forwardSelectionAfterActionAccepted() {
        return true;
    }

    /**
     * <p>
     * This method can modify the {@link Selection}, changing way the user scrolls through the stream of tokens.
     * For example, if the user moves the selection to right, the {@link AnnotationBehavior} could expand the selection
     * across multiple tokens.
     * </p>
     * <p>
     * Make sure that you call the base method and evaluate the result if you do not want to activate multiple tokens
     * across multiple sentences.
     * </p>
     *
     * @param oldSelection
     * @param newSelection
     * @return Returns the suggested selection, by default {@code newSelection} will be returned. If the method returns
     * {@code null}, the suggestion will be refused.
     * @see #supportsMultiTokenSelection()
     */
    @SuppressWarnings("squid:S1172")
    public Selection suggestNewSelection(Selection oldSelection, Selection newSelection) {
        if (supportsMultiTokenSelection() && newSelection.coversMultipleSentences()) {
            return null;
        }

        return newSelection;
    }

    /**
     * Depending on the annotation task, annotators can activate multiple tokens, for example, when they annotate
     * named entities, they activate multiple tokens to assign one named entity type to them at once. Other tasks,
     * e.g. POS tagging allows to activate just one token.
     *
     * @return
     */
    public boolean supportsMultiTokenSelection() {
        return false;
    }

    public abstract Region getTokenTagsRegion();

    public abstract Region createTokenRelationsRegion();

    public void activate(PaginationAdapter<TokenLabel, Token> tokenLabelAdapter) {
        ChangeListener<JCas> jCasChangeListener = getJCasChangeListener();
        if (jCasChangeListener != null) {
            jCasService.jCasProperty().addListener(jCasChangeListener);

            JCas jCas = jCasService.jCasProperty().get();
            if (jCas != null) {
                jCasChangeListener.changed(jCasService.jCasProperty(), null, jCas);
            }
        }

        ListChangeListener<Token> tokensChangeListener = getTokensChangeListener();
        if (tokensChangeListener != null) {
            jCasService.tokensProperty().addListener(tokensChangeListener);
            tokensChangeListener.onChanged(new ForcedListChange<>(jCasService.tokensProperty()));
        }

        ListChangeListener<Sentence> sentencesChangeListener = getSentencesChangeListener();
        if (sentencesChangeListener != null) {
            jCasService.sentencesProperty().addListener(sentencesChangeListener);
            sentencesChangeListener.onChanged(new ForcedListChange<>(jCasService.sentencesProperty()));
        }

        ChangeListener<Selection> selectionChangeListener = getSelectionChangeListener();
        if (selectionChangeListener != null) {
            selectionProperty.addListener(selectionChangeListener);

            Selection selection = selectionProperty.get();
            if (selection != null) {
                selectionChangeListener.changed(selectionProperty, null, selection);
            }
        }

        tokenLabelAdapter.rebind(jCasService.tokensProperty());
    }

    public void deactivate() {
        ListChangeListener<Token> tokensChangeListener = getTokensChangeListener();
        if (tokensChangeListener != null) {
            jCasService.tokensProperty().removeListener(tokensChangeListener);
        }

        ListChangeListener<Sentence> sentencesChangeListener = getSentencesChangeListener();
        if (sentencesChangeListener != null) {
            jCasService.sentencesProperty().removeListener(sentencesChangeListener);
        }

        ChangeListener<Selection> selectionChangeListener = getSelectionChangeListener();
        if (selectionChangeListener != null) {
            selectionProperty.removeListener(selectionChangeListener);
        }

        ChangeListener<JCas> jCasChangeListener = getJCasChangeListener();
        if (jCasChangeListener != null) {
            jCasService.jCasProperty().removeListener(jCasChangeListener);
        }
    }

    public ListChangeListener<Token> getTokensChangeListener() {
        return null;
    }

    public ListChangeListener<Sentence> getSentencesChangeListener() {
        return null;
    }

    public ChangeListener<Selection> getSelectionChangeListener() {
        return null;
    }

    public ChangeListener<JCas> getJCasChangeListener() {
        return null;
    }

    public void unbindBidirectional(SimpleObjectProperty<Selection> selectionProperty) {
        this.selectionProperty.unbindBidirectional(selectionProperty);
    }

    public void bindBidirectional(SimpleObjectProperty<Selection> selectionProperty) {
        this.selectionProperty.bindBidirectional(selectionProperty);
    }

    private static class ForcedListChange<T> extends ListChangeListener.Change<T> {

        private final ObservableList<T> list;

        public ForcedListChange(ObservableList<T> list) {
            super(list);
            this.list = list;
        }

        @Override
        public boolean next() {
            return false;
        }

        @Override
        public void reset() {
            // not used by any event handler
        }

        @Override
        public int getFrom() {
            return 0;
        }

        @Override
        public int getTo() {
            return list.size();
        }

        @Override
        public List<T> getRemoved() {
            return asList();
        }

        @Override
        protected int[] getPermutation() {
            return new int[0];
        }
    }
}
