package de.schrieveslaach.nlpf.qpt.annotation;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.service.ProjectService;
import de.schrieveslaach.nlpf.qpt.view.control.NamedEntityStream;
import de.schrieveslaach.nlpf.qpt.view.control.TokenLabel;
import de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.Region;
import lombok.Getter;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;


@Component
@Order(3)
@Qualifier("behavior-consumer")
public class NamedEntityTaggingBehavior extends AnnotationBehavior {

    private NamedEntityStream stream;

    private final ObservableList<String> namedEntitiesProperty;

    @Getter
    private final ChangeListener<Selection> selectionChangeListener = (observableValue, oldValue, newValue) -> {
        if (stream != null) {
            stream.setSelection(newValue);
        }
    };

    @Getter
    private final ChangeListener<JCas> jCasChangeListener = (observableValue, oldValue, newValue) -> {
        if (stream != null) {
            stream.setJCas(newValue);
        }
    };

    @Getter
    private final ListChangeListener<Token> tokensChangeListener = change -> {
        if (stream != null) {
            stream.setTokens((List<Token>) change.getList());
        }
    };

    @Autowired
    public NamedEntityTaggingBehavior(JCasService jCasService, ProjectService projectService) {
        super(jCasService);
        this.namedEntitiesProperty = projectService.namedEntitiesProperty();
    }

    @Override
    public void activate(PaginationAdapter<TokenLabel, Token> parentPaginationAdapter) {
        if (stream == null) {
            stream = new NamedEntityStream();
            stream.setId("ner-tag-stream");
            stream.setParentPaginationAdapter(parentPaginationAdapter);
            stream.setNamedEntitiesSet(namedEntitiesProperty);
        }

        super.activate(parentPaginationAdapter);
    }

    @Override
    public void deactivate() {
        super.deactivate();

        if (stream != null) {
            stream = null;
        }
    }

    @Override
    public boolean consume(Action event) {
        if (event.isAcceptButtonPressed()) {
            addNamedEntityGroup();
            return true;
        }

        if (event.isRemoveLeftAnnotationPressed()) {
            removeNamedEntityGroup();
            return true;
        }

        if (event.isMoveSelectionUp()) {
            stream.selectPreviousNamedEntityTag();
            return true;
        }
        if (event.isMoveSelectionDown()) {
            stream.selectNextNamedEntityTag();
            return true;
        }

        return false;
    }

    @Override
    public boolean supportsMultiTokenSelection() {
        return true;
    }

    @Override
    public Selection suggestNewSelection(Selection oldSelection, Selection newSelection) {
        newSelection = super.suggestNewSelection(oldSelection, newSelection);
        if (newSelection == null || oldSelection == null) {
            return null;
        }

        // Should not be allowed to expand selection while the old selection covers an named entity
        if (newSelection.size() > oldSelection.size() && coversSelectionANamedEntity(oldSelection)) {
            return null;
        }

        JCas jCas = jCasService.jCasProperty().get();

        if (coversANamedEntityTouchingEndOfDocument(oldSelection) &&
                oldSelection.overlapsWith(newSelection)) {
            return null;
        }

        //Cases:
        //  - Old Selection: NamedEntity -> Switch to next no named entity || Old Selection: No NamedEntity normal switch
        //  - New Selection: Covered by NamedEntity -> Select complete ne || New Selection: No NE -> Select next token
        List<NamedEntity> oldNamedEntity = jCasService.selectCovering(NamedEntity.class, oldSelection.getBegin(), oldSelection.getEnd());
        if (!oldNamedEntity.isEmpty()) {
            List<Token> oldNamedEntityTokens = jCasService.selectCovered(Token.class, oldNamedEntity.get(0));
            List<Token> followingTokens = new ArrayList<>();
            if (oldSelection.getBegin() < newSelection.getBegin()) {
                followingTokens.addAll(JCasUtil.selectFollowing(jCas, Token.class, oldNamedEntityTokens.get(oldNamedEntityTokens.size() - 1), 1));
            } else {
                followingTokens.addAll(JCasUtil.selectPreceding(jCas, Token.class, oldNamedEntityTokens.get(0), 1));
            }

            if (!followingTokens.isEmpty()) {
                List<NamedEntity> newNamedEntity = jCasService.selectCovering(NamedEntity.class, followingTokens.get(0));
                if (!newNamedEntity.isEmpty()) {
                    List<Token> newNamedEntityTokens = jCasService.selectCovered(Token.class, newNamedEntity.get(0));
                    return new Selection(jCas, newNamedEntityTokens);
                } else {
                    return new Selection(jCas, followingTokens);
                }
            }
        } else {
            List<NamedEntity> newNamedEntity = jCasService.selectCovering(NamedEntity.class, newSelection.getBegin(), newSelection.getEnd());
            if (!newNamedEntity.isEmpty()) {
                List<Token> newNamedEntityTokens = jCasService.selectCovered(Token.class, newNamedEntity.get(0));
                return new Selection(jCas, newNamedEntityTokens);
            }
        }

        return newSelection;
    }

    private boolean coversANamedEntityTouchingEndOfDocument(Selection selection) {
        if (!coversSelectionANamedEntity(selection)) {
            return false;
        }

        NamedEntity namedEntity = jCasService.selectAt(NamedEntity.class,
                selection.getBegin(), selection.getEnd()
        ).get(0);

        Sentence sentence = jCasService.selectCovering(Sentence.class, namedEntity).get(0);

        List<Sentence> followingSentences = jCasService.selectFollowing(Sentence.class, sentence, 1);
        return sentence.getEnd() == namedEntity.getEnd() && followingSentences.isEmpty();
    }

    private boolean coversSelectionANamedEntity(Selection selection) {
        List<NamedEntity> namedEntities = jCasService.selectAt(NamedEntity.class,
                selection.getBegin(), selection.getEnd()
        );
        return !namedEntities.isEmpty();
    }

    private void removeNamedEntityGroup() {
        List<Token> tokens = jCasService.removeNamedEntity(selectionProperty.get());

        if (!tokens.isEmpty()) {
            stream.rebindTokens(tokens);
            selectionProperty.setValue(new Selection(jCasService.jCasProperty().get(), tokens.get(0)));
        }
    }

    private void addNamedEntityGroup() {
        Selection selection = selectionProperty.get();
        jCasService.createNamedEntity(selection, stream.namedEntityTagProperty().get());
        stream.rebindTokens(selection.getSelectedTokens());
    }

    @Override
    public Region getTokenTagsRegion() {
        return stream;
    }

    @Override
    public Region createTokenRelationsRegion() {
        return null;
    }
}
