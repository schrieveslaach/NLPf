package de.schrieveslaach.nlpf.qpt.annotation;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.view.control.PosTagStream;
import de.schrieveslaach.nlpf.qpt.view.control.TokenLabel;
import de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter;
import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.resources.MappingProvider;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.collections.ListChangeListener;
import javafx.scene.layout.Region;
import lombok.Getter;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.jcas.JCas;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

import java.util.List;

import static de.tudarmstadt.ukp.dkpro.core.api.resources.MappingProviderFactory.createPosMappingProvider;

/**
 * Implements {@link AnnotationBehavior annotation behavior} for POS tags.
 */
@Component
@Order(2)
@Qualifier("behavior-consumer")
public class PosTaggingBehavior extends AnnotationBehavior {

    private static final Logger LOGGER = LoggerFactory.getLogger(PosTaggingBehavior.class);

    private PosTagStream stream;

    private ReadOnlyStringProperty posTagSetProperty;
    private ReadOnlyStringProperty defaultLanguageProperty;

    private MappingProvider posMappingProvider;

    @Getter
    private final ChangeListener<Selection> selectionChangeListener = (observableValue, oldValue, newValue) -> {
        if (stream != null) {
            stream.setSelection(newValue);
        }
    };

    @Getter
    private final ChangeListener<JCas> jCasChangeListener = (observableValue, oldValue, newValue) -> {
        if (stream != null) {
            stream.setJCas(newValue);
        }

        String documentLanguage = newValue.getDocumentLanguage();
        if ("x-unspecified".equals(documentLanguage)) {
            documentLanguage = defaultLanguageProperty.get();
        }

        posMappingProvider = createPosMappingProvider(
                null,
                posTagSetProperty.get(),
                documentLanguage);
        try {
            posMappingProvider.configure(newValue.getCas());

            stream.setTags(posMappingProvider.getTags());
        } catch (AnalysisEngineProcessException e) {
            LOGGER.error("Could not init POS mapping provider", e);
        }
    };

    @Getter
    private final ListChangeListener<Token> tokensChangeListener = change -> {
        if (stream != null) {
            stream.setTokens((List<Token>) change.getList());
        }
    };

    @Autowired
    public PosTaggingBehavior(JCasService jCasService) {
        super(jCasService);
    }

    @Override
    public void activate(PaginationAdapter<TokenLabel, Token> parentPaginationAdapter) {
        if (stream == null) {
            stream = new PosTagStream();
            stream.setId("pos-tag-stream");
            stream.setParentPaginationAdapter(parentPaginationAdapter);
        }

        super.activate(parentPaginationAdapter);
    }

    @Override
    public void deactivate() {
        super.deactivate();

        if (stream != null) {
            stream = null;
        }
    }

    @Override
    public boolean consume(Action event) {
        if (event.isMoveSelectionUp()) {
            if (event.isLeftTriggerPressed()) {
                stream.selectPreviousPosTagGroup();
            } else {
                stream.selectPreviousPosTag();
            }
            return true;
        }

        if (event.isMoveSelectionDown()) {
            if (event.isLeftTriggerPressed()) {
                stream.selectNextPosTagGroup();
            } else {
                stream.selectNextPosTag();
            }
            return true;
        }

        if (event.isAcceptButtonPressed()) {
            assignPosTag();
            return true;
        }

        return false;
    }

    private void assignPosTag() {
        Token token = selectionProperty.get().getSelectedTokens().get(0);

        POS previousPosTag = token.getPos();
        if (previousPosTag != null) {
            previousPosTag.removeFromIndexes();
        }

        String tag = stream.posTagProperty().get();

        Type posTag = posMappingProvider.getTagType(tag);
        POS pos = (POS) jCasService.jCasProperty().get().getCas().createAnnotation(posTag, token.getBegin(), token.getEnd());
        pos.setPosValue(tag.intern());
        pos.addToIndexes();
        token.setPos(pos);

        stream.rebindSelection();
    }


    @Autowired
    public void bindToPosTagSet(ReadOnlyStringProperty posTagSetProperty) {
        this.posTagSetProperty = posTagSetProperty;
    }

    @Autowired
    public void bindToDefaultDocumentLanguage(ReadOnlyStringProperty defaultLanguageProperty) {
        this.defaultLanguageProperty = defaultLanguageProperty;
    }

    @Override
    public Region getTokenTagsRegion() {
        return stream;
    }

    @Override
    public Region createTokenRelationsRegion() {
        return null;
    }
}
