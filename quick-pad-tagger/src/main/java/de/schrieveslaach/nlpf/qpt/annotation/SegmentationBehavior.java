package de.schrieveslaach.nlpf.qpt.annotation;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.view.control.SegmentationTag;
import de.schrieveslaach.nlpf.qpt.view.control.TokenLabel;
import de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.value.ChangeListener;
import javafx.scene.layout.Region;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

/**
 * Implements {@link AnnotationBehavior annotation behavior} for sentences and tokens.
 */
@Component
@Order(1)
@Qualifier("behavior-consumer")
public class SegmentationBehavior extends AnnotationBehavior {

    private SegmentationTag segmentationTag;

    @Getter
    private final ChangeListener<Selection> selectionChangeListener = (observableValue, oldSelection, newSelection) -> {
        if (newSelection.isEmpty()) {
            return;
        }
        if (segmentationTag != null) {
            segmentationTag.updateToken(newSelection.getSelectedTokens().get(0));
        }
    };

    @Autowired
    public SegmentationBehavior(JCasService jCasService) {
        super(jCasService);
    }

    @Override
    public void activate(PaginationAdapter<TokenLabel, Token> tokenLabelAdapter) {
        super.activate(tokenLabelAdapter);

        if (segmentationTag == null) {
            segmentationTag = new SegmentationTag();
            segmentationTag.setId("segmentation-tag");
        }
    }

    @Override
    public void deactivate() {
        super.deactivate();

        if (segmentationTag != null) {
            segmentationTag = null;
        }
    }

    @Override
    public boolean consume(Action event) {
        if (event.isLeftTriggerPressed()) {
            return handleActionWhileSegmentationIsVisible(event);
        } else {
            return handleNotExpandSelectionEvent(event);
        }
    }

    @Override
    public boolean forwardSelectionAfterActionAccepted() {
        return false;
    }

    @Override
    public Region getTokenTagsRegion() {
        return segmentationTag;
    }

    @Override
    public Region createTokenRelationsRegion() {
        return null;
    }

    private boolean handleActionWhileSegmentationIsVisible(Action event) {
        if (!segmentationTag.isVisible()) {
            return segmentationTag.show();
        } else if (event.isMoveSelectionToRight()) {
            return segmentationTag.moveRight();
        } else if (event.isMoveSelectionToLeft()) {
            return segmentationTag.moveLeft();
        } else if (event.isAcceptButtonPressed()) {
            Token token = selectionProperty.get().getSelectedTokens().get(0);

            Selection selection = jCasService.splitToken(token, segmentationTag.getIndex());
            if (selection != null) {
                selectionProperty.set(selection);
                segmentationTag.close();
            }
        }
        return true;
    }

    private boolean handleNotExpandSelectionEvent(Action event) {
        if (segmentationTag.isVisible()) {
            segmentationTag.close();
        }

        if (event.isAcceptButtonPressed()) {
            Token currentToken = selectionProperty.get().getSelectedTokens().get(0);

            Selection selection = jCasService.splitSentenceAtToken(currentToken);
            if (selection != null) {
                selectionProperty.set(selection);
            }
            return true;
        }

        if (event.isRemoveLeftAnnotationPressed()) {
            Selection selection = jCasService.mergeTokenWithPrevious(selectionProperty.get());
            if (selection != null) {
                selectionProperty.set(selection);
            }
            return true;
        }

        if (event.isRemoveRightAnnotationPressed()) {
            Selection selection = jCasService.mergeSentences(selectionProperty.get());
            if (selection != null) {
                selectionProperty.set(selection);
            }
            return true;
        }

        return false;
    }

}
