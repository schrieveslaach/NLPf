package de.schrieveslaach.nlpf.qpt.controller;
/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableList;
import de.felixroske.jfxsupport.FXMLController;
import de.schrieveslaach.nlpf.qpt.annotation.AnnotationBehavior;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.view.control.TokenStream;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.fxml.FXML;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;

import java.util.Collection;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static de.schrieveslaach.nlpf.qpt.view.control.TokenLabel.VERTICAL_BAR_HEIGHT;
import static org.apache.uima.fit.util.JCasUtil.select;

@FXMLController
@Order(3)
@Qualifier("root-consumer")
public class AnnotationController implements ActionConsumer {

    private final List<AnnotationBehavior> behaviors;

    private final JCasService jCasService;

    private AnnotationBehavior selectedAnnotationBehavior;

    private final SimpleObjectProperty<Selection> selectionProperty = new SimpleObjectProperty<>(new Selection());

    private final AtomicBoolean canApplyAction = new AtomicBoolean(true);

    private final AtomicInteger selectionOffset = new AtomicInteger(1);

    @FXML
    private AnchorPane annotation;

    @FXML
    private TokenStream tokenStream;

    @Autowired
    public AnnotationController(JCasService jCasService, @Qualifier("behavior-consumer") List<AnnotationBehavior> behaviors) {
        this.jCasService = jCasService;
        this.behaviors = behaviors;
    }

    @FXML
    public void initialize() {
        tokenStream.bindToJCas(jCasService.jCasProperty());
        tokenStream.bindToTokens(jCasService.tokensProperty());
        tokenStream.bindToSentences(jCasService.sentencesProperty());
        tokenStream.bindToSelection(selectionProperty, () -> {
            synchronized (canApplyAction) {
                canApplyAction.set(true);
            }
        });

        jCasService.jCasProperty().addListener((observable, oldValue, newValue) -> {
            // Clean up old values
            selectionProperty.set(new Selection());

            // Init default selection
            Collection<Token> newTokens = select(newValue, Token.class);
            Token firstToken = newTokens.iterator().next();
            selectionProperty.set(new Selection(newValue, firstToken));
        });
    }

    @Autowired
    public void bindToSelectAnnotationBehaviorProperty(ReadOnlyStringProperty selectedAnnotationBehaviorProperty) {
        selectedAnnotationBehaviorProperty.addListener((observable, oldValue, newAnnotationBehavior) -> {
            if (selectedAnnotationBehavior != null) {
                annotation.getChildren().remove(selectedAnnotationBehavior.getTokenTagsRegion());

                selectedAnnotationBehavior.unbindBidirectional(selectionProperty);
                selectedAnnotationBehavior.deactivate();
            }

            selectedAnnotationBehavior = behaviors.stream()
                    .filter(annotationBehavior -> Objects.equals(newAnnotationBehavior, annotationBehavior.getName()))
                    .findFirst()
                    .get();

            selectedAnnotationBehavior.bindBidirectional(selectionProperty);
            selectedAnnotationBehavior.activate(tokenStream.getTokenLabelAdapter());

            Region tokenTagsRegion = selectedAnnotationBehavior.getTokenTagsRegion();
            if (tokenTagsRegion != null) {
                AnchorPane.setBottomAnchor(tokenTagsRegion, 60.0 + 0.25 * VERTICAL_BAR_HEIGHT);
                AnchorPane.setLeftAnchor(tokenTagsRegion, 0.0);
                AnchorPane.setRightAnchor(tokenTagsRegion, 0.0);

                annotation.getChildren().add(tokenTagsRegion);
                tokenTagsRegion.toBack();
            }
        });
    }

    @Bean
    public ObjectProperty<Selection> selectionProperty() {
        return selectionProperty;
    }

    @Override
    public boolean consume(Action event) {
        synchronized (canApplyAction) {
            if (!canApplyAction.get()) {
                return false;
            }
        }

        if (selectedAnnotationBehavior == null) {
            return false;
        }
        if (selectedAnnotationBehavior.consume(event)) {
            if (selectedAnnotationBehavior.forwardSelectionAfterActionAccepted() && event.isAcceptButtonPressed()) {
                changeSelection(selectionOffset.get(), event);
            }
            return true;
        }

        if (event.isMoveSelectionToLeft()) {
            changeSelection(-1, event);
        } else if (event.isMoveSelectionToRight()) {
            changeSelection(1, event);
        } else if (event.isMoveSelectionToBegin()) {
            selectionProperty.set(jCasService.createSelectionForFirstToken());
        } else if (event.isMoveSelectionToEnd()) {
            selectionProperty.set(jCasService.createSelectionForLastToken());
        } else {
            return false;
        }
        return true;
    }

    private void changeSelection(int offset, Action action) {
        Selection oldSelection = selectionProperty.get();

        if (oldSelection == null || oldSelection.isEmpty()) {
            synchronized (canApplyAction) {
                canApplyAction.set(false);
            }
            Selection newSelection = jCasService.createSelectionForFirstToken();
            Selection suggestedSelection = selectedAnnotationBehavior.suggestNewSelection(oldSelection, newSelection);
            if (suggestedSelection != null) {
                newSelection = suggestedSelection;
            }

            selectionProperty.set(newSelection);
            selectionOffset.set(offset);
            return;
        }

        Selection newSelection = suggestDefaultSelection(offset, action);
        Selection suggestedSelection = selectedAnnotationBehavior.suggestNewSelection(oldSelection, newSelection);
        if (suggestedSelection == null || suggestedSelection.equals(oldSelection)) {
            return;
        }
        newSelection = suggestedSelection;

        synchronized (canApplyAction) {
            canApplyAction.set(false);
        }

        selectionProperty.set(newSelection);
        selectionOffset.set(offset);
    }

    private Selection suggestDefaultSelection(int offset, Action action) {
        Selection oldSelection = selectionProperty.get();

        ImmutableList<Token> selectedTokens = oldSelection.getSelectedTokens();
        List<Token> tokens = jCasService.tokensProperty();
        int indexOfFirstToken = tokens.indexOf(selectedTokens.get(0));
        int indexOfLastToken = tokens.indexOf(selectedTokens.get(selectedTokens.size() - 1));

        boolean expandSelection = selectedAnnotationBehavior.supportsMultiTokenSelection() && action.isLeftTriggerPressed();
        if (!expandSelection) {
            return new Selection(jCasService.jCasProperty().get(), tokens.get(Math.min(tokens.size() - 1, Math.max(0, indexOfFirstToken + offset))));
        }

        List<Token> newSelectedTokens;
        if (offset > 0) {
            if (selectionOffset.get() < 0 && oldSelection.size() > 1) {
                // Reduce selection
                newSelectedTokens = tokens.subList(indexOfFirstToken + 1, Math.min(tokens.size(), indexOfLastToken + 1));
            } else {
                newSelectedTokens = tokens.subList(indexOfFirstToken, Math.min(tokens.size(), indexOfLastToken + 2));
            }
        } else {
            if (selectionOffset.get() > 0 && oldSelection.size() > 1) {
                // Reduce selection
                newSelectedTokens = tokens.subList(Math.max(0, indexOfFirstToken - 1), indexOfLastToken);
            } else {
                newSelectedTokens = tokens.subList(Math.max(0, indexOfFirstToken - 1), indexOfLastToken + 1);
            }
        }

        return new Selection(jCasService.jCasProperty().get(), newSelectedTokens);
    }

}
