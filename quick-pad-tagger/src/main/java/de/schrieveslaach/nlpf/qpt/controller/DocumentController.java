package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.felixroske.jfxsupport.FXMLController;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.view.control.NaturalLanguageText;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.application.Platform;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import org.apache.uima.jcas.JCas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Lazy;

@FXMLController
@Lazy
public class DocumentController {

    @FXML
    private NaturalLanguageText naturalLanguageText;

    @Autowired
    private ObservableObjectValue<JCas> jCas;

    @Autowired
    private ObservableObjectValue<Selection> selectionProperty;

    @Autowired
    private ObservableList<Token> tokens;

    @Autowired
    private ObservableList<Sentence> sentences;

    @FXML
    public void initialize() {
        naturalLanguageText.selectionProperty().bind(selectionProperty);

        jCas.addListener((observableValue, oldValue, newValue) ->
                Platform.runLater(() -> naturalLanguageText.showJCas(newValue))
        );

        tokens.addListener((ListChangeListener<? super Token>) this::onChangedTextSegmentation);
        sentences.addListener((ListChangeListener<? super Sentence>) this::onChangedTextSegmentation);
    }

    private void onChangedTextSegmentation(ListChangeListener.Change<?> c) {
        if (jCas.get() != null) {
            Platform.runLater(() -> naturalLanguageText.showJCas(jCas.get()));
        }
    }
}
