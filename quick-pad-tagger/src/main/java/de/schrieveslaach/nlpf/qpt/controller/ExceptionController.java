package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.felixroske.jfxsupport.FXMLController;
import de.schrieveslaach.nlpf.qpt.input.Action;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;

@FXMLController
@Order(Integer.MIN_VALUE)
@Qualifier("root-consumer")
public class ExceptionController extends OverlayController implements ActionConsumer, TaskConsumer {

    @FXML
    private StackPane background;

    @FXML
    private Pane exception;

    @FXML
    private TextArea messages;

    @Override
    protected Node getOverlayNode() {
        return exception;
    }

    @Override
    protected StackPane getBackgroundStackPane() {
        return background;
    }

    @Override
    public boolean consume(Action event) {
        if (!isOpen()) {
            return false;
        }

        if (event.isAcceptButtonPressed()) {
            setOpen(false);
        }

        return true;
    }

    @Override
    public void consume(Task<?> task) {
        task.exceptionProperty().addListener((observableValue, oldException, newException) -> {
            setOpen(newException != null);

            if (newException != null) {
                messages.setText(newException.getMessage());
            }
        });
    }
}
