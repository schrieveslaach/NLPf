package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.felixroske.jfxsupport.FXMLController;
import de.schrieveslaach.nlpf.qpt.input.Gamepad;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Group;
import javafx.scene.control.Label;
import javafx.util.Duration;
import org.springframework.context.annotation.Bean;

@FXMLController
public class GamepadStatusController {

    @FXML
    private Label statusLabel;

    @FXML
    private Group gamepadConnected;

    @FXML
    private Group gamepadDisconnected;

    @Bean
    public Gamepad.ConnectionStateHandler connectionStateHandler() {
        return connected -> Platform.runLater(() -> {
            if (statusLabel == null) {
                // this happens when the ui is not yet visible but the controller is already connected
                Timeline timeline = new Timeline(new KeyFrame(
                        Duration.millis(500),
                        ae -> Platform.runLater(() -> displayStatus(connected))));
                timeline.setCycleCount(5);
                timeline.play();

                return;
            }

            displayStatus(connected);
        });
    }

    private void displayStatus(boolean connected) {
        if (statusLabel == null) {
            return;
        }

        statusLabel.setText(connected ? "Connected" : "Disconnected");
        gamepadConnected.setVisible(connected);
        gamepadDisconnected.setVisible(!connected);
    }

}
