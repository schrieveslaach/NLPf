package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.felixroske.jfxsupport.FXMLController;
import de.schrieveslaach.nlpf.qpt.annotation.AnnotationBehavior;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Document;
import de.schrieveslaach.nlpf.qpt.service.ProjectService;
import de.schrieveslaach.nlpf.qpt.view.control.Coloring;
import de.schrieveslaach.nlpf.qpt.view.control.TokenLabel;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import javafx.animation.ParallelTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.NumberBinding;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.StackPane;
import javafx.stage.FileChooser;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NonNull;
import org.apache.uima.cas.CAS;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.jcas.JCas;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.core.annotation.Order;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

@FXMLController
@Order(1)
@Qualifier("root-consumer")
public class MenuController extends OverlayController implements ActionConsumer {

    @FXML
    private TokenLabel openProject;

    @FXML
    private StackPane background;

    @FXML
    private StackPane menu;

    @FXML
    private GridPane menuGrid;
    @FXML
    private Label documentTitle;

    @FXML
    private Hyperlink documentUri;

    @FXML
    private ListView<Document> documents;

    private final ObjectProperty<Document> selectedDocument = new SimpleObjectProperty<>();

    @Autowired
    @Qualifier("behavior-consumer")
    private List<AnnotationBehavior> behaviors;

    private List<TokenLabel> annotationBehaviorTokenLabels = new ArrayList<>();
    private NumberBinding menuLabelsMinWidth;

    @Autowired
    private ProjectService projectService;

    @Autowired
    private List<TaskConsumer> taskConsumers;

    private final StringProperty selectedAnnotationBehavior = new SimpleStringProperty();

    private final ObjectProperty<MenuSelection> menuSelection = new SimpleObjectProperty<>();

    /**
     * Stores the last selected row index in each column. Key is equal to column and value is equal to row.
     */
    private final Map<Integer, Integer> lastSelectedRowIndex = new HashMap<>();

    @FXML
    public void initialize() {
        documents.setItems(projectService.documentsProperty());
        documents.setCellFactory(param -> new DocumentCell());

        menuLabelsMinWidth = Bindings.max(0, openProject.fullWidthProperty());
        for (int i = 0; i < behaviors.size(); i++) {
            AnnotationBehavior annotationBehavior = behaviors.get(i);
            TokenLabel label = new TokenLabel();
            label.setText(annotationBehavior.getName());
            label.setId(annotationBehavior.getName());

            menuLabelsMinWidth = Bindings.max(menuLabelsMinWidth, label.fullWidthProperty());

            GridPane.setColumnIndex(label, 0);
            GridPane.setRowIndex(label, i + 1);

            menuGrid.getChildren().add(label);
            annotationBehaviorTokenLabels.add(label);
        }

        selectedDocument.addListener((observable, oldDocument, newDocument) ->
                applyDocumentChange(oldDocument, newDocument)
        );

        selectedAnnotationBehavior.addListener((observable, oldAnnotationBehavior, newAnnotationBehavior) ->
                applyAnnotationBehaviorChange(oldAnnotationBehavior, newAnnotationBehavior)
        );

        menuSelection.addListener((observable, oldSelection, newSelection) ->
                applySelectionChangeTransition(oldSelection, newSelection)
        );

        // Set initial selection
        menuSelection.set(new MenuSelection(0, 0, openProject));
    }

    private void applyDocumentChange(Document oldDocument, Document newDocument) {
        TokenLabel tokenLabel = lookupTokenLabelFor(oldDocument);
        if (tokenLabel != null) {
            tokenLabel.showSelectedIndicatorProperty().set(false);
        }
        tokenLabel = lookupTokenLabelFor(newDocument);
        if (tokenLabel != null) {
            tokenLabel.showSelectedIndicatorProperty().set(true);
        }
    }

    private void applyAnnotationBehaviorChange(String oldAnnotationBehavior, String newAnnotationBehavior) {
        TokenLabel tokenLabel = lookupTokenLabelFor(oldAnnotationBehavior);
        if (tokenLabel != null) {
            tokenLabel.showSelectedIndicatorProperty().set(false);
        }
        tokenLabel = lookupTokenLabelFor(newAnnotationBehavior);
        if (tokenLabel != null) {
            tokenLabel.showSelectedIndicatorProperty().set(true);
        }
    }

    private void applySelectionChangeTransition(MenuSelection oldSelection, MenuSelection newSelection) {
        if (!beginChangeTransaction()) {
            return;
        }

        ParallelTransition pt = new ParallelTransition();
        if (newSelection != null) {
            pt.getChildren().add(
                    newSelection.getLabel().createColoringTransition(Coloring.LESS_FOCUS, Coloring.FOCUS)
            );
        }
        if (oldSelection != null) {
            pt.getChildren().add(
                    oldSelection.getLabel().createColoringTransition(Coloring.FOCUS, Coloring.LESS_FOCUS)
            );
        }
        pt.setOnFinished(event -> {
            endChangeTransaction();

            // Request focus after each animation because some times the list view catches the focus and in this
            // case the UI will not react to key events anymore.
            menu.requestFocus();

            if (newSelection.pointsToSecondColumn()) {
                int rowIndex = newSelection.getRow();
                documents.scrollTo(rowIndex >= 1 ? rowIndex - 1 : rowIndex);
            }

            lastSelectedRowIndex.put(newSelection.getColumn(), newSelection.getRow());
        });
        pt.play();
    }

    @Override
    public void setOpen(boolean open) {
        super.setOpen(open);

        if (open) {
            openProject.labelMinWidthProperty().set(menuLabelsMinWidth.doubleValue());
            annotationBehaviorTokenLabels.forEach(label -> label.labelMinWidthProperty().set(menuLabelsMinWidth.doubleValue()));
        }
    }

    @Override
    protected Node getOverlayNode() {
        return menu;
    }

    @Override
    protected StackPane getBackgroundStackPane() {
        return background;
    }

    @Bean
    public ReadOnlyStringProperty selectedAnnotationBehaviorProperty() {
        return selectedAnnotationBehavior;
    }

    public void navigateDown() {
        MenuSelection currentSelection = menuSelection.get();
        MenuSelection selection = findMenuSelectionByRowColumnIndex(currentSelection.getRow() + 1, currentSelection.getColumn());
        if (selection == null) {
            return;
        }

        menuSelection.set(selection);
    }

    public void navigateUp() {
        MenuSelection currentSelection = menuSelection.get();
        MenuSelection selection = findMenuSelectionByRowColumnIndex(currentSelection.getRow() - 1, currentSelection.getColumn());
        if (selection == null) {
            return;
        }

        menuSelection.set(selection);
    }

    public void navigateLeft() {
        MenuSelection currentSelection = menuSelection.get();
        int rowIndex = Math.max(lastSelectedRowIndex.getOrDefault(currentSelection.getColumn() - 1, 0), currentSelection.getRow());
        MenuSelection selection = findMenuSelectionByRowColumnIndex(rowIndex, currentSelection.getColumn() - 1);
        if (selection == null) {
            return;
        }

        menuSelection.set(selection);
    }

    public void navigateRight() {
        MenuSelection currentSelection = menuSelection.get();
        int rowIndex = Math.max(lastSelectedRowIndex.getOrDefault(currentSelection.getColumn() + 1, 0), currentSelection.getRow());
        MenuSelection selection = findMenuSelectionByRowColumnIndex(rowIndex, currentSelection.getColumn() + 1);
        if (selection == null) {
            return;
        }

        menuSelection.set(selection);
    }

    public void acceptPressed() {
        MenuSelection selection = menuSelection.get();
        if (selection == null) {
            return;
        }

        switch (selection.getId()) {
            case "openProject":
                selectOpenProject();
                break;
            case "Segmentation":
                selectedAnnotationBehavior.set("Segmentation");
                break;
            case "PosTagging":
                selectedAnnotationBehavior.set("PosTagging");
                break;
            case "NamedEntityTagging":
                selectedAnnotationBehavior.set("NamedEntityTagging");
                break;
            default:
                Document document = selection.getSelectedDocument();

                Task<JCas> loadJCasTask = projectService.loadJCas(document);
                taskConsumers.forEach(tc -> tc.consume(loadJCasTask));

                loadJCasTask.valueProperty().addListener((observableValue, oldJCas, newJCas) -> {
                    showJCasMetaData(newJCas);
                    selectedDocument.set(document);
                    setOpen(false);
                });
                break;
        }
    }

    private void showJCasMetaData(JCas jCas) {
        if (jCas == null || jCas.getCas() == null) {
            documentTitle.setText("–");
            documentUri.setText("–");
            return;
        }

        CAS aCas = jCas.getCas();
        if (!aCas.getIndexRepository().getAllIndexedFS(CasUtil.getType(aCas, DocumentMetaData.class)).hasNext()) {
            documentTitle.setText("?");
            documentUri.setText("?");
            return;
        }

        DocumentMetaData metaData = DocumentMetaData.get(jCas);

        String documentTitleValue = metaData.getDocumentTitle();
        documentTitle.setText(documentTitleValue != null ? documentTitleValue : "?");
        String documentUriValue = metaData.getDocumentUri();
        documentUri.setText(documentUriValue != null ? documentUriValue : "?");
    }

    private void selectOpenProject() {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Project File");
        fileChooser.getExtensionFilters().addAll(new FileChooser.ExtensionFilter(
                "POM Files", "pom.xml"
        ));

        File selectedFile = fileChooser.showOpenDialog(menu.getScene().getWindow());
        if (selectedFile != null) {
            Task openProjectTask = projectService.open(selectedFile);
            taskConsumers.forEach(tc -> tc.consume(openProjectTask));
            showJCasMetaData(null);
        }
    }

    public MenuSelection findMenuSelectionByRowColumnIndex(final int row, final int column) {
        if (column == 0) {
            return findMenuSelectionInFirstColumn(row);
        }

        if (column == 1) {
            ObservableList<Document> items = documents.getItems();
            if (0 <= row && row < items.size()) {
                return new MenuSelection(row, column, lookupTokenLabelFor(items.get(row)));
            }
        }

        return null;
    }

    private MenuSelection findMenuSelectionInFirstColumn(int row) {
        if (row < 0) {
            return null;
        }

        MenuSelection selection = null;
        int actualRow = 0;
        for (Node node : menuGrid.getChildren()) {
            if (menuGrid.getColumnIndex(node) == 0 && node instanceof TokenLabel) {

                selection = new MenuSelection(actualRow, 0, (TokenLabel) node);

                if (menuGrid.getRowIndex(node) == row) {
                    break;
                }
                ++actualRow;
            }
        }
        return selection;
    }

    private TokenLabel lookupTokenLabelFor(String annotationBehavior) {
        return annotationBehaviorTokenLabels.stream().filter(label -> Objects.equals(label.getId(), annotationBehavior))
                .findFirst().orElse(null);
    }

    private TokenLabel lookupTokenLabelFor(Document document) {
        if (document == null) {
            return null;
        }
        return (TokenLabel) documents.getScene().lookup("#doc_" + document.getName().hashCode());
    }

    @Override
    public boolean consume(Action event) {
        if (event.isMenuButtonPressed()) {
            setOpen(!isOpen());
            return true;
        }

        if (isOpen()) {
            if (isTransactionRunning()) {
                return true;
            }

            if (event.isMoveSelectionDown()) {
                navigateDown();
            } else if (event.isMoveSelectionUp()) {
                navigateUp();
            } else if (event.isMoveSelectionToLeft()) {
                navigateLeft();
            } else if (event.isMoveSelectionToRight()) {
                navigateRight();
            } else if (event.isAcceptButtonPressed()) {
                acceptPressed();
            }
            return true;
        }
        return false;
    }

    private class DocumentCell extends ListCell<Document> {

        @Override
        protected void updateItem(Document document, boolean empty) {
            super.updateItem(document, empty);

            if (empty) {
                setGraphic(null);
            } else {
                TokenLabel tokenLabel = new TokenLabel(document.getName());
                tokenLabel.setId("doc_" + document.getName().hashCode());
                tokenLabel.showSelectedIndicatorProperty().set(document.equals(selectedDocument.get()));
                tokenLabel.labelMinWidthProperty().bind(documents.prefWidthProperty().multiply(0.95));
                setGraphic(tokenLabel);
            }
        }
    }

    @EqualsAndHashCode(exclude = "label")
    private class MenuSelection {

        @Getter
        private final int row;

        @Getter
        private final int column;

        @Getter
        private final TokenLabel label;

        private MenuSelection(int row, int column, @NonNull TokenLabel label) {
            this.row = row;
            this.column = column;
            this.label = label;
        }

        private String getId() {
            return label.getId();
        }

        public boolean pointsToSecondColumn() {
            return column == 1;
        }

        public Document getSelectedDocument() {
            if (!pointsToSecondColumn()) {
                throw new IllegalStateException();
            }

            return projectService.documentsProperty().get(row);
        }
    }
}
