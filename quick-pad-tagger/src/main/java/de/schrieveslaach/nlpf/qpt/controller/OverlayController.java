package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import javafx.animation.FadeTransition;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import javafx.scene.effect.BoxBlur;
import javafx.scene.effect.Effect;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import javafx.util.Duration;
import lombok.Setter;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Base class for all {@link de.felixroske.jfxsupport.FXMLController controllers} which
 * display content above the annotation view.
 */
public abstract class OverlayController {

    private static final Logger LOGGER = LoggerFactory.getLogger(OverlayController.class);

    private static final int BLUR_AMOUNT = 25;
    private static final Effect FROST_EFFECT = new BoxBlur(BLUR_AMOUNT, BLUR_AMOUNT, 3);

    private ObjectProperty<Boolean> open = new SimpleObjectProperty<>(false);
    private ObjectProperty<Boolean> canChange = new SimpleObjectProperty<>(true);

    @Setter
    private SnapshotProvider snapshotProvider;

    public OverlayController() {
        open.addListener((observable, oldIsOpen, newIsOpen) -> {
            if (newIsOpen) {
                updateBackgroundFromSnapshot();
            }

            FadeTransition ft = new FadeTransition(Duration.millis(200), getOverlayNode());
            if (newIsOpen) {
                ft.setToValue(1.0);
            } else {
                ft.setToValue(0);
            }
            ft.setOnFinished(event -> Platform.runLater(() -> {
                endChangeTransaction();

                getFinishFadeTransitionHandler().finished(newIsOpen);
            }));
            ft.play();
        });
    }

    public void setOpen(boolean open) {
        synchronized (this.open) {
            if (this.open.get() == open) {
                return;
            }

            if (!beginChangeTransaction()) {
                return;
            }

            this.open.set(open);
        }
    }

    public boolean isOpen() {
        synchronized (open) {
            return open.get();
        }
    }

    protected boolean beginChangeTransaction() {
        synchronized (canChange) {
            if (!canChange.get()) {
                return false;
            }

            canChange.set(false);
        }
        return true;
    }

    protected void endChangeTransaction() {
        synchronized (canChange) {
            canChange.set(true);
        }
    }

    protected boolean isTransactionRunning() {
        synchronized (canChange) {
            return !canChange.get();
        }
    }

    private void updateBackgroundFromSnapshot() {
        StackPane background = getBackgroundStackPane();
        if (background == null) {
            return;
        }

        WritableImage snapshot = snapshotProvider.takeSnapshot();

        ImageView frost = new ImageView(snapshot);
        frost.fitWidthProperty().bind(background.widthProperty());
        frost.fitHeightProperty().bind(background.heightProperty());

        Pane frostPane = new Pane(frost);
        frostPane.setEffect(FROST_EFFECT);

        background.getChildren().clear();
        background.getChildren().add(frostPane);
    }

    protected abstract Node getOverlayNode();

    protected abstract StackPane getBackgroundStackPane();

    /**
     * Returns a {@link FinishFadeTransitionHandler handler} which will be invoked when the {@link FadeTransition}
     * has been finished.
     *
     * @return
     */
    protected FinishFadeTransitionHandler getFinishFadeTransitionHandler() {
        return isOpen -> LOGGER.debug("No listener provided for finished fade transition. {}", OverlayController.this.getClass());
    }

    @FunctionalInterface
    protected interface FinishFadeTransitionHandler {

        void finished(boolean isOpen);
    }
}
