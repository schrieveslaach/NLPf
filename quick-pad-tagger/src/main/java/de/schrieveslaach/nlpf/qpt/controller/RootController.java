package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.felixroske.jfxsupport.FXMLController;
import de.felixroske.jfxsupport.GUIState;
import de.schrieveslaach.nlpf.qpt.input.Keyboard;
import de.schrieveslaach.nlpf.qpt.input.event.ActionListener;
import de.schrieveslaach.nlpf.qpt.service.ProjectService;
import javafx.application.Platform;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Region;
import javafx.stage.Stage;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;

import java.util.List;


@FXMLController
public class RootController implements SnapshotProvider {

    @FXML
    private GridPane annotationGrid;

    @FXML
    private Region root;

    private final List<ActionConsumer> consumers;

    private final List<TaskConsumer> taskConsumers;

    private final Keyboard keyboard;

    private ProjectService projectService;

    @Autowired
    public RootController(
            @Qualifier("root-consumer") List<ActionConsumer> consumers,
            List<OverlayController> overlayControllers,
            List<TaskConsumer> taskConsumers,
            Keyboard keyboard,
            ProjectService projectService) {
        this.consumers = consumers;
        this.taskConsumers = taskConsumers;
        this.projectService = projectService;
        this.keyboard = keyboard;
        this.keyboard.add(inputActionListener());

        overlayControllers.forEach(oc -> oc.setSnapshotProvider(this));
    }

    @FXML
    public void initialize() {
        annotationGrid.setMaxWidth(Region.USE_COMPUTED_SIZE);

        // Set global key handler in order to control with keyboard
        Platform.runLater(() -> {
            root.getScene().setOnKeyPressed(keyboard);
            root.getScene().setOnKeyReleased(keyboard);
        });
    }

    @Bean
    public ActionListener inputActionListener() {
        return action -> Platform.runLater(() -> {
            if (action.isSaveButtonPressed()) {
                Task saveJCasTask = projectService.saveJCas();
                taskConsumers.forEach(tc -> tc.consume(saveJCasTask));
                return;
            }

            if (action.isFullscreenPressed()) {
                Stage stage = GUIState.getStage();
                stage.setFullScreen(!stage.isFullScreen());
                return;
            }

            for (ActionConsumer consumer : consumers) {
                if (consumer.consume(action)) {
                    break;
                }
            }
        });
    }

    @Override
    public WritableImage takeSnapshot() {
        WritableImage snapshot = new WritableImage(root.widthProperty().intValue(), root.heightProperty().intValue());
        root.getScene().snapshot(snapshot);
        return snapshot;
    }
}
