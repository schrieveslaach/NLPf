package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.felixroske.jfxsupport.FXMLController;
import de.schrieveslaach.nlpf.qpt.input.Action;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.control.TextArea;
import javafx.scene.layout.Pane;
import javafx.scene.layout.StackPane;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.annotation.Order;

import java.util.concurrent.atomic.AtomicBoolean;


@FXMLController
@Order(Integer.MIN_VALUE)
@Qualifier("root-consumer")
public class WaitController extends OverlayController implements ActionConsumer, TaskConsumer {

    @FXML
    private StackPane background;

    @FXML
    private Pane wait;

    @FXML
    private TextArea messages;

    @FXML
    private ProgressIndicator progressIndicator;

    private final AtomicBoolean closeImmediately = new AtomicBoolean(false);

    @Override
    public boolean consume(Action event) {
        return isOpen();
    }

    @Override
    public void consume(Task<?> task) {
        task.runningProperty().addListener((observableValue, oldIsRunning, newIsRunning) -> {
            progressIndicator.setVisible(newIsRunning);

            if (!newIsRunning) {
                closeImmediately.set(true);
            }
            setOpen(newIsRunning);

            if (oldIsRunning) {
                messages.clear();
            }
        });

        task.messageProperty().addListener((observableValue, oldMessage, newMessage) -> {
            messages.appendText(newMessage);
            messages.appendText("\n");
        });

        task.exceptionProperty().addListener((observableValue, oldException, newException) -> {
            closeImmediately.set(true);
            setOpen(false);
        });
    }

    @Override
    protected Node getOverlayNode() {
        return wait;
    }

    @Override
    protected StackPane getBackgroundStackPane() {
        return background;
    }

    @Override
    protected FinishFadeTransitionHandler getFinishFadeTransitionHandler() {
        return isOpen -> {
            if (closeImmediately.get()) {
                setOpen(false);
                closeImmediately.set(false);
            }
        };
    }
}
