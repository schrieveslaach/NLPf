package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.felixroske.jfxsupport.FXMLController;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.animation.KeyFrame;
import javafx.animation.Timeline;
import javafx.application.Platform;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ObservableObjectValue;
import javafx.fxml.FXML;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.StackPane;
import javafx.scene.web.WebView;
import javafx.util.Duration;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.annotation.Order;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.concurrent.atomic.AtomicLong;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

@FXMLController
@Order(2)
@Qualifier("root-consumer")
public class WiktionaryController extends OverlayController implements ActionConsumer {

    /**
     * Duration of scroll animation in ms
     */
    private static final long SCROLL_ANIMATION_DURATION = 100;

    private static final Logger LOGGER = LoggerFactory.getLogger(WiktionaryController.class);

    private static final String HTTPS_EN_WIKTIONARY_ORG = "https://en.wiktionary.org/wiki/";

    final ObjectProperty<String> url = new SimpleObjectProperty<>(HTTPS_EN_WIKTIONARY_ORG);

    /**
     * Workaround for unit tests: the {@link WebView#getEngine() WebEngine load method} causes
     * a core dump… This property disables the loading during tests.
     */
    @Value("${invoke.web.engine.load:true}")
    private boolean invokeWebEngineLoad;

    @FXML
    private AnchorPane wiktionary;

    @FXML
    private WebView webview;

    @FXML
    private StackPane background;

    private final AtomicLong top = new AtomicLong(0L);

    @Autowired
    public WiktionaryController(ObservableObjectValue<Selection> selectionProperty) {
        bindTo(selectionProperty);
    }

    @Override
    protected Node getOverlayNode() {
        return wiktionary;
    }

    @Override
    protected StackPane getBackgroundStackPane() {
        return background;
    }

    @Override
    protected FinishFadeTransitionHandler getFinishFadeTransitionHandler() {
        return isOpen -> {
            if (isOpen) {
                synchronized (url) {
                    // Test workaround. see property definition
                    if (invokeWebEngineLoad) {
                        top.set(0L);
                        webview.getEngine().load(url.get());
                    }
                }
            }
        };
    }

    @FXML
    public void initialize() {
        webview.getEngine().setUserAgent("Mozilla/5.0 (Android; Mobile; rv:13.0) Gecko/13.0 Firefox/13.0");
    }

    private void bindTo(ObservableObjectValue<Selection> selectionProperty) {
        selectionProperty.addListener((observable, oldValue, newValue) ->
                Platform.runLater(() -> {
                    if (newValue == null) {
                        return;
                    }

                    // if there is more than one selected token, they are combined with "_" for the search
                    String coveredText = newValue.getSelectedTokens().stream().map(Token::getCoveredText).collect(Collectors.joining("_"));
                    // not sure if this is the correct regex
                    if (!Pattern.matches("^[A-Za-z0-9]+[A-Za-z0-9_]+", coveredText)) {
                        url.setValue(HTTPS_EN_WIKTIONARY_ORG);
                        return;
                    }

                    synchronized (url) {
                        try {
                            url.set(String.format(
                                    "https://%s.wiktionary.org/wiki/%s",
                                    URLEncoder.encode(newValue.getSelectedTokens().iterator().next().getCAS().getDocumentLanguage(), "UTF-8"),
                                    URLEncoder.encode(coveredText, "UTF-8")
                            ));
                        } catch (UnsupportedEncodingException e) {
                            LOGGER.error("Cannot encode url, this should never happen.", e);
                        }
                    }
                })
        );
    }


    private void scroll(boolean down) {
        int y = down ? 75 : -75;

        if (top.get() + y < 0) {
            return;
        }

        if (!beginChangeTransaction()) {
            return;
        }

        webview.getEngine().executeScript(String.format(
                "$('html, body').animate({ " +
                        "scrollTop: %d " +
                        "}, %d);",
                top.addAndGet(y),
                SCROLL_ANIMATION_DURATION
        ));

        new Timeline(new KeyFrame(Duration.millis(SCROLL_ANIMATION_DURATION), ae ->
                endChangeTransaction()
        )).play();
    }

    @Override
    public boolean consume(Action event) {
        if (event.isInfoButtonPressed()) {
            setOpen(!isOpen());
            return true;
        }

        if (isOpen()) {
            if (event.isMoveViewUp()) {
                scroll(false);
            }

            if (event.isMoveViewDown()) {
                scroll(true);
            }

            return true;
        }

        return false;
    }

}
