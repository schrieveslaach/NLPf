package de.schrieveslaach.nlpf.qpt.input;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.studiohartman.jamepad.ControllerState;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.Builder;
import lombok.EqualsAndHashCode;
import lombok.Getter;

@Builder
@EqualsAndHashCode
public class Action {

    @Getter
    private boolean menuButtonPressed;

    @Getter
    private boolean moveSelectionToLeft;

    @Getter
    private boolean moveSelectionToRight;

    @Getter
    private boolean moveSelectionUp;

    @Getter
    private boolean moveSelectionDown;

    @Getter
    private boolean moveSelectionToBegin;

    @Getter
    private boolean moveSelectionToEnd;

    @Getter
    private boolean moveViewToLeft;

    @Getter
    private boolean moveViewToRight;

    @Getter
    private boolean moveViewUp;

    @Getter
    private boolean moveViewDown;

    @Getter
    private boolean acceptButtonPressed;

    @Getter
    private boolean removeLeftAnnotationPressed;

    @Getter
    private boolean removeRightAnnotationPressed;

    @Getter
    private boolean infoButtonPressed;

    @Getter
    private boolean saveButtonPressed;

    @Getter
    private boolean leftTriggerPressed;

    @Getter
    private boolean fullscreenPressed;

    static Action create(ControllerState state) {
        return builder()
                .menuButtonPressed(state.guideJustPressed)
                .moveSelectionToRight(state.leftStickX > 0.7)
                .moveSelectionToLeft(state.leftStickX < -0.7)
                .moveSelectionUp(state.leftStickY > 0.7)
                .moveSelectionDown(state.leftStickY < -0.7)
                .moveSelectionToBegin(state.dpadUp)
                .moveSelectionToEnd(state.dpadDown)
                .acceptButtonPressed(state.aJustPressed)
                .removeLeftAnnotationPressed(state.xJustPressed)
                .removeRightAnnotationPressed(state.bJustPressed)
                .infoButtonPressed(state.yJustPressed)
                .saveButtonPressed(state.startJustPressed)
                .moveViewToRight(state.rightStickX > 0.7)
                .moveViewToLeft(state.rightStickX < -0.7)
                .moveViewUp(state.rightStickY > 0.7)
                .moveViewDown(state.rightStickY < -0.7)
                .leftTriggerPressed(state.leftTrigger > 0.7)
                .build();
    }

    static Action create(KeyEvent event) {
        return builder()
                .menuButtonPressed(event.getCode() == KeyCode.M)
                .moveSelectionToRight(event.getCode() == KeyCode.RIGHT)
                .moveSelectionToLeft(event.getCode() == KeyCode.LEFT)
                .moveSelectionUp(event.getCode() == KeyCode.UP)
                .moveSelectionDown(event.getCode() == KeyCode.DOWN)
                .moveSelectionToBegin(event.isAltDown() && event.getCode() == KeyCode.HOME)
                .moveSelectionToEnd(event.isAltDown() && event.getCode() == KeyCode.END)
                .acceptButtonPressed(event.getCode() == KeyCode.ENTER)
                .removeLeftAnnotationPressed(event.getCode() == KeyCode.DELETE)
                .removeRightAnnotationPressed(!event.isAltDown() && event.getCode() == KeyCode.END)
                .infoButtonPressed(event.getCode() == KeyCode.I)
                .saveButtonPressed(event.getCode() == KeyCode.S && event.isControlDown())
                .leftTriggerPressed(event.isControlDown())
                .fullscreenPressed(event.getCode() == KeyCode.F11)
                .build();
    }

}
