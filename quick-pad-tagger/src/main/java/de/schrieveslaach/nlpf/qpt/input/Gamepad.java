package de.schrieveslaach.nlpf.qpt.input;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.studiohartman.jamepad.ControllerManager;
import com.studiohartman.jamepad.ControllerState;
import de.schrieveslaach.nlpf.qpt.input.event.ActionListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;

/**
 *
 */
@Component
@Profile("!test")
public class Gamepad {

    private final ControllerManager controllerManager;

    private final List<ConnectionStateHandler> connectionStateHandlers;

    private final List<ActionListener> actionListeners;

    private AtomicBoolean wasConnected = new AtomicBoolean(false);

    @Autowired
    public Gamepad(List<ConnectionStateHandler> connectionStateHandlers, List<ActionListener> actionListeners) {
        this.actionListeners = actionListeners;
        this.connectionStateHandlers = connectionStateHandlers;

        this.controllerManager = new ControllerManager();
        this.controllerManager.initSDLGamepad();
    }

    private void notifyOfConnectionStateChange(ControllerState state) {
        if (state.isConnected != wasConnected.get()) {
            wasConnected.set(state.isConnected);
            connectionStateHandlers.forEach(csh -> csh.changed(state.isConnected));
        }
    }

    @Scheduled(fixedRate = 50)
    public void requestControllerState() {
        ControllerState state = controllerManager.getState(0);
        notifyOfConnectionStateChange(state);

        if (state.isConnected) {
            Action action = Action.create(state);
            actionListeners.stream().forEach(l -> l.handle(action));
        }
    }

    @FunctionalInterface
    public interface ConnectionStateHandler {

        void changed(boolean connected);

    }

}
