package de.schrieveslaach.nlpf.qpt.input;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.input.event.ActionListener;
import javafx.event.EventHandler;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Keyboard implements EventHandler<KeyEvent> {

    private final List<ActionListener> actionListeners = new ArrayList<>();

    public void add(ActionListener actionListener) {
        actionListeners.add(actionListener);
    }

    @Override
    public void handle(KeyEvent keyEvent) {
        if (keyEvent.getEventType() == KeyEvent.KEY_RELEASED) {
            if (keyEvent.getCode() == KeyCode.CONTROL) {
                Action action = Action.builder().build();
                actionListeners.stream().forEach(l -> l.handle(action));
            }

            return;
        }

        Action action = Action.create(keyEvent);
        actionListeners.stream().forEach(l -> l.handle(action));
    }
}
