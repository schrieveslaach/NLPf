package de.schrieveslaach.nlpf.qpt.model;
/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.commons.io.FilenameUtils;

import java.io.File;

@EqualsAndHashCode
public class Document implements Comparable<Document> {

    @Getter
    private File doc;

    public Document(File file) {
        doc = new File(file.toURI());
    }

    public String getName() {
        return doc.getName();
    }

    public String getExtension() {
        return "." + FilenameUtils.getExtension(getName());
    }

    @Override
    public int compareTo(Document document) {
        return getName().toLowerCase().compareTo(document.getName().toLowerCase());
    }
}
