package de.schrieveslaach.nlpf.qpt.model;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableList;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import org.apache.uima.jcas.JCas;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

import static java.util.Arrays.asList;
import static org.apache.uima.fit.util.JCasUtil.selectCovered;
import static org.apache.uima.fit.util.JCasUtil.selectCovering;

@EqualsAndHashCode
public class Selection {

    @Getter
    private final ImmutableList<Token> selectedTokens;

    @Getter
    private final ImmutableList<Token> selectedSentenceTokens;

    @Getter
    private final Sentence selectedSentence;

    private final boolean coversMultipleSentences;

    public Selection() {
        this.selectedSentenceTokens = ImmutableList.of();
        this.selectedTokens = ImmutableList.of();
        this.selectedSentence = null;
        this.coversMultipleSentences = false;
    }

    public Selection(JCas jCas, Token... selectedTokens) {
        this(jCas, asList(selectedTokens));
    }

    public Selection(JCas jCas, List<Token> selectedTokens) {
        this.selectedTokens = ImmutableList.copyOf(selectedTokens);

        Token firstSelectedToken = this.selectedTokens.get(0);
        Token lastSelectedToken = this.selectedTokens.get(this.selectedTokens.size() - 1);

        Set<Sentence> sentences = new LinkedHashSet<>();
        sentences.addAll(selectCovering(jCas, Sentence.class, firstSelectedToken.getBegin(), firstSelectedToken.getEnd()));
        sentences.addAll(selectCovering(jCas, Sentence.class, lastSelectedToken.getBegin(), lastSelectedToken.getEnd()));
        this.coversMultipleSentences = sentences.size() > 1;

        if (!sentences.isEmpty()) {
            selectedSentence = sentences.iterator().next();
            selectedSentenceTokens = ImmutableList.copyOf(selectCovered(jCas, Token.class, selectedSentence.getBegin(), selectedSentence.getEnd()));
        } else {
            selectedSentence = null;
            selectedSentenceTokens = ImmutableList.of();
        }
    }

    public boolean coversMultipleSentences() {
        return coversMultipleSentences;
    }

    public int size() {
        return selectedTokens.size();
    }

    public boolean isEmpty() {
        return selectedTokens.isEmpty();
    }

    public ImmutableList<Token> getFocusedTokens() {
        ImmutableList.Builder<Token> focusedTokens = ImmutableList.builder();

        for (Token sentenceToken : selectedSentenceTokens) {
            if (selectedTokens.contains(sentenceToken)) {
                continue;
            }

            focusedTokens.add(sentenceToken);
        }

        return focusedTokens.build();
    }

    /**
     * @return the minimum {@link Token#getBegin()} of {@link #getSelectedTokens() selected tokens}
     */
    public int getBegin() {
        return selectedTokens.stream()
                .mapToInt(Token::getBegin)
                .min()
                .orElse(0);
    }

    /**
     * @return the maximum {@link Token#getEnd()} of {@link #getSelectedTokens() selected tokens}
     */
    public int getEnd() {
        return selectedTokens.stream()
                .mapToInt(Token::getEnd)
                .max()
                .orElse(0);
    }

    public boolean beginsWith(Token token) {
        return !isEmpty() && Objects.equals(selectedTokens.get(0), token);
    }

    public boolean beginsWith(Selection other) {
        if (isEmpty() || other == null || other.isEmpty()) {
            return false;
        }

        return Objects.equals(other.selectedTokens.get(0), selectedTokens.get(0));
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        Token previous = null;
        boolean tokenContainedInSelection = false;

        for (Token t : selectedSentenceTokens) {
            if (tokenContainedInSelection && !selectedTokens.contains(t)) {
                str.append('«');
                tokenContainedInSelection = false;
            }

            if (previous != null && previous.getEnd() < t.getBegin()) {
                str.append(' ');
            }

            if (!tokenContainedInSelection && selectedTokens.contains(t)) {
                str.append('»');
                tokenContainedInSelection = true;
            }

            str.append(t.getText());

            previous = t;
        }

        if (tokenContainedInSelection) {
            str.append('«');
        }

        return str.toString();
    }

    public boolean overlapsWith(Selection other) {
        return (getBegin() <= other.getBegin() && getEnd() >= other.getBegin()) ||
                (getBegin() >= other.getBegin() && getBegin() <= other.getEnd());
    }
}
