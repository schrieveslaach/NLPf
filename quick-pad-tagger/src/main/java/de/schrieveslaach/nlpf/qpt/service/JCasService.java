package de.schrieveslaach.nlpf.qpt.service;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableList;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import org.apache.uima.UIMAException;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.jcas.tcas.Annotation;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.apache.uima.fit.pipeline.SimplePipeline.runPipeline;

@Service
public class JCasService {

    private final SimpleObjectProperty<JCas> jCas = new SimpleObjectProperty<>();

    private final ObservableList<Token> tokensProperty = FXCollections.observableArrayList();
    private final ObservableList<Token> unmodifiableTokensProperty = FXCollections.unmodifiableObservableList(tokensProperty);

    private final ObservableList<Sentence> sentencesProperty = FXCollections.observableArrayList();
    private final ObservableList<Sentence> unmodifiableSentencesProperty = FXCollections.unmodifiableObservableList(sentencesProperty);

    private final ObservableList<NamedEntity> namedEntitiesProperty = FXCollections.observableArrayList();
    private final ObservableList<NamedEntity> unmodifiableNamedEntitiesProperty = FXCollections.unmodifiableObservableList(namedEntitiesProperty);

    public JCasService() {
        jCas.addListener((observableValue, oldJCas, newJCas) -> {
            sentencesProperty.clear();
            tokensProperty.clear();
            namedEntitiesProperty.clear();

            if (newJCas != null) {
                tokensProperty.addAll(JCasUtil.select(newJCas, Token.class));
                sentencesProperty.addAll(JCasUtil.select(newJCas, Sentence.class));
                namedEntitiesProperty.addAll(JCasUtil.select(newJCas, NamedEntity.class));
            }
        });
    }

    @Bean
    public ObservableList<Token> tokensProperty() {
        return unmodifiableTokensProperty;
    }

    @Bean
    public ObservableList<Sentence> sentencesProperty() {
        return unmodifiableSentencesProperty;
    }

    @Bean
    public ObservableList<NamedEntity> namedEntitiesProperty() {
        return unmodifiableNamedEntitiesProperty;
    }

    @Bean
    public ReadOnlyObjectProperty<JCas> jCasProperty() {
        return jCas;
    }

    public Selection createSelectionForFirstToken() {
        return new Selection(
                jCasProperty().get(),
                tokensProperty.get(0)
        );
    }

    public Selection createSelectionForLastToken() {
        return new Selection(
                jCasProperty().get(),
                tokensProperty.get(tokensProperty.size() - 1)
        );
    }

    public void setJCas(JCas jCas) {
        this.jCas.set(jCas);
    }

    public void save(AnalysisEngineDescription writerDescription) throws UIMAException {
        runPipeline(jCas.get(), writerDescription);
    }

    public <T extends Annotation> Collection<T> select(Class<T> cls) {
        return JCasUtil.select(jCas.get(), cls);
    }

    public <T extends Annotation> List<T> selectFollowing(Class<T> cls, T annotation, int size) {
        return JCasUtil.selectFollowing(jCas.get(), cls, annotation, size);
    }

    public <T extends Annotation> List<T> selectPreceding(Class<T> cls, T annotation, int size) {
        return JCasUtil.selectPreceding(jCas.get(), cls, annotation, size);
    }

    public <T extends Annotation> List<T> selectAt(Class<T> cls, int begin, int end) {
        return JCasUtil.selectAt(jCasProperty().get(), cls, begin, end);
    }

    public <T extends Annotation> List<T> selectCovering(Class<T> cls, int begin, int end) {
        return JCasUtil.selectCovering(jCasProperty().get(), cls, begin, end);
    }

    public <T extends Annotation> List<T> selectCovering(Class<T> cls, Annotation annotation) {
        return JCasUtil.selectCovering(jCasProperty().get(), cls, annotation);
    }

    /**
     * @see JCasUtil#indexCovering(JCas, Class, Class)
     */
    private <T extends Annotation, S extends Annotation> Map<T, Collection<S>> indexCovering(Class<T> cls, Class<S> coveringCls) {
        return JCasUtil.indexCovering(jCasProperty().get(), cls, coveringCls);
    }

    /**
     * @see JCasUtil#selectCovered(JCas, Class, AnnotationFS)
     */
    public <T extends Annotation> List<T> selectCovered(Class<T> cls, AnnotationFS annotationFS) {
        return JCasUtil.selectCovered(jCasProperty().get(), cls, annotationFS);
    }

    public Selection splitToken(Token tokenToSplit, int cursorIndex) {
        int i = tokensProperty.indexOf(tokenToSplit);
        if (i < 0) {
            return null;
        }

        int begin = tokenToSplit.getBegin();
        int end = tokenToSplit.getEnd();

        Token firstToken = new Token(jCas.get(), begin, begin + cursorIndex + 1);
        Token secondToken = new Token(jCas.get(), begin + cursorIndex + 1, end);

        tokenToSplit.removeFromIndexes();

        firstToken.addToIndexes();
        secondToken.addToIndexes();

        tokensProperty.remove(tokenToSplit);
        tokensProperty.addAll(i, asList(firstToken, secondToken));

        return new Selection(jCas.get(), secondToken);
    }

    public Selection splitSentenceAtToken(Token token) {
        Map<Token, Collection<Sentence>> tokenSentenceMap = indexCovering(Token.class, Sentence.class);
        Optional<Sentence> optSentence = tokenSentenceMap.get(token).stream().findFirst();

        if (optSentence.isPresent()) {
            Sentence sentence = optSentence.get();

            if (token.getEnd() == sentence.getEnd()) {
                return null;
            }

            Sentence firstSentence = new Sentence(jCas.get(), sentence.getBegin(), token.getEnd());
            Sentence secondSentence = new Sentence(jCas.get(), token.getEnd(), sentence.getEnd());

            sentence.removeFromIndexes();
            firstSentence.addToIndexes();
            secondSentence.addToIndexes();

            int i = sentencesProperty.indexOf(sentence);
            sentencesProperty.remove(sentence);
            sentencesProperty.addAll(i, asList(firstSentence, secondSentence));

            return new Selection(jCas.get(), token);
        }

        return null;
    }

    public Selection mergeTokenWithPrevious(Selection selection) {
        if (selection.isEmpty()) {
            return null;
        }

        ImmutableList<Token> selectedTokens = selection.getSelectedTokens();
        Token selectedToken = selectedTokens.get(0);

        List<Token> preceding = selectPreceding(Token.class, selectedToken, 1);
        if (preceding.isEmpty()) {
            return null;
        }

        Token precedingToken = preceding.get(0);
        if (precedingToken.getEnd() == selectedToken.getBegin()) {
            Token combinedToken = new Token(jCas.get(), precedingToken.getBegin(), selectedToken.getEnd());
            combinedToken.addToIndexes();

            selectedToken.removeFromIndexes();
            precedingToken.removeFromIndexes();

            int i = tokensProperty.indexOf(precedingToken);
            tokensProperty.removeAll(Arrays.asList(precedingToken, selectedToken));
            tokensProperty.add(i, combinedToken);

            return new Selection(jCas.get(), combinedToken);
        }

        return null;
    }

    public Selection mergeSentences(Selection selection) {
        if (selection.isEmpty()) {
            return null;
        }

        Sentence selectedSentence = selection.getSelectedSentence();
        Token selectedToken = selection.getSelectedTokens().get(0);
        if (selectedSentence.getEnd() != selectedToken.getEnd()) {
            return null;
        }

        List<Sentence> followingSentences = selectFollowing(Sentence.class, selectedSentence, 1);
        if (followingSentences.isEmpty()) {
            return null;
        }

        Sentence followingSentence = followingSentences.get(0);

        Sentence combinedSentence = new Sentence(jCas.get(), selectedSentence.getBegin(), followingSentence.getEnd());
        combinedSentence.addToIndexes();

        selectedSentence.removeFromIndexes();
        followingSentence.removeFromIndexes();

        int i = sentencesProperty.indexOf(selectedSentence);
        sentencesProperty.removeAll(selectedSentence, followingSentence);
        sentencesProperty.add(i, combinedSentence);

        return new Selection(jCas.get(), selectedToken);
    }

    public List<Token> removeNamedEntity(Selection selection) {
        List<NamedEntity> namedEntities = selectCovering(NamedEntity.class, selection.getBegin(), selection.getEnd());

        for (NamedEntity namedEntity : namedEntities) {
            List<Token> namedEntityTokens = selectCovered(Token.class, namedEntity);

            jCas.get().removeFsFromIndexes(namedEntity);
            namedEntitiesProperty.remove(namedEntity);

            return namedEntityTokens;
        }

        return asList();
    }

    public void createNamedEntity(Selection selection, String namedEntityType) {
        List<NamedEntity> namedEntities = selectCovering(NamedEntity.class, selection.getBegin(), selection.getEnd());

        if (!namedEntities.isEmpty()) {
            namedEntities.forEach(ne -> {
                ne.removeFromIndexes(jCas.get());
                namedEntitiesProperty.remove(ne);
            });
        }

        NamedEntity namedEntity = new NamedEntity(jCas.get(), selection.getBegin(), selection.getEnd());
        namedEntity.setValue(namedEntityType);
        namedEntity.addToIndexes();
        namedEntitiesProperty.add(namedEntity);
    }
}
