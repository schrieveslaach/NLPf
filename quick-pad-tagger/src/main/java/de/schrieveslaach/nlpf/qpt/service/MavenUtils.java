package de.schrieveslaach.nlpf.qpt.service;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.SneakyThrows;
import org.apache.maven.cli.MavenCli;
import org.apache.maven.model.Model;
import org.apache.maven.model.io.xpp3.MavenXpp3Reader;
import org.codehaus.plexus.classworlds.ClassWorld;
import org.codehaus.plexus.util.xml.pull.XmlPullParserException;
import org.fusesource.jansi.AnsiOutputStream;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.PrintStream;
import java.io.StringReader;
import java.nio.charset.StandardCharsets;
import java.util.Scanner;

public class MavenUtils {

    private MavenUtils() {
    }

    @SneakyThrows(IOException.class)
    public static <R> R doMavenCli(File pom, String goal, ClassLoader classLoader, DoMavenCliResult<R> doMavenCliResult) throws ServiceException {
        MavenCli cli = new MavenCli(new ClassWorld("qpt", classLoader));
        System.setProperty(MavenCli.MULTIMODULE_PROJECT_DIRECTORY, pom.getParentFile().getAbsolutePath());

        try (ByteArrayOutputStream baosStd = new ByteArrayOutputStream();
             ByteArrayOutputStream baosErr = new ByteArrayOutputStream()) {

            int exitCode = cli.doMain(
                    new String[]{goal},
                    pom.getParentFile().getAbsolutePath(),
                    new PrintStream(new AnsiOutputStream(baosStd)),
                    new PrintStream(new AnsiOutputStream(baosErr))
            );

            String stdout = new String(baosStd.toByteArray(), StandardCharsets.UTF_8);

            if (exitCode != 0) {
                throw new ServiceException(String.format("Could not execute `%s`%n%n%s", goal, stdout));
            }

            return doMavenCliResult.doCli(stdout);
        }

    }

    /**
     * Executes {@code mvn help:effective-pom} and parses the {@link Model Maven model}
     * from standard out.
     *
     * @param pom
     * @param classLoader
     * @return
     * @throws ServiceException
     */
    public static Model effectivePom(File pom, ClassLoader classLoader) throws ServiceException {
        return doMavenCli(pom, "help:effective-pom", classLoader, (String stdout) -> {
            MavenXpp3Reader reader = new MavenXpp3Reader();
            try {
                return reader.read(new StringReader(parsePomXml(stdout)));
            } catch (IOException | XmlPullParserException e) {
                throw new ServiceException("Could not parse effective pom.", e);
            }
        });
    }

    private static String parsePomXml(String stdout) {
        try (Scanner scanner = new Scanner(stdout)) {

            StringBuilder pomXml = new StringBuilder();
            boolean addToXml = false;
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();

                if (!addToXml) {
                    if (line.startsWith("<project")) {
                        addToXml = true;
                    } else {
                        continue;
                    }
                } else {
                    if (line.startsWith("</project>")) {
                        addToXml = false;
                    }
                }

                pomXml.append(line).append("\n");
            }
            return pomXml.toString();
        }
    }

    @FunctionalInterface
    public interface DoMavenCliResult<R> {

        R doCli(String stdout) throws ServiceException;
    }

}
