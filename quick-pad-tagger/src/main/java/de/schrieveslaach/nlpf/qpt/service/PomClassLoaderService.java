package de.schrieveslaach.nlpf.qpt.service;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.maven.model.Model;
import org.apache.maven.model.Repository;
import org.apache.maven.repository.internal.MavenRepositorySystemUtils;
import org.eclipse.aether.DefaultRepositorySystemSession;
import org.eclipse.aether.RepositorySystem;
import org.eclipse.aether.artifact.Artifact;
import org.eclipse.aether.artifact.DefaultArtifact;
import org.eclipse.aether.connector.basic.BasicRepositoryConnectorFactory;
import org.eclipse.aether.impl.DefaultServiceLocator;
import org.eclipse.aether.repository.LocalRepository;
import org.eclipse.aether.repository.RemoteRepository;
import org.eclipse.aether.resolution.ArtifactRequest;
import org.eclipse.aether.resolution.ArtifactResolutionException;
import org.eclipse.aether.resolution.ArtifactResult;
import org.eclipse.aether.spi.connector.RepositoryConnectorFactory;
import org.eclipse.aether.spi.connector.transport.TransporterFactory;
import org.eclipse.aether.transport.file.FileTransporterFactory;
import org.eclipse.aether.transport.http.HttpTransporterFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static de.schrieveslaach.nlpf.qpt.service.MavenUtils.doMavenCli;
import static de.schrieveslaach.nlpf.qpt.service.MavenUtils.effectivePom;

@Service
public class PomClassLoaderService {

    private static final Logger LOGGER = LoggerFactory.getLogger(PomClassLoaderService.class);

    private final DefaultServiceLocator locator;

    private final RepositorySystem repositorySystem;

    private final DefaultRepositorySystemSession session;

    public PomClassLoaderService() {
        locator = MavenRepositorySystemUtils.newServiceLocator();
        locator.addService(RepositoryConnectorFactory.class, BasicRepositoryConnectorFactory.class);
        locator.addService(TransporterFactory.class, FileTransporterFactory.class);
        locator.addService(TransporterFactory.class, HttpTransporterFactory.class);

        repositorySystem = locator.getService(RepositorySystem.class);

        session = MavenRepositorySystemUtils.newSession();

        LocalRepository localRepo = new LocalRepository(System.getProperty("user.home") + "/.m2/repository");
        session.setLocalRepositoryManager(repositorySystem.newLocalRepositoryManager(session, localRepo));
    }

    /**
     * Parses the given {@link File pom.xml file} and creates a {@link ClassLoader} which extends the classpath by the
     * Maven dependencies.
     *
     * @param pom
     * @return
     */
    @SneakyThrows(MalformedURLException.class)
    public ClassLoader getClassLoaderFromPom(File pom) throws ServiceException {
        if (!pom.isFile()) {
            throw new ServiceException("Could not read pom file: file does not exists");
        }

        List<URL> jars = new ArrayList<>();

        Model model = effectivePom(pom, getClass().getClassLoader());

        tryToAddProjectArtifact(model, jars);

        List<Repository> repositories = model.getRepositories();

        for (Dependency dependency : parseDependencies(pom)) {
            Artifact artifact = getArtifact(repositories, dependency);
            jars.add(artifact.getFile().toURI().toURL());
        }

        return new URLClassLoader(jars.toArray(new URL[jars.size()]), getClass().getClassLoader());
    }

    private Artifact getArtifact(List<Repository> repositories, Dependency dependency) throws ServiceException {
        try {
            Artifact artifact = new DefaultArtifact(dependency.getGroupId(),
                    dependency.getArtifactId(),
                    dependency.getType(),
                    dependency.getVersion());

            ArtifactRequest artifactRequest = new ArtifactRequest();
            artifactRequest.setArtifact(artifact);

            for (Repository repository : repositories) {
                artifactRequest.addRepository(new RemoteRepository.Builder(
                        repository.getId(),
                        repository.getLayout(),
                        repository.getUrl()
                ).build());
            }

            ArtifactResult artifactResult = repositorySystem.resolveArtifact(session, artifactRequest);
            return artifactResult.getArtifact();
        } catch (ArtifactResolutionException e) {
            LOGGER.error("Could not resolve artifact.", e);
            throw new ServiceException("Could not resolve artifact.", e);
        }
    }

    /**
     * Tries to load the project coordinates as artifact in order to load a pre-compiled best-performing NLP pipeline
     * into the classpath.
     *
     * @param model
     * @param jars
     */
    @SneakyThrows(MalformedURLException.class)
    private void tryToAddProjectArtifact(Model model, List<URL> jars) {
        try {
            jars.add(getArtifact(model.getRepositories(), new Dependency(
                    model.getGroupId(),
                    model.getArtifactId(),
                    model.getVersion(),
                    "jar"
            )).getFile().toURI().toURL());
        } catch (ServiceException e) {
            LOGGER.warn("could not resolve project artifact, no pipeline available", e);
        }
    }

    private List<Dependency> parseDependencies(File pom) throws ServiceException {
        List<Dependency> dependencies = new ArrayList<>();

        doMavenCli(pom, "dependency:tree", getClass().getClassLoader(), stdout -> {
            Pattern artifactPattern = Pattern.compile("^.*\\S\\s+(?<groupId>.*):(?<artifactId>.*):(?<type>.*):(?<version>.*):compile$");

            Scanner scanner = new Scanner(stdout);
            while (scanner.hasNextLine()) {
                String line = scanner.nextLine();
                Matcher matcher = artifactPattern.matcher(line);

                if (matcher.matches()) {
                    dependencies.add(new Dependency(
                            matcher.group("groupId"),
                            matcher.group("artifactId"),
                            matcher.group("version"),
                            matcher.group("type")
                    ));
                }
            }

            return null;
        });

        return dependencies;
    }

    @AllArgsConstructor
    private static class Dependency {

        @Getter
        private final String groupId;
        @Getter
        private final String artifactId;
        @Getter
        private final String version;
        @Getter
        private final String type;
    }

}
