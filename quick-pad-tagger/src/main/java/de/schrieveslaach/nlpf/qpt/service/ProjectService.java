package de.schrieveslaach.nlpf.qpt.service;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import de.schrieveslaach.nlpf.qpt.model.Document;
import de.tudarmstadt.ukp.dkpro.core.api.io.JCasFileWriter_ImplBase;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.tokit.RegexSegmenter;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.uima.UIMAFramework;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.cas.Type;
import org.apache.uima.cas.text.AnnotationFS;
import org.apache.uima.collection.CollectionReaderDescription;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.fit.internal.ResourceManagerFactory;
import org.apache.uima.fit.pipeline.JCasIterable;
import org.apache.uima.fit.util.CasUtil;
import org.apache.uima.fit.util.LifeCycleUtil;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;
import org.apache.uima.resource.ResourceManager;
import org.apache.uima.util.InvalidXMLException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import java.io.File;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ExecutorService;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.plumbing.BestPerformingPipelineFactory.containsClasspathPipelinesTxt;
import static de.schrieveslaach.nlpf.plumbing.BestPerformingPipelineFactory.createBestPerformingPipelineEngineDescription;
import static de.schrieveslaach.nlpf.plumbing.CollectionReaderFactory.findReaderDescriptionByFileExtension;
import static de.schrieveslaach.nlpf.plumbing.JCasFileWriterFactory.findWriterDescriptionByFileExtension;
import static de.schrieveslaach.nlpf.qpt.service.MavenUtils.effectivePom;
import static javafx.collections.FXCollections.observableArrayList;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.pipeline.SimplePipeline.iteratePipeline;

@Service
public class ProjectService {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProjectService.class);

    private final StringProperty posTagSetProperty = new SimpleStringProperty();

    private final StringProperty defaultLanguageProperty = new SimpleStringProperty();

    private final ObservableList<String> namedEntitiesProperty = FXCollections.observableArrayList();
    private final ObservableList<String> unmodifiableNamedEntitiesProperty = FXCollections.unmodifiableObservableList(namedEntitiesProperty);

    private final SimpleObjectProperty<Document> currentDocument = new SimpleObjectProperty<>();

    private final ObservableList<Document> documents = observableArrayList();
    private final ObservableList<Document> unmodifiableDocuments = FXCollections.unmodifiableObservableList(documents);

    private final PomClassLoaderService classLoaderService;

    private final ExecutorService executor;

    private final JCasService jCasService;

    /**
     * Currently selected class loader of the project
     */
    private ClassLoader classLoader;

    @Autowired
    public ProjectService(PomClassLoaderService classLoaderService, ExecutorService executor, JCasService jCasService) {
        this.classLoaderService = classLoaderService;
        this.executor = executor;
        this.jCasService = jCasService;
    }

    public Task open(File pom) {
        Task<LoadProjectResult> loadDocsTask = new Task<LoadProjectResult>() {
            @Override
            protected LoadProjectResult call() throws Exception {
                LOGGER.info("Start to load Maven project from {}", pom);

                updateMessage("Create class loader from pom dependencies…");
                ClassLoader classLoaderFromPom = classLoaderService.getClassLoaderFromPom(pom);

                updateMessage("Load project properties…");
                Properties properties = effectivePom(pom, classLoaderFromPom).getProperties();

                List<Document> newDocuments = new ArrayList<>();

                updateMessage("List files from project…");
                File corpusDir = new File(pom.getParentFile(), "src/main/corpus");
                if (corpusDir.isDirectory()) {
                    for (File f : FileUtils.listFiles(corpusDir, null, false)) {
                        newDocuments.add(new Document(f));
                    }
                }
                corpusDir = new File(pom.getParentFile(), "src/test/corpus");
                if (corpusDir.isDirectory()) {
                    for (File f : FileUtils.listFiles(corpusDir, null, false)) {
                        newDocuments.add(new Document(f));
                    }
                }

                Map<String, List<CollectionReaderDescription>> readers = new ConcurrentHashMap<>();

                newDocuments = newDocuments.parallelStream()
                        .filter(document -> {
                            Thread.currentThread().setContextClassLoader(classLoaderFromPom);

                            List<CollectionReaderDescription> descriptions = readers.computeIfAbsent(
                                    document.getExtension(),
                                    extension -> findReaderDescriptionByFileExtension(
                                            classLoaderFromPom, document.getExtension()));
                            return !descriptions.isEmpty();
                        })
                        .collect(Collectors.toList());

                Collections.sort(newDocuments);

                return new LoadProjectResult(
                        newDocuments,
                        classLoaderFromPom,
                        properties
                );
            }
        };

        loadDocsTask.valueProperty().addListener((observableValue, oldDocuments, newLoadProjectResult) -> {
            LOGGER.info("Finished loading of Maven project from {}", pom);

            documents.clear();
            jCasService.setJCas(null);

            classLoader = newLoadProjectResult.getClassLoader();
            posTagSetProperty.set(newLoadProjectResult.getPosTagSetName());
            defaultLanguageProperty.set(newLoadProjectResult.getDefaultLanguage());

            namedEntitiesProperty.clear();
            namedEntitiesProperty.addAll(newLoadProjectResult.getNamedEntites());

            documents.addAll(newLoadProjectResult.getDocuments());
        });

        executor.submit(loadDocsTask);

        return loadDocsTask;
    }

    public ObservableList<Document> documentsProperty() {
        return unmodifiableDocuments;
    }

    @Bean
    public ReadOnlyStringProperty posTagSetProperty() {
        return posTagSetProperty;
    }

    @Bean
    public ObservableList<String> namedEntitiesProperty() {
        return unmodifiableNamedEntitiesProperty;
    }

    @Bean
    public ReadOnlyStringProperty defaultLanguageProperty() {
        return defaultLanguageProperty;
    }

    /**
     * Loads the {@link JCas} for the given {@link Document document}. The result will be published
     * through {@link JCasService}.
     *
     * @param document
     * @throws ServiceException
     */
    public Task<JCas> loadJCas(Document document) {
        Task<JCas> loadJCasTask = new Task<JCas>() {

            @Override
            protected JCas call() throws Exception {
                // Set context class loader because umiaFIT relies on this one
                Thread.currentThread().setContextClassLoader(classLoader);

                updateMessage("Look up UIMA reader description for " + document.getName());
                List<CollectionReaderDescription> readerDescription = findReaderDescriptionByFileExtension(
                        classLoader,
                        document.getExtension(),
                        ComponentParameters.PARAM_SOURCE_LOCATION, document.getDoc(),
                        ComponentParameters.PARAM_LANGUAGE, defaultLanguageProperty().get());

                if (readerDescription.isEmpty()) {
                    throw new ServiceException("Did not find any suitable reader description");
                }

                updateMessage("Load document");
                JCasIterable jCasIterable = iteratePipeline(readerDescription.get(0));
                JCas newJCas;
                try {
                    newJCas = jCasIterable.iterator().next();
                } catch (RuntimeException ex) {
                    throw new ServiceException("Cannot read document", ex);
                }

                updateMessage("Generate suggestions");
                provideSuggestedAnnotation(newJCas);

                return newJCas;
            }
        };

        loadJCasTask.valueProperty().addListener((observableValue, oldJCas, newJCas) -> {
            this.jCasService.setJCas(newJCas);
            this.currentDocument.set(document);
        });

        executor.submit(loadJCasTask);

        return loadJCasTask;
    }

    /**
     * <p>
     * Tries to load the best-performing NLP pipeline and runs it on the provided {@link JCas}. It checks also for each
     * {@link org.apache.uima.fit.descriptor.TypeCapability#outputs() output type} if the annotations are already in the
     * document. If there are already available, the {@link AnalysisEngineDescription NLP tool} will not be used to suggest
     * the annotations.
     * </p>
     * <p>
     * <p>
     * If there is no NLP pipeline available and there are no {@link Token tokens}, the {@link RegexSegmenter} will
     * be used to determine an initial set of tokens and sentences.
     * </p>
     *
     * @param jCas
     */
    @SneakyThrows(InvalidXMLException.class)
    private void provideSuggestedAnnotation(JCas jCas) throws ServiceException {
        try {
            ResourceManager resMgr = ResourceManagerFactory.newResourceManager();

            AnalysisEngineDescription suggestionPipeline = getSuggestionPipeline();
            List<AnalysisEngineDescription> descriptions = suggestionPipeline.getDelegateAnalysisEngineSpecifiers().values()
                    .stream()
                    .map(rs -> (AnalysisEngineDescription) rs)
                    .collect(Collectors.toList());

            for (AnalysisEngineDescription aed : descriptions) {
                if (containsJCasAnnotations(jCas, aed)) {
                    continue;
                }

                AnalysisEngine analysisEngine = UIMAFramework.produceAnalysisEngine(aed, resMgr, null);
                analysisEngine.process(jCas);
                analysisEngine.collectionProcessComplete();

                LifeCycleUtil.destroy(analysisEngine);
            }
        } catch (ResourceInitializationException | ClassNotFoundException | AnalysisEngineProcessException ex) {
            String msg = "Could not suggest annotations for loaded common analysis system.";
            LOGGER.error(msg, ex);
            throw new ServiceException(msg, ex);
        }
    }

    private boolean containsJCasAnnotations(JCas jCas, AnalysisEngineDescription aed) throws ClassNotFoundException {
        Class<?> annotatorCls = classLoader.loadClass(aed.getAnnotatorImplementationName());
        TypeCapability typeCapability = annotatorCls.getAnnotation(TypeCapability.class);

        for (String outputType : typeCapability.outputs()) {
            Type type = jCas.getTypeSystem().getType(outputType);
            Collection<AnnotationFS> annotations = CasUtil.select(jCas.getCas(), type);

            if (!annotations.isEmpty()) {
                return true;
            }
        }
        return false;
    }

    @SneakyThrows(InvalidXMLException.class)
    private AnalysisEngineDescription getSuggestionPipeline() throws ResourceInitializationException {
        if (containsClasspathPipelinesTxt(classLoader)) {
            AnalysisEngineDescription pipeline = createBestPerformingPipelineEngineDescription(classLoader);
            if (!pipeline.getDelegateAnalysisEngineSpecifiers().isEmpty()) {
                return pipeline;
            }
        }

        AggregateBuilder aggregateBuilder = new AggregateBuilder();
        aggregateBuilder.add(createEngineDescription(
                RegexSegmenter.class
        ));
        return aggregateBuilder.createAggregateDescription();
    }

    public Task saveJCas() {
        Task saveTask = new Task() {
            @Override
            protected Object call() throws Exception {

                // Set context class loader because umiaFIT relies on this one
                Thread.currentThread().setContextClassLoader(classLoader);

                Document document = currentDocument.get();

                updateMessage("Look up UIMA writer description");
                List<AnalysisEngineDescription> writerDescription = findWriterDescriptionByFileExtension(
                        classLoader,
                        document.getExtension(),
                        JCasFileWriter_ImplBase.PARAM_TARGET_LOCATION, document.getDoc().getParentFile(),
                        JCasFileWriter_ImplBase.PARAM_OVERWRITE, true,
                        JCasFileWriter_ImplBase.PARAM_STRIP_EXTENSION, true
                );

                if (writerDescription.isEmpty()) {
                    throw new ServiceException("Did not find any suitable reader description");
                }

                // Set context class loader because umiaFIT relies on this one
                Thread.currentThread().setContextClassLoader(classLoader);

                try {
                    LOGGER.info("Starting to save CAS object.");

                    updateMessage("Saving JCas");
                    jCasService.save(writerDescription.get(0));

                    LOGGER.info("Saved CAS object.");
                } catch (AnalysisEngineProcessException | ResourceInitializationException ex) {
                    throw new ServiceException("Cannot write document", ex);
                }

                return null;
            }
        };

        executor.submit(saveTask);

        return saveTask;
    }

    private static class LoadProjectResult {

        @Getter
        private final List<Document> documents;

        @Getter
        private final ClassLoader classLoader;

        /**
         * c.f. <a href="https://github.com/dkpro/dkpro-core/tree/master/dkpro-core-api-lexmorph-asl/src/main/resources/de/tudarmstadt/ukp/dkpro/core/api/lexmorph/tagset">here</a>
         */
        @Getter
        private final String posTagSetName;

        @Getter
        private final String defaultLanguage;

        @Getter
        private ImmutableList<String> namedEntites;

        public LoadProjectResult(List<Document> documents, ClassLoader classLoader, Properties properties) {
            this.documents = documents;
            this.classLoader = classLoader;
            this.posTagSetName = properties.getProperty("pos.tag.set", "ud");
            this.defaultLanguage = properties.getProperty("default.document.language", "en");

            String namedEntityNames = properties.getProperty("named.entity.types", "location,person,organization");
            List<String> namedEntites = Lists.newArrayList(namedEntityNames.split("\\s*,\\s*"));
            Collections.sort(namedEntites);
            this.namedEntites = ImmutableList.copyOf(namedEntites);
        }

    }
}
