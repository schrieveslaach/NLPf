package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import javafx.scene.paint.Color;
import lombok.Getter;

/**
 * This enum provides {@link Color colors} which will be used across all controls in this package.
 * These colors have been evaluated with the <a href="http://www.eyequant.com/">EyeQuant analysis engine</a>.
 */
@SuppressWarnings("squid:S1192")
public enum Coloring {
    /**
     * Grayish coloring, based on material grey, which does not distracts the user from other components, but provides
     * less important information.
     *
     * <ul>
     *     <li>{@link #getBackgroundFillColor() Background fill}: Grey 100</li>
     *     <li>{@link #getBackgroundStrokeColor() Background stroke}: Grey 400</li>
     *     <li>{@link #getTextColor() Text color}: Grey 300</li>
     *     <li>{@link #getSelectionIndicatorFillColor() Selection indicator fill}: Grey 300</li>
     * </ul>
     */
    LEAST_FOCUS(
            Color.web("#F5F5F5"),
            Color.web("#BDBDBD"),
            Color.web("#E0E0E0"),
            Color.web("#E0E0E0")
    ),
    /**
     * Greyish coloring, based on material grey, which does not distracts the user from other components, but provides
     * important information.
     *
     * <ul>
     *     <li>{@link #getBackgroundFillColor() Background fill}: Grey 200</li>
     *     <li>{@link #getBackgroundStrokeColor() Background stroke}: Grey 500</li>
     *     <li>{@link #getTextColor() Text color}: Grey 400</li>
     *     <li>{@link #getSelectionIndicatorFillColor() Selection indicator fill}: Grey 300</li>
     * </ul>
     */
    LESS_FOCUS(
            Color.web("#EEEEEE"),
            Color.web("#9E9E9E"),
            Color.web("#BDBDBD"),
            Color.web("#E0E0E0")
    ),
    /**
     * Greenish coloring, based on material green, which moves the focus of user to the components, providing
     * more important information.
     *
     * <ul>
     *     <li>{@link #getBackgroundFillColor() Background fill}: Green 600</li>
     *     <li>{@link #getBackgroundStrokeColor() Background stroke}: Green 800</li>
     *     <li>{@link #getTextColor() Text color}: Green 900</li>
     *     <li>{@link #getSelectionIndicatorFillColor() Selection indicator fill}: Green 300</li>
     * </ul>
     */
    FOCUS(
            Color.web("#43A047"),
            Color.web("#2e7d32"),
            Color.web("#1B5E20"),
            Color.web("#81C784")
    ),
    /**
     * Bluish coloring, based on material light blue, which draws the attention to components, which provide highly
     * relevant information to the user
     *
     * <ul>
     *     <li>{@link #getBackgroundFillColor() Background fill}: Light Blue 700</li>
     *     <li>{@link #getBackgroundStrokeColor() Background stroke}: Light Blue 900</li>
     *     <li>{@link #getTextColor() Text color}: white</li>
     *     <li>{@link #getSelectionIndicatorFillColor() Selection indicator fill}: Light Blue 300</li>
     * </ul>
     */
    MORE_FOCUS(
            Color.web("#0288d1"),
            Color.web("#01579b"),
            Color.WHITE,
            Color.web("#4FC3F7")
    ),
    /**
     * Reddish coloring, based on material red, which highlights the components which is the most important for the
     * current annotation task
     *
     * <ul>
     *     <li>{@link #getBackgroundFillColor() Background fill}: Red 700</li>
     *     <li>{@link #getBackgroundStrokeColor() Background stroke}: Red 900</li>
     *     <li>{@link #getTextColor() Text color}: white</li>
     *     <li>{@link #getSelectionIndicatorFillColor() Selection indicator fill}: Red 300</li>
     * </ul>
     */
    MOST_FOCUS(
            Color.web("#d32f2f"),
            Color.web("#b71c1c"),
            Color.WHITE,
            Color.web("#E57373")
    );

    @Getter
    private final Color backgroundFillColor;
    @Getter
    private final Color backgroundStrokeColor;
    @Getter
    private final Color textColor;
    @Getter
    private final Color selectionIndicatorFillColor;

    Coloring(Color backgroundFillColor, Color backgroundStrokeColor, Color textColor,  Color selectionIndicatorFillColor) {
        this.backgroundFillColor = backgroundFillColor;
        this.backgroundStrokeColor = backgroundStrokeColor;
        this.textColor = textColor;
        this.selectionIndicatorFillColor = selectionIndicatorFillColor;
    }
}
