package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableDoubleValue;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.scene.Group;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.Pane;
import javafx.util.StringConverter;

import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.qpt.view.control.Constants.TRANSITION_DURATION;

/**
 * A custom {@link javafx.scene.control.Control} which will show a spinner like selection.
 */
public class LabelSpinner<T> extends Pane implements ChangeListener<T>, MinWidthLabel {

    private static final int X_OFFSET = 10;

    private static final int Y_OFFSET = 60;

    private final LinkedList<InternalLabel> labels = new LinkedList<>();

    private final ObjectProperty<T> valueProperty = new SimpleObjectProperty<>();

    private ObjectProperty<SpinnerValueFactory.ListSpinnerValueFactory<T>> factory = new SimpleObjectProperty<>();

    private AtomicBoolean expanded = new AtomicBoolean(false);

    private AtomicBoolean canChange = new AtomicBoolean(true);

    private final ObjectProperty<String> collapseText = new SimpleObjectProperty<>();

    public LabelSpinner() {
        valueProperty.addListener((observable, oldValue, newValue) -> updateLabelTextValues());
        factory.addListener((observable, oldValue, newValue) -> updateFactoryListener(oldValue, newValue));

        collapseText.addListener((observableValue, s, t1) -> updateLabelTextValues());

        layoutTokenLabels();
        bringLabelsIntoCorrectZOrder();
    }

    private void layoutTokenLabels() {
        int x = -1;
        int y = 4 * Y_OFFSET;

        for (int i = 0; i < 5; ++i) {
            InternalLabel lb = new InternalLabel(i);
            lb.relocate(x, y);
            labels.add(lb);
        }

        getChildren().addAll(labels);
    }

    /**
     * @deprecated Use {@link #setLabelMinWidth(double)} instead
     */
    @Deprecated
    public void unbindLabelsMinWidth() {
        labels.forEach(l -> l.label.labelMinWidthProperty().unbind());
    }

    /**
     *
     * @param minWidthExpression
     * @deprecated Use {@link #setLabelMinWidth(double)} instead
     */
    @Deprecated
    public void bindLabelsMinWidth(DoubleExpression minWidthExpression) {
        labels.forEach(l -> l.label.labelMinWidthProperty().bind(minWidthExpression));
    }

    @Override
    public DoubleBinding labelMinWidthProperty() {
        List<DoubleProperty> minWidthProperties = labels.stream()
                .map(l -> l.label.labelMinWidthProperty())
                .collect(Collectors.toList());

        return Bindings.createDoubleBinding(
                () -> minWidthProperties.stream()
                        .mapToDouble(ObservableDoubleValue::get)
                        .min()
                        .orElse(TokenLabel.TOKEN_LABEL_WIDTH),
                minWidthProperties.toArray(new ObservableValue[minWidthProperties.size()])
        );
    }

    public void setLabelMinWidth(double labelMinWidth) {
        labels.forEach(l -> l.label.labelMinWidthProperty().setValue(labelMinWidth));
    }

    /**
     * The property containing the selected value.
     *
     * @return
     */
    public ObjectProperty<T> valueProperty() {
        return factory.getValue().valueProperty();
    }

    public ParallelTransition createMinWidthAnimation(double minWidth) {
        ParallelTransition pt = new ParallelTransition();

        synchronized (canChange) {
            if (!canChange.get()) {
                return pt;
            }
        }

        for (InternalLabel lb : labels) {
            pt.getChildren().add(new MinWidthTransition(lb, minWidth));
        }

        return pt;
    }

    public ParallelTransition createExpandTransition(boolean expanded) {
        ParallelTransition pt = new ParallelTransition();

        synchronized (canChange) {
            if (!canChange.get()) {
                return pt;
            }
        }

        if (isExpanded() != expanded) {
            if (expanded) {
                updateSelectionBasedOnCollapseText();
            }

            for (int i = 0; i < labels.size(); i++) {
                InternalLabel lb = labels.get(i);
                pt.getChildren().addAll(lb.createExpandTransition(i, expanded));
            }

            TranslateTransition tt = new TranslateTransition(TRANSITION_DURATION, this);
            if (expanded) {
                tt.setByY(-2 * Y_OFFSET);
            } else {
                tt.setByY(2 * Y_OFFSET);
            }
            pt.getChildren().add(tt);

            pt.statusProperty().addListener((observable, oldStatus, newStatus) -> {
                if (newStatus == javafx.animation.Animation.Status.RUNNING) {
                    synchronized (canChange) {
                        canChange.set(false);
                    }

                } else if (newStatus == javafx.animation.Animation.Status.STOPPED) {
                    this.expanded.set(expanded);
                    synchronized (canChange) {
                        canChange.set(true);
                    }

                    // Update the label's text because the collapse text needs to be replaced if it isn't null.
                    updateLabelTextValues();
                }
            });
        }
        return pt;
    }

    private void updateSelectionBasedOnCollapseText() {
        String text = collapseText.get();
        T value = null;
        if (text != null) {
            StringConverter<T> stringConverter = factory.getValue().converterProperty().getValue();
            value = factory.get().getItems().stream()
                    .filter(Objects::nonNull)
                    .filter(t -> stringConverter.toString(t).equals(text))
                    .findFirst()
                    .orElse(null);
        }
        if (value == null) {
            value = factory.get().getItems().stream()
                    .filter(Objects::nonNull)
                    .findAny()
                    .orElse(null);
        }
        factory.getValue().valueProperty().set(value);
    }

    public void setValueFactory(SpinnerValueFactory.ListSpinnerValueFactory<T> factory) {
        this.factory.set(factory);
    }

    private void updateFactoryListener(SpinnerValueFactory.ListSpinnerValueFactory<T> oldValue, SpinnerValueFactory.ListSpinnerValueFactory<T> newValue) {
        if (oldValue != null) {
            oldValue.valueProperty().removeListener(this);
        }
        newValue.valueProperty().addListener(this);
        updateLabelTextValues();
    }

    private void updateLabelTextValues() {
        if (factory.isNull().get() || factory.get().getItems().isEmpty()) {
            return;
        }

        StringConverter<T> stringConverter = factory.getValue().converterProperty().getValue();
        ObservableList<T> items = factory.getValue().getItems();

        int selectedValueIndex = getSelectedValueIndex();

        for (int i = 0; i < labels.size(); ++i) {
            InternalLabel label = labels.get(i);

            if (!isExpanded() && collapseText.isNotNull().get() && i == 2) {
                label.setTextStatus(collapseText.get());
                continue;
            }

            int indexInList = selectedValueIndex + i - 2;
            while (indexInList < 0) {
                indexInList += items.size();
            }
            while (indexInList >= items.size()) {
                indexInList -= items.size();
            }
            T item = items.get(indexInList);
            label.setTextStatus(stringConverter.toString(item));
        }

        T t = items.get(selectedValueIndex);
        valueProperty.set(t);
        if (t == null) {
            factory.get().increment(1);
        }
    }

    public void selectFirstElement() {
        if (factory.isNull().get() || factory.get().getItems().isEmpty()) {
            return;
        }

        valueProperty.set(factory.get().getItems().stream()
                .filter(Objects::nonNull)
                .findFirst()
                .get());
    }

    private int getSelectedValueIndex() {
        T selectedValue = factory.getValue().valueProperty().get();
        ObservableList<T> items = factory.getValue().getItems();
        return items.indexOf(selectedValue);
    }

    private void bringLabelsIntoCorrectZOrder() {
        for (int i = 1; i < 3; ++i) {
            labels.get(i).toFront();
        }
        for (int i = 3; i < 5; ++i) {
            labels.get(i).toBack();
        }
    }

    public void selectPrevious() {
        if (!isExpanded() || !beginAnimation()) {
            return;
        }

        labels.addFirst(labels.removeLast());

        ParallelTransition pt = new ParallelTransition();

        for (int i = 0; i < labels.size(); i++) {
            InternalLabel lb = labels.get(i);
            pt.getChildren().addAll(lb.createNewPositionTransition(i, i - 1 >= 0 ? i - 1 : 4, true));
        }

        pt.setOnFinished(e -> {
            bringLabelsIntoCorrectZOrder();
            if (factory.isNotNull().get()) {
                if (getSelectedValueIndex() <= 0) {
                    factory.getValue().increment(factory.getValue().getItems().size() - 1);
                } else {
                    factory.getValue().decrement(1);
                }
            }
            endAnimation();
        });
        pt.play();
        labels.get(2).toFront();
    }

    public void selectNext() {
        if (!isExpanded() || !beginAnimation()) {
            return;
        }

        labels.addLast(labels.removeFirst());

        ParallelTransition pt = new ParallelTransition();

        for (int i = 0; i < labels.size(); i++) {
            InternalLabel lb = labels.get(i);
            pt.getChildren().addAll(lb.createNewPositionTransition(i, i + 1 <= 4 ? i + 1 : 0, false));
        }

        pt.setOnFinished(e -> {
            bringLabelsIntoCorrectZOrder();
            if (factory.isNotNull().get()) {
                if (getSelectedValueIndex() >= factory.getValue().getItems().size() - 1) {
                    factory.getValue().decrement(factory.getValue().getItems().size() - 1);
                } else {
                    factory.getValue().increment(1);
                }
            }
            endAnimation();
        });
        pt.play();
        labels.get(2).toFront();
    }

    private int distanceToNextGroup() {
        if (factory.isNotNull().get()) {
            StringConverter<T> stringConverter = factory.get().converterProperty().get();
            String selectedValue = stringConverter.toString(factory.get().valueProperty().get());

            ObservableList<T> items = factory.get().getItems();
            final int selectedValueIndex = getSelectedValueIndex();
            int i = selectedValueIndex + 1;
            for (; i < items.size(); ++i) {
                String s = stringConverter.toString(items.get(i));

                if (s.charAt(0) != selectedValue.charAt(0)) {
                    break;
                }
            }

            return i - selectedValueIndex;
        }

        return 1;
    }

    public void selectNextGroup() {
        if (!isExpanded() || !beginAnimation()) {
            return;
        }

        SequentialTransition sq = new SequentialTransition();

        int distanceToNextGroup = distanceToNextGroup();

        for (int animationIndex = 0; animationIndex < Math.min(distanceToNextGroup, 3); ++animationIndex) {
            labels.addLast(labels.removeFirst());

            ParallelTransition pt = new ParallelTransition();

            for (int i = 0; i < labels.size(); i++) {
                InternalLabel lb = labels.get(i);
                pt.getChildren().addAll(lb.createNewPositionTransition(i, i + 1 <= 4 ? i + 1 : 0, false));
            }

            sq.getChildren().add(pt);
        }

        sq.setOnFinished(event -> {
            bringLabelsIntoCorrectZOrder();
            if (factory.isNotNull().get()) {
                factory.get().increment(distanceToNextGroup);
            }
            endAnimation();
        });

        sq.play();
    }

    private int distanceToPreviousGroup() {
        if (factory.isNotNull().get()) {
            StringConverter<T> stringConverter = factory.get().converterProperty().get();
            String selectedValue = stringConverter.toString(factory.get().valueProperty().get());

            ObservableList<T> items = factory.get().getItems();
            final int selectedValueIndex = getSelectedValueIndex();
            int i = selectedValueIndex - 1;
            for (; i >= 0; --i) {
                String s = stringConverter.toString(items.get(i));

                if (s.charAt(0) != selectedValue.charAt(0)) {
                    break;
                }
            }

            return selectedValueIndex - i;
        }

        return 1;
    }

    public void selectPreviousGroup() {
        if (!isExpanded() || !beginAnimation()) {
            return;
        }

        int distanceToPreviousGroup = distanceToPreviousGroup();

        SequentialTransition sq = new SequentialTransition();

        for (int animationIndex = 0; animationIndex < Math.min(3, distanceToPreviousGroup); ++animationIndex) {
            labels.addFirst(labels.removeLast());

            ParallelTransition pt = new ParallelTransition();

            for (int i = 0; i < labels.size(); i++) {
                InternalLabel lb = labels.get(i);
                pt.getChildren().addAll(lb.createNewPositionTransition(i, i - 1 >= 0 ? i - 1 : 4, true));
            }

            sq.getChildren().add(pt);
        }

        sq.setOnFinished(event -> {
            bringLabelsIntoCorrectZOrder();
            if (factory.isNotNull().get()) {
                factory.get().decrement(distanceToPreviousGroup);
            }
            endAnimation();
        });

        sq.play();
    }

    private void endAnimation() {
        synchronized (canChange) {
            canChange.set(true);
        }
    }

    private boolean beginAnimation() {
        synchronized (canChange) {
            if (!canChange.get()) {
                return false;
            }
            canChange.set(false);
        }
        return true;
    }

    @Override
    public void changed(ObservableValue<? extends T> observable, T oldValue, T newValue) {
        updateLabelTextValues();
    }

    public boolean isExpanded() {
        return expanded.get();
    }

    /**
     * Manages a text value which will be shown instead of the selection while the spinner is not
     * {@link #isExpanded() expanded}.
     */
    public ObjectProperty<String> collapseText() {
        return collapseText;
    }

    private class InternalLabel extends Group {

        private final TokenLabel label;

        private InternalLabel(int index) {
            label = new TokenLabel();
            setTextStatus(Integer.toString(index));
            label.setColoring(getColoring(index, isExpanded()));

            updateLabelId(index);

            if (index != 2) {
                setOpacity(0.0);
            }
            getChildren().add(label);
        }

        private Coloring getColoring(int index, boolean expanded) {
            switch (index) {
                case 0:
                case 4:
                    return Coloring.LEAST_FOCUS;
                case 1:
                case 3:
                    return Coloring.LESS_FOCUS;
                default:
                    if (!expanded) {
                        return Coloring.LESS_FOCUS;
                    }
                    return Coloring.MOST_FOCUS;
            }
        }

        private Transition createNewPositionTransition(int newIndex, int oldIndex, boolean previous) {
            ParallelTransition pt = new ParallelTransition();

            pt.getChildren().addAll(
                    createTranslateTransition(newIndex, previous),
                    label.createColoringTransition(getColoring(oldIndex, isExpanded()), getColoring(newIndex, isExpanded()))
            );

            updateLabelId(newIndex);

            return pt;
        }

        private void updateLabelId(int newIndex) {
            if (newIndex == 2) {
                label.setId("labelspinner-selection");
            } else {
                label.setId("spinner-label-" + newIndex);
            }
        }

        private Transition createTranslateTransition(int newIndex, boolean previous) {
            TranslateTransition tt = new TranslateTransition(TRANSITION_DURATION, this);

            if (previous) {
                switch (newIndex) {
                    case 1:
                    case 2:
                        tt.setByX(-X_OFFSET);
                        tt.setByY(Y_OFFSET);
                        break;
                    case 3:
                    case 4:
                        tt.setByX(X_OFFSET);
                        tt.setByY(Y_OFFSET);
                        break;
                    default:
                        tt.setByY(-4 * Y_OFFSET);
                        break;
                }
            } else {
                switch (newIndex) {
                    case 0:
                    case 1:
                        tt.setByX(X_OFFSET);
                        tt.setByY(-Y_OFFSET);
                        break;
                    case 2:
                    case 3:
                        tt.setByX(-X_OFFSET);
                        tt.setByY(-Y_OFFSET);
                        break;
                    default:
                        tt.setByY(4 * Y_OFFSET);
                        break;
                }
            }

            return tt;
        }

        private Transition createExpandTransition(int index, boolean expand) {
            FadeTransition ft = new FadeTransition(TRANSITION_DURATION, this);
            TranslateTransition tt = new TranslateTransition(TRANSITION_DURATION, this);
            if (expand) {
                tt.setByX(Math.abs(2 - index) * X_OFFSET);
                tt.setByY(-(2 - index) * Y_OFFSET);

                if (index != 2) {
                    ft.setFromValue(0.0);
                    ft.setToValue(1.0);
                }
            } else {
                tt.setByX(-Math.abs(2 - index) * X_OFFSET);
                tt.setByY((2 - index) * Y_OFFSET);

                if (index != 2) {
                    ft.setFromValue(1.0);
                    ft.setToValue(0.0);
                }
            }

            ParallelTransition pt = new ParallelTransition();
            pt.getChildren().addAll(
                    tt, ft,
                    label.createColoringTransition(getColoring(index, expand), getColoring(index, expand))
            );
            return pt;
        }

        private void setTextStatus(String text) {
            boolean showSelectionIndicator = false;
            if (isExpanded()) {
                showSelectionIndicator = Objects.equals(text, collapseText.get());
            }
            label.showSelectedIndicatorProperty().set(showSelectionIndicator);
            label.setText(text);
        }
    }

    private class MinWidthTransition extends Transition {

        private final InternalLabel lb;

        private final double from;
        private final double to;

        private MinWidthTransition(InternalLabel lb, double to) {
            this.lb = lb;
            this.from = lb.label.labelMinWidthProperty().doubleValue();
            this.to = to;

            setCycleDuration(TRANSITION_DURATION);
        }

        @Override
        protected void interpolate(double v) {
            if (lb.label.labelMinWidthProperty().isBound()) {
                lb.label.labelMinWidthProperty().unbind();
            }
            lb.label.labelMinWidthProperty().set(from + (to - from) * v);
        }
    }
}
