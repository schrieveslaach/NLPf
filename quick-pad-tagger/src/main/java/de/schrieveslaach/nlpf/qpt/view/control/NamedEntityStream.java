package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.animation.FadeTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.Transition;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleDoubleProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.Region;
import lombok.Getter;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;

import java.util.Collections;
import java.util.List;

import static de.schrieveslaach.nlpf.qpt.view.control.Constants.TRANSITION_DURATION;
import static de.schrieveslaach.nlpf.qpt.view.control.TokenLabel.TOKEN_LABEL_WIDTH;
import static de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter.REGION_DISTANCE;
import static javafx.collections.FXCollections.observableArrayList;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectCovered;
import static org.apache.uima.fit.util.JCasUtil.selectCovering;

public class NamedEntityStream extends Region {

    @Getter
    private Selection selection;
    private JCas jCas;
    private final ObservableList<Token> tokens = observableArrayList();

    private final ObservableList<String> namedEntityTags = observableArrayList();

    private final SimpleStringProperty namedEntityTag = new SimpleStringProperty();

    private final PaginationAdapter<LabelSpinner<String>, Token> namedEntityPaginationAdapter = new PaginationAdapter<>(
            () -> {
                LabelSpinner spinner = new LabelSpinner();
                spinner.setLabelMinWidth(TOKEN_LABEL_WIDTH);
                spinner.setValueFactory(new SpinnerValueFactory.ListSpinnerValueFactory<>(namedEntityTags));
                spinner.collapseText().setValue("");
                spinner.setOpacity(0.0);
                spinner.valueProperty().addListener((observable, oldValue, newValue) -> namedEntityTag.set(newValue != null ? newValue.toString() : null));
                return spinner;
            },
            this::bind,
            Region::widthProperty
    );

    public NamedEntityStream() {
        widthProperty().addListener(namedEntityPaginationAdapter);

        namedEntityPaginationAdapter.regionsProperty().addListener((ListChangeListener<? super LabelSpinner<String>>) c -> {
            while (c.next()) {
                getChildren().removeAll(c.getRemoved());
                getChildren().addAll(c.getAddedSubList());
            }
        });

        tokens.addListener(namedEntityPaginationAdapter);
    }

    public ReadOnlyStringProperty namedEntityTagProperty() {
        return namedEntityTag;
    }

    public void setJCas(JCas jCas) {
        this.jCas = jCas;
        rebindTokens(Lists.newArrayList(select(jCas, Token.class)));
    }

    public void setTokens(List<Token> tokens) {
        this.tokens.clear();
        this.tokens.addAll(tokens);
    }

    public void setSelection(Selection selection) {
        updateSelection(selection);
        this.selection = selection;
    }

    public void setParentPaginationAdapter(PaginationAdapter<?, Token> parentPaginationAdapter) {
        namedEntityPaginationAdapter.setParentPaginationAdapter(parentPaginationAdapter);
        parentPaginationAdapter.addChildPaginationAdapter(namedEntityPaginationAdapter);
    }

    public void setNamedEntitiesSet(List<String> namedEntitiesSet) {
        namedEntityTags.clear();
        namedEntityTags.addAll(namedEntitiesSet);
        if (jCas != null) {
            namedEntityPaginationAdapter.rebind(Lists.newArrayList(select(jCas, Token.class)));
        }
    }

    public void selectPreviousNamedEntityTagGroup() {
        namedEntityPaginationAdapter.apply(selection.getSelectedTokens(), LabelSpinner::selectPreviousGroup);
    }

    public void selectPreviousNamedEntityTag() {
        namedEntityPaginationAdapter.apply(selection.getSelectedTokens(), LabelSpinner::selectPrevious);
    }

    public void selectNextNamedEntityTagGroup() {
        namedEntityPaginationAdapter.apply(selection.getSelectedTokens(), LabelSpinner::selectNextGroup);
    }

    public void selectNextNamedEntityTag() {
        namedEntityPaginationAdapter.apply(selection.getSelectedTokens(), LabelSpinner::selectNext);
    }

    public void rebindTokens(List<Token> tokens) {
        namedEntityPaginationAdapter.rebind(tokens);
    }

    private void updateSelection(Selection newSelection) {
        ParallelTransition pt = new ParallelTransition();

        if (selection != null && !selection.isEmpty() && newSelection != null && !newSelection.beginsWith(selection)) {
            Token firstToken = selection.getSelectedTokens().get(0);
            namedEntityPaginationAdapter.apply(firstToken, spinner ->
                    pt.getChildren().add(spinner.createExpandTransition(false))
            );
            pt.getChildren().addAll(
                    createFadeTransition(selection, false)
            );
        }

        if (newSelection != null && !newSelection.isEmpty()) {
            Token firstToken = newSelection.getSelectedTokens().get(0);
            namedEntityPaginationAdapter.apply(firstToken, spinner -> {
                        pt.getChildren().add(spinner.createExpandTransition(true));
                        if (jCas != null) {
                            pt.getChildren().add(spinner.createMinWidthAnimation(calculateLabelWidth(newSelection.getBegin(), newSelection.getEnd())));
                        }
                    }
            );
        }

        pt.getChildren().addAll(createFadeTransition(newSelection, true));

        pt.play();
    }

    private Transition createFadeTransition(Selection selection, boolean show) {
        ParallelTransition pt = new ParallelTransition();

        if (jCas == null || selection == null) {
            return pt;
        }

        List<NamedEntity> namedEntities = selectCovering(jCas, NamedEntity.class, selection.getBegin(), selection.getEnd());
        if (!namedEntities.isEmpty()) {
            return pt;
        }

        if (!selection.isEmpty()) {
            Token firstToken = selection.getSelectedTokens().get(0);
            namedEntityPaginationAdapter.apply(firstToken, spinner -> {
                FadeTransition ft = new FadeTransition(TRANSITION_DURATION, spinner);
                if (show) {
                    ft.setToValue(1.0);
                    ft.setFromValue(5.0);
                } else {
                    ft.setToValue(0.0);
                    ft.setFromValue(1.0);
                }
                pt.getChildren().add(ft);
            });
        }

        return pt;
    }

    private double calculateLabelWidth(int begin, int end) {
        double width = 0;
        for (Token t : selectCovered(jCas, Token.class, begin, end)) {
            MinWidthLabel region = (MinWidthLabel) namedEntityPaginationAdapter.getParentOrSelfRegion(t);
            if (region == null) {
                continue;
            }
            //TODO: There is a bug here, the width is always one REGION_DISTANCE too much
            width += region.labelMinWidthProperty().getValue();
            width += REGION_DISTANCE;
        }
        width -= REGION_DISTANCE;
        return width;
    }

    private void bind(LabelSpinner<String> spinner, Token token) {
        spinner.unbindLabelsMinWidth();
        BooleanProperty visible = new SimpleBooleanProperty(false);
        //TODO: Refactor the minLabelWidth = minLabelWidth.add(...) because minLabelWidth is 0.0?
        DoubleExpression minLabelWidth = new SimpleDoubleProperty();

        if (token != null) {
            spinner.setId("spinner-" + token.hashCode());
            if (jCas != null) {
                minLabelWidth = bindTokenToSpinner(spinner, token, visible, minLabelWidth);
            } else {
                spinner.setId(null);
                spinner.collapseText().setValue("");
            }
        } else {
            spinner.setId(null);
            spinner.collapseText().setValue("");
        }
        spinner.setOpacity(visible.get() ? 1.0 : 0.0);
        spinner.bindLabelsMinWidth(minLabelWidth);
    }

    private DoubleExpression bindTokenToSpinner(LabelSpinner<String> spinner, Token token, BooleanProperty visible, DoubleExpression minLabelWidth) {
        List<NamedEntity> coveredNamedEntities = selectCovering(jCas, NamedEntity.class, token);

        if (!coveredNamedEntities.isEmpty()) {
            NamedEntity namedEntity = coveredNamedEntities.get(0);
            if (selection != null && selection.beginsWith(token) && namedEntity.getBegin() == token.getBegin()) {
                minLabelWidth = minLabelWidth.add(bindSpinnerOfSelectedNamedEntityToken(namedEntity, spinner, visible));
            } else if (namedEntity.getBegin() == token.getBegin()) {
                minLabelWidth = minLabelWidth.add(bindSpinnerOfNamedEntityToken(namedEntity, spinner, visible));
            } else {
                minLabelWidth = minLabelWidth.add(rebindSpinnerOfNamedEntityToken(namedEntity, token, visible));
            }
        } else {
            if (selection != null && selection.beginsWith(token)) {
                minLabelWidth = minLabelWidth.add(bindSpinnerOfFirstSelectedToken(visible));
            } else {
                minLabelWidth = minLabelWidth.add(bindSpinnerOfToken(spinner, visible));
            }
        }

        return minLabelWidth;
    }

    private Double bindSpinnerOfSelectedNamedEntityToken(NamedEntity namedEntity, LabelSpinner<String> spinner, BooleanProperty visible) {
        String namedEntityValue = namedEntity.getValue();
        spinner.valueProperty().setValue(namedEntityValue);
        spinner.collapseText().setValue(namedEntityValue);
        visible.setValue(true);
        return calculateLabelWidth(namedEntity.getBegin(), namedEntity.getEnd());
    }

    private double bindSpinnerOfNamedEntityToken(NamedEntity namedEntity, LabelSpinner<String> spinner, BooleanProperty visible) {
        String namedEntityValue = namedEntity.getValue();
        spinner.collapseText().setValue(namedEntityValue);
        visible.setValue(true);
        return calculateLabelWidth(namedEntity.getBegin(), namedEntity.getEnd());
    }

    private double rebindSpinnerOfNamedEntityToken(NamedEntity namedEntity, Token token, BooleanProperty visible) {
        final DoubleExpression firstRegionWidth = new SimpleDoubleProperty(calculateLabelWidth(namedEntity.getBegin(), token.getEnd()));
        Token firstTokenOfNamedEntity = JCasUtil.selectAt(jCas, Token.class, namedEntity.getBegin(), namedEntity.getEnd()).get(0);
        namedEntityPaginationAdapter.apply(Collections.singletonList(firstTokenOfNamedEntity), c -> {
            c.unbindLabelsMinWidth();
            c.bindLabelsMinWidth(firstRegionWidth);
        });
        visible.setValue(false);
        return 0.0;
    }

    private double bindSpinnerOfFirstSelectedToken(BooleanProperty visible) {
        visible.setValue(true);
        return calculateLabelWidth(selection.getBegin(), selection.getEnd());
    }

    private int bindSpinnerOfToken(LabelSpinner<String> spinner, BooleanProperty visible) {
        spinner.selectFirstElement();
        spinner.collapseText().setValue("");
        visible.setValue(false);
        return TOKEN_LABEL_WIDTH;
    }
}
