package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.googlecode.jatl.Html;
import com.googlecode.jatl.SimpleIndenter;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.value.ChangeListener;
import javafx.scene.layout.AnchorPane;
import javafx.scene.web.WebEngine;
import javafx.scene.web.WebView;
import lombok.Getter;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import java.nio.file.Files;
import java.util.Collection;
import java.util.List;
import java.util.Map;

import static org.apache.uima.fit.util.JCasUtil.indexCovered;
import static org.apache.uima.fit.util.JCasUtil.selectCovered;

/**
 * Displays the currently displayed {@link org.apache.uima.jcas.JCas Common Analysis System} as natural language
 * document.
 */
public class NaturalLanguageText extends AnchorPane {

    @Getter(lazy = true)
    private final File jQueryPath = extractJQuery();

    private final WebView browser = new WebView();
    private final WebEngine webEngine = browser.getEngine();

    private final ObjectProperty<Selection> selectionProperty = new SimpleObjectProperty<>();

    public NaturalLanguageText() {
        browser.setId("natural-language-browser");
        AnchorPane.setBottomAnchor(browser, 0.0);
        AnchorPane.setLeftAnchor(browser, 0.0);
        AnchorPane.setRightAnchor(browser, 0.0);
        AnchorPane.setTopAnchor(browser, 0.0);
        getChildren().add(browser);

        webEngine.setUserStyleSheetLocation(getClass().getResource("/natural-language.css").toString());
        widthProperty().addListener((observable, oldValue, newValue) -> browser.setMinWidth(newValue.doubleValue()));

        selectionProperty.addListener(selectionChangeListener());
    }

    private ChangeListener<Selection> selectionChangeListener() {
        return (observable, oldSelection, newSelection) -> {
            StringBuilder script = new StringBuilder();

            appendClearSelectionJQueryScript(oldSelection, script);
            appendShowSelectionJQueryScript(newSelection, script);

            webEngine.executeScript(script.toString());
        };
    }

    private void appendShowSelectionJQueryScript(Selection newSelection, StringBuilder script) {
        if (newSelection != null && !newSelection.isEmpty()) {
            script.append(String.format("$('#%s').addClass('sentence-selected');",
                    newSelection.getSelectedSentence().hashCode())).append("\n");

            Token first = null;
            for (Token t : newSelection.getSelectedTokens()) {
                script.append(String.format("$('#%s').addClass('token-selected');", t.hashCode())).append("\n");
                if (first == null) {
                    first = t;
                    script.append(String.format("$('html,body').animate({ scrollTop: $('#%s').offset().top }, 150);", first.hashCode())).append("\n");
                }
            }
        }
    }

    private void appendClearSelectionJQueryScript(Selection oldSelection, StringBuilder script) {
        if (oldSelection != null && !oldSelection.isEmpty()) {
            for (Token t : oldSelection.getSelectedTokens()) {
                script.append(String.format("$('#%s').removeClass('token-selected');", t.hashCode())).append("\n");
            }

            script.append(String.format("$('#%s').removeClass('sentence-selected');",
                    oldSelection.getSelectedSentence().hashCode())).append("\n");
        }
    }

    /**
     * Sets and displays the {@link JCas common analysis system}
     *
     * @param cas
     */
    public void showJCas(JCas cas) {
        webEngine.loadContent(createContent(cas));
    }

    private String createContent(JCas jCas) {
        if (jCas == null) {
            return "";
        }
        StringWriter writer = new StringWriter();

        Html html = new Html(writer);
        html.indent(new SimpleIndenter("", "", "", ""));

        Html body = html.html().head()
                .script().src(getJQueryPath().toURI().toString()).end()
                .end()
                .body();

        appendAllParagraphs(jCas, body);

        Selection selection = selectionProperty.get();
        if (selection != null && !selection.isEmpty()) {
            body.script()
                    .type("text/javascript")
                    .text(String.format("%n$('html,body').animate({ scrollTop: $('#%s').offset().top }, 0);%n", selection.getSelectedTokens().get(0).hashCode()))
                    .end();
        }

        html.endAll().done();
        return writer.toString();
    }

    @SneakyThrows(IOException.class)
    private File extractJQuery() {
        File tempDir = Files.createTempDirectory("jQuery").toFile();
        File jQuery = new File(tempDir, "jquery.min.js");

        try (InputStream is = getClass().getResourceAsStream("/META-INF/resources/webjars/jquery/3.2.1/jquery.min.js");
             FileOutputStream fos = new FileOutputStream(jQuery)) {
            IOUtils.copy(is, fos);
        }

        jQuery.deleteOnExit();
        tempDir.deleteOnExit();

        return jQuery;
    }

    private void appendAllParagraphs(JCas jCas, Html body) {
        Map<Paragraph, Collection<Sentence>> paragraphsCoveringSentences = indexCovered(jCas, Paragraph.class, Sentence.class);

        Html p = null;
        Paragraph lastParagraph = null;
        for (Sentence sentence : JCasUtil.select(jCas, Sentence.class)) {
            Paragraph paragraph = paragraphsCoveringSentences.entrySet().stream()
                    .filter(e -> e.getValue().contains(sentence))
                    .map(Map.Entry::getKey)
                    .findFirst()
                    .orElse(null);

            if (paragraph == null) {
                if (p != null) {
                    p.end();
                    lastParagraph = null;
                    p = null;
                }

                Html p1 = body.p();
                appendSentence(p1, jCas, sentence);
                p1.end();

            } else {
                if (p == null) {
                    p = body.p();
                } else {
                    if (lastParagraph != paragraph) {
                        p.end();
                        p = body.p();
                    }
                }

                appendSentence(p, jCas, sentence);

                lastParagraph = paragraph;
            }
        }
    }

    private void appendSentence(Html p, JCas jCas, Sentence sentence) {
        List<Token> tokens = selectCovered(jCas, Token.class, sentence);

        String classAttr = "sentence";
        Selection selection = selectionProperty.get();
        if (selection != null && !selection.isEmpty() && selection.getSelectedSentence().equals(sentence)) {
            classAttr = "sentence sentence-selected";
        }

        Html span = p.span()
                .classAttr(classAttr)
                .id(Integer.toString(sentence.hashCode()));

        if (tokens.isEmpty()) {
            span.text(sentence.getCoveredText().trim());
        } else {

            for (int i = 0; i < tokens.size(); i++) {
                Token t = tokens.get(i);

                classAttr = "token";
                if (selection != null && !selection.isEmpty() && selection.getSelectedTokens().contains(t)) {
                    classAttr = "token token-selected";
                }

                span.span()
                        .id(Integer.toString(t.hashCode()))
                        .classAttr(classAttr)
                        .text(t.getText())
                        .end();

                if (i + 1 < tokens.size() && t.getEnd() < tokens.get(i + 1).getBegin()) {
                    span.text(" ");
                }
            }
        }
        span.end();
    }

    public ObjectProperty<Selection> selectionProperty() {
        return selectionProperty;
    }

}
