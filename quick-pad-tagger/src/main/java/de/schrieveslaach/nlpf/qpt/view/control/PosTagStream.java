package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableList;
import com.google.common.collect.Lists;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter;
import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.animation.ParallelTransition;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.SpinnerValueFactory;
import javafx.scene.layout.Region;
import org.apache.uima.jcas.JCas;

import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import static de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter.REGION_DISTANCE;
import static javafx.collections.FXCollections.observableArrayList;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectSingleRelative;

public class PosTagStream extends Region {

    private JCas jCas;

    private Selection selection;

    private ObservableList<Token> tokens = observableArrayList();

    private final StringProperty posTagValue = new SimpleStringProperty();

    private final ObservableList<String> posTags = observableArrayList();

    private PaginationAdapter<LabelSpinner<String>, Token> labelSpinnerAdapter = new PaginationAdapter<>(
            () -> {
                LabelSpinner spinner = new LabelSpinner();
                spinner.setValueFactory(new SpinnerValueFactory.ListSpinnerValueFactory<>(posTags));
                spinner.collapseText().setValue("");
                spinner.valueProperty().addListener((observable, oldValue, newValue) -> posTagValue.set(newValue != null ? newValue.toString() : null));
                return spinner;
            },
            (spinner, token) -> {
                double layoutX = 0.0;

                if (token != null) {
                    spinner.setId("spinner-" + token.hashCode());

                    POS pos = token.getPos();
                    if (pos != null) {
                        String posValue = pos.getPosValue();
                        spinner.valueProperty().setValue(posValue);
                        spinner.collapseText().setValue(posValue);
                    } else {
                        spinner.selectFirstElement();
                        spinner.collapseText().setValue("");
                    }

                    if (jCas != null && token.getBegin() > 0) {
                        Token preceding = selectSingleRelative(jCas, Token.class, token, -1);
                        if (preceding != null && preceding.getEnd() == token.getBegin()) {
                            layoutX = REGION_DISTANCE + 0.3 * TokenLabel.TOKEN_LABEL_HEIGHT;
                        }
                    }
                } else {
                    spinner.setId(null);
                    spinner.collapseText().setValue("");
                }

                spinner.setLayoutX(layoutX);
            },
            Region::widthProperty
    );

    public PosTagStream() {
        widthProperty().addListener(labelSpinnerAdapter);

        labelSpinnerAdapter.regionsProperty().addListener((ListChangeListener<LabelSpinner<String>>) c -> {
            while (c.next()) {
                getChildren().removeAll(c.getRemoved());
                getChildren().addAll(c.getAddedSubList());
            }
        });
        tokens.addListener(labelSpinnerAdapter);
    }

    private void updateSelection(Selection newSelection) {
        Selection oldSelection = this.selection;
        ParallelTransition pt = new ParallelTransition();

        if (oldSelection != null) {
            labelSpinnerAdapter.apply(oldSelection.getSelectedTokens(), spinner ->
                    pt.getChildren().add(spinner.createExpandTransition(false))
            );
        }
        labelSpinnerAdapter.apply(newSelection.getSelectedTokens(), spinner -> {
            pt.getChildren().add(spinner.createExpandTransition(true));

            // Update selected pos tag value in order to assign correctly
            posTagValue.set(spinner.valueProperty().get());
        });

        pt.getChildren().add(labelSpinnerAdapter.createScrollToAnimation(newSelection.getSelectedTokens()));

        pt.play();
    }

    public void setTags(Set<String> tags) {
        posTags.clear();
        posTags.addAll(new TreeSet<>(tags));
        if (jCas != null) {
            labelSpinnerAdapter.rebind(Lists.newArrayList(select(jCas, Token.class)));
        }
    }

    public void setJCas(JCas jCas) {
        this.jCas = jCas;
        if (jCas != null) {
            labelSpinnerAdapter.rebind(Lists.newArrayList(select(jCas, Token.class)));
        }
    }

    public void setTokens(List<Token> tokens) {
        this.tokens.clear();
        this.tokens.addAll(tokens);
    }

    public void setSelection(Selection selection) {
        updateSelection(selection);
        this.selection = selection;
    }

    public void setParentPaginationAdapter(PaginationAdapter<?, Token> parentPaginationAdapter) {
        labelSpinnerAdapter.setParentPaginationAdapter(parentPaginationAdapter);
        parentPaginationAdapter.addChildPaginationAdapter(labelSpinnerAdapter);
    }

    public ImmutableList<String> getTags() {
        return ImmutableList.copyOf(posTags);
    }

    public ReadOnlyStringProperty posTagProperty() {
        return posTagValue;
    }

    public void rebindSelection() {
        labelSpinnerAdapter.rebind(selection.getSelectedTokens());
    }

    public void selectNextPosTagGroup() {
        labelSpinnerAdapter.apply(selection.getSelectedTokens(), LabelSpinner::selectNextGroup);
    }

    public void selectNextPosTag() {
        labelSpinnerAdapter.apply(selection.getSelectedTokens(), LabelSpinner::selectNext);
    }

    public void selectPreviousPosTag() {
        labelSpinnerAdapter.apply(selection.getSelectedTokens(), LabelSpinner::selectPrevious);
    }

    public void selectPreviousPosTagGroup() {
        labelSpinnerAdapter.apply(selection.getSelectedTokens(), LabelSpinner::selectPreviousGroup);
    }
}
