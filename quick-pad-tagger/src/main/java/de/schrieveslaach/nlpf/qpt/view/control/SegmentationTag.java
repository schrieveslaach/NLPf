package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.animation.ParallelTransition;
import javafx.animation.ScaleTransition;
import javafx.animation.Transition;
import javafx.animation.TranslateTransition;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.IntegerBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.IntegerProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.Region;
import javafx.scene.layout.StackPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Rectangle;

import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static de.schrieveslaach.nlpf.qpt.view.control.Coloring.MOST_FOCUS;
import static de.schrieveslaach.nlpf.qpt.view.control.Constants.SEGMENTATION_TAG_SCALE_TRANSITION_DURATION;
import static de.schrieveslaach.nlpf.qpt.view.control.Constants.SEGMENTATION_TAG_TRANSLATE_TRANSITION_DURATION;
import static de.schrieveslaach.nlpf.qpt.view.control.TokenLabel.TOKEN_LABEL_HEIGHT;
import static de.schrieveslaach.nlpf.qpt.view.control.TokenLabel.TOKEN_LABEL_WIDTH;
import static de.schrieveslaach.nlpf.qpt.view.control.TokenLabel.VERTICAL_BAR_HEIGHT;

/**
 * Displays content of a {@link Token}.
 */
public class SegmentationTag extends Region {

    private static final int EVENT_COUNTER_MAX = 10;

    final Rectangle background;

    private final Cursor cursor;

    private final Labels labels;

    private Group group;

    private final ObjectProperty<Token> currentSelectedToken = new SimpleObjectProperty<>();

    private final IntegerProperty index = new SimpleIntegerProperty();

    private final BooleanProperty showProperty = new SimpleBooleanProperty(false);

    private AtomicInteger eventCounter = new AtomicInteger(0);

    private final AtomicBoolean canChange = new AtomicBoolean(true);

    public SegmentationTag() {
        this.setId("segmentation-tag");

        currentSelectedToken.addListener((observableValue, oldValue, newValue) -> {
            if (newValue != null && showProperty.get() && newValue.getText().length() <= 1) {
                close();
            }
            index.set(0);
        });
        showProperty.addListener((observableValue, oldValue, newValue) -> {
            if (newValue && !oldValue) {
                index.set(0);
                openTransition();
            }
        });
        visibleProperty().bind(showProperty);


        labels = new Labels(TOKEN_LABEL_WIDTH, TOKEN_LABEL_HEIGHT);
        StackPane stackPane = new StackPane(labels);
        stackPane.setMinWidth(TOKEN_LABEL_WIDTH);
        stackPane.minWidthProperty().bind(labels.widthProperty());

        background = new Rectangle(0, 0);
        background.setHeight(TOKEN_LABEL_HEIGHT);
        background.widthProperty().bind(labels.widthProperty());
        background.setStrokeWidth(3);
        background.setFill(MOST_FOCUS.getBackgroundFillColor());
        background.setStroke(MOST_FOCUS.getBackgroundStrokeColor());

        cursor = new Cursor();

        group = new Group();
        group.translateXProperty().bind(labels.widthProperty().negate().divide(2));
        group.layoutXProperty().bind(widthProperty().divide(2));
        group.getChildren().addAll(background, stackPane, cursor);
        getChildren().addAll(group);
    }

    public void updateToken(Token token) {
        this.currentSelectedToken.set(token);
    }

    /**
     * Displays the {@code SegmentationTag} for the {@link Token currentSelectedToken}.
     */
    public boolean show() {
        if (currentSelectedToken.isNotNull().get() && currentSelectedToken.get().getText().length() > 1) {
            showProperty.set(true);
        }
        return true;
    }

    /**
     * Closes the {@code SegmentationTag}.
     */
    public void close() {
        closeTransition();
    }

    /**
     * Moves the cursor to the left.
     */
    public boolean moveLeft() {
        if (index.lessThanOrEqualTo(-(currentSelectedToken.get().getText().length() - 2)).get()) {
            if (eventCounter.addAndGet(1) > EVENT_COUNTER_MAX) {
                eventCounter.set(0);
                return false;
            }
            return true;
        } else {
            eventCounter.set(0);
            index.setValue(index.subtract(1).get());
            return true;
        }
    }

    /**
     * Moves the cursor to the right.
     */
    public boolean moveRight() {
        if (index.greaterThanOrEqualTo(0).get()) {
            if (eventCounter.addAndGet(1) > EVENT_COUNTER_MAX) {
                eventCounter.set(0);
                return false;
            }
            return true;
        } else {
            eventCounter.set(0);
            index.setValue(index.add(1).get());
            return true;
        }
    }

    /**
     * @return current index.
     */
    public int getIndex() {
        return cursor.getCurrentIndex();
    }

    /**
     * @return currently displayed text by {@link SegmentationTag#labels}.
     */
    String getDisplayedTokenText() {
        StringBuilder sb = new StringBuilder();
        labels.getChildren().stream()
                .map(label -> (Label) label)
                .forEach(label -> sb.append(label.getText()));
        return sb.toString();
    }

    private void openTransition() {
        synchronized (canChange) {
            if (!canChange.get()) {
                return;
            }
            canChange.set(false);
        }

        ParallelTransition parallelTransition = createParallelTransition(event -> canChange.set(true)
                , createScaleTransition(0, 0, 2., 2.)
                , createTranslateTransition(group.getLayoutY() + VERTICAL_BAR_HEIGHT, group.getLayoutY() - VERTICAL_BAR_HEIGHT));

        parallelTransition.play();
    }

    private void closeTransition() {
        synchronized (canChange) {
            if (!canChange.get()) {
                return;
            }
            canChange.set(false);
        }

        ParallelTransition parallelTransition = createParallelTransition(event -> {
                    showProperty.set(false);
                    canChange.set(true);
                },
                createScaleTransition(2., 2., 0, 0),
                createTranslateTransition(group.getLayoutY() - VERTICAL_BAR_HEIGHT, group.getLayoutY() + VERTICAL_BAR_HEIGHT));

        parallelTransition.play();
    }

    private ParallelTransition createParallelTransition(EventHandler<ActionEvent> onFinishHandler, Transition... transitions) {
        ParallelTransition parallelTransition = new ParallelTransition(transitions);
        parallelTransition.setOnFinished(onFinishHandler);
        return parallelTransition;
    }

    private TranslateTransition createTranslateTransition(double startY, double endY) {
        TranslateTransition translateTransition = new TranslateTransition(SEGMENTATION_TAG_TRANSLATE_TRANSITION_DURATION, this);

        translateTransition.setFromY(startY);
        translateTransition.setToY(endY);

        return translateTransition;
    }

    private ScaleTransition createScaleTransition(double startX, double startY, double endX, double endY) {
        ScaleTransition scaleTransition = new ScaleTransition(SEGMENTATION_TAG_SCALE_TRANSITION_DURATION, this);

        scaleTransition.setFromX(startX);
        scaleTransition.setFromY(startY);
        scaleTransition.setToX(endX);
        scaleTransition.setToY(endY);

        return scaleTransition;
    }

    /**
     * Represents a textcursor.
     */
    public class Cursor extends Rectangle {

        private IntegerBinding currentIndex = Bindings.createIntegerBinding(() -> {
            if (currentSelectedToken.isNull().get()) {
                return -1;
            }

            int i = currentSelectedToken.get().getText().length() - 2;
            i += index.get();
            return i;
        }, index, currentSelectedToken);

        private DoubleBinding indexPosition = Bindings.createDoubleBinding(() -> {
            ObservableList<Node> observableLabels = labels.getChildrenUnmodifiable();
            if (observableLabels.isEmpty() || currentIndex.get() < 0 || observableLabels.size() <= currentIndex.get()) {
                return 0.0;
            }

            double imageOffset = getWidth() / 2;
            Label label = (Label) observableLabels.get(currentIndex.get());
            return label.getLayoutX() + label.getWidth() - imageOffset;
        }, labels.widthProperty(), currentIndex);

        Cursor() {
            super(3, TOKEN_LABEL_HEIGHT * 0.5, Color.WHITE);
            layoutYProperty().setValue(TOKEN_LABEL_HEIGHT * 0.25);
            layoutXProperty().bind(indexPosition);
            visibleProperty().bind(showProperty);
        }

        int getCurrentIndex() {
            return currentIndex.get();
        }
    }

    public class Labels extends HBox {

        Labels(double minWidth, double minHeight) {
            super();
            setAlignment(Pos.CENTER);
            setMinWidth(minWidth);
            setMinHeight(minHeight);
            visibleProperty().bind(showProperty);
            currentSelectedToken.addListener((observableValue, oldValue, newValue) -> {
                if (newValue == null) {
                    resetLabels();
                } else {
                    updateLabels(newValue.getText());
                }
            });
        }

        void updateLabels(String text) {
            setWidth(getMinWidth());
            getChildren().clear();
            for (char c : text.toCharArray()) {
                Label label = new Label(String.valueOf(c));
                label.getStyleClass().add("segmentation-label");
                label.setTextFill(MOST_FOCUS.getTextColor());
                getChildren().add(label);
                HBox.setHgrow(label, Priority.ALWAYS);
            }
            requestLayout();
        }

        void resetLabels() {
            getChildren().clear();
        }
    }
}
