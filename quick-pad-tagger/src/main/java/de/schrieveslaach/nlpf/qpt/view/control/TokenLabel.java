package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import javafx.animation.FillTransition;
import javafx.animation.ParallelTransition;
import javafx.animation.StrokeTransition;
import javafx.animation.Transition;
import javafx.beans.binding.Bindings;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.DoubleProperty;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import javafx.scene.shape.Circle;
import javafx.scene.shape.ClosePath;
import javafx.scene.shape.LineTo;
import javafx.scene.shape.MoveTo;
import javafx.scene.shape.Path;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.VLineTo;
import lombok.Getter;

import static de.schrieveslaach.nlpf.qpt.view.control.Constants.TRANSITION_DURATION;
import static de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter.REGION_DISTANCE;

/**
 * Custom Label that displays the content of a {@link de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token}
 * in a {@link TokenStream}.
 */
public class TokenLabel extends Region implements MinWidthLabel {

    public static final int TOKEN_LABEL_WIDTH = 200;

    public static final int TOKEN_LABEL_HEIGHT = 75;

    public static final double VERTICAL_BAR_HEIGHT = 112.5;

    private final Label label;

    private final Rectangle background;

    private final Path selection;

    private final BooleanProperty showSelectedIndicatorProperty = new SimpleBooleanProperty();

    private ObjectProperty<Rectangle> sentenceSplitSymbol = new SimpleObjectProperty<>();

    private ObjectProperty<Circle> tokenSplitSymbol = new SimpleObjectProperty<>();

    @Getter
    private Coloring coloring;

    public TokenLabel() {
        this(null);
    }

    /**
     * Creates a {@code TokenLabel} with the specified height.
     *
     * @param prefHeight
     */
    public TokenLabel(double prefHeight) {
        this(null);
        setPrefHeight(prefHeight);
    }

    public TokenLabel(String text) {
        label = new Label(text);
        label.getStyleClass().add("token-label");

        background = new Rectangle(0, 0, TOKEN_LABEL_WIDTH, TOKEN_LABEL_HEIGHT);
        background.strokeWidthProperty().set(3.0);
        background.widthProperty().bind(label.widthProperty());
        label.minWidthProperty().set(TOKEN_LABEL_WIDTH);

        selection = createSelection();
        showSelectedIndicatorProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue) {
                selection.opacityProperty().set(1.0);
            } else {
                selection.opacityProperty().set(0.0);
            }
        });

        setColoring(Coloring.LESS_FOCUS);

        getChildren().addAll(background, selection, label);
    }

    private Path createSelection() {
        Path p = new Path();
        p.setId("selection-indicator");

        MoveTo leftMoveTo = new MoveTo();
        leftMoveTo.setX(1.5);
        leftMoveTo.setY(1.5);

        VLineTo leftLineTo1 = new VLineTo();
        leftLineTo1.setY(TOKEN_LABEL_HEIGHT * 0.3 - 1.5);

        LineTo leftLineTo2 = new LineTo();
        leftLineTo2.setX(TOKEN_LABEL_HEIGHT * 0.3);
        leftLineTo2.setY(1.5);

        LineTo leftLineTo3 = new LineTo();
        leftLineTo3.setX(1.5);
        leftLineTo3.setY(1.5);

        p.strokeWidthProperty().set(0.0);
        p.opacityProperty().set(0.0);
        p.getElements().addAll(
                leftMoveTo, leftLineTo1, leftLineTo2, leftLineTo3, new ClosePath()
        );

        return p;
    }

    public void setText(String text) {
        label.setText(text);
    }

    public String getText() {
        return label.getText();
    }

    public void setColoring(Coloring coloring) {
        this.coloring = coloring;
        background.setFill(coloring.getBackgroundFillColor());
        background.setStroke(coloring.getBackgroundStrokeColor());
        selection.setFill(coloring.getSelectionIndicatorFillColor());
        label.setTextFill(coloring.getTextColor());
    }

    /**
     * @return {@link DoubleBinding binding} for the full width of this label (including token and sentence split
     * symbol).
     */
    public DoubleBinding fullWidthProperty() {
        return Bindings.createDoubleBinding(() -> {
            double d = background.widthProperty().get();
            d += sentenceSplitSymbol.get() == null ? 0 : sentenceSplitSymbol.get().getWidth() + REGION_DISTANCE;
            d += tokenSplitSymbol.get() == null ? 0 : tokenSplitSymbol.get().getRadius() * 2 + REGION_DISTANCE;
            return d;
        }, tokenSplitSymbol, sentenceSplitSymbol, background.widthProperty());
    }

    /**
     * @return {@link DoubleProperty Property} which provides the label minimal width.
     */
    @Override
    public DoubleProperty labelMinWidthProperty() {
        return label.minWidthProperty();
    }

    public Transition createColoringTransition(Coloring from, Coloring to) {
        ParallelTransition pt = new ParallelTransition();
        pt.getChildren().addAll(
                new FillTransition(TRANSITION_DURATION, background, from.getBackgroundFillColor(), to.getBackgroundFillColor()),
                new FillTransition(TRANSITION_DURATION, selection, from.getSelectionIndicatorFillColor(), to.getSelectionIndicatorFillColor()),
                new StrokeTransition(TRANSITION_DURATION, background, from.getBackgroundStrokeColor(), to.getBackgroundStrokeColor())
        );
        pt.setOnFinished(e -> {
            label.setTextFill(to.getTextColor());
            this.coloring = to;
        });
        return pt;
    }

    /**
     * If true, adds a vertical bar to the end of this {@code TokenLabel}
     * else removes it.
     *
     * @param showVerticalBar
     */
    public void showVerticalBar(boolean showVerticalBar) {
        if (showVerticalBar) {
            if (sentenceSplitSymbol.isNull().get()) {
                addSentenceSplit();
            }
        } else {
            if (sentenceSplitSymbol.isNotNull().get()) {
                getChildren().remove(sentenceSplitSymbol.get());
                sentenceSplitSymbol.set(null);
            }
        }
    }

    /**
     * If true, adds a circle to the start of this {@code TokenLabel}
     * else removes it.
     *
     * @param showCircle
     */
    public void showCircle(boolean showCircle) {
        if (showCircle) {
            if (tokenSplitSymbol.isNull().get()) {
                addTokenSplit();
            }
        } else {
            if (tokenSplitSymbol.isNotNull().get()) {
                Circle circle = tokenSplitSymbol.get();
                getChildren().stream()
                        .filter(node -> node != circle && node != sentenceSplitSymbol.get())
                        .forEach(node -> node.setTranslateX(node.translateXProperty().subtract(circle.getRadius() * 2 + REGION_DISTANCE).get()));
                getChildren().remove(circle);
                tokenSplitSymbol.set(null);
            }
        }
    }

    private void addSentenceSplit() {
        Rectangle rec = new Rectangle(10, 1.5 * TOKEN_LABEL_HEIGHT);
        rec.setId("sentence-split-indicator");
        rec.setFill(Coloring.LESS_FOCUS.getBackgroundStrokeColor());
        sentenceSplitSymbol.set(rec);
        getChildren().add(sentenceSplitSymbol.get());

        DoubleBinding doubleBinding = Bindings.createDoubleBinding(() -> {
            double d = background.widthProperty().get() + REGION_DISTANCE;
            d += tokenSplitSymbol.get() == null ? 0 : tokenSplitSymbol.get().getRadius() * 2 + REGION_DISTANCE;
            return d;
        }, tokenSplitSymbol, background.widthProperty());
        sentenceSplitSymbol.get().translateXProperty().bind(doubleBinding);
        sentenceSplitSymbol.get().translateYProperty().set(translateYProperty().add(TOKEN_LABEL_HEIGHT * -0.25).get());
    }

    private void addTokenSplit() {
        Circle circle = new Circle(TOKEN_LABEL_HEIGHT * 0.15);
        circle.setId("token-split-indicator");
        circle.setFill(Coloring.LESS_FOCUS.getBackgroundStrokeColor());
        tokenSplitSymbol.set(circle);
        getChildren().add(circle);

        getChildren().stream()
                .filter(node -> node != circle && node != sentenceSplitSymbol.get())
                .forEach(node -> node.setTranslateX(node.translateXProperty().add(circle.getRadius() * 2 + REGION_DISTANCE).get()));
        circle.translateXProperty().bind(circle.radiusProperty());
        circle.setTranslateY(circle.getTranslateY() + TOKEN_LABEL_HEIGHT * 0.5);
    }

    public BooleanProperty showSelectedIndicatorProperty() {
        return showSelectedIndicatorProperty;
    }
}
