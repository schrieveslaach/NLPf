package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.animation.ParallelTransition;
import javafx.beans.value.ObservableObjectValue;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.Region;
import lombok.Getter;
import org.apache.uima.fit.util.JCasUtil;
import org.apache.uima.jcas.JCas;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectCovered;

public class TokenStream extends Region {

    private Selection selection = new Selection();

    private JCas jCas;

    @Getter
    private PaginationAdapter<TokenLabel, Token> tokenLabelAdapter = new PaginationAdapter<>(
            () -> new TokenLabel(TokenLabel.VERTICAL_BAR_HEIGHT),
            (TokenLabel tokenLabel, Token token) -> {
                boolean showCircle = false;
                boolean showVerticalBar = false;
                if (token != null) {
                    if (jCas != null) {
                        Map<Token, Collection<Sentence>> tokenSentenceMap = JCasUtil.indexCovering(jCas, Token.class, Sentence.class);
                        Optional<Sentence> sentence = tokenSentenceMap.get(token)
                                .stream()
                                .findFirst();
                        showVerticalBar = sentence.isPresent() && sentence.get().getEnd() == token.getEnd();

                        if (token.getBegin() > 0) {
                            Token preceding = JCasUtil.selectSingleRelative(jCas, Token.class, token, -1);
                            showCircle = preceding != null && preceding.getEnd() == token.getBegin();
                        }
                    }
                    tokenLabel.setText(token.getText());

                    if (selection != null && selection.getSelectedSentenceTokens().contains(token)) {
                        tokenLabel.setColoring(Coloring.FOCUS);
                    }
                    if (selection != null && selection.getSelectedTokens().contains(token)) {
                        tokenLabel.setColoring(Coloring.MORE_FOCUS);
                    }

                    tokenLabel.setId("token-" + token.hashCode());
                } else {
                    tokenLabel.setText(null);
                    tokenLabel.setColoring(Coloring.LESS_FOCUS);
                    tokenLabel.setId(null);
                }
                tokenLabel.showVerticalBar(showVerticalBar);
                tokenLabel.showCircle(showCircle);
            },
            TokenLabel::fullWidthProperty
    );

    /**
     * Creates a new instance of {@link TokenStream}.
     */
    public TokenStream() {
        widthProperty().addListener(tokenLabelAdapter);

        tokenLabelAdapter.regionsProperty().addListener((ListChangeListener<TokenLabel>) c -> {
            while (c.next()) {
                getChildren().removeAll(c.getRemoved());
                getChildren().addAll(c.getAddedSubList());
            }
        });
    }

    public void bindToSentences(ObservableList<Sentence> sentences) {
        sentences.addListener((ListChangeListener<Sentence>) c -> {
            if (jCas == null) {
                return;
            }
            while (c.next()) {
                for (Sentence sentence : c.getAddedSubList()) {
                    tokenLabelAdapter.rebind(selectCovered(jCas, Token.class, sentence));
                }
            }
        });
    }

    public void bindToTokens(ObservableList<Token> tokens) {
        tokens.addListener(tokenLabelAdapter);
    }

    public void bindToSelection(ObservableObjectValue<Selection> selectionProperty, Runnable animationFinished) {
        selectionProperty.addListener((observable, oldValue, newValue) -> {
            if (newValue == null) {
                return;
            }

            ParallelTransition pt = new ParallelTransition();
            selection = newValue;

            // Current sentence
            if (oldValue != null) {
                tokenLabelAdapter.apply(oldValue.getSelectedTokens(), tokenLabel ->
                        pt.getChildren().add(tokenLabel.createColoringTransition(tokenLabel.getColoring(), Coloring.LESS_FOCUS))
                );
                tokenLabelAdapter.apply(oldValue.getFocusedTokens(), tokenLabel ->
                        pt.getChildren().add(tokenLabel.createColoringTransition(tokenLabel.getColoring(), Coloring.LESS_FOCUS))
                );
            }

            tokenLabelAdapter.apply(newValue.getFocusedTokens(), tokenLabel ->
                    pt.getChildren().add(tokenLabel.createColoringTransition(tokenLabel.getColoring(), Coloring.FOCUS))
            );
            tokenLabelAdapter.apply(newValue.getSelectedTokens(), tokenLabel ->
                    pt.getChildren().add(tokenLabel.createColoringTransition(tokenLabel.getColoring(), Coloring.MORE_FOCUS))
            );

            pt.getChildren().add(tokenLabelAdapter.createScrollToAnimation(newValue.getSelectedTokens()));
            pt.setOnFinished(event -> animationFinished.run());

            pt.play();
        });
    }

    public void bindToJCas(ObservableObjectValue<JCas> jCas) {
        jCas.addListener((observable, oldValue, newValue) -> {
            this.jCas = newValue;
            if (newValue != null) {
                tokenLabelAdapter.rebind(Lists.newArrayList(select(newValue, Token.class)));
            }
        });
    }
}
