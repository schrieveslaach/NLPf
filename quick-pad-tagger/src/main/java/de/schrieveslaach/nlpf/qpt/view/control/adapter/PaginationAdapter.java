package de.schrieveslaach.nlpf.qpt.view.control.adapter;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.view.control.Constants;
import javafx.animation.Animation;
import javafx.animation.ParallelTransition;
import javafx.animation.SequentialTransition;
import javafx.animation.TranslateTransition;
import javafx.beans.binding.DoubleBinding;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.layout.Region;
import lombok.Getter;
import lombok.Setter;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import static de.schrieveslaach.nlpf.qpt.view.control.Constants.TRANSITION_DURATION;
import static javafx.beans.binding.Bindings.createDoubleBinding;
import static javafx.collections.FXCollections.observableArrayList;

public class PaginationAdapter<R extends Region, V> implements ChangeListener<Number>, ListChangeListener<V> {

    public static final double REGION_DISTANCE = 20;

    private final RegionCreator<R> regionCreator;

    private final RegionBinder<R, V> regionBinder;

    private final RegionWidthPropertyResolver<R> regionWidthPropertyResolver;

    private boolean reorderRegions = false;

    private Number width;
    private int viewCount;

    private final ObservableList<R> regions = observableArrayList();

    private final DoubleBinding regionsMinWidth;

    private final DoubleBinding regionsAccumulatedWidth;

    private final ChangeListener<Number> widthChangeListener;

    private int index = 0;

    private ObservableList<? extends V> values = FXCollections.observableArrayList();

    private Map<R, V> regionsAndValues = new LinkedHashMap<>();

    @Getter
    @Setter
    private PaginationAdapter<? extends Region, V> parentPaginationAdapter;

    @Getter
    private List<PaginationAdapter<? extends Region, V>> childPaginationAdapters = new ArrayList<>();

    public void addChildPaginationAdapter(PaginationAdapter<? extends Region, V> paginationAdapter) {
        childPaginationAdapters.clear();
        childPaginationAdapters.add(paginationAdapter);
    }

    public PaginationAdapter(RegionCreator<R> regionCreator, RegionBinder<R, V> regionBinder, RegionWidthPropertyResolver<R> regionWidthPropertyResolver) {
        this.regionCreator = regionCreator;
        this.regionBinder = regionBinder;
        this.regionWidthPropertyResolver = regionWidthPropertyResolver;
        this.regionsMinWidth = createDoubleBinding(
                () -> {
                    if (parentPaginationAdapter != null) {
                        return parentPaginationAdapter.regionsMinWidth.doubleValue();
                    }

                    return regions.stream()
                            .mapToDouble(r -> regionWidthPropertyResolver.widthProperty(r).doubleValue())
                            .min()
                            .orElse(200.0);
                },
                regions
        );

        this.regionsAccumulatedWidth = createDoubleBinding(
                () -> {
                    if (parentPaginationAdapter != null) {
                        return parentPaginationAdapter.regionsAccumulatedWidth.doubleValue();
                    }

                    return regions.stream()
                            .mapToDouble(r -> regionWidthPropertyResolver.widthProperty(r).doubleValue() + REGION_DISTANCE)
                            .sum() - REGION_DISTANCE;
                },
                regions
        );

        this.regionsAccumulatedWidth.addListener((observable, oldWidth, newWidth) -> {
            synchronized (regionsAccumulatedWidth) {
                if (reorderRegions) {
                    return;
                }
            }
            layoutRegions();
        });

        widthChangeListener = (observable, oldValue, newValue) -> {
            regionsAccumulatedWidth.invalidate();
            regionsMinWidth.invalidate();
        };
    }

    public ObservableList<R> regionsProperty() {
        return regions;
    }

    @Override
    public void onChanged(Change<? extends V> c) {
        values = c.getList();
        int valueCount = values.size();

        boolean create = true;
        while (c.next()) {
            if (c.getRemovedSize() < valueCount && c.getRemovedSize() > 0) {
                List<? extends V> removedValues = c.getRemoved();
                int from = determineNodeIndexOfValue(c.getRemoved().get(0));
                int to = regions.size() - 1;
                removeNodesAndMoveTo(from, to, removedValues);
                create = false;
            } else if (c.getAddedSize() < valueCount && c.getAddedSize() > 0) {
                List<? extends V> addedValues = c.getAddedSubList();
                int from = regions.size() - 1;
                int indexOf = values.indexOf(addedValues.get(0));
                if (indexOf != 0) {
                    int to = determineNodeIndexOfValue(values.get(indexOf - 1)) + 1;
                    addNode(from, to, addedValues);
                } else {
                    int to = determineNodeIndexOfValue(values.get(addedValues.size()));
                    addNode(from, to, addedValues);
                }
                create = false;
            }
        }

        if (create) {
            index = 0;
            bindNodes(createRegions());
        }
    }

    private void removeNodesAndMoveTo(int from, int to, List<? extends V> removedValues) {
        ParallelTransition pt = new ParallelTransition();
        double tx = getDistanceBetween(from, to - removedValues.size() - 1);
        double removedNodesDistance = getDistanceBetween(from, from + removedValues.size() - 1);

        pt.getChildren().addAll(removeNodes(from, to, tx, removedValues));

        int removedSize = removedValues.size();
        int sign = to > from ? -1 : 1;

        for (int i = from + (removedSize - 1); i < to; i++) {
            R r = regions.get(i);
            TranslateTransition tt = new TranslateTransition(Constants.TRANSITION_DURATION, r);
            tt.setByX(sign * (REGION_DISTANCE + removedNodesDistance));
            pt.getChildren().add(tt);
        }

        pt.setOnFinished(event -> layoutRegions());
        pt.play();
    }

    private void addNode(int from, int to, List<? extends V> addedValues) {
        if (regions.isEmpty()) {
            return;
        }

        ParallelTransition pt = new ParallelTransition();
        int addedSize = addedValues.size();

        int i = addedSize - 1;
        for (V v : addedValues) {
            R r = regions.remove(from - i);
            regions.add(to + (addedSize - 1) - i, r);
            bind(v, r);

            TranslateTransition tt = new TranslateTransition(Constants.TRANSITION_DURATION, r);
            tt.setByX((to - from + (addedSize - 1)) * (REGION_DISTANCE + regionsMinWidth.doubleValue()));
            pt.getChildren().add(tt);
            --i;
        }

        for (int k = to; k < from - (addedSize - 1); k++) {
            R r = regions.get(k);
            TranslateTransition tt = new TranslateTransition(Constants.TRANSITION_DURATION, r);
            tt.setByX(Math.signum(((float) from) - to) * (addedSize) * (REGION_DISTANCE + regionsMinWidth.doubleValue()));
            pt.getChildren().add(tt);
        }

        pt.setOnFinished(event -> layoutRegions());
        pt.play();
    }

    @Override
    public void changed(ObservableValue<? extends Number> observable, Number oldWidth, Number newWidth) {
        int newCount;
        if (parentPaginationAdapter == null) {
            newCount = (int) (3 * Math.ceil(newWidth.doubleValue() / (regionsMinWidth.doubleValue() + REGION_DISTANCE)));

            // The algorithm for pagination relies on an odd number
            if (newCount % 2 == 0) {
                ++newCount;
            }
        } else {
            newCount = parentPaginationAdapter.viewCount;
        }

        this.width = newWidth;
        this.viewCount = newCount;

        List<R> tmpNodes = createRegions();
        bindNodes(tmpNodes);
    }

    private List<R> createRegions() {
        List<R> tmpNodes = new ArrayList<>();
        for (int i = 0; i < viewCount; ++i) {
            R region = regionCreator.create();
            regionWidthPropertyResolver.widthProperty(region).addListener(widthChangeListener);
            tmpNodes.add(region);
        }

        regions.forEach(r -> regionWidthPropertyResolver.widthProperty(r).removeListener(widthChangeListener));
        regions.clear();
        regions.addAll(tmpNodes);
        return tmpNodes;
    }

    private void bindNodes(List<R> regions) {
        regionsAndValues.clear();

        if (values == null || regions.isEmpty()) {
            regions.forEach(
                    r -> bind(null, r)
            );
            return;
        }

        int tokenIndex;
        int filledTokens = 0;

        int firstTokenIndexInPaginationWindow = this.index - (regions.size() >> 1);
        if (firstTokenIndexInPaginationWindow >= 0) {
            tokenIndex = firstTokenIndexInPaginationWindow;
        } else {
            tokenIndex = 0;
            filledTokens = Math.abs(firstTokenIndexInPaginationWindow);
        }

        int i = 0;
        for (V value : values) {
            if (i >= tokenIndex) {
                R r = regions.get(filledTokens);
                bind(value, r);

                if (++filledTokens >= regions.size()) {
                    break;
                }
            }
            ++i;
        }

        for (; filledTokens < regions.size(); ++filledTokens) {
            R r = regions.get(filledTokens);
            bind(null, r);
        }
    }

    private void bind(V value, R region) {
        regionsAndValues.put(region, value);
        regionBinder.bind(region, value);
    }

    private void layoutRegions() {
        if (width == null || regions.isEmpty()) {
            return;
        }

        if (parentPaginationAdapter != null) {
            for (int i = 0; i < viewCount; ++i) {
                Region parentRegion = parentPaginationAdapter.regions.get(i);
                regions.get(i).translateXProperty().set(parentRegion.getTranslateX());
            }

            return;
        }

        final int middleIdx = regions.size() >> 1;
        R middleRegion = regions.get(middleIdx);

        final double middleRegionLeft = width.doubleValue() * 0.5 - regionWidthPropertyResolver.widthProperty(middleRegion).doubleValue() * 0.5;
        final double middleRegionRight = middleRegionLeft + regionWidthPropertyResolver.widthProperty(middleRegion).doubleValue();

        middleRegion.translateXProperty().set(middleRegionLeft);

        double x = middleRegionLeft;
        for (int i = middleIdx - 1; i >= 0; --i) {
            R region = regions.get(i);
            x -= REGION_DISTANCE + regionWidthPropertyResolver.widthProperty(region).doubleValue();

            if (Math.abs(region.getTranslateX() - x) > 0) {
                region.translateXProperty().set(x);
            }
        }

        x = middleRegionRight + REGION_DISTANCE;
        for (int i = middleIdx + 1; i < regions.size(); ++i) {
            R region = regions.get(i);
            if (Math.abs(region.getTranslateX() - x) > 0) {
                region.translateXProperty().set(x);
            }
            x += REGION_DISTANCE + regionWidthPropertyResolver.widthProperty(region).doubleValue();
        }
    }

    public void rebind(List<? extends V> values) {
        regionsAndValues.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .filter(e -> values.contains(e.getValue()))
                .forEach(e -> regionBinder.bind(e.getKey(), e.getValue()));
    }

    public void apply(V value, Consumer<R> consumer) {
        regionsAndValues.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .filter(e -> Objects.equals(value, e.getValue()))
                .forEach(e -> consumer.accept(e.getKey()));
    }

    public void apply(List<? extends V> values, Consumer<R> consumer) {
        regionsAndValues.entrySet().stream()
                .filter(e -> e.getValue() != null)
                .filter(e -> values.contains(e.getValue()))
                .forEach(e -> consumer.accept(e.getKey()));
    }

    public Animation createScrollToAnimation(List<? extends V> values) {
        ParallelTransition pt = new ParallelTransition();

        if (values.isEmpty() || regions.isEmpty()) {
            return pt;
        }

        int newIndex = this.values.indexOf(values.get(values.size() - 1));
        if (index == -1 || newIndex == index) {
            return pt;
        }

        synchronized (regionsAccumulatedWidth) {
            reorderRegions = true;
        }

        SequentialTransition st = new SequentialTransition();
        st.rateProperty().set(Math.abs(index - newIndex));

        if (newIndex > index) {
            for (int i = index + 1; i <= newIndex; ++i) {
                st.getChildren().add(createScrollToAnimation(i));
            }
        } else {
            for (int i = index - 1; i >= newIndex; --i) {
                st.getChildren().add(createScrollToAnimation(i));
            }
        }

        pt.getChildren().add(st);
        pt.getChildren().addAll(childPaginationAdapters.stream()
                .map(vPaginationAdapter -> vPaginationAdapter.createScrollToAnimation(values))
                .collect(Collectors.toList()));

        pt.setOnFinished(event -> {
            synchronized (regionsAccumulatedWidth) {
                reorderRegions = false;
            }
            layoutRegions();
        });

        return pt;
    }

    private Animation createScrollToAnimation(int newIndex) {
        ParallelTransition pt = new ParallelTransition();

        int middleRegionIndex = regions.size() >> 1;

        double tx;
        if (newIndex > index) {
            tx = -(getParentOrSelfRegionWidth(middleRegionIndex + 1) * 0.5
                    + REGION_DISTANCE
                    + getParentOrSelfRegionWidth(middleRegionIndex) * 0.5);
        } else {
            tx = getParentOrSelfRegionWidth(middleRegionIndex) * 0.5
                    + REGION_DISTANCE
                    + getParentOrSelfRegionWidth(middleRegionIndex - 1) * 0.5;
        }

        for (int i = 0; i < regions.size(); ++i) {
            R region = regions.get(i);

            // skip first of last region because this region will be translated
            // by an additional transform
            if (isRegionWhichMovesToFrontOrBack(newIndex, i)) continue;

            TranslateTransition tt = new TranslateTransition(TRANSITION_DURATION, region);
            tt.setByX(tx);
            pt.getChildren().add(tt);
        }

        pt.getChildren().add(createPaginationTransform(newIndex, tx));


        this.index = newIndex;

        return pt;
    }

    private Animation createPaginationTransform(int newIndex, double tx) {
        int sign;
        int from;
        int to;
        if (newIndex > index) {
            sign = 1;
            from = 0;
            to = regions.size() - 1;
        } else {
            sign = -1;
            to = 0;
            from = regions.size() - 1;
        }

        V value = getValue(newIndex + sign * (regions.size() / 2));
        R region = regions.remove(from);
        regions.add(to, region);
        bind(value, region);

        TranslateTransition tt = new TranslateTransition(TRANSITION_DURATION, region);
        tt.setByX(
                sign * regionsAccumulatedWidth.doubleValue()
                        + sign * REGION_DISTANCE
                        + tx
        );
        return tt;
    }

    private List<Animation> removeNodes(int from, int to, double tx, List<? extends V> removed) {
        List<Animation> removeTransform = new ArrayList<>();
        int size = removed.size();

        int sign = to > from ? 1 : -1;
        for (int i = size - 1; i >= 0; i--) {
            V value = getValue(determineValueIndexOfNode(regions.get(to - (size - 1 - i))) + (sign * (i + 1)));
            R region = regions.remove(from + (sign * i));
            regions.add(to - (size - 1) + i, region);
            bind(value, region);

            TranslateTransition tt = new TranslateTransition(TRANSITION_DURATION, region);
            tt.setByX(sign * tx);
            removeTransform.add(tt);
        }
        return removeTransform;
    }

    private double getDistanceBetween(int start, int end) {
        double tx = 0;
        if (regions.isEmpty()) {
            return tx;
        }

        for (int i = start; i <= end; i++) {
            R r = regions.get(i);
            tx += regionWidthPropertyResolver.widthProperty(r).doubleValue();
        }
        return tx;
    }

    private boolean isRegionWhichMovesToFrontOrBack(int newIndex, int i) {
        if (newIndex > index) {
            if (i == 0) {
                return true;
            }
        } else {
            if (i == regions.size() - 1) {
                return true;
            }
        }
        return false;
    }

    public Region getParentOrSelfRegion(V value) {
        if (parentPaginationAdapter != null) {
            return parentPaginationAdapter.getParentOrSelfRegion(value);
        }

        int i = values.indexOf(value);
        if (i < 0) {
            return null;
        }

        return regionsAndValues.entrySet().stream()
                .filter(e -> Objects.equals(e.getValue(), value))
                .map(Map.Entry::getKey)
                .findFirst()
                .orElse(null);
    }

    private double getParentOrSelfRegionWidth(int nodeIndex) {
        if (parentPaginationAdapter != null) {
            return parentPaginationAdapter.getParentOrSelfRegionWidth(nodeIndex);
        }

        R region = regions.get(nodeIndex);
        return regionWidthPropertyResolver.widthProperty(region).doubleValue();
    }

    private V getValue(int i) {
        if (i < 0 || values.size() <= i) {
            return null;
        }
        return values.get(i);
    }

    private int determineNodeIndexOfValue(V v) {
        Optional<R> first = regionsAndValues.entrySet().stream()
                .filter(rvEntry -> Objects.equals(rvEntry.getValue(), v))
                .map(Map.Entry::getKey)
                .findFirst();
        return first.map(regions::indexOf).orElse(-1);
    }

    private int determineValueIndexOfNode(R r) {
        Optional<Map.Entry<R, V>> first = regionsAndValues.entrySet().stream()
                .filter(rvEntry -> Objects.equals(rvEntry.getKey(), r))
                .findFirst();
        return first.map(rvEntry -> values.indexOf(rvEntry.getValue())).orElse(-1);
    }

    @FunctionalInterface
    public interface RegionCreator<R extends Region> {

        R create();

    }

    @FunctionalInterface
    public interface RegionBinder<R extends Region, V> {

        void bind(R region, V value);

    }

    @FunctionalInterface
    public interface RegionWidthPropertyResolver<R extends Region> {

        DoubleExpression widthProperty(R region);

    }
}
