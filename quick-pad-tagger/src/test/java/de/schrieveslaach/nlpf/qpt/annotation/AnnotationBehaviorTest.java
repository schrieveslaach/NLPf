package de.schrieveslaach.nlpf.qpt.annotation;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableList;
import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.view.AnnotationView;
import de.schrieveslaach.nlpf.qpt.view.control.TokenStream;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.Region;
import org.apache.uima.jcas.JCas;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;

import javax.annotation.PostConstruct;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

public abstract class AnnotationBehaviorTest<B extends AnnotationBehavior> extends GuiTest {

    public static final int ATTEMPTS_COUNTS = 20;

    @Autowired
    protected B behavior;

    @Autowired
    protected JCasService jCasService;

    @Autowired
    protected SimpleObjectProperty<Selection> selectionProperty;

    @PostConstruct
    public void initView() throws Exception {
        init(AnnotationView.class);
    }

    @Before
    public void setupTokenTagsRegion() {
        interact(() -> {
            TokenStream tokenStream = find("#tokenStream");
            tokenStream.bindToJCas(jCasService.jCasProperty());
            tokenStream.bindToSentences(jCasService.sentencesProperty());
            tokenStream.bindToTokens(jCasService.tokensProperty());
            tokenStream.bindToSelection(selectionProperty, () -> {
            });

            behavior.selectionProperty.bindBidirectional(selectionProperty);
            behavior.activate(tokenStream.getTokenLabelAdapter());

            Region tokenTagsRegion = behavior.getTokenTagsRegion();

            AnchorPane.setBottomAnchor(tokenTagsRegion, 85.0);
            AnchorPane.setLeftAnchor(tokenTagsRegion, 0.0);
            AnchorPane.setRightAnchor(tokenTagsRegion, 0.0);

            AnchorPane pane = find("#annotation");
            pane.getChildren().add(tokenTagsRegion);

            tokenTagsRegion.toBack();
        });
    }

    @After
    public void deactivate() {
        interact(() -> {
            behavior.deactivate();
        });
    }

    protected Token getTokenByText(String label) {
        return jCasService.select(Token.class).stream()
                .filter(token -> token.getText().equals(label))
                .findFirst()
                .orElse(null);
    }

    protected void moveSelectionToRight() {
        interact(() -> {
            Selection oldSelection = selectionProperty.get();
            List<Token> tokens = jCasService.selectFollowing(Token.class, oldSelection.getSelectedTokens().get(0), 1);

            updateSelectionProperty(oldSelection, tokens);
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    protected void expandSelectionToRight() {
        interact(() -> {
            Selection oldSelection = selectionProperty.get();
            ImmutableList<Token> selectedTokens = oldSelection.getSelectedTokens();
            List<Token> tokens = jCasService.selectFollowing(Token.class, selectedTokens.get(0), selectedTokens.size());
            tokens.add(0, selectedTokens.get(0));

            updateSelectionProperty(oldSelection, tokens);
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    protected void moveSelectionToLeft() {
        interact(() -> {
            Selection oldSelection = selectionProperty.get();
            List<Token> tokens = jCasService.selectPreceding(Token.class, oldSelection.getSelectedTokens().get(0), 1);

            updateSelectionProperty(oldSelection, tokens);
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    protected void expandSelectionToLeft() {
        interact(() -> {
            Selection oldSelection = selectionProperty.get();
            ImmutableList<Token> selectedTokens = oldSelection.getSelectedTokens();
            int indexOfLastToken = selectedTokens.size() - 1;
            List<Token> tokens = jCasService.selectPreceding(Token.class, selectedTokens.get(indexOfLastToken), selectedTokens.size());
            tokens.add(selectedTokens.get(indexOfLastToken));

            updateSelectionProperty(oldSelection, tokens);
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void updateSelectionProperty(Selection oldSelection, List<Token> tokens) {
        if (!tokens.isEmpty()) {
            Selection newSelection = new Selection(getJCas(), tokens);
            Selection suggestedSelection = behavior.suggestNewSelection(oldSelection, newSelection);
            if (suggestedSelection == null) {
                return;
            }
            newSelection = suggestedSelection;

            selectionProperty.set(newSelection);
        }
    }

    protected void moveSelectionToPreviousPosTagGroup() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isLeftTriggerPressed()).thenReturn(true);
            when(action.isMoveSelectionUp()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(100);
    }

    protected void moveSelectionToNextPosTagGroup() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isLeftTriggerPressed()).thenReturn(true);
            when(action.isMoveSelectionDown()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(100);
    }

    protected void moveSelectionUp() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionUp()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    protected void moveSelectionDown() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionDown()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    protected void pressAcceptButton() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isAcceptButtonPressed()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    protected void pressRemoveLeftButton() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isRemoveLeftAnnotationPressed()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    public void assertThatSelectionIsEqualTo(Token... tokens) {
        Selection selection = selectionProperty.get();
        assertThat(selection, is(equalTo(new Selection(getJCas(), tokens))));
    }

    public JCas getJCas() {
        return jCasService.jCasProperty().get();
    }

    /**
     * @deprecated use {@link #expandSelectionToRight()} instead
     */
    @Deprecated
    protected void pressExpandSelectionAndMoveRight() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isLeftTriggerPressed()).thenReturn(true);
            when(action.isMoveSelectionToRight()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    /**
     * @deprecated use {@link #expandSelectionToLeft()} instead
     */
    @Deprecated
    protected void pressExpandSelectionAndMoveLeft() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isLeftTriggerPressed()).thenReturn(true);
            when(action.isMoveSelectionToLeft()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }
}
