package de.schrieveslaach.nlpf.qpt.annotation;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.controller.AnnotationController;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.service.ProjectService;
import de.schrieveslaach.nlpf.qpt.view.AnnotationView;
import de.schrieveslaach.nlpf.qpt.view.control.LabelSpinner;
import de.schrieveslaach.nlpf.qpt.view.control.NamedEntityStream;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.collections.ObservableList;
import lombok.SneakyThrows;
import org.apache.uima.UIMAException;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Objects;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static javafx.collections.FXCollections.observableArrayList;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {
        AnnotationView.class,
        NamedEntityTaggingBehavior.class,
        NamedEntityTaggingBehaviorTest.Config.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class NamedEntityTaggingBehaviorTest extends AnnotationBehaviorTest<NamedEntityTaggingBehavior> {

    private static final String TOKEN_TAGS_REGION_QUERY = "#ner-tag-stream";

    @Test
    public void shouldLoadDocumentWithoutLanguageProperty() {
        loadJCasAndSelectFirstToken();

        interact(() -> {
            NamedEntityStream stream = find(TOKEN_TAGS_REGION_QUERY);
            assertThat(stream, is(not(nullValue())));
        });
    }

    @Test
    public void shouldShowAssignedNamedEntityTag() {
        loadJCasAndSelectFirstToken();

        // TODO workaround
        moveSelectionToRight();
        moveSelectionToLeft();

        pressAcceptButton();

        LabelSpinner<String> spinner = find("#spinner-13");
        assertThat(spinner.valueProperty().get(), is(equalTo("location")));
    }

    @Test
    public void shouldAssignNamedEntityTag() {
        loadJCasAndSelectFirstToken();

        // TODO workaround
        moveSelectionToRight();
        moveSelectionToLeft();
        moveSelectionDown();
        moveSelectionUp();

        pressAcceptButton();

        Collection<NamedEntity> namedEntities = jCasService.select(NamedEntity.class);
        assertThat(namedEntities, hasSize(1));
        assertThat(namedEntities, contains(
                namedEntity("location")
        ));
    }

    @Test
    public void shouldRemoveAssignedNamedEntityTag() {
        loadJCasAndSelectFirstToken();

        moveSelectionToRight();
        moveSelectionToLeft();

        pressAcceptButton();
        pressRemoveLeftButton();

        Collection<NamedEntity> namedEntities = jCasService.select(NamedEntity.class);
        assertThat(namedEntities, hasSize(0));
        //TODO: Check the spinner size for the first token of the namedEntity
    }

    @Test
    public void shouldRemoveReassignedNamedEntityTag() {
        loadJCasAndSelectFirstToken();

        //TODO: Workaround
        moveSelectionToRight();
        moveSelectionToLeft();
        moveSelectionDown();
        moveSelectionUp();

        pressAcceptButton();
        moveSelectionDown();
        pressAcceptButton();

        Collection<NamedEntity> namedEntities = jCasService.select(NamedEntity.class);
        assertThat(namedEntities, hasSize(1));
        assertThat(namedEntities, contains(
                namedEntity("person")
        ));
    }

    @Test
    public void shouldShowAssignedNamedEntityTag_AfterChangingSelection() {
        loadJCasAndSelectFirstToken();

        moveSelectionToRight();
        moveSelectionToLeft();

        moveSelectionDown();
        pressAcceptButton();

        LabelSpinner<String> spinner = find("#spinner-13");
        assertThat(spinner.valueProperty().get(), is(equalTo("person")));
        assertThat(spinner.collapseText().get(), is(equalTo("person")));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNamedEntity_NewSelection_Following_ContainsNoNamedEntity() {
        loadJCasWithFirstTokenNamedEntityAndSelectFirstToken();

        moveSelectionToRight();

        assertThatSelectionIsEqualTo(getTokenByText("Pierre"));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNamedEntity_NewSelection_Preceding_ContainsNoNamedEntity() {
        loadJCasWithLastTokensNamedEntityAndSelectLastTokens();

        moveSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("morning"));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNamedEntity_NewSelection_Following_ContainsNamedEntity() {
        loadJCasWithFirstTokenNamedEntityAndLastTokensNamedEntityAndSelectFirstToken();

        moveSelectionToRight();

        assertThatSelectionIsEqualTo(getTokenByText("Pierre"), getTokenByText("Vinken"));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNamedEntity_NewSelection_Preceding_ContainsNamedEntity() {
        loadJCasWithFirstTokensAndLastTokensNamedEntityAndSelectLastNamedEntity();

        moveSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("Good"), getTokenByText("morning"));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNoNamedEntity_NewSelection_Following_ContainsNamedEntity() {
        loadJCasWithLastTokensNamedEntityAndSelectFirstToken();

        moveSelectionToRight();
        moveSelectionToRight();

        assertThatSelectionIsEqualTo(getTokenByText("Pierre"), getTokenByText("Vinken"));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNoNamedEntity_NewSelection_Preceding_ContainsNamedEntity() {
        loadJCasAndSelectLastNamedEntity();

        moveSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("morning"));
    }

    @Test
    public void shouldSuggestSelectionToLeft_SelectedNamedEntityTouchingEndOfDocument() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinken();

            jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(3)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);

        moveSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("Pierre"));
    }

    @Test
    public void shouldNotSuggestSelection_OldSelectionContainsNamedEntityWhichTouchesEndOfSentence_NoMoreSentences() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinken();

            NamedEntity ne1 = new NamedEntity(helloWorld, 13, 26);
            ne1.setValue("person");
            ne1.addToIndexes();

            jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(2), jCasService.tokensProperty().get(3)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);

        moveSelectionToRight();

        assertThatSelectionIsEqualTo(getTokenByText("Pierre"), getTokenByText("Vinken"));
    }


    @Test
    public void shouldSuggestSelection_OldSelectionContainsNamedEntityWhichTouchesEndOfSentence_NoMoreSentences() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinken();

            NamedEntity ne1 = new NamedEntity(helloWorld, 13, 26);
            ne1.setValue("person");
            ne1.addToIndexes();

            jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(2), jCasService.tokensProperty().get(3)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);

        moveSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("morning"));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNamedEntityWhichTouchesEndOfSentence_MoreSentences() {
        interact(() -> {
            JCas openNlpExample = createOpenNlpExample();

            // create a fake NE which touches the end of sentence
            NamedEntity ne1 = new NamedEntity(openNlpExample, 79, 87);
            ne1.setValue("fake");
            ne1.addToIndexes();

            jCasService.setJCas(openNlpExample);
            selectionProperty.set(new Selection(openNlpExample,
                    jCasService.tokensProperty().get(16),
                    jCasService.tokensProperty().get(17),
                    jCasService.tokensProperty().get(18)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);

        moveSelectionToRight();

        assertThatSelectionIsEqualTo(jCasService.tokensProperty().get(19), jCasService.tokensProperty().get(20));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNoNamedEntity_NewSelection_Following_ContainsNoNamedEntity() {
        loadJCasAndSelectFirstToken();

        moveSelectionToRight();

        assertThatSelectionIsEqualTo(getTokenByText("Pierre"));
    }

    @Test
    public void shouldSuggestSelection_OldSelectionContainsNoNamedEntity_NewSelection_Preceding_ContainsNoNamedEntity() {
        loadJCasAndSelectFirstToken();

        moveSelectionToRight();
        moveSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("Hello"));
    }

    @Test
    public void shouldExpandSelectionToRight_IfCurrentSelection_IsNamedEntity() {
        loadJCasWithFirstTokenNamedEntityAndSelectFirstToken();
        moveSelectionToRight();
        moveSelectionToLeft();

        expandSelectionToRight();

        assertThatSelectionIsEqualTo(getTokenByText("Good"), getTokenByText("morning"));
    }

    @Test
    public void shouldNotExpandSelectionToLeft_IfCurrentSelection_IsNamedEntity() {
        loadJCasWithLastTokensNamedEntityAndSelectLastTokens();
        moveSelectionToLeft();
        moveSelectionToRight();

        expandSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("Pierre"), getTokenByText("Vinken"));
    }

    @Test
    public void shouldExpandSelectionToRightAndAssignNamedEntity() {
        loadJCasWithTowSentencesAndSelectLastTokenOfFirstSentence();

        moveSelectionToRight();
        expandSelectionToRight();
        expandSelectionToRight();

        assertThatSelectionIsEqualTo(getTokenByText("How"), getTokenByText("are"), getTokenByText("you"));

        moveSelectionUp();
        pressAcceptButton();

        Collection<NamedEntity> namedEntities = jCasService.select(NamedEntity.class);
        assertThat(namedEntities, hasSize(1));
        assertThat(namedEntities.iterator().next(), is(namedEntity("organization", 28, 39)));
    }

    @Test
    public void shouldExpandSelectionToLeftAndAssignNamedEntity() {
        loadJCasWithTowSentencesAndSelectLastTokenOfFirstSentence();

        expandSelectionToLeft();
        expandSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("Pierre"), getTokenByText("Vinken"), getTokenByText("."));

        moveSelectionUp();
        pressAcceptButton();

        Collection<NamedEntity> namedEntities = jCasService.select(NamedEntity.class);
        assertThat(namedEntities, hasSize(1));
        assertThat(namedEntities, contains(
                namedEntity("organization", 13, 27)
        ));
    }

    @Test
    public void shouldNotExpandSelectionAcrossSentenceBoundaries_FromEndOfSentence() {
        loadJCasWithTowSentencesAndSelectLastTokenOfFirstSentence();

        expandSelectionToRight();

        assertThatSelectionIsEqualTo(getTokenByText("."));
    }

    @Test
    public void shouldNotExpandSelectionAcrossSentenceBoundaries_FromBeginningOfSentence() {
        loadJCasWithTowSentencesAndSelectLastTokenOfFirstSentence();

        moveSelectionToRight();
        expandSelectionToLeft();

        assertThatSelectionIsEqualTo(getTokenByText("How"));
    }

    private void loadJCasAndSelectLastNamedEntity() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinken();

            NamedEntity ne1 = new NamedEntity(helloWorld, 13, 26);
            ne1.setValue("location");
            ne1.addToIndexes();

            jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(2)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    @SneakyThrows(UIMAException.class)
    private JCas createGoodMorningPierreVinken() {
        JCas helloWorld = JCasFactory.createText("Good morning Pierre Vinken");

        Token t = new Token(helloWorld, 0, 4);
        t.addToIndexes();
        t = new Token(helloWorld, 5, 12);
        t.addToIndexes();
        t = new Token(helloWorld, 13, 19);
        t.addToIndexes();
        t = new Token(helloWorld, 20, 26);
        t.addToIndexes();

        Sentence s = new Sentence(helloWorld, 0, 26);
        s.addToIndexes();
        return helloWorld;
    }

    @SneakyThrows(UIMAException.class)
    private void loadJCasWithTowSentencesAndSelectLastTokenOfFirstSentence() {
        JCas jCas = JCasFactory.createText("Good morning Pierre Vinken. How are you?");

        Token t = new Token(jCas, 0, 4);
        t.addToIndexes();
        t = new Token(jCas, 5, 12);
        t.addToIndexes();
        t = new Token(jCas, 13, 19);
        t.addToIndexes();
        t = new Token(jCas, 20, 26);
        t.addToIndexes();
        t = new Token(jCas, 26, 27);
        t.addToIndexes();

        Sentence s = new Sentence(jCas, 0, 27);
        s.addToIndexes();

        t = new Token(jCas, 28, 31);
        t.addToIndexes();
        t = new Token(jCas, 32, 35);
        t.addToIndexes();
        t = new Token(jCas, 36, 39);
        t.addToIndexes();
        t = new Token(jCas, 39, 40);
        t.addToIndexes();

        s = new Sentence(jCas, 28, 48);
        s.addToIndexes();

        interact(() -> {
            this.jCasService.setJCas(jCas);
            selectionProperty.set(new Selection(jCas, jCasService.tokensProperty().get(4)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void loadJCasWithFirstTokensAndLastTokensNamedEntityAndSelectLastNamedEntity() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinkenWithLocationAndPerson();

            this.jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(2)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private JCas createGoodMorningPierreVinkenWithLocationAndPerson() {
        JCas helloWorld = createGoodMorningPierreVinken();

        NamedEntity ne1 = new NamedEntity(helloWorld, 0, 12);
        ne1.setValue("location");
        ne1.addToIndexes();

        NamedEntity ne2 = new NamedEntity(helloWorld, 13, 26);
        ne2.setValue("person");
        ne2.addToIndexes();

        return helloWorld;
    }

    private void loadJCasWithLastTokensNamedEntityAndSelectLastTokens() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinken();

            NamedEntity namedEntity = new NamedEntity(helloWorld, 13, 26);
            namedEntity.setValue("person");
            namedEntity.addToIndexes();

            this.jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(2)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void loadJCasWithFirstTokenNamedEntityAndLastTokensNamedEntityAndSelectFirstToken() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinkenWithLocationAndPerson();

            this.jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(0)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void loadJCasWithLastTokensNamedEntityAndSelectFirstToken() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinken();

            NamedEntity namedEntity = new NamedEntity(helloWorld, 13, 26);
            namedEntity.setValue("location");
            namedEntity.addToIndexes();

            this.jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(0)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void loadJCasWithFirstTokenNamedEntityAndSelectFirstToken() {
        interact(() -> {
            JCas helloWorld = createGoodMorningPierreVinken();

            NamedEntity namedEntity = new NamedEntity(helloWorld, 0, 12);
            namedEntity.setValue("location");
            namedEntity.addToIndexes();

            this.jCasService.setJCas(helloWorld);
            selectionProperty.set(new Selection(helloWorld, jCasService.tokensProperty().get(0)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    @SneakyThrows(UIMAException.class)
    private void loadJCasAndSelectFirstToken() {
        JCas jCas = JCasFactory.createText("Hello Pierre Vinken");

        Token t = new Token(jCas, 0, 5);
        t.addToIndexes();
        t = new Token(jCas, 6, 12);
        t.addToIndexes();
        t = new Token(jCas, 13, 19);
        t.addToIndexes();

        interact(() -> {
            this.jCasService.setJCas(jCas);
            selectionProperty.set(new Selection(jCas, jCasService.tokensProperty().get(0)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private Matcher<NamedEntity> namedEntity(String namedEntityTagValue) {
        return new TypeSafeMatcher<NamedEntity>() {
            @Override
            protected boolean matchesSafely(NamedEntity namedEntity) {
                return Objects.equals(namedEntity.getValue(), namedEntityTagValue);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Named entity tag ").appendValue(namedEntityTagValue);
            }

            @Override
            protected void describeMismatchSafely(NamedEntity namedEntity, Description mismatchDescription) {
                mismatchDescription.appendText("Named entity tag ");
                if (namedEntity.getValue() != null) {
                    mismatchDescription.appendValue(namedEntity.getValue());
                } else {
                    mismatchDescription.appendValue(namedEntity.getType().getShortName());
                }
            }
        };
    }

    public static Matcher<NamedEntity> namedEntity(String namedEntityTagValue, int begin, int end) {
        return new TypeSafeMatcher<NamedEntity>() {
            @Override
            protected boolean matchesSafely(NamedEntity namedEntity) {
                return Objects.equals(namedEntity.getValue(), namedEntityTagValue)
                        && namedEntity.getBegin() == begin
                        && namedEntity.getEnd() == end;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("Named entity tag ")
                        .appendValue(namedEntityTagValue)
                        .appendText(" from ")
                        .appendValue(begin)
                        .appendText(" to ")
                        .appendValue(end);
            }

            @Override
            protected void describeMismatchSafely(NamedEntity namedEntity, Description mismatchDescription) {
                mismatchDescription.appendText("Named entity tag ");
                if (namedEntity.getValue() != null) {
                    mismatchDescription.appendValue(namedEntity.getValue());
                } else {
                    mismatchDescription.appendText(" without value ");
                }
                mismatchDescription.appendText(" from ")
                        .appendValue(namedEntity.getBegin())
                        .appendText(" to ")
                        .appendValue(namedEntity.getEnd());
            }
        };
    }

    public static class Config {

        @Bean
        public AnnotationController annotationController() {
            return mock(AnnotationController.class);
        }

        @Bean
        public SimpleObjectProperty<Selection> selectionProperty() {
            return new SimpleObjectProperty<>();
        }

        @Bean
        public JCasService jCasService() {
            return new JCasService();
        }

        @Bean
        public ReadOnlyStringProperty selectedAnnotationBehaviorProperty() {
            return new SimpleStringProperty();
        }

        @Bean
        public ProjectService projectService() {
            ObservableList<String> namedEntities = observableArrayList("location", "person", "organization");

            ProjectService service = mock(ProjectService.class);
            when(service.namedEntitiesProperty()).thenReturn(namedEntities);
            return service;
        }
    }
}
