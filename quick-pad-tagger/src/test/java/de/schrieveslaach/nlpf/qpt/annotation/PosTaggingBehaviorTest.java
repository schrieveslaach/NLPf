package de.schrieveslaach.nlpf.qpt.annotation;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.controller.AnnotationController;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.view.AnnotationView;
import de.schrieveslaach.nlpf.qpt.view.control.LabelSpinner;
import de.schrieveslaach.nlpf.qpt.view.control.PosTagStream;
import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS_NOUN;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import lombok.SneakyThrows;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;
import java.util.Objects;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createQuickBrownFox;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.everyItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {
        AnnotationView.class,
        PosTaggingBehavior.class,
        PosTaggingBehaviorTest.Config.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PosTaggingBehaviorTest extends AnnotationBehaviorTest<PosTaggingBehavior> {

    private static final String TOKEN_TAGS_REGION_QUERY = "#pos-tag-stream";

    @After
    public void cleanTokenTagsRegion() {
        AnchorPane pane = find("#annotation");
        Node tokenTagsRegion = find(TOKEN_TAGS_REGION_QUERY);

        interact(() -> pane.getChildren().remove(tokenTagsRegion));
    }

    @Test
    public void shouldLoadDocumentWithoutLanguageProperty() {
        loadJCasAndSelectFirstToken();

        interact(() -> {
            PosTagStream stream = find(TOKEN_TAGS_REGION_QUERY);
            assertThat(stream.getTags(), is(not(empty())));
        });
    }

    @Test
    public void shouldShowAssignedPosTag() {
        loadJCasAndSelectFirstToken();

        pressAcceptButton();

        LabelSpinner<String> spinner = find("#spinner-13");
        assertThat(spinner.valueProperty().get(), is(equalTo("*")));
        assertThat(spinner.collapseText().get(), is(equalTo("*")));
    }

    @Test
    public void shouldRemoveReassignedPosTag() {
        loadJCasAndSelectFirstToken();

        pressAcceptButton();
        moveSelectionDown();
        pressAcceptButton();

        Collection<POS> pos = jCasService.select(POS.class);
        assertThat(pos, hasSize(1));
        assertThat(pos, contains(
                posTag("ADJ")
        ));
    }

    @Test
    public void shouldShowAssignedPosTag_AfterChangingSelection() {
        loadJCasAndSelectFirstToken();

        moveSelectionToRight();
        moveSelectionToLeft();

        moveSelectionDown();
        pressAcceptButton();

        LabelSpinner<String> spinner = find("#spinner-13");
        assertThat(spinner.valueProperty().get(), is(equalTo("ADJ")));
        assertThat(spinner.collapseText().get(), is(equalTo("ADJ")));
    }

    @Test
    public void shouldNotChangeAssignedPosTags_UserJustPressesAcceptButton() {
        JCas quickBrownFox = createQuickBrownFox();
        select(quickBrownFox, Token.class).forEach(t -> {
            POS p = new POS_NOUN(quickBrownFox, t.getBegin(), t.getEnd());
            p.setPosValue("NOUN");
            p.addToIndexes();
            t.setPos(p);
        });
        loadJCasAndSelectFirstToken(quickBrownFox);

        int numberOfToken = select(quickBrownFox, Token.class).size();
        for (int i = 0; i < numberOfToken; ++i) {
            pressAcceptButton();
            moveSelectionToRight();
        }

        assertThat(select(quickBrownFox, POS.class), everyItem(posTag("NOUN")));
    }

    @Test
    public void shouldAssignPosTag() {
        loadJCasAndSelectFirstToken();

        moveSelectionDown();
        moveSelectionDown();
        pressAcceptButton();

        assertThat(jCasService.select(POS.class), contains(
                posTag("ADP")
        ));
    }

    @Test
    public void shouldAssignPosTag_SelectNextGroup() {
        loadJCasAndSelectFirstToken();

        moveSelectionToNextPosTagGroup();
        moveSelectionToNextPosTagGroup();
        pressAcceptButton();

        assertThat(jCasService.select(POS.class), contains(
                posTag("CONJ")
        ));
    }

    @Test
    public void shouldAssignPosTag_SelectPreviousGroup() {
        loadJCasAndSelectFirstToken();

        moveSelectionUp();
        moveSelectionToPreviousPosTagGroup();
        moveSelectionToPreviousPosTagGroup();
        moveSelectionToPreviousPosTagGroup();
        moveSelectionToPreviousPosTagGroup();
        pressAcceptButton();

        assertThat(jCasService.select(POS.class), contains(
                posTag("NUM")
        ));
    }

    @Test
    public void shouldAssignPosTag_RandomWalkThrough() {
        loadJCasAndSelectFirstToken();

        moveSelectionToRight();
        moveSelectionDown();
        moveSelectionToRight();
        moveSelectionToLeft();

        pressAcceptButton();

        assertThat(jCasService.select(POS.class), contains(
                posTag("*")
        ));
    }

    @SneakyThrows
    private void loadJCasAndSelectFirstToken() {
        JCas helloWorld = JCasFactory.createText("Hello World");

        Token t = new Token(helloWorld, 0, 5);
        t.addToIndexes();
        t = new Token(helloWorld, 6, 11);
        t.addToIndexes();

        loadJCasAndSelectFirstToken(helloWorld);
    }

    private void loadJCasAndSelectFirstToken(JCas jCas) {
        interact(() -> {
            jCasService.setJCas(jCas);
            selectionProperty.set(new Selection(jCas, jCasService.tokensProperty().get(0)));
        });

        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private Matcher<POS> posTag(String posTagValue) {
        return new TypeSafeMatcher<POS>() {
            @Override
            protected boolean matchesSafely(POS pos) {
                return Objects.equals(pos.getPosValue(), posTagValue);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("POS tag ").appendValue(posTagValue);
            }

            @Override
            protected void describeMismatchSafely(POS pos, Description mismatchDescription) {
                mismatchDescription.appendText("POS tag ");
                if (pos.getPosValue() != null) {
                    mismatchDescription.appendValue(pos.getPosValue());
                } else {
                    mismatchDescription.appendValue(pos.getType().getShortName());
                }
            }
        };
    }

    public static class Config {
        @Bean
        public AnnotationController annotationController() {
            return mock(AnnotationController.class);
        }

        @Bean
        public SimpleObjectProperty<Selection> selectionProperty() {
            return new SimpleObjectProperty<>();
        }

        @Bean
        public JCasService jCasService() {
            return new JCasService();
        }

        @Bean
        public ReadOnlyStringProperty selectedAnnotationBehaviorProperty() {
            return new SimpleStringProperty();
        }

        @Bean
        public ReadOnlyStringProperty defaultLanguageProperty() {
            return new SimpleStringProperty("en");
        }

        @Bean
        public ReadOnlyStringProperty posTagSetProperty() {
            return new SimpleStringProperty("ud");
        }

    }

}
