package de.schrieveslaach.nlpf.qpt.annotation;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.controller.AnnotationController;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.view.AnnotationView;
import de.schrieveslaach.nlpf.qpt.view.control.SegmentationTag;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import org.apache.uima.jcas.JCas;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Collection;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.matcher.base.NodeMatchers.isVisible;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@ActiveProfiles("test")
@SpringBootTest(classes = {
        AnnotationView.class,
        SegmentationBehavior.class,
        SegmentationBehaviorTest.Config.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class SegmentationBehaviorTest extends AnnotationBehaviorTest<SegmentationBehavior> {

    private static final String TOKEN_TAGS_REGION_QUERY = "#segmentation-tag";

    @After
    public void cleanTokenTagsRegion() {
        AnchorPane pane = find("#annotation");
        Node tokenTagsRegion = find(TOKEN_TAGS_REGION_QUERY);

        interact(() -> pane.getChildren().remove(tokenTagsRegion));
    }

    @Test
    public void shouldShow_SegmentationTag_OnExpandSelection() {
        loadJCasAndSelectFirstToken();
        pressExpandSelectionButton();

        SegmentationTag segmentationTag = find(TOKEN_TAGS_REGION_QUERY);
        assertThat(segmentationTag, isVisible());
    }

    @Test
    public void shouldNotShow_SegmentationTag() {
        loadJCasAndSelectFirstToken();

        SegmentationTag segmentationTag = find(TOKEN_TAGS_REGION_QUERY);
        assertThat(segmentationTag, not(isVisible()));
    }

    @Test
    public void shouldChangeCursorIndex_OnMoveLeft() {
        loadJCasAndSelectFirstToken();
        pressExpandSelectionButton();
        SegmentationTag segmentationTag = find(TOKEN_TAGS_REGION_QUERY);

        int index = segmentationTag.getIndex();

        pressExpandSelectionAndMoveLeft();

        assertThat(segmentationTag.getIndex(), is(index - 1));
    }

    @Test
    public void shouldChangeCursorIndex_OnMoveRight() {
        loadJCasAndSelectFirstToken();
        pressExpandSelectionButton();

        SegmentationTag segmentationTag = find(TOKEN_TAGS_REGION_QUERY);
        int index = segmentationTag.getIndex();

        pressExpandSelectionAndMoveLeft();

        assertThat(segmentationTag.getIndex(), is(index - 1));

        pressExpandSelectionAndMoveRight();

        assertThat(segmentationTag.getIndex(), is(index));
    }

    @Test
    public void shouldNotChangeCursorIndex_AtBoundsRight() {
        loadJCasAndSelectFirstToken();
        pressExpandSelectionButton();
        SegmentationTag segmentationTag = find(TOKEN_TAGS_REGION_QUERY);

        int index = segmentationTag.getIndex();

        pressExpandSelectionAndMoveRight();

        assertThat(segmentationTag.getIndex(), is(index));
    }


    @Test
    public void shouldNotChangeCursorIndex_AtBoundsLeft() {
        loadJCasAndSelectFirstToken();
        pressExpandSelectionButton();
        SegmentationTag segmentationTag = find(TOKEN_TAGS_REGION_QUERY);

        int index = segmentationTag.getIndex();

        for (int i = 0; i <= index; i++) {
            pressExpandSelectionAndMoveLeft();
        }

        assertThat(segmentationTag.getIndex(), is(0));
    }

    @Test
    public void shouldAddNewToken_OnSplitToken() {
        loadJCasAndSelectFirstToken();
        pressExpandSelectionButton();

        pressExpandSelectionAndAccept();

        moveSelectionToLeft();

        Token pierr = getTokenByText("Pierr");
        Token e = getTokenByText("e");

        assertThat(pierr, is(notNullValue()));
        assertThat(e, is(notNullValue()));
        assertThat(selectionProperty.get().getSelectedTokens().get(0), is(pierr));
    }

    @Test
    public void shouldNotRemoveTokenSplit_WithoutAnySplit() {
        loadJCasAndSelectFirstToken();

        moveSelectionToRight();
        removeTokenSplit();

        Token pierre = getTokenByText("Pierre");
        assertThat(pierre, is(notNullValue()));
        Token vinken = getTokenByText("Vinken");
        assertThat(vinken, is(notNullValue()));
    }

    @Test
    public void shouldNotRemoveTokenSplit_WithoutAnySplit_NoPreceedingToken() {
        loadJCasAndSelectFirstToken();

        removeTokenSplit();

        Token pierre = getTokenByText("Pierre");
        assertThat(pierre, is(notNullValue()));
    }

    @Test
    public void shouldRemoveTokenSplit_AfterSplitToken() {
        loadJCasAndSelectFirstToken();
        pressExpandSelectionButton();
        pressExpandSelectionAndAccept();

        removeTokenSplit();

        Token pierr = getTokenByText("Pierr");
        Token e = getTokenByText("e");
        assertThat(pierr, is(nullValue()));
        assertThat(e, is(nullValue()));

        Token pierre = getTokenByText("Pierre");
        assertThat(pierre, is(notNullValue()));
        assertThat(selectionProperty.get().getSelectedTokens().get(0), is(equalTo(pierre)));
    }

    @Test
    public void shouldRemoveToken_OnSplitToken() {
        loadJCasAndSelectFirstToken();
        pressExpandSelectionButton();

        pressExpandSelectionAndAccept();

        Token pierre = getTokenByText("Pierre");
        assertThat(pierre, is(nullValue()));
    }

    @Test
    public void shouldNotRemoveSentenceSplit_DidNotSelectLastTokenInTheSentence() {
        loadJCasAndSelectFirstToken();

        Token middleOfSentence = jCasService.selectAt(Token.class, 15, 17).iterator().next();
        interact(() ->
                selectionProperty.set(new Selection(getJCas(), middleOfSentence))
        );

        removeSentenceSplit();

        Collection<Sentence> sentences = jCasService.select(Sentence.class);
        assertThat(sentences, hasSize(4));
    }

    @Test
    public void shouldNotRemoveSentenceSplit_NoFollowingSentence() {
        loadJCasAndSelectFirstToken();

        // Token at [170,171) is the last end of sentence in the whole document
        Token endOfSentence = jCasService.selectAt(Token.class, 170, 171).iterator().next();
        interact(() ->
                selectionProperty.set(new Selection(getJCas(), endOfSentence))
        );

        removeSentenceSplit();

        Collection<Sentence> sentences = jCasService.select(Sentence.class);
        assertThat(sentences, hasSize(4));
    }

    @Test
    public void shouldRemoveSentenceSplit() {
        loadJCasAndSelectFirstToken();

        // Token at [86,87) is the first end of sentence
        Token endOfSentence = jCasService.selectAt(Token.class, 86, 87).iterator().next();
        interact(() ->
                selectionProperty.set(new Selection(getJCas(), endOfSentence))
        );

        removeSentenceSplit();

        Collection<Sentence> sentences = jCasService.select(Sentence.class);
        assertThat(sentences, hasSize(3));

        Selection selection = selectionProperty.get();
        assertThat(selection.getSelectedTokens().get(0), is(equalTo(endOfSentence)));
        assertThat(selection.getSelectedSentence().getBegin(), is(equalTo(0)));
        assertThat(selection.getSelectedSentence().getEnd(), is(equalTo(156)));
    }

    private void removeSentenceSplit() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isRemoveRightAnnotationPressed()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void removeTokenSplit() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isRemoveLeftAnnotationPressed()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void pressExpandSelectionAndAccept() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isLeftTriggerPressed()).thenReturn(true);
            when(action.isAcceptButtonPressed()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void pressExpandSelectionButton() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isLeftTriggerPressed()).thenReturn(true);
            behavior.consume(action);
        });
        waitForFxEvents(ATTEMPTS_COUNTS);
    }

    private void loadJCasAndSelectFirstToken() {
        interact(() -> {
            JCas openNlpExample = createOpenNlpExample();

            Collection<Token> tokens = select(openNlpExample, Token.class);

            selectionProperty.set(new Selection(openNlpExample, tokens.iterator().next()));

            jCasService.setJCas(openNlpExample);
        });
    }

    public static class Config {

        @Bean
        public JCasService jCasService() {
            return new JCasService();
        }

        @Bean
        public AnnotationController annotationController() {
            return mock(AnnotationController.class);
        }

        @Bean
        public SimpleObjectProperty<Selection> selectionProperty() {
            return new SimpleObjectProperty<>();
        }

        @Bean
        public ReadOnlyStringProperty selectedAnnotationBehaviorProperty() {
            return new SimpleStringProperty();
        }
    }
}
