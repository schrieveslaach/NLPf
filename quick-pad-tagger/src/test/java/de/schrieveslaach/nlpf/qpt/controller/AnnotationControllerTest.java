package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.annotation.AnnotationBehavior;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.service.JCasService;
import de.schrieveslaach.nlpf.qpt.view.AnnotationView;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyStringProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.Objects;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@ActiveProfiles("test")
@SpringBootTest(classes = {
        AnnotationController.class,
        AnnotationControllerTest.AnnotationControllerConfig.class,
        AnnotationView.class
})
public class AnnotationControllerTest extends GuiTest {

    @Autowired
    private AnnotationController annotationController;

    @Autowired
    private JCasService jCasService;

    @Autowired
    private StringProperty selectedAnnotationBehaviorProperty;

    @Autowired
    private AnnotationBehavior mockedAnnotationBehavior;

    @PostConstruct
    public void constructView() throws Exception {
        init(AnnotationView.class);
    }

    @Before
    public void loadJCasAndSelectMockedBehavior() {
        interact(() -> {
            JCas jcas = createOpenNlpExample();
            jCasService.setJCas(jcas);
            selectedAnnotationBehaviorProperty.setValue("MockedBehavior");
        });

        waitForFxEvents(20);
    }

    @Test
    public void shouldSwitchToRightToken() {
        moveSelectionToRight();

        assertThat(annotationController, hasSelected(getTokenByText("Vinken")));
    }

    @Test
    public void shouldSwitchToLeftToken() {
        moveSelectionToRight();
        moveSelectionToLeft();

        assertThat(annotationController, hasSelected(getTokenByText("Pierre")));
    }

    @Test
    public void shouldForwardSelection_BehaviourConsumedAction_AfterPressingAccept() {
        when(mockedAnnotationBehavior.forwardSelectionAfterActionAccepted()).thenReturn(true);
        when(mockedAnnotationBehavior.consume(any(Action.class))).thenReturn(true);

        pressAcceptButton();

        assertThat(annotationController, hasSelected(getTokenByText("Vinken")));
    }

    @Test
    public void shouldChangeSelection_AnnotationBehaviourSuggestSelection() {
        when(mockedAnnotationBehavior.suggestNewSelection(any(Selection.class), any(Selection.class))).thenReturn(
                new Selection(jCasService.jCasProperty().get(), getTokenByText(","), getTokenByText("61"))
        );

        moveSelectionToRight();

        assertThat(annotationController, hasSelected(getTokenByText(","), getTokenByText("61")));
    }

    @Test
    public void shouldNotMoveSelectionToRight_AnnotationBehaviourRefusesSelection() {
        when(mockedAnnotationBehavior.suggestNewSelection(any(Selection.class), any(Selection.class))).thenReturn(null);

        moveSelectionToRight();

        assertThat(annotationController, hasSelected(getTokenByText("Pierre")));
    }

    @Test
    public void shouldNotExpandSelectionToRight_AnnotationBehaviourRefusesSelection() {
        when(mockedAnnotationBehavior.suggestNewSelection(any(Selection.class), any(Selection.class))).thenReturn(null);

        expandSelectionToRight();

        assertThat(annotationController, hasSelected(getTokenByText("Pierre")));
    }

    @Test
    public void shouldNotExpandSelectionToRight_AnnotationDoesNotSupportMultiSelection() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(false);

        expandSelectionToRight();

        assertThat(annotationController, hasSelected(getTokenByText("Vinken")));
    }

    @Test
    public void shouldNotExpandSelectionToRight_SelectionAtTheEndOfASentence() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(true);
        when(mockedAnnotationBehavior.suggestNewSelection(any(), any())).thenCallRealMethod();

        // Moves selection to the end of the sentence “Pierre Vinken, 61 years…”
        for (int i = 0; i < 18; ++i) {
            moveSelectionToRight();
        }

        expandSelectionToRight();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("."));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldNotExpandSelectionToRight_SelectionAtTheBeginningOfASentence() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(true);
        when(mockedAnnotationBehavior.suggestNewSelection(any(), any())).thenCallRealMethod();

        // Moves selection to the beginning of the sentence “Mr. Vinken is chairman…”
        for (int i = 0; i < 19; ++i) {
            moveSelectionToRight();
        }

        expandSelectionToLeft();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("Mr."));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldExpandSelectionToRight_AnnotationBehaviourAcceptsSelection() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(true);

        expandSelectionToRight();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("Pierre"), getTokenByText("Vinken"));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldExpandSelectionToRightTwice_AnnotationBehaviourAcceptsSelection() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(true);

        expandSelectionToRight();
        expandSelectionToRight();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("Pierre"), getTokenByText("Vinken"), getTokenByText(","));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldExpandSelectionToLeft_AnnotationBehaviourAcceptsSelection() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(true);

        moveSelectionToRight();
        expandSelectionToLeft();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("Pierre"), getTokenByText("Vinken"));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldExpandSelectionToLeftTwice_AnnotationBehaviourAcceptsSelection() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(true);

        moveSelectionToRight();
        moveSelectionToRight();
        expandSelectionToLeft();
        expandSelectionToLeft();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("Pierre"), getTokenByText("Vinken"), getTokenByText(","));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldNotExpandSelectionToLeft_AnnotationBehaviourDoesNotSupportMultiSelection() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(false);

        moveSelectionToRight();
        expandSelectionToLeft();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("Pierre"));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldReduceSelectionToLeft_AfterExpandedToRight() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(true);

        expandSelectionToRight();
        expandSelectionToRight();
        expandSelectionToLeft();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("Pierre"), getTokenByText("Vinken"));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldReduceSelectionToRight_AfterExpandedToLeft() {
        when(mockedAnnotationBehavior.supportsMultiTokenSelection()).thenReturn(true);

        moveSelectionToRight();
        moveSelectionToRight();
        expandSelectionToLeft();
        expandSelectionToLeft();
        expandSelectionToRight();

        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), getTokenByText("Vinken"), getTokenByText(","));
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldMoveSelectionToBegin() {
        for (int i = 0; i < 10; ++i) {
            moveSelectionToRight();
        }

        moveSelectionToBegin();

        Token firstToken = select(jCasService.jCasProperty().get(), Token.class).stream().findFirst().get();
        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), firstToken);
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    @Test
    public void shouldMoveSelectionToEnd() {
        moveSelectionToEnd();

        Token lastToken = select(jCasService.jCasProperty().get(), Token.class).stream().reduce((first, second) -> second).get();
        Selection suggestedSelection = new Selection(jCasService.jCasProperty().get(), lastToken);
        assertThat(annotationController, hasSelected(suggestedSelection));
    }

    private void moveSelectionToRight() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionToRight()).thenReturn(true);
            annotationController.consume(action);
        });

        waitForFxEvents(20);
    }

    private void moveSelectionToLeft() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionToLeft()).thenReturn(true);
            annotationController.consume(action);
        });

        waitForFxEvents(20);
    }

    private void expandSelectionToRight() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionToRight()).thenReturn(true);
            when(action.isLeftTriggerPressed()).thenReturn(true);
            annotationController.consume(action);
        });

        waitForFxEvents(20);
    }

    private void expandSelectionToLeft() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionToLeft()).thenReturn(true);
            when(action.isLeftTriggerPressed()).thenReturn(true);
            annotationController.consume(action);
        });

        waitForFxEvents(20);
    }

    private void moveSelectionDown() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionDown()).thenReturn(true);
            annotationController.consume(action);
        });
        waitForFxEvents(20);
    }

    private void moveSelectionToBegin() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionToBegin()).thenReturn(true);
            annotationController.consume(action);
        });

        waitForFxEvents(20);
    }

    private void moveSelectionToEnd() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionToEnd()).thenReturn(true);
            annotationController.consume(action);
        });

        waitForFxEvents(20);
    }

    private void pressAcceptButton() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isAcceptButtonPressed()).thenReturn(true);
            annotationController.consume(action);
        });
        waitForFxEvents(20);
    }

    private Token getTokenByText(String label) {
        return jCasService.select(Token.class).stream()
                .filter(token -> token.getCoveredText().equals(label))
                .findFirst()
                .get();
    }

    public Matcher<AnnotationController> hasSelected(final Token... tokens) {
        return hasSelected(new Selection(jCasService.jCasProperty().get(), tokens));
    }

    public static Matcher<AnnotationController> hasSelected(final Selection selection) {
        return new TypeSafeMatcher<AnnotationController>() {
            @Override
            protected boolean matchesSafely(AnnotationController annotationController) {
                return Objects.equals(annotationController.selectionProperty().get(), selection);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has selected ").appendValue(selection);
            }

            @Override
            protected void describeMismatchSafely(AnnotationController annotationController, Description mismatchDescription) {
                mismatchDescription.appendText("has selected ").appendValue(annotationController.selectionProperty().get());
            }
        };
    }

    public static class AnnotationControllerConfig {

        @Bean
        @Qualifier("behavior-consumer")
        public AnnotationBehavior mockedAnnotationBehavior() {
            AnnotationBehavior annotationBehavior = mock(AnnotationBehavior.class);
            when(annotationBehavior.getName()).thenReturn("MockedBehavior");
            when(annotationBehavior.suggestNewSelection(any(Selection.class), any(Selection.class)))
                    .then(invocation -> invocation.<Selection>getArgument(1));
            return annotationBehavior;
        }

        @Bean
        public StringProperty selectedAnnotationBehaviorProperty() {
            return new SimpleStringProperty();
        }

        @Bean
        public JCasService jCasService() {
            return new JCasService();
        }

        @Bean
        public ReadOnlyStringProperty posTagSetProperty() {
            // Universal pos tags
            return new SimpleStringProperty("ud");
        }

        @Bean
        public ReadOnlyStringProperty defaultLanguageProperty() {
            return new SimpleStringProperty("en");
        }

    }
}
