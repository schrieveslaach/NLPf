package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.view.DocumentView;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.web.WebView;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit4.SpringRunner;
import org.xmlunit.builder.Input;

import javax.annotation.PostConstruct;

import static javafx.collections.FXCollections.observableArrayList;
import static org.junit.Assert.assertThat;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        DocumentController.class,
        DocumentView.class,
        DocumentControllerTest.Config.class
})
public class DocumentControllerTest extends GuiTest {

    @Autowired
    private DocumentController documentController;

    @Autowired
    private ObjectProperty<JCas> cas;

    @Autowired
    private ObservableList<Token> tokens;

    @PostConstruct
    public void constructView() throws Exception {
        init(DocumentView.class);
    }

    @Test
    public void shouldUpdateNaturalLanguageTextView() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World");
        Sentence sentence = new Sentence(jCas, 0, 11);
        sentence.addToIndexes();

        cas.setValue(jCas);
        waitForFxEvents(10);

        interact(() -> {
            WebView webView = find("#natural-language-browser");
            String html = (String) webView.getEngine().executeScript("document.documentElement.outerHTML");

            assertThat(
                    Input.fromString(html),
                    isSimilarTo(Input.fromString("<html><head></head><body><p><span class=\"sentence\" id=\"13\">Hello World</span></p></body></html>"))
                            .ignoreWhitespace()
                            .withNodeFilter(node -> !"script".equals(node.getNodeName()))
            );
        });
    }

    @Test
    public void shouldUpdateNaturalLanguageTextView_TokensChanged() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World");
        Sentence sentence = new Sentence(jCas, 0, 11);
        sentence.addToIndexes();

        cas.setValue(jCas);
        waitForFxEvents(10);

        Token hello = new Token(jCas, 0, 5);
        hello.addToIndexes();

        Token world = new Token(jCas, 6, 11);
        world.addToIndexes();

        tokens.addAll(hello, world);
        waitForFxEvents(10);

        interact(() -> {
            WebView webView = find("#natural-language-browser");
            String html = (String) webView.getEngine().executeScript("document.documentElement.outerHTML");

            assertThat(
                    Input.fromString(html),
                    isSimilarTo("<html><head></head><body><p><span class=\"sentence\" id=\"13\"><span id=\"18\" class=\"token\">Hello</span> <span id=\"30\" class=\"token\">World</span></span></p></body></html>")
                            .ignoreWhitespace()
                            .withNodeFilter(node -> !"script".equals(node.getNodeName()))
            );
        });
    }

    public static class Config {

        @Bean
        public ObjectProperty<JCas> casObjectProperty() {
            return new SimpleObjectProperty<>();
        }

        @Bean
        public ObjectProperty<Selection> selectionObjectProperty() {
            return new SimpleObjectProperty<>();
        }

        @Bean
        public ObservableList<Token> tokens() {
            return observableArrayList();
        }

        @Bean
        public ObservableList<Sentence> sentences() {
            return observableArrayList();
        }
    }

}
