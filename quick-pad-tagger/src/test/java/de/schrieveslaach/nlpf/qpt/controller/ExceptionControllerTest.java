package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.view.ExceptionView;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.scene.control.TextArea;
import javafx.scene.image.WritableImage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        ExceptionController.class,
        ExceptionView.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class ExceptionControllerTest extends GuiTest {

    @Autowired
    private ExceptionController exceptionController;

    @PostConstruct
    public void initView() throws Exception {
        init(ExceptionView.class);

        SnapshotProvider snapshotProvider = mock(SnapshotProvider.class);
        when(snapshotProvider.takeSnapshot()).thenReturn(new WritableImage(10, 10));
        exceptionController.setSnapshotProvider(snapshotProvider);
    }

    @Test
    public void shouldShowExceptionOverlay_TaskRaisedException() {
        Task<?> task = mockTask();
        exceptionController.consume(task);

        updateException(task, new Exception("Hello Exception"));

        assertThat(find("#exception").getOpacity(), is(equalTo(1.0)));
    }

    @Test
    public void shouldCloseExceptionOverlay_TaskRaisedException_PressedAcceptButtom() {
        Task<?> task = mockTask();
        exceptionController.consume(task);

        updateException(task, new Exception("Hello Exception"));
        pressAcceptButton();

        assertThat(find("#exception").getOpacity(), is(equalTo(0.0)));
    }

    private void pressAcceptButton() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isAcceptButtonPressed()).thenReturn(true);
            exceptionController.consume(action);
        });
        waitForFxEvents(100);
    }

    @Test
    public void shouldShowExceptionMessage_TaskRaisedException() {
        Task<?> task = mockTask();
        exceptionController.consume(task);

        updateException(task, new Exception("Hello Exception"));

        TextArea textArea = find("#messages");
        assertThat(textArea.getText(), is(equalTo("Hello Exception")));
    }

    @Test
    public void shouldNotConsumeAllActions_TaskDidNotRaisedException() {
        Task<?> task = mockTask();
        exceptionController.consume(task);

        boolean consumed = exceptionController.consume(mock(Action.class));
        assertThat(consumed, is(equalTo(false)));
    }

    @Test
    public void shouldConsumeAllActions_TaskRaisedException() {
        Task<?> task = mockTask();
        exceptionController.consume(task);

        updateException(task, new Exception("Hello Exception"));

        boolean consumed = exceptionController.consume(mock(Action.class));
        assertThat(consumed, is(equalTo(true)));
    }

    private Task<?> mockTask() {
        Task t = mock(Task.class);
        when(t.exceptionProperty()).thenReturn(new SimpleObjectProperty());
        return t;
    }

    private void updateException(Task<?> task, Exception ex) {
        interact(() -> ((SimpleObjectProperty) task.exceptionProperty()).set(ex));
        waitForFxEvents(100);
    }
}
