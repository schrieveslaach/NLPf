package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.annotation.AnnotationBehavior;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Document;
import de.schrieveslaach.nlpf.qpt.service.ProjectService;
import de.schrieveslaach.nlpf.qpt.view.MenuView;
import de.schrieveslaach.nlpf.qpt.view.control.TokenLabel;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import javafx.scene.control.Hyperlink;
import javafx.scene.control.Label;
import javafx.scene.image.WritableImage;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.testfx.service.query.EmptyNodeQueryException;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createHelloWorld;
import static java.util.Arrays.asList;
import static javafx.collections.FXCollections.observableArrayList;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = {
        MenuController.class,
        MenuControllerTest.Config.class,
        MenuView.class
})
public class MenuControllerTest extends GuiTest {

    private static final int ATTEMPTS_COUNT = 50;

    @Autowired
    private MenuController menuController;

    @Autowired
    private ProjectService projectService;

    @PostConstruct
    public void constructView() throws Exception {
        init(MenuView.class);

        SnapshotProvider snapshotProvider = mock(SnapshotProvider.class);
        when(snapshotProvider.takeSnapshot()).thenReturn(new WritableImage(10, 10));
        menuController.setSnapshotProvider(snapshotProvider);
    }

    @Test
    public void shouldOpenMenu_WithMenuButton() {
        // Open menu
        pressMenuButton();

        assertThat(find("#menu").getOpacity(), is(equalTo(1.0)));
    }

    @Test
    public void shouldCloseMenu_WithMenuButton() {
        // Open and then close menu
        pressMenuButton();
        pressMenuButton();

        assertThat(find("#menu").getOpacity(), is(equalTo(0.0)));
    }

    @Test
    public void shouldSelectTextSegmentationBehavior_OnMoveSelectionDown() {
        pressMenuButton();
        moveSelectionDown();

        pressAcceptButton();

        assertThat(menuController.selectedAnnotationBehaviorProperty().get(), is("Segmentation"));
    }

    @Test
    public void shouldSelectTextSegmentationBehavior_AfterScrollingThroughBehaviors_ThenOpeningProject_ThenSelectBehavior() {
        pressMenuButton();
        for (int i = 0; i < 5; ++i) {
            moveSelectionDown();
        }
        for (int i = 0; i < 5; ++i) {
            moveSelectionUp();
        }

        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        Document document2 = mock(Document.class);
        when(document2.getName()).thenReturn("doc2");
        openDocuments(document1, document2);

        moveSelectionToRight();
        moveSelectionToLeft();
        moveSelectionDown();
        pressAcceptButton();

        assertThat(menuController.selectedAnnotationBehaviorProperty().get(), is("Segmentation"));
    }

    @Test
    public void shouldHighlightSelectedAnnotationBehaviour_TextSegmentation() {
        pressMenuButton();
        moveSelectionDown();

        pressAcceptButton();

        TokenLabel lb = find("#Segmentation");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(true)));

        lb = find("#PosTagging");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(false)));

        lb = find("#NamedEntityTagging");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(false)));
    }

    @Test
    public void shouldSelectPosTaggingBehavior_OnMoveSelectionDown() {
        pressMenuButton();
        moveSelectionDown();
        moveSelectionDown();

        pressAcceptButton();

        assertThat(menuController.selectedAnnotationBehaviorProperty().get(), is("PosTagging"));
    }

    @Test
    public void shouldHighlightSelectedAnnotationBehaviour_PosTagging() {
        pressMenuButton();
        moveSelectionDown();
        moveSelectionDown();

        pressAcceptButton();

        TokenLabel lb = find("#Segmentation");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(false)));
        lb = find("#PosTagging");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(true)));
        lb = find("#NamedEntityTagging");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(false)));
    }

    @Test
    public void shouldHighlightSelectedAnnotationBehaviour_NamedEntityTagging() {
        pressMenuButton();
        moveSelectionDown();
        moveSelectionDown();
        moveSelectionDown();

        pressAcceptButton();

        TokenLabel lb = find("#Segmentation");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(false)));
        lb = find("#PosTagging");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(false)));
        lb = find("#NamedEntityTagging");
        assertThat(lb.showSelectedIndicatorProperty().get(), is(equalTo(true)));
    }


    @Test
    public void shouldSwitchFromDocumentsToAnnotationBehaviors_Segmentation_LongListOfDocuments() {
        List<Document> documents = new ArrayList<>();
        for (int i = 0; i < 20; ++i) {
            Document doc = mock(Document.class);
            when(doc.getName()).thenReturn("doc" + i);
            documents.add(doc);
        }
        openDocuments(documents.toArray(new Document[documents.size()]));

        pressMenuButton();
        moveSelectionToRight();
        for (int i = 0; i < 10; ++i) {
            moveSelectionDown();
        }
        moveSelectionToLeft();
        moveSelectionUp();
        moveSelectionUp();
        pressAcceptButton();

        assertThat(menuController.selectedAnnotationBehaviorProperty().get(), is("Segmentation"));
    }

    @Test
    public void shouldSwitchFromDocumentsToAnnotationBehaviors_PosTagging_LongListOfDocuments() {
        List<Document> documents = new ArrayList<>();
        for (int i = 0; i < 20; ++i) {
            Document doc = mock(Document.class);
            when(doc.getName()).thenReturn("doc" + i);
            documents.add(doc);
        }
        openDocuments(documents.toArray(new Document[documents.size()]));

        pressMenuButton();
        moveSelectionToRight();
        for (int i = 0; i < 10; ++i) {
            moveSelectionDown();
        }
        moveSelectionToLeft();
        moveSelectionUp();
        pressAcceptButton();

        assertThat(menuController.selectedAnnotationBehaviorProperty().get(), is("PosTagging"));
    }

    @Test
    public void shouldSwitchFromDocumentsToAnnotationBehaviors_NamedEntityTagging_LongListOfDocuments() {
        List<Document> documents = new ArrayList<>();
        for (int i = 0; i < 20; ++i) {
            Document doc = mock(Document.class);
            when(doc.getName()).thenReturn("doc" + i);
            documents.add(doc);
        }
        openDocuments(documents.toArray(new Document[documents.size()]));

        pressMenuButton();
        moveSelectionToRight();
        for (int i = 0; i < 10; ++i) {
            moveSelectionDown();
        }
        moveSelectionToLeft();
        pressAcceptButton();

        assertThat(menuController.selectedAnnotationBehaviorProperty().get(), is("NamedEntityTagging"));
    }

    @Test
    public void shouldSwitchFromDocumentsToAnnotationBehaviorsAndBackAgain_LongListOfDocuments() {
        List<Document> documents = new ArrayList<>();
        for (int i = 0; i < 20; ++i) {
            Document doc = mock(Document.class);
            when(doc.getName()).thenReturn("doc" + i);
            documents.add(doc);
        }
        openDocuments(documents.toArray(new Document[documents.size()]));

        Task task = mock(Task.class);
        when(task.valueProperty()).thenReturn(new SimpleObjectProperty());
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        for (int i = 0; i < 10; ++i) {
            moveSelectionDown();
        }
        moveSelectionToLeft();
        moveSelectionToRight();
        pressAcceptButton();

        pressMenuButton();
        moveSelectionToRight();
        moveSelectionDown();
        pressAcceptButton();

        verify(projectService).loadJCas(documents.get(10));
    }

    @Test
    public void shouldShowDocuments_OpenProject_InvokeDocumentListChange() {
        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        Document document2 = mock(Document.class);
        when(document2.getName()).thenReturn("doc2");
        openDocuments(document1, document2);

        assertThat(find("#doc_" + "doc1".hashCode()), is(notNullValue()));
        assertThat(find("#doc_" + "doc2".hashCode()), is(notNullValue()));
    }

    @Test
    public void shouldShowDocuments_OpenProject_InvokeDocumentListChangeAgain() {
        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        Document document2 = mock(Document.class);
        when(document2.getName()).thenReturn("doc2");
        openDocuments(document1, document2);

        Document document3 = mock(Document.class);
        when(document3.getName()).thenReturn("doc3");
        openDocuments(document3);

        try {
            find("#doc_" + "doc1".hashCode());
            fail();
        } catch (EmptyNodeQueryException e) {}
        try {
            find("#doc_" + "doc2".hashCode());
            fail();
        } catch (EmptyNodeQueryException e) {}
        assertThat(find("#doc_" + "doc3".hashCode()), is(notNullValue()));
    }

    @Test
    public void shouldOpenDocument_CloseMenu() throws Exception {
        Document document = mock(Document.class);
        when(document.getName()).thenReturn("my-doc.odt");
        openDocuments(document);

        Task task = mock(Task.class);
        SimpleObjectProperty jCas = new SimpleObjectProperty();
        when(task.valueProperty()).thenReturn(jCas);
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        pressAcceptButton();

        jCas.set(mock(JCas.class));
        waitForFxEvents(ATTEMPTS_COUNT);

        assertThat(find("#menu").getOpacity(), is(equalTo(0.0)));
    }

    @Test
    public void shouldOpenDocument_LoadJCas() throws Exception {
        Document document = mock(Document.class);
        when(document.getName()).thenReturn("my-doc.odt");
        openDocuments(document);

        Task task = mock(Task.class);
        when(task.valueProperty()).thenReturn(new SimpleObjectProperty());
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        pressAcceptButton();

        verify(projectService).loadJCas(document);
        verify(task).valueProperty();
    }

    @Test
    public void shouldOpenSecondDocument_LoadJCas() throws Exception {
        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        Document document2 = mock(Document.class);
        when(document2.getName()).thenReturn("doc2");
        openDocuments(document1, document2);

        Task task = mock(Task.class);
        when(task.valueProperty()).thenReturn(new SimpleObjectProperty());
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        moveSelectionDown();
        pressAcceptButton();

        verify(projectService).loadJCas(document2);
    }

    @Test
    public void shouldOpenAndHighlightFirstDocument() {
        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        Document document2 = mock(Document.class);
        when(document2.getName()).thenReturn("doc2");
        openDocuments(document1, document2);

        Task task = mock(Task.class);
        SimpleObjectProperty simpleObjectProperty = new SimpleObjectProperty();
        when(task.valueProperty()).thenReturn(simpleObjectProperty);
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        pressAcceptButton();

        interact(() -> {
            simpleObjectProperty.set(mock(JCas.class));

            TokenLabel lb = find("#doc_" + "doc1".hashCode());
            assertThat(lb.showSelectedIndicatorProperty().get(), is(true));
            lb = find("#doc_" + "doc2".hashCode());
            assertThat(lb.showSelectedIndicatorProperty().get(), is(false));
        });
    }

    @Test
    public void shouldOpenAndHighlightSecondDocument() {
        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        Document document2 = mock(Document.class);
        when(document2.getName()).thenReturn("doc2");
        openDocuments(document1, document2);

        Task task = mock(Task.class);
        SimpleObjectProperty simpleObjectProperty = new SimpleObjectProperty();
        when(task.valueProperty()).thenReturn(simpleObjectProperty);
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        moveSelectionDown();
        pressAcceptButton();

        interact(() -> {
            simpleObjectProperty.set(mock(JCas.class));

            TokenLabel lb = find("#doc_" + "doc1".hashCode());
            assertThat(lb.showSelectedIndicatorProperty().get(), is(false));
            lb = find("#doc_" + "doc2".hashCode());
            assertThat(lb.showSelectedIndicatorProperty().get(), is(true));
        });
    }

    @Test
    public void shouldDisplayDocumentInformation_WithDocumentTitleAndUri() {
        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        openDocuments(document1);

        Task task = mock(Task.class);
        SimpleObjectProperty simpleObjectProperty = new SimpleObjectProperty();
        when(task.valueProperty()).thenReturn(simpleObjectProperty);
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        pressAcceptButton();

        interact(() -> {
            simpleObjectProperty.set(createHelloWorld());

            Label lb = find("#documentTitle");
            assertThat(lb.getText(), is("Hello_World"));
            Hyperlink link = find("#documentUri");
            assertThat(link.getText(), is("https://en.wikipedia.org/wiki/Hello_World"));
        });
    }

    @Test
    public void shouldDisplayDocumentInformation_WithoutDocumentMetaData() throws Exception {
        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        openDocuments(document1);

        Task task = mock(Task.class);
        SimpleObjectProperty simpleObjectProperty = new SimpleObjectProperty();
        when(task.valueProperty()).thenReturn(simpleObjectProperty);
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        pressAcceptButton();

        JCas text = JCasFactory.createText("Hello World", "en");

        interact(() -> {
            simpleObjectProperty.set(text);

            Label lb = find("#documentTitle");
            assertThat(lb.getText(), is("?"));
            Hyperlink link = find("#documentUri");
            assertThat(link.getText(), is("?"));
        });
    }

    @Test
    public void shouldDisplayDocumentInformation_WithoutDocumentTitleAndUri() throws Exception {
        Document document1 = mock(Document.class);
        when(document1.getName()).thenReturn("doc1");
        openDocuments(document1);

        Task task = mock(Task.class);
        SimpleObjectProperty simpleObjectProperty = new SimpleObjectProperty();
        when(task.valueProperty()).thenReturn(simpleObjectProperty);
        when(projectService.loadJCas(any())).thenReturn(task);

        pressMenuButton();
        moveSelectionToRight();
        pressAcceptButton();

        JCas text = JCasFactory.createText("Hello World", "en");
        DocumentMetaData.create(text);

        interact(() -> {
            simpleObjectProperty.set(text);

            Label lb = find("#documentTitle");
            assertThat(lb.getText(), is("?"));
            Hyperlink link = find("#documentUri");
            assertThat(link.getText(), is("?"));
        });
    }

    private void pressAcceptButton() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isAcceptButtonPressed()).thenReturn(true);
            menuController.consume(action);
        });

        waitForFxEvents(ATTEMPTS_COUNT);
    }

    private void pressMenuButton() {
        waitForFxEvents(ATTEMPTS_COUNT);

        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMenuButtonPressed()).thenReturn(true);
            menuController.consume(action);
        });

        waitForFxEvents(ATTEMPTS_COUNT);
    }

    private void moveSelectionToRight() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionToRight()).thenReturn(true);
            menuController.consume(action);
        });

        waitForFxEvents(ATTEMPTS_COUNT);
    }

    private void moveSelectionToLeft() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionToLeft()).thenReturn(true);
            menuController.consume(action);
        });

        waitForFxEvents(ATTEMPTS_COUNT);
    }

    private void moveSelectionDown() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionDown()).thenReturn(true);
            menuController.consume(action);
        });

        waitForFxEvents(ATTEMPTS_COUNT);
    }

    private void moveSelectionUp() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isMoveSelectionUp()).thenReturn(true);
            menuController.consume(action);
        });

        waitForFxEvents(ATTEMPTS_COUNT);
    }

    private void openDocuments(Document... documents) {
        interact(() -> {
            projectService.documentsProperty().clear();
            projectService.documentsProperty().addAll(documents);
        });
        waitForFxEvents(ATTEMPTS_COUNT);
    }

    @After
    public void closeMenu() {
        menuController.setOpen(false);
        waitForFxEvents(ATTEMPTS_COUNT);
    }

    public static class Config {

        @Bean
        public ProjectService projectService() {
            ProjectService projectService = mock(ProjectService.class);
            when(projectService.documentsProperty()).thenReturn(observableArrayList());
            return projectService;
        }

        @Bean
        public TaskConsumer taskConsumer() {
            return mock(TaskConsumer.class);
        }

        @Bean
        @Qualifier("behavior-consumer")
        public List<AnnotationBehavior> behaviors() {
            AnnotationBehavior segmentation = mock(AnnotationBehavior.class);
            when(segmentation.getName()).thenReturn("Segmentation");

            AnnotationBehavior posTagging = mock(AnnotationBehavior.class);
            when(posTagging.getName()).thenReturn("PosTagging");

            AnnotationBehavior namedEntity = mock(AnnotationBehavior.class);
            when(namedEntity.getName()).thenReturn("NamedEntityTagging");

            return asList(segmentation, posTagging, namedEntity);
        }
    }
}
