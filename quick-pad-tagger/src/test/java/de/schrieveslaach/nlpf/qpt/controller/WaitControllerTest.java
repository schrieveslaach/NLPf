package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.view.WaitView;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.concurrent.Task;
import javafx.scene.control.TextArea;
import javafx.scene.image.WritableImage;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        WaitController.class,
        WaitView.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class WaitControllerTest extends GuiTest {

    @Autowired
    private WaitController waitController;

    @PostConstruct
    public void constructView() throws Exception {
        init(WaitView.class);

        SnapshotProvider snapshotProvider = mock(SnapshotProvider.class);
        when(snapshotProvider.takeSnapshot()).thenReturn(new WritableImage(10, 10));
        waitController.setSnapshotProvider(snapshotProvider);
    }

    @Test
    public void shouldOpenView_StateChangesToRunning() {
        Task<?> task = mockTask();
        waitController.consume(task);

        changeRunStatus(task, true);

        assertThat(find("#wait").getOpacity(), is(equalTo(1.0)));
    }

    @Test
    public void shouldCloseView_StateChangesToRunning() {
        Task<?> task = mockTask();
        waitController.consume(task);

        changeRunStatus(task, true);
        changeRunStatus(task, false);

        assertThat(find("#wait").getOpacity(), is(equalTo(0.0)));
    }

    @Test
    public void shouldCloseView_TaskRaisedException() {
        Task<?> task = mockTask();
        waitController.consume(task);

        changeRunStatus(task, true);
        updateException(task, new Exception("Hello Exception"));

        assertThat(find("#wait").getOpacity(), is(equalTo(0.0)));
    }

    @Test
    public void shouldDisplayMessage() {
        Task<?> task = mockTask();
        waitController.consume(task);

        updateMessage(task, "Hello");
        updateMessage(task, "World");

        TextArea textArea = find("#messages");
        assertThat(textArea.getText(), is(equalTo("Hello\nWorld\n")));
    }

    @Test
    public void shouldConsumeAllActionWhileVisible() {
        Task<?> task = mockTask();
        waitController.consume(task);
        changeRunStatus(task, true);

        boolean consumed = waitController.consume(mock(Action.class));
        assertThat(consumed, is(equalTo(true)));
    }

    private void updateMessage(Task<?> task, String msg) {
        interact(() -> ((SimpleStringProperty) task.messageProperty()).set(msg));
        waitForFxEvents(10);
    }

    private void changeRunStatus(Task<?> task, boolean isRunning) {
        interact(() -> ((SimpleBooleanProperty) task.runningProperty()).set(isRunning));
        waitForFxEvents(100);
    }

    private void updateException(Task<?> task, Exception ex) {
        interact(() -> ((SimpleObjectProperty) task.exceptionProperty()).set(ex));
        waitForFxEvents(100);
    }

    private Task<?> mockTask() {
        Task t = mock(Task.class);
        when(t.runningProperty()).thenReturn(new SimpleBooleanProperty());
        when(t.messageProperty()).thenReturn(new SimpleStringProperty());
        when(t.exceptionProperty()).thenReturn(new SimpleObjectProperty());
        return t;
    }
}

