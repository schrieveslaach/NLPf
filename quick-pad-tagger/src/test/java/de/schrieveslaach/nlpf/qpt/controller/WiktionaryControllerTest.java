package de.schrieveslaach.nlpf.qpt.controller;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.input.Action;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.schrieveslaach.nlpf.qpt.view.WiktionaryView;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.tokit.RegexSegmenter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.WritableImage;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.cas.CASException;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.net.URI;
import java.util.Objects;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        WiktionaryController.class,
        WiktionaryView.class,
        WiktionaryControllerTest.Config.class
})
@TestPropertySource(locations = "classpath:/de/schrieveslaach/nlpf/qpt/controller/test.properties")
public class WiktionaryControllerTest extends GuiTest {

    @Autowired
    private WiktionaryController wiktionaryController;

    @Autowired
    private ObjectProperty<JCas> cas;

    @Autowired
    private ObjectProperty<Selection> selection;

    @PostConstruct
    public void constructView() throws Exception {
        init(WiktionaryView.class);

        SnapshotProvider snapshotProvider = mock(SnapshotProvider.class);
        when(snapshotProvider.takeSnapshot()).thenReturn(new WritableImage(10, 10));
        wiktionaryController.setSnapshotProvider(snapshotProvider);
    }

    @Test
    public void shouldOpenMenu() {
        pressInfoButton();

        assertThat(find("#wiktionary").getOpacity(), is(equalTo(1.0)));
    }

    @Test
    public void shouldCloseMenu() {
        pressInfoButton();
        pressInfoButton();

        assertThat(find("#wiktionary").getOpacity(), is(equalTo(0.0)));
    }

    @Test
    public void shouldLoadWiktionaryArticle_SelectedTokenChanged() throws CASException {
        loadJCasWithText("Microsoft Office");

        Token microsoft = select(cas.get(), Token.class).iterator().next();
        selection.set(new Selection(cas.get(), microsoft));
        waitForFxEvents(10);

        pressInfoButton();

        assertThat(getWebViewLocation(), hasPath("/wiki/Microsoft"));
    }

    @Test
    public void shouldNotLoadWiktionaryArticle_SelectedTokenDoesNotContainLetters() {
        loadJCasWithText(".");

        Token dot = select(cas.get(), Token.class).iterator().next();
        selection.set(new Selection(cas.get(), dot));
        waitForFxEvents(10);

        pressInfoButton();

        assertThat(getWebViewLocation(), hasPath("/wiki/"));
    }

    private URI getWebViewLocation() {
        return URI.create(wiktionaryController.url.get());
    }

    private void pressInfoButton() {
        interact(() -> {
            Action action = mock(Action.class);
            when(action.isInfoButtonPressed()).thenReturn(true);
            wiktionaryController.consume(action);
        });

        waitForFxEvents(100);
    }

    @After
    public void closeMenu() {
        wiktionaryController.setOpen(false);
        waitForFxEvents(100);
    }

    private Matcher<URI> hasPath(String path) {
        return new TypeSafeMatcher<URI>() {
            @Override
            protected boolean matchesSafely(URI uri) {
                return Objects.equals(uri.getPath(), path);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has path ").appendValue(path);
            }

            @Override
            protected void describeMismatchSafely(URI uri, Description mismatchDescription) {
                mismatchDescription.appendText("has path ").appendValue(uri.getPath());
            }
        };
    }

    private void loadJCasWithText(String text) {
        interact(() -> {
            try {
                AggregateBuilder builder = new AggregateBuilder();
                builder.add(createEngineDescription(
                        RegexSegmenter.class
                ));
                AnalysisEngine engine = builder.createAggregate();

                JCas jcas = engine.newJCas();
                jcas.setDocumentText(text);
                jcas.setDocumentLanguage("en");
                DocumentMetaData.create(jcas);

                engine.process(jcas);

                cas.setValue(jcas);
            } catch (Exception ex) {
                throw new AssertionError("this should never happen", ex);
            }
        });
    }

    public static class Config {

        @Bean
        public ObjectProperty<JCas> casObjectProperty() {
            return new SimpleObjectProperty<>();
        }

        @Bean
        public ObjectProperty<Selection> selectionProperty() {
            return new SimpleObjectProperty<>();
        }
    }

}
