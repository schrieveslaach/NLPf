package de.schrieveslaach.nlpf.qpt.input;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.studiohartman.jamepad.ControllerState;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import lombok.SneakyThrows;
import org.junit.Test;

import java.lang.reflect.Field;

import static de.schrieveslaach.nlpf.qpt.input.Action.create;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class ActionTest {

    @Test
    public void shouldCreateAction_KeyEvent_Accept() {
        KeyEvent event = mockKeyEvent(KeyCode.ENTER);
        Action action = create(event);
        assertTrue(action.isAcceptButtonPressed());
    }

    @Test
    public void shouldCreateAction_ControllerState_Accept() {
        ControllerState state = mockControllerState("aJustPressed");
        Action action = create(state);
        assertTrue(action.isAcceptButtonPressed());
    }

    @Test
    public void shouldCreateAction_KeyEvent_Menu() {
        KeyEvent event = mockKeyEvent(KeyCode.M);
        Action action = create(event);
        assertTrue(action.isMenuButtonPressed());
    }

    @Test
    public void shouldCreateAction_ControllerState_Menu() {
        ControllerState state = mockControllerState("guideJustPressed");
        Action action = create(state);
        assertTrue(action.isMenuButtonPressed());
    }

    @Test
    public void shouldCreateAction_KeyEvent_SelectionToLeft() {
        KeyEvent event = mockKeyEvent(KeyCode.LEFT);
        Action action = create(event);
        assertTrue(action.isMoveSelectionToLeft());
    }

    @Test
    public void shouldCreateAction_ControllerState_SelectionToLeft() {
        ControllerState state = mockControllerState("leftStickX", -1.0f);
        Action action = create(state);
        assertTrue(action.isMoveSelectionToLeft());
    }

    @Test
    public void shouldCreateAction_ControllerState_NoSelectionToLeft() {
        ControllerState state = mockControllerState("leftStickX", 0.0f);
        Action action = create(state);
        assertFalse(action.isMoveSelectionToLeft());
    }

    @Test
    public void shouldCreateAction_KeyEvent_SelectionToRight() {
        KeyEvent event = mockKeyEvent(KeyCode.RIGHT);
        Action action = create(event);
        assertTrue(action.isMoveSelectionToRight());
    }

    @Test
    public void shouldCreateAction_ControllerState_SelectionToRight() {
        ControllerState state = mockControllerState("leftStickX", 1.0f);
        Action action = create(state);
        assertTrue(action.isMoveSelectionToRight());
    }

    @Test
    public void shouldCreateAction_ControllerState_NoSelectionToRight() {
        ControllerState state = mockControllerState("leftStickX", 0.0f);
        Action action = create(state);
        assertFalse(action.isMoveSelectionToRight());
    }

    @Test
    public void shouldCreateAction_KeyEvent_SelectionDown() {
        KeyEvent event = mockKeyEvent(KeyCode.DOWN);
        Action action = create(event);
        assertTrue(action.isMoveSelectionDown());
    }

    @Test
    public void shouldCreateAction_ControllerState_SelectionDown() {
        ControllerState state = mockControllerState("leftStickY", -1.0f);
        Action action = create(state);
        assertTrue(action.isMoveSelectionDown());
    }

    @Test
    public void shouldCreateAction_ControllerState_NoSelectionDown() {
        ControllerState state = mockControllerState("leftStickY", 0.0f);
        Action action = create(state);
        assertFalse(action.isMoveSelectionDown());
    }

    @Test
    public void shouldCreateAction_KeyEvent_SelectionUp() {
        KeyEvent event = mockKeyEvent(KeyCode.UP);
        Action action = create(event);
        assertTrue(action.isMoveSelectionUp());
    }

    @Test
    public void shouldCreateAction_ControllerState_SelectionUp() {
        ControllerState state = mockControllerState("leftStickY", 1.0f);
        Action action = create(state);
        assertTrue(action.isMoveSelectionUp());
    }

    @Test
    public void shouldCreateAction_ControllerState_NoSelectionUp() {
        ControllerState state = mockControllerState("leftStickY", 0.0f);
        Action action = create(state);
        assertFalse(action.isMoveSelectionUp());
    }

    @Test
    public void shouldCreateAction_KeyEvent_Info() {
        KeyEvent event = mockKeyEvent(KeyCode.I);
        Action action = create(event);
        assertTrue(action.isInfoButtonPressed());
    }

    @Test
    public void shouldCreateAction_ControllerState_Info() {
        ControllerState state = mockControllerState("yJustPressed");
        Action action = create(state);
        assertTrue(action.isInfoButtonPressed());
    }

    @Test
    public void shouldCreateAction_KeyEvent_Save() {
        KeyEvent event = mockKeyEvent(KeyCode.S);
        when(event.isControlDown()).thenReturn(true);
        Action action = create(event);
        assertTrue(action.isSaveButtonPressed());
    }

    @Test
    public void shouldCreateAction_ControllerState_Save() {
        ControllerState state = mockControllerState("startJustPressed");
        Action action = create(state);
        assertTrue(action.isSaveButtonPressed());
    }

    @Test
    public void shouldCreateAction_ControllerState_ExpandSelection() {
        ControllerState state = mockControllerState("leftTrigger", 1f);
        Action action = create(state);
        assertTrue(action.isLeftTriggerPressed());
    }

    @Test
    public void shouldCreateAction_KeyEvent_ExpandSelection() {
        KeyEvent event = mockKeyEvent(KeyCode.CONTROL);
        when(event.isControlDown()).thenReturn(true);
        Action action = create(event);
        assertTrue(action.isLeftTriggerPressed());
    }

    @Test
    public void shouldCreateAction_ControllerState_RemoveLeftAnnotation() {
        ControllerState state = mockControllerState("xJustPressed");
        Action action = create(state);
        assertTrue(action.isRemoveLeftAnnotationPressed());
    }

    @Test
    public void shouldCreateAction_KeyEvent_RemoveLeftAnnotation() {
        KeyEvent event = mockKeyEvent(KeyCode.DELETE);
        Action action = create(event);
        assertTrue(action.isRemoveLeftAnnotationPressed());
    }

    @Test
    public void shouldCreateAction_ControllerState_RemoveRightAnnotation() {
        ControllerState state = mockControllerState("bJustPressed");
        Action action = create(state);
        assertTrue(action.isRemoveRightAnnotationPressed());
    }

    @Test
    public void shouldCreateAction_KeyEvent_RemoveRightAnnotation() {
        KeyEvent event = mockKeyEvent(KeyCode.END);
        when(event.isShiftDown()).thenReturn(true);
        Action action = create(event);
        assertTrue(action.isRemoveRightAnnotationPressed());
    }

    @Test
    public void shouldCreateAction_KeyEvent_Fullscreen() {
        KeyEvent event = mockKeyEvent(KeyCode.F11);
        Action action = create(event);
        assertTrue(action.isFullscreenPressed());
    }

    @Test
    public void shouldCreateAction_ControllerState_MoveSelectionToBegin() {
        ControllerState state = mockControllerState("dpadUp");
        Action action = create(state);
        assertTrue(action.isMoveSelectionToBegin());
    }

    @Test
    public void shouldCreateAction_KeyEvent_MoveSelectionToBegin() {
        KeyEvent event = mockKeyEvent(KeyCode.HOME);
        when(event.isAltDown()).thenReturn(true);
        Action action = create(event);
        assertTrue(action.isMoveSelectionToBegin());
    }

    @Test
    public void shouldCreateAction_ControllerState_MoveSelectionToEnd() {
        ControllerState state = mockControllerState("dpadDown");
        Action action = create(state);
        assertTrue(action.isMoveSelectionToEnd());
    }

    @Test
    public void shouldCreateAction_KeyEvent_MoveSelectionToEnd() {
        KeyEvent event = mockKeyEvent(KeyCode.END);
        when(event.isAltDown()).thenReturn(true);
        Action action = create(event);
        assertTrue(action.isMoveSelectionToEnd());
    }

    @SneakyThrows
    private ControllerState mockControllerState(String fieldToTrue) {
        return mockControllerState(fieldToTrue, true);
    }

    @SneakyThrows
    private <V> ControllerState mockControllerState(String fieldToValue, V v) {
        ControllerState state = mock(ControllerState.class);
        Field field = state.getClass().getDeclaredField(fieldToValue);
        field.setAccessible(true);
        field.set(state, v);
        return state;
    }


    private KeyEvent mockKeyEvent(KeyCode code) {
        KeyEvent event = mock(KeyEvent.class);
        when(event.getCode()).thenReturn(code);
        return event;
    }
}
