package de.schrieveslaach.nlpf.qpt.input;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.input.event.ActionListener;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import org.junit.Before;
import org.junit.Test;

import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.verifyZeroInteractions;
import static org.mockito.Mockito.when;

public class KeyboardTest {

    private Keyboard keyboard;

    private ActionListener listener;

    @Before
    public void init() {
        listener = mock(ActionListener.class);
        keyboard = new Keyboard();
        keyboard.add(listener);
    }

    @Test
    public void shouldNotInvokeAnyAction_WhenArbitraryKeyHasBeenReleased() {
        KeyEvent event = mock(KeyEvent.class);
        when(event.getCode()).thenReturn(KeyCode.K);
        when(event.getEventType()).thenReturn(KeyEvent.KEY_RELEASED);

        keyboard.handle(event);

        verifyZeroInteractions(listener);
    }

    @Test
    public void shouldInvokeEmptyAction_WhenControlHasBeenReleased() {
        KeyEvent event = mock(KeyEvent.class);
        when(event.getCode()).thenReturn(KeyCode.CONTROL);
        when(event.getEventType()).thenReturn(KeyEvent.KEY_RELEASED);

        keyboard.handle(event);

        Action emptyAction = Action.builder().build();
        verify(listener).handle(emptyAction);
    }
}
