package de.schrieveslaach.nlpf.qpt.model;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Test;

import java.util.List;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createHelloWorld;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.hamcrest.Matchers.hasToString;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.*;

public class SelectionTest {

    @Test
    public void shouldNotBeginWithToken_TokenIsNull() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)));

        assertThat(selection, not(beginsWithToken(null)));
    }

    @Test
    public void shouldNotBeginWithSelection_OtherSelectionIsNull() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)));

        assertThat(selection, not(beginsWithSelection(null)));
    }

    @Test
    public void shouldNotBeginWithSelection_OtherSelectionIsEmpty() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)));

        assertThat(selection, not(beginsWithSelection(new Selection())));
    }

    @Test
    public void shouldNotBeginWithSelection_ThisSelectionIsEmpty() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)));

        assertThat(new Selection(), not(beginsWithSelection(selection)));
    }

    @Test
    public void shouldBeginWithSelection() {
        JCas jCas = createHelloWorld();
        Selection selection1 = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)));
        Selection selection2 = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)));

        assertThat(selection1, beginsWithSelection(selection2));
    }

    @Test
    public void shouldDisplaySelectionAsString_SelectedOneToken() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)).subList(0,1));

        assertThat(selection, hasToString("»Hello« World."));
    }

    @Test
    public void shouldDisplaySelectionAsString_SelectedTwoTokens() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)).subList(0,2));

        assertThat(selection, hasToString("»Hello World«."));
    }

    @Test
    public void shouldDisplaySelectionAsString_SelectedOneTokenInTheMiddle() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)).subList(1,2));

        assertThat(selection, hasToString("Hello »World«."));
    }

    @Test
    public void shouldDisplaySelectionAsString_SelectedLastToken() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)).subList(2,3));

        assertThat(selection, hasToString("Hello World».«"));
    }

    @Test
    public void shouldDisplaySelectionAsString_FullSentence() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)));

        assertThat(selection, hasToString("»Hello World.«"));
    }

    @Test
    public void shouldDetermineOverlapping_Self() {
        JCas jCas = createHelloWorld();
        Selection selection = new Selection(jCas, Lists.newArrayList(select(jCas, Token.class)));

        assertThat(selection, overlapsWith(selection));
    }

    @Test
    public void shouldDetermineOverlapping_OneToken() {
        JCas jCas = createHelloWorld();
        List<Token> tokens = Lists.newArrayList(select(jCas, Token.class));

        Selection selection1 = new Selection(jCas, tokens.get(0));
        Selection selection2 = new Selection(jCas, tokens.subList(0,2));

        assertThat(selection1, overlapsWith(selection2));
        assertThat(selection2, overlapsWith(selection1));
    }

    @Test
    public void shouldNotDetermineOverlapping() {
        JCas jCas = createHelloWorld();
        List<Token> tokens = Lists.newArrayList(select(jCas, Token.class));

        Selection selection1 = new Selection(jCas, tokens.get(0));
        Selection selection2 = new Selection(jCas, tokens.subList(1,2));

        assertThat(selection1, not(overlapsWith(selection2)));
        assertThat(selection2, not(overlapsWith(selection1)));

    }

    public static Matcher<Selection> beginsWithSelection(final Selection selection) {
        return new TypeSafeMatcher<Selection>() {
            @Override
            protected boolean matchesSafely(Selection s) {
                return s.beginsWith(selection);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("begins with selection ").appendValue(selection);
            }
        };
    }


    public static Matcher<Selection> beginsWithToken(final Token token) {
        return new TypeSafeMatcher<Selection>() {
            @Override
            protected boolean matchesSafely(Selection s) {
                return s.beginsWith(token);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("begins with token ").appendValue(token);
            }
        };
    }

    public static Matcher<Selection> overlapsWith(Selection selection) {
        return new TypeSafeMatcher<Selection>() {
            @Override
            protected boolean matchesSafely(Selection s) {
                return s.overlapsWith(selection);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("overlaps with ").appendValue(selection);
            }
        };
    }
}
