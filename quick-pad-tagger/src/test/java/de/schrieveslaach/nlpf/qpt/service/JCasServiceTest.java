package de.schrieveslaach.nlpf.qpt.service;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import org.apache.uima.jcas.JCas;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static de.schrieveslaach.nlpf.qpt.annotation.NamedEntityTaggingBehaviorTest.namedEntity;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createHelloWorld;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.junit.Assert.assertThat;

public class JCasServiceTest {

    private JCasService jCasService;

    @Before
    public void setup() {
        jCasService = new JCasService();
    }

    @Test
    public void shouldManageNamedEntitiesThroughProperty() {
        JCas openNlpExample = createOpenNlpExample();
        jCasService.setJCas(openNlpExample);

        assertThat(jCasService.namedEntitiesProperty(), is(not(empty())));
    }

    @Test
    public void shouldRemoveNamedEntity() {
        JCas openNlpExample = createOpenNlpExample();
        jCasService.setJCas(openNlpExample);

        NamedEntity namedEntity = jCasService.select(NamedEntity.class).iterator().next();

        List<Token> tokens = jCasService.selectAt(Token.class, namedEntity.getBegin(), namedEntity.getEnd());
        jCasService.removeNamedEntity(new Selection(jCasService.jCasProperty().get(), tokens.get(0)));

        assertThat(jCasService.namedEntitiesProperty(), not(contains(namedEntity)));
    }

    @Test
    public void shouldCreateNamedEntity() {
        jCasService.setJCas(createHelloWorld());

        Selection selection = new Selection(jCasService.jCasProperty().get(), jCasService.tokensProperty().get(0));
        jCasService.createNamedEntity(selection, "Person");

        assertThat(jCasService.namedEntitiesProperty(), contains(
                namedEntity("Person", 0, 5)
        ));
    }

    @Test
    public void shouldReplaceNamedEntity() {
        jCasService.setJCas(createHelloWorld());

        Selection selection = new Selection(jCasService.jCasProperty().get(), jCasService.tokensProperty().get(0));
        jCasService.createNamedEntity(selection, "Person");
        jCasService.createNamedEntity(selection, "Organization");

        assertThat(jCasService.namedEntitiesProperty(), contains(
                namedEntity("Organization", 0, 5)
        ));
    }

}
