package de.schrieveslaach.nlpf.qpt.service;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;

import java.io.File;

import static de.schrieveslaach.nlpf.qpt.service.CorpusFactory.createPom;
import static de.schrieveslaach.nlpf.qpt.service.CorpusFactory.createPomPointingToSnapshotRespository;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.*;

public class PomClassLoaderServiceTest {

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    private PomClassLoaderService pomClassLoaderService;

    @Before
    public void init() {
        pomClassLoaderService = new PomClassLoaderService();
    }

    @Test
    public void shouldCreateClassLoader_ContainingConll2000Writer_MavenCentral() throws Exception {
        File pom = createPom(folder.getRoot(),
                "de.tudarmstadt.ukp.dkpro.core",
                "de.tudarmstadt.ukp.dkpro.core.io.conll-asl",
                "1.8.0");

        ClassLoader classLoader = pomClassLoaderService.getClassLoaderFromPom(pom);

        Class<?> conll2000WriterClass = classLoader.loadClass("de.tudarmstadt.ukp.dkpro.core.io.conll.Conll2000Writer");
        assertThat(conll2000WriterClass, is(notNullValue()));
    }

    @Test
    public void shouldCreateClassLoader_ContainingConll2000Writer_ZoidbergSnapshotRepositry() throws Exception {
        File pom = createPomPointingToSnapshotRespository(folder.getRoot(),
                "de.tudarmstadt.ukp.dkpro.core",
                "de.tudarmstadt.ukp.dkpro.core.io.conll-asl",
                "1.9.0");

        ClassLoader classLoader = pomClassLoaderService.getClassLoaderFromPom(pom);

        Class<?> conll2000WriterClass = classLoader.loadClass("de.tudarmstadt.ukp.dkpro.core.io.conll.Conll2000Writer");
        assertThat(conll2000WriterClass, is(notNullValue()));
    }

    @Test
    public void shouldCreateClassLoader_NoneExistingPom() throws Exception {
        expectedException.expectMessage("Could not read pom file");
        expectedException.expect(ServiceException.class);

        File pom = new File(folder.getRoot(), "pom.xml");
        pomClassLoaderService.getClassLoaderFromPom(pom);
    }

    @Test
    public void shouldCreateClassLoader_InvalidCoordinates() throws Exception {
        expectedException.expectMessage("Could not execute `dependency:tree`");
        expectedException.expect(ServiceException.class);

        File pom = createPom(folder.getRoot(),
                "de.tudarmstadt.ukp.dkpro.core",
                "none-existing-artifact",
                "1.8.0");
        pomClassLoaderService.getClassLoaderFromPom(pom);
    }

}
