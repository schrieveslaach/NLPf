package de.schrieveslaach.nlpf.qpt.service;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.model.Document;
import de.schrieveslaach.nlpf.qpt.view.control.LabelSpinnerView;
import de.schrieveslaach.nlpf.testing.TempClassLoaderRule;
import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.tokit.RegexSegmenter;
import javafx.beans.property.SimpleObjectProperty;
import javafx.concurrent.Task;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.rules.ExpectedException;
import org.junit.rules.TemporaryFolder;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.xmlunit.builder.Input;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.URISyntaxException;
import java.util.Collection;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.ExecutorService;

import static de.schrieveslaach.nlpf.qpt.service.CorpusFactory.createPom;
import static de.schrieveslaach.nlpf.qpt.service.CorpusFactory.createPomPointingToSnapshotRespository;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectCovering;
import static org.apache.uima.fit.util.JCasUtil.selectSingleAt;
import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.not;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.io.FileMatchers.anExistingFile;
import static org.junit.Assert.assertThat;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = LabelSpinnerView.class)
public class ProjectServiceTest extends GuiTest {

    @Rule
    public final TempClassLoaderRule tempClassLoaderRule = new TempClassLoaderRule(getClass().getClassLoader());

    @Rule
    public final ExpectedException expectedException = ExpectedException.none();

    @Rule
    public final TemporaryFolder folder = new TemporaryFolder();

    private ProjectService projectService;

    private PomClassLoaderService pomClassLoaderService;

    private ExecutorService executorService;

    private JCasService jCasService;

    @PostConstruct
    public void constructView() throws Exception {
        init(LabelSpinnerView.class);
    }

    @Before
    public void setupMocks() throws Exception {
        pomClassLoaderService = mock(PomClassLoaderService.class);
        when(pomClassLoaderService.getClassLoaderFromPom(any())).then(invocationOnMock -> tempClassLoaderRule.getTempClassLoader());

        executorService = mock(ExecutorService.class);
        when(executorService.submit(any(Runnable.class))).then(invocation -> {
            Task task = invocation.getArgument(0);

            Method call = Task.class.getDeclaredMethod("call");
            call.setAccessible(true);
            Object result;
            try {
                result = call.invoke(task);
                ((SimpleObjectProperty) task.valueProperty()).set(result);
            } catch (InvocationTargetException ex) {
                throw ex.getCause();
            }

            return null;
        });

        jCasService = new JCasService();
        projectService = new ProjectService(pomClassLoaderService, executorService, jCasService);
    }

    @Test
    public void shouldLoadPosTagSet_PennTreebankTagSet() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("pos.tag.set", "ptb");
        File pom = createExampleCorpus(properties);

        projectService.open(pom);

        assertThat(projectService.posTagSetProperty().get(), is("ptb"));
    }

    @Test
    public void shouldLoadDefaultLanguage_NoProperties() throws Exception {
        File pom = createExampleCorpus();

        projectService.open(pom);

        assertThat(projectService.defaultLanguageProperty().get(), is("en"));
    }

    @Test
    public void shouldLoadDefaultLanguageFromProperties() throws Exception {
        Properties properties = new Properties();
        properties.setProperty("default.document.language", "de");
        File pom = createExampleCorpus(properties);

        projectService.open(pom);

        assertThat(projectService.defaultLanguageProperty().get(), is("de"));
    }

    @Test
    public void shouldLoadPosTagSet_NoValueSpecifiedInMavenProperties() throws Exception {
        File pom = createExampleCorpus();

        projectService.open(pom);

        assertThat(projectService.posTagSetProperty().get(), is("ud"));
    }

    @Test
    public void shouldLoadNamedEntitySetFromProperties() {
        Properties properties = new Properties();
        properties.setProperty("named.entity.types", "time,location, antigene");
        File pom = createExampleCorpus(properties);

        projectService.open(pom);

        assertThat(projectService.namedEntitiesProperty(), contains("antigene", "location", "time"));
    }

    @Test
    public void shouldLoadNamedEntitySet_NoValueSpecifiedInMavenProperties() {
        File pom = createExampleCorpus();

        projectService.open(pom);

        assertThat(projectService.namedEntitiesProperty(), contains("location", "organization", "person"));
    }

    @Test
    public void shouldLoadDocuments() {
        File pom = createExampleCorpus();

        projectService.open(pom);

        assertThat(projectService.documentsProperty(), hasSize(4));
    }

    @Test
    public void shouldSaveJCas_WithStrippedExtension() throws Exception {
        File pom = createExampleOdtCorpus();
        projectService.open(pom);

        openDocument("Plain-ODT-with-Title-created-by-LibreOffice.odt");

        JCas jCas = jCasService.jCasProperty().get();
        Token well = new Token(jCas, 0, 4);
        well.addToIndexes();

        projectService.saveJCas();

        File withDoubleExtension = new File(pom.getParent(), "src/main/corpus/Plain-ODT-with-Title-created-by-LibreOffice.odt.odt");
        assertThat(withDoubleExtension, is(not(anExistingFile())));
    }

    @Test
    public void shouldSaveJCas_WithModifiedTokens() throws Exception {
        File pom = createExampleCorpus();
        projectService.open(pom);

        openDocument("vinken.xmi");

        JCas jCas = jCasService.jCasProperty().get();
        Token mr = selectSingleAt(jCas, Token.class, 0, 3);
        POS noun = new POS(jCas, 0, 3);
        noun.setPosValue("NOUN");
        noun.addToIndexes();
        mr.setPos(noun);

        projectService.saveJCas();

        assertThat(
                Input.fromFile(new File(folder.getRoot(), "src/main/corpus/vinken.xmi")),
                isSimilarTo(Input.fromFile(new File(getClass().getResource("vinken-with-noun.xmi").toURI())))
                        .withNodeFilter(
                                toTest -> "type:DocumentMetaData".equals(toTest.getNodeName())
                        )
        );
    }

    @Test
    public void shouldLoadJCas_WithExistingTokens() throws Exception {
        File pom = createExampleCorpus();
        projectService.open(pom);

        openDocument("vinken.xmi");

        assertThat(jCasService.jCasProperty(), is(notNullValue()));
    }

    @Test
    public void shouldLoadJCas_AndTokenizeText() throws Exception {
        File pom = createExampleCorpus();
        projectService.open(pom);

        openDocument("vinken-without-tokens.xmi");

        JCas jCas = jCasService.jCasProperty().get();

        assertThat(jCas, is(notNullValue()));
        assertThat(select(jCas, Token.class), is(not(empty())));
    }

    @Test
    public void shouldNotListDocuments_NoReaderDescription() throws Exception {
        File pom = createExampleCorpus();
        File file = new File(pom.getParentFile(), "src/main/corpus/text.abcde");
        file.createNewFile();

        projectService.open(pom);

        assertThat(projectService.documentsProperty(), not(hasItem(documentWithName("text.abcde"))));
    }

    @Test
    public void shouldNotLoadJCas_InvalidXmi() throws Exception {
        expectedException.expectMessage("Cannot read document");
        expectedException.expect(ServiceException.class);

        File pom = createExampleCorpus();
        projectService.open(pom);

        openDocument("vinken-invalid.xmi");
    }

    @Test
    public void shouldNotLoadDocuments_EmptyCorpus() throws Exception {
        File pom = createPom(folder.newFolder(),
                "de.tudarmstadt.ukp.dkpro.core",
                "de.tudarmstadt.ukp.dkpro.core.io.xmi-asl",
                "1.9.0");

        projectService.open(pom);

        assertThat(projectService, isEmptyProject());
    }

    @Test
    public void shouldReloadDocuments_ChangedProject() throws Exception {
        File pom = createExampleCorpus();
        projectService.open(pom);

        pom = createPom(folder.newFolder(),
                "de.tudarmstadt.ukp.dkpro.core",
                "de.tudarmstadt.ukp.dkpro.core.io.xmi-asl",
                "1.9.0");
        projectService.open(pom);

        assertThat(projectService, isEmptyProject());
    }

    @Test
    public void shouldReloadDocuments_JCasHasBeenLoaded_ChangedProject() throws Exception {
        File pom = createExampleCorpus();
        projectService.open(pom);
        openDocument("vinken.xmi");

        pom = createPom(folder.newFolder(),
                "de.tudarmstadt.ukp.dkpro.core",
                "de.tudarmstadt.ukp.dkpro.core.io.xmi-asl",
                "1.9.0");
        projectService.open(pom);

        assertThat(projectService, isEmptyProject());
    }

    @Test
    public void shouldLoadBestNlpPipeline_PipelineAvailableInClassPath() throws Exception {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(
                RegexSegmenter.class,
                DummyNER.class
        );

        File pom = createExampleCorpus();
        projectService.open(pom);

        openDocument("vinken.xmi");

        Collection<NamedEntity> namedEntities = select(jCasService.jCasProperty().get(), NamedEntity.class);
        assertThat(namedEntities, is(not(empty())));
    }

    @Test
    public void shouldNotRunSegmenter_JCasContainsAlreadyTokensButNoSentences() throws Exception {
        tempClassLoaderRule.storePipelineConfigurationIntoClasspath(
                RegexSegmenter.class
        );

        File pom = createExampleCorpus();
        projectService.open(pom);

        openDocument("Manual+Stack+Overflow.xmi");

        Collection<Sentence> sentences = select(jCasService.jCasProperty().get(), Sentence.class);
        assertThat(sentences, is(empty()));
    }

    @Test
    public void shouldReadDocumentWithDefaultDocumentLanguage_OdtDoesNotContainLanguageProperty() throws Exception {
        File pom = createExampleOdtCorpus();
        projectService.open(pom);

        openDocument("Plain-ODT-with-Title-created-by-LibreOffice.odt");

        JCas jCas = jCasService.jCasProperty().get();
        assertThat(jCas.getDocumentLanguage(), is(equalTo("en")));
    }

    private void openDocument(String documentName) throws ServiceException {
        projectService.loadJCas(projectService.documentsProperty().stream()
                .filter(document -> documentName.equals(document.getName()))
                .findFirst()
                .get());
    }

    private File createExampleCorpus() {
        return createExampleCorpus(new Properties());
    }

    @SneakyThrows({IOException.class, URISyntaxException.class})
    private File createExampleCorpus(Properties properties) {
        File pom = createPomPointingToSnapshotRespository(folder.getRoot(),
                "de.tudarmstadt.ukp.dkpro.core",
                "de.tudarmstadt.ukp.dkpro.core.io.xmi-asl",
                "1.9.0",
                properties);

        File corpusDirectory = folder.newFolder("src", "main", "corpus");

        File vinken = new File(getClass().getResource("vinken.xmi").toURI());
        FileUtils.copyFileToDirectory(vinken, corpusDirectory);

        File vinkenWithoutTokens = new File(getClass().getResource("vinken-without-tokens.xmi").toURI());
        FileUtils.copyFileToDirectory(vinkenWithoutTokens, corpusDirectory);

        File vinkenInvalid = new File(getClass().getResource("vinken-invalid.xmi").toURI());
        FileUtils.copyFileToDirectory(vinkenInvalid, corpusDirectory);

        File manualStackoverflow = new File(getClass().getResource("Manual+Stack+Overflow.xmi").toURI());
        FileUtils.copyFileToDirectory(manualStackoverflow, corpusDirectory);

        return pom;
    }

    @SneakyThrows({IOException.class, URISyntaxException.class})
    private File createExampleOdtCorpus() {
        File pom = createExampleCorpus();

        File corpusDirectory = new File(folder.getRoot(), "src/main/corpus");

        File odt = new File(getClass().getResource("Plain-ODT-with-Title-created-by-LibreOffice.odt").toURI());
        FileUtils.copyFileToDirectory(odt, corpusDirectory);

        return pom;
    }

    public Matcher<ProjectService> isEmptyProject() {
        return new TypeSafeMatcher<ProjectService>() {
            @Override
            protected boolean matchesSafely(ProjectService projectService) {
                return projectService.documentsProperty().isEmpty()
                        && jCasService.jCasProperty().get() == null;
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("is empty project");
            }
        };
    }

    @TypeCapability(
            inputs = {
                    "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token",
                    "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence"},
            outputs = {
                    "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity"})
    public static class DummyNER extends JCasAnnotator_ImplBase {

        @Override
        public void process(JCas aJCas) throws AnalysisEngineProcessException {
            for (Sentence sentence : select(aJCas, Sentence.class)) {
                List<Token> tokens = selectCovering(aJCas, Token.class, sentence);
                if (tokens.isEmpty()) {
                    continue;
                }

                NamedEntity ne = new NamedEntity(aJCas, tokens.get(0).getBegin(), tokens.get(0).getEnd());
                ne.setValue("dummy");
                ne.addToIndexes();
            }
        }
    }

    private static Matcher<Document> documentWithName(String name) {
        return new TypeSafeMatcher<Document>() {
            @Override
            protected boolean matchesSafely(Document document) {
                return document.getName().equals(name);
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("document with name ").appendValue(name);
            }
        };
    }
}
