package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.Parent;
import javafx.scene.control.SpinnerValueFactory;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        LabelSpinnerView.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class LabelSpinnerTest extends GuiTest {

    @PostConstruct
    public void constructView() throws Exception {
        init(LabelSpinnerView.class);
    }

    @Test
    public void shouldShowChangedValue() {
        displayMonths();

        expand();

        setSelectedValue("December");

        assertThat(getDisplayedSelectedValue(), is(equalTo("December")));
    }

    @Test
    public void shouldShowFiveLabelsWithTwelveItems_PredefinedSelectedItem() {
        displayMonths();

        expand();

        assertThat(getDisplayedSelectedValue(), is(equalTo("February")));
    }

    @Test
    public void shouldSelectedNext() {
        displayMonths();

        expand();

        selectNext();
        selectNext();

        assertThat(getDisplayedSelectedValue(), is(equalTo("April")));
    }

    @Test
    public void shouldSelectedPrevious() {
        displayMonths();

        expand();

        selectPrevious();
        selectPrevious();

        assertThat(getDisplayedSelectedValue(), is(equalTo("December")));
    }

    @Test
    public void shouldSetAndShowCollapseText() {
        displayMonths();

        setCollapseText("Hello World");

        assertThat(getDisplayedSelectedValue(), is(equalTo("Hello World")));
    }

    @Test
    public void shouldSetCollapseText_DoNotShowCollapseTextWhileExpanded() {
        displayMonths();

        setCollapseText("Hello World");
        expand();

        assertThat(getDisplayedSelectedValue(), is(equalTo("January")));
    }

    @Test
    public void shouldSelectNextValueWithNextStartingLetter_DisplayingSingleMonth() {
        displayMonths();

        expand();
        selectNextGroup();

        assertThat(getDisplayedSelectedValue(), is(equalTo("March")));
    }

    @Test
    public void shouldSelectNextValueWithNextStartingLetter_DisplayingSingleMonth_SelectedLastEntry() {
        displayMonths("December");

        expand();
        selectNextGroup();

        assertThat(getDisplayedSelectedValue(), is(equalTo("January")));
    }

    @Test
    public void shouldSelectPreviousValueWithNextStartingLetter_DisplayingSingleMonth() {
        displayMonths();

        expand();
        selectPreviousGroup();

        assertThat(getDisplayedSelectedValue(), is(equalTo("January")));
    }

    @Test
    public void shouldSelectNextValueWithNextStartingLetter_DisplayingRepeatedMonth_SelectedLastGroup() {
        displayRepeatedMonths("December-1");

        expand();
        selectNextGroup();

        assertThat(getDisplayedSelectedValue(), is(equalTo("December-3")));
    }

    @Test
    public void shouldSelectNextValueWithNextStartingLetter_DisplayingRepeatedMonth() {
        displayRepeatedMonths();

        expand();
        selectNextGroup();

        assertThat(getDisplayedSelectedValue(), is(equalTo("March-1")));
    }

    @Test
    public void shouldSelectPreviousValueWithNextStartingLetter_DisplayingRepeatedMonth() {
        displayRepeatedMonths("February-3");

        expand();
        selectPreviousGroup();

        assertThat(getDisplayedSelectedValue(), is(equalTo("January-3")));
    }

    @Test
    public void shouldSelectPreviousValueWithNextStartingLetter_DisplayingRepeatedMonth_SelectedFirstGroup() {
        displayRepeatedMonths("January-3");

        expand();
        selectPreviousGroup();

        assertThat(getDisplayedSelectedValue(), is(equalTo("January-1")));
    }

    @Test
    public void shouldSetOpacityZeroForLabelsInTheBackground_SpinnerHasBeenCollapsed() {
        displayMonths();

        collapse();

        Parent backgroundLabel = find("#spinner-label-0").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(0.0)));
        backgroundLabel = find("#spinner-label-1").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(0.0)));
        backgroundLabel = find("#spinner-label-3").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(0.0)));
        backgroundLabel = find("#spinner-label-4").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(0.0)));
    }

    @Test
    public void shouldSetOpacityOneForLabelsInTheBackground_SpinnerHasBeenExpanded() {
        displayMonths();

        expand();

        Parent backgroundLabel = find("#spinner-label-0").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(1.0)));
        backgroundLabel = find("#spinner-label-1").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(1.0)));
        backgroundLabel = find("#spinner-label-3").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(1.0)));
        backgroundLabel = find("#spinner-label-4").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(1.0)));
    }

    @Test
    public void shouldSetOpacityZeroForLabelsInTheBackground_SpinnerHasBeenExpandedAndHasBeenCollapsed() {
        displayMonths();

        expand();
        collapse();

        Parent backgroundLabel = find("#spinner-label-0").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(0.0)));
        backgroundLabel = find("#spinner-label-1").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(0.0)));
        backgroundLabel = find("#spinner-label-3").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(0.0)));
        backgroundLabel = find("#spinner-label-4").getParent();
        assertThat(backgroundLabel.getOpacity(), is(equalTo(0.0)));
    }

    @Test
    public void shouldHighlightCollapsedValue_ShowSelectionIndicator() {
        displayMonths();

        expand();

        setCollapseText("March");

        TokenLabel lb = find("#spinner-label-3");
        assertThat("shows selection indicator", lb.showSelectedIndicatorProperty().get(), is(equalTo(true)));
    }

    @Test
    public void shouldHighlightCollapsedValue_RotateSpinner() {
        displayMonths();

        expand();
        setCollapseText("March");
        selectNext();

        TokenLabel lb = find("#spinner-label-3");
        assertThat("shows selection indicator", lb.showSelectedIndicatorProperty().get(), is(equalTo(false)));
        lb = find("#labelspinner-selection");
        assertThat("shows selection indicator", lb.showSelectedIndicatorProperty().get(), is(equalTo(true)));
    }

    @Test
    public void shouldNotHighlightCollapsedValue_RotateAndCollapseSpinner() {
        displayMonths();

        expand();
        setCollapseText("March");
        selectNext();
        collapse();

        TokenLabel lb = find("#labelspinner-selection");
        assertThat("shows selection indicator", lb.showSelectedIndicatorProperty().get(), is(equalTo(false)));
    }

    @Test
    public void shouldSetSelectedValueEqualToCollapseText_CollapseTextSetHasBeenSet() {
        displayMonths();

        collapse();
        setCollapseText("March");
        expand();

        assertThat(getDisplayedSelectedValue(), is(equalTo("March")));
    }

    @Test
    public void shouldSetFirstValueAsSelectedValue_CollapseTextSetHasBeenSet() {
        displayMonths();

        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.collapseText().set(null);
        });
        collapse();
        expand();

        assertThat(getDisplayedSelectedValue(), is(equalTo("January")));
    }

    @Test
    public void shouldAnimateMinWidth_Expanded() {
        displayMonths();
        expand();

        changeMinWidth(500.0);

        TokenLabel lb = find("#labelspinner-selection");
        assertThat(lb.labelMinWidthProperty().get(), is(equalTo(500.0)));

        LabelSpinner<String> spinner = find("#spinner");
        assertThat(spinner.labelMinWidthProperty().get(), is(equalTo(500.0)));
    }

    @Test
    public void shouldKeepMinWidth_Expanded() {
        displayMonths();

        changeMinWidth(500.0);
        expand();

        TokenLabel lb = find("#labelspinner-selection");
        assertThat(lb.labelMinWidthProperty().get(), is(equalTo(500.0)));

        LabelSpinner<String> spinner = find("#spinner");
        assertThat(spinner.labelMinWidthProperty().get(), is(equalTo(500.0)));
    }

    @Test
    public void shouldAnimateMinWidth_Collapsed() {
        displayMonths();

        changeMinWidth(500.0);

        TokenLabel lb = find("#labelspinner-selection");
        assertThat(lb.labelMinWidthProperty().get(), is(equalTo(500.0)));

        LabelSpinner<String> spinner = find("#spinner");
        assertThat(spinner.labelMinWidthProperty().get(), is(equalTo(500.0)));
    }

    @Test
    public void shouldKeepMinWidth_WhileRotating() {
        displayMonths();
        expand();

        changeMinWidth(500.0);
        selectNext();
        selectNext();

        TokenLabel lb = find("#labelspinner-selection");
        assertThat(lb.labelMinWidthProperty().get(), is(equalTo(500.0)));

        LabelSpinner<String> spinner = find("#spinner");
        assertThat(spinner.labelMinWidthProperty().get(), is(equalTo(500.0)));
    }

    private void changeMinWidth(double width) {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.createMinWidthAnimation(width).play();
        });
        waitForFxEvents(10);
    }

    private void displayRepeatedMonths() {
        displayRepeatedMonths("February-1");
    }

    private void displayRepeatedMonths(String selectedMonth) {

        ObservableList<String> months = FXCollections.observableArrayList(
                "January-1",
                "January-2",
                "January-3",
                "February-1",
                "February-2",
                "February-3",
                "March-1",
                "March-2",
                "March-3",
                "April-1",
                "April-2",
                "April-3",
                "May-1",
                "May-2",
                "May-3",
                "June-1",
                "June-2",
                "June-3",
                "July-1",
                "July-2",
                "July-3",
                "August-1",
                "August-2",
                "August-3",
                "September-1",
                "September-2",
                "September-3",
                "October-1",
                "October-2",
                "October-3",
                "November-1",
                "November-2",
                "November-3",
                "December-1",
                "December-2",
                "December-3"
        );

        displayValues(months, selectedMonth);
    }

    private void displayMonths() {
        displayMonths("February");
    }

    private void displayMonths(String selectedMonth) {

        ObservableList<String> months = FXCollections.observableArrayList(
                "January", "February", "March", "April",
                "May", "June", "July", "August",
                "September", "October", "November", "December"
        );

        displayValues(months, selectedMonth);
    }

    private void displayValues(ObservableList<String> values, String selectedValue) {
        SpinnerValueFactory.ListSpinnerValueFactory valueFactory = new SpinnerValueFactory.ListSpinnerValueFactory<>(values);
        valueFactory.setValue(selectedValue);

        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.setValueFactory(valueFactory);
            spinner.collapseText().set(selectedValue);
        });
    }

    public void selectNextGroup() {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.selectNextGroup();
            // doing twice, this call should be ignore because animation just started
            spinner.selectNextGroup();
        });
        waitForFxEvents(100);
    }

    public void selectNext() {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.selectNext();
            // doing twice, this call should be ignore because animation just started
            spinner.selectNext();
        });
        waitForFxEvents(10);
    }

    public void selectPrevious() {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.selectPrevious();
            // doing twice, this call should be ignore because animation just started
            spinner.selectPrevious();
        });
        waitForFxEvents(10);
    }

    public void selectPreviousGroup() {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.selectPreviousGroup();
            // doing twice, this call should be ignore because animation just started
            spinner.selectPreviousGroup();
        });
        waitForFxEvents(100);
    }

    private void expand() {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.createExpandTransition(true).play();
        });
        waitForFxEvents(10);
    }

    private void collapse() {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.createExpandTransition(false).play();
        });
        waitForFxEvents(10);
    }

    private void setSelectedValue(String value) {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.valueProperty().set(value);
        });
    }

    private void setCollapseText(String text) {
        interact(() -> {
            LabelSpinner<String> spinner = find("#spinner");
            spinner.collapseText().setValue(text);
        });
    }

    private String getDisplayedSelectedValue() {
        return ((TokenLabel) find("#labelspinner-selection")).getText();
    }

}
