package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.Lists;
import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.Node;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Matcher;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

import static de.schrieveslaach.nlpf.qpt.view.control.TokenLabel.TOKEN_LABEL_WIDTH;
import static de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter.REGION_DISTANCE;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.apache.uima.fit.util.JCasUtil.selectFollowing;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.testfx.matcher.base.GeneralMatchers.baseMatcher;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        NamedEntityStreamView.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class NamedEntityStreamTest extends GuiTest {

    private ObjectProperty<JCas> jCas = new SimpleObjectProperty<>();

    @PostConstruct
    public void constructView() throws Exception {
        init(NamedEntityStreamView.class);
    }

    @Test
    public void shouldShowNamedEntitySpinner_ForNoNamedEntity_SpinnerShouldBeTokenSize() {
        loadOpenNlpExample();
        moveSelectionToRight();
        moveSelectionToRight();

        LabelSpinner<String> labelSpinnerThirdToken = find("#spinner-59");
        assertThat(labelSpinnerThirdToken.labelMinWidthProperty().get(), is((double) TOKEN_LABEL_WIDTH));
    }

    @Test
    public void shouldShowNamedEntitySpinner_ForPierreVinken_ShowNamedEntityType() {
        loadOpenNlpExample();

        LabelSpinner<String> labelSpinnerFirstToken = find("#spinner-29");
        assertThat(labelSpinnerFirstToken.valueProperty().get(), is(equalTo("person")));
    }

    @Test
    public void shouldShowNamedEntitySpinner_ForPierreVinken_SpinnerAboveVinkenShouldBeInvisible() {
        loadOpenNlpExample();

        LabelSpinner<String> labelSpinnerFirstToken = find("#spinner-29");
        assertThat(labelSpinnerFirstToken, isOpaque());

        LabelSpinner<String> labelSpinnerSecondToken = find("#spinner-41");
        assertThat(labelSpinnerSecondToken, isTranslucent());
    }

    @Test
    public void shouldShowNamedEntitySpinner_ForPierreVinken_SpinnerShouldOverlapFollowingToken() {
        loadOpenNlpExample();

        LabelSpinner<String> labelSpinnerFirstToken = find("#spinner-29");
        assertThat(labelSpinnerFirstToken.labelMinWidthProperty().get(), is(equalTo(400.0 + REGION_DISTANCE)));
    }

    @Test
    public void shouldNotShowNamedEntitySpinner_SelectionCoversExistingNamedEntities() {
        loadOpenNlpExample();

        moveSelectionToRight();

        LabelSpinner<String> labelSpinnerSecondToken = find("#spinner-41");
        assertThat(labelSpinnerSecondToken, isTranslucent());
    }

    @Test
    public void shouldShowNamedEntitySpinner_SelectionDoesNotCoverExistingNamedEntities() {
        loadOpenNlpExample();

        moveSelectionToRight();
        moveSelectionToRight();

        LabelSpinner<String> labelSpinnerThirdToken = find("#spinner-59");
        assertThat(labelSpinnerThirdToken, isOpaque());
    }

    @Test
    public void shouldNotShowNamedEntitySpinner_DeselectionNoneCoveredNamedEntities() {
        loadOpenNlpExample();

        moveSelectionToRight();
        moveSelectionToRight();
        moveSelectionToRight();

        LabelSpinner<String> labelSpinnerThirdToken = find("#spinner-59");
        assertThat(labelSpinnerThirdToken, isTranslucent());
    }

    private void moveSelectionToRight() {
        interact(() -> {
            NamedEntityStream nes = find("#namedEntityStream");

            List<Token> tokens = selectFollowing(jCas.get(), Token.class, nes.getSelection().getSelectedTokens().get(0), 1);

            if (!tokens.isEmpty()) {
                nes.setSelection(new Selection(jCas.get(), tokens.get(0)));
            }
        });

        waitForFxEvents(20);
    }

    private void loadOpenNlpExample() {
        interact(() -> {
            JCas openNlpExample = createOpenNlpExample();
            this.jCas.setValue(openNlpExample);

            ArrayList<Token> tokens = Lists.newArrayList(select(openNlpExample, Token.class));

            NamedEntityStream nes = find("#namedEntityStream");
            nes.setSelection(new Selection(openNlpExample, tokens.get(0)));
            nes.setJCas(openNlpExample);
            nes.setTokens(tokens);
        });
        waitForFxEvents(10);
    }

    private Matcher<Node> isOpaque() {
        return baseMatcher("Node is opaque", node -> node.getOpacity() == 1.0);
    }

    private Matcher<Node> isTranslucent() {
        return baseMatcher("Node is translucent", node -> node.getOpacity() == 0.0);
    }
}
