package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import com.google.common.collect.ImmutableList;
import com.googlecode.jatl.Html;
import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.scene.web.WebView;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.xmlunit.builder.Input;

import javax.annotation.PostConstruct;
import java.io.StringWriter;

import static org.apache.uima.fit.util.JCasUtil.selectAt;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;
import static org.xmlunit.matchers.CompareMatcher.isSimilarTo;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        NaturalLanguageTextView.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class NaturalLanguageTextTest extends GuiTest {

    @PostConstruct
    public void constructView() throws Exception {
        init(NaturalLanguageTextView.class);
    }

    @Test
    public void shouldShowEmptyDocument_JCasIsNull() {
        show(null);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        html.html()
                .head().end()
                .body()
                .endAll()
                .done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowSelectedSentence() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World.");
        Sentence sentence = new Sentence(jCas, 0, 12);
        sentence.addToIndexes();

        show(jCas);
        changeSelection(jCas, sentence);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence sentence-selected")
                .id("13")
                .text("Hello World.")
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowSelectedSentence_ChangeSelection() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World.\nHello World.\nHello World.");
        Sentence sentence1 = new Sentence(jCas, 0, 12);
        sentence1.addToIndexes();
        Paragraph paragraph = new Paragraph(jCas, 0, 38);
        paragraph.addToIndexes();
        Sentence sentence2 = new Sentence(jCas, 13, 25);
        sentence2.addToIndexes();
        Sentence sentence3 = new Sentence(jCas, 26, 38);
        sentence3.addToIndexes();

        show(jCas);
        changeSelection(jCas, sentence1);
        changeSelection(jCas, sentence2);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence")
                .id("13")
                .text("Hello World.")
                .end()
                .span()
                .classAttr("sentence sentence-selected")
                .id("24")
                .text("Hello World.")
                .end()
                .span()
                .classAttr("sentence")
                .id("29")
                .text("Hello World.")
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowSelectedTokens() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World.");
        Sentence sentence = new Sentence(jCas, 0, 12);
        sentence.addToIndexes();

        Token token1 = new Token(jCas, 0, 5);
        token1.addToIndexes();
        Token token2 = new Token(jCas, 6, 11);
        token2.addToIndexes();
        Token token3 = new Token(jCas, 11, 12);
        token3.addToIndexes();

        show(jCas);
        Selection selection = mock(Selection.class);
        when(selection.isEmpty()).thenReturn(false);
        when(selection.getSelectedSentence()).thenReturn(sentence);
        when(selection.getSelectedTokens()).thenReturn(ImmutableList.of(token2, token3));
        changeSelection(selection);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence sentence-selected")
                .id("13")
                .span()
                .classAttr("token")
                .id("18")
                .text("Hello")
                .end()
                .span()
                .classAttr("token token-selected")
                .id("30")
                .text("World")
                .end()
                .span()
                .classAttr("token token-selected")
                .id("42")
                .text(".")
                .end()
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowSelectedTokens_KeepSelectionAfterReloadingJCas() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World.");
        Sentence sentence = new Sentence(jCas, 0, 12);
        sentence.addToIndexes();

        Token token1 = new Token(jCas, 0, 5);
        token1.addToIndexes();
        Token token2 = new Token(jCas, 6, 11);
        token2.addToIndexes();
        Token token3 = new Token(jCas, 11, 12);
        token3.addToIndexes();

        show(jCas);
        Selection selection = mock(Selection.class);
        when(selection.isEmpty()).thenReturn(false);
        when(selection.getSelectedSentence()).thenReturn(sentence);
        when(selection.getSelectedTokens()).thenReturn(ImmutableList.of(token2, token3));
        changeSelection(selection);

        show(jCas);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence sentence-selected")
                .id("13")
                .span()
                .classAttr("token")
                .id("18")
                .text("Hello")
                .end()
                .span()
                .classAttr("token token-selected")
                .id("30")
                .text("World")
                .end()
                .span()
                .classAttr("token token-selected")
                .id("42")
                .text(".")
                .end()
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowJCas_WithoutParagraphs() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World");
        Sentence sentence = new Sentence(jCas, 0, 11);
        sentence.addToIndexes();

        show(jCas);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence")
                .id("13")
                .text("Hello World")
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowJCas_WithParagraphs() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World.\nHello World.\nHello World.");
        Sentence sentence = new Sentence(jCas, 0, 12);
        sentence.addToIndexes();
        Paragraph paragraph = new Paragraph(jCas, sentence.getBegin(), sentence.getEnd());
        paragraph.addToIndexes();
        sentence = new Sentence(jCas, 13, 25);
        sentence.addToIndexes();
        sentence = new Sentence(jCas, 26, 38);
        sentence.addToIndexes();
        paragraph = new Paragraph(jCas, 13, 38);
        paragraph.addToIndexes();

        show(jCas);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence")
                .id("13")
                .text("Hello World.")
                .end()
                .end();

        body.p().span()
                .classAttr("sentence")
                .id("24")
                .text("Hello World.")
                .end()
                .span()
                .classAttr("sentence")
                .id("29")
                .text("Hello World.")
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowJCas_WithParagraph_AndSentenceInTheBeginningWithoutParagraphs() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World.\nHello World.\nHello World.");
        Sentence sentence = new Sentence(jCas, 0, 12);
        sentence.addToIndexes();
        sentence = new Sentence(jCas, 13, 25);
        sentence.addToIndexes();
        Paragraph paragraph = new Paragraph(jCas, sentence.getBegin(), sentence.getEnd());
        paragraph.addToIndexes();
        sentence = new Sentence(jCas, 26, 38);
        sentence.addToIndexes();
        paragraph = new Paragraph(jCas, 26, 38);
        paragraph.addToIndexes();

        show(jCas);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence")
                .id("13")
                .text("Hello World.")
                .end()
                .end();

        body.p().span()
                .classAttr("sentence")
                .id("18")
                .text("Hello World.")
                .end()
                .end();

        body.p().span()
                .classAttr("sentence")
                .id("29")
                .text("Hello World.")
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowJCas_WithParagraph_AndSentenceInTheMiddleWithoutParagraphs() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World.\nHello World.\nHello World.");
        Sentence sentence = new Sentence(jCas, 0, 12);
        sentence.addToIndexes();
        Paragraph paragraph = new Paragraph(jCas, sentence.getBegin(), sentence.getEnd());
        paragraph.addToIndexes();
        sentence = new Sentence(jCas, 13, 25);
        sentence.addToIndexes();
        sentence = new Sentence(jCas, 26, 38);
        sentence.addToIndexes();
        paragraph = new Paragraph(jCas, 26, 38);
        paragraph.addToIndexes();

        show(jCas);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence")
                .id("13")
                .text("Hello World.")
                .end()
                .end();

        body.p().span()
                .classAttr("sentence")
                .id("24")
                .text("Hello World.")
                .end()
                .end();

        body.p().span()
                .classAttr("sentence")
                .id("29")
                .text("Hello World.")
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    @Test
    public void shouldShowJCas_WithParagraph_AndSentenceInTheEndWithoutParagraphs() throws Exception {
        JCas jCas = JCasFactory.createText("Hello World.\nHello World.\nHello World.");
        Sentence sentence = new Sentence(jCas, 0, 12);
        sentence.addToIndexes();
        sentence = new Sentence(jCas, 13, 25);
        sentence.addToIndexes();
        Paragraph paragraph = new Paragraph(jCas, 0, sentence.getEnd());
        paragraph.addToIndexes();
        sentence = new Sentence(jCas, 26, 38);
        sentence.addToIndexes();

        show(jCas);

        StringWriter htmlContent = new StringWriter();
        Html html = new Html(htmlContent);
        Html body = html
                .html()
                .head().end()
                .body();

        body.p().span()
                .classAttr("sentence")
                .id("13")
                .text("Hello World.")
                .end()
                .span()
                .classAttr("sentence")
                .id("18")
                .text("Hello World.")
                .end()
                .end();

        body.p().span()
                .classAttr("sentence")
                .id("29")
                .text("Hello World.")
                .end()
                .end();

        html.endAll().done();

        assertThatWebBrowserShowsHtmlContent(htmlContent);
    }

    private void show(JCas jCas) {
        interact(() -> {
            NaturalLanguageText naturalLanguageText = find("#naturalLanguageText");
            naturalLanguageText.showJCas(jCas);
        });
        waitForFxEvents();
    }

    private void changeSelection(JCas jCas, Sentence newSelectedSentence) {
        Selection newSelection = mock(Selection.class);
        when(newSelection.isEmpty()).thenReturn(newSelectedSentence == null);
        when(newSelection.getSelectedSentence()).thenReturn(newSelectedSentence);
        if (newSelectedSentence != null) {
            when(newSelection.getSelectedTokens()).thenReturn(
                    ImmutableList.copyOf(selectAt(jCas, Token.class, newSelectedSentence.getBegin(), newSelectedSentence.getEnd()))
            );
        }

        changeSelection(newSelection);
    }

    private void changeSelection(Selection newSelection) {
        interact(() -> {
            NaturalLanguageText naturalLanguageText = find("#naturalLanguageText");
            naturalLanguageText.selectionProperty().set(newSelection);
        });
        waitForFxEvents(10);
    }

    private void assertThatWebBrowserShowsHtmlContent(StringWriter htmlContent) {
        interact(() -> {
            WebView webView = find("#natural-language-browser");
            String htmlString = (String) webView.getEngine().executeScript("document.documentElement.outerHTML");

            assertThat(Input.from(htmlString), isSimilarTo(htmlContent.toString())
                    .withNodeFilter(toTest -> !toTest.getNodeName().equals("script"))
                    .ignoreWhitespace());
        });
    }
}
