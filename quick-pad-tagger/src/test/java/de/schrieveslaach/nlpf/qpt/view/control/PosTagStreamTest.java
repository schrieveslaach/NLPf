package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.resources.MappingProvider;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.jcas.JCas;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.Collection;
import java.util.Iterator;

import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createOpenNlpExample;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createQuickBrownFox;
import static de.schrieveslaach.nlpf.testing.JCasTestFactory.createTorwaldsExampleWithPosTags;
import static de.tudarmstadt.ukp.dkpro.core.api.resources.MappingProviderFactory.createPosMappingProvider;
import static javafx.collections.FXCollections.observableArrayList;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.hamcrest.Matchers.is;
import static org.junit.Assert.assertThat;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        PostagstreamView.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class PosTagStreamTest extends GuiTest {

    private ObservableList<Token> tokens = observableArrayList();

    private ObjectProperty<JCas> jCas = new SimpleObjectProperty<>();

    private PosTagStream posTagStream;

    @PostConstruct
    public void constructView() throws Exception {
        init(PostagstreamView.class);
    }

    @After
    public void cleanUpTokens() {
        interact(() -> {
            tokens.clear();
            jCas.set(null);
        });
    }

    @Test
    public void shouldSelectNextPosTag() {
        show(createOpenNlpExample());

        applyChange(() -> posTagStream.selectNextPosTag());

        LabelSpinner<String> spinner = find("#spinner-29");
        assertThat(spinner.valueProperty().get(), is("ADJ"));
    }

    @Test
    public void shouldSelectPreviousPosTag() {
        show(createOpenNlpExample());

        changeSelection("Vinken");
        applyChange(() -> posTagStream.selectPreviousPosTag());

        LabelSpinner<String> spinner = find("#spinner-41");
        // TODO fix that assert: assertThat(spinner.valueProperty().get(), is("X"));
    }

    @Test
    public void shouldShowJCasWithPOSTags() {
        show(createTorwaldsExampleWithPosTags());

        // Linus Torvalds gives Nvidia…
        LabelSpinner<String> spinner = find("#spinner-29");
        assertThat(spinner.valueProperty().get(), is("NOUN"));

        spinner = find("#spinner-41");
        assertThat(spinner.valueProperty().get(), is("NOUN"));

        spinner = find("#spinner-59");
        assertThat(spinner.valueProperty().get(), is("VERB"));

        spinner = find("#spinner-71");
        assertThat(spinner.valueProperty().get(), is("NOUN"));
    }

    @Test
    public void shouldShowNoCollapsedText_AfterBindingOneTokenWithoutPosTag() throws Exception {
        show(createQuickBrownFoxWithPartiallyAssignedPosTags());

        changeSelection("jumps");

        LabelSpinner<String> spinner = find("#spinner-125");
        assertThat(spinner.valueProperty().get(), is("*"));
        assertThat(spinner.collapseText().get(), is(""));
    }

    private JCas createQuickBrownFoxWithPartiallyAssignedPosTags() throws Exception {
        JCas brownFox = createQuickBrownFox();

        MappingProvider mappingProvider = createPosMappingProvider(
                null,
                "ud",
                brownFox.getDocumentLanguage());
        mappingProvider.configure(brownFox.getCas());

        // Generate some random POS tags to bind later a token without pos tag
        Collection<Token> tokens = select(brownFox, Token.class);
        Iterator<Token> tokenIterator = tokens.iterator();
        for (int i = 0; i < 8; ++i) {
            Token token = tokenIterator.next();

            POS pos = (POS) brownFox.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), token.getBegin(), token.getEnd());
            pos.setPosValue("NOUN");
            pos.addToIndexes();

            token.setPos(pos);
        }

        return brownFox;
    }

    private void applyChange(Runnable runnable) {
        interact(runnable);
        waitForFxEvents(10);
    }

    private void changeSelection(String token) {
        interact(() -> {
            posTagStream.setSelection(new Selection(jCas.get(),
                    tokens.stream()
                            .filter(t -> t.getCoveredText().equals(token))
                            .findFirst()
                            .get()
            ));
        });
    }

    private void show(JCas jcas) {
        interact(() -> {
            jCas.set(jcas);

            MappingProvider mappingProvider = createPosMappingProvider(
                    null,
                    "ud",
                    jcas.getDocumentLanguage());
            try {
                mappingProvider.configure(jcas.getCas());
            } catch (AnalysisEngineProcessException e) {
                e.printStackTrace();
            }

            posTagStream = find("#posTagStream");
            posTagStream.setTags(mappingProvider.getTags());
            posTagStream.setTokens(tokens);

            Collection<Token> newTokens = select(jcas, Token.class);
            tokens.addAll(newTokens);
            posTagStream.setTokens(tokens);

            posTagStream.setSelection(new Selection(jCas.get(), tokens.get(0)));
        });
        waitForFxEvents(10);
    }
}
