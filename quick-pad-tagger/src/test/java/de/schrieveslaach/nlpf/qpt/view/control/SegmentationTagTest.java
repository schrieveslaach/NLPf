package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */


import de.roskenet.jfxsupport.test.GuiTest;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.not;
import static org.hamcrest.Matchers.greaterThan;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.testfx.matcher.base.NodeMatchers.isVisible;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = {
        SegmentationTagView.class
})
public class SegmentationTagTest extends GuiTest {

    private static final int ATTEMPTS_COUNT = 50;
    private SegmentationTag segmentationTag;

    @PostConstruct
    public void constructView() throws Exception {
        init(SegmentationTagView.class);
    }

    @Before
    public void findSegmentationTag() {
        interact(() -> segmentationTag = find("#segmentation-tag"));
    }

    @Test
    public void shouldScaleDown_OnShow() {
        setUpSegmentationTag("Text");
        interact(() -> segmentationTag.close());
        waitForFxEvents(ATTEMPTS_COUNT);


        assertThat(segmentationTag.scaleXProperty().get(), is(0.0));
        assertThat(segmentationTag.scaleYProperty().get(), is(0.0));
    }

    @Test
    public void shouldScaleUp_OnShow() {
        setUpSegmentationTag("Text");
        waitForFxEvents(ATTEMPTS_COUNT);


        assertThat(segmentationTag.scaleXProperty().get(), is(2.0));
        assertThat(segmentationTag.scaleYProperty().get(), is(2.0));
    }

    @Test
    public void shouldBeStandardWidth_NormalToken() {
        setUpSegmentationTag("Text");
        waitForFxEvents(ATTEMPTS_COUNT);

        assertThat(segmentationTag.scaleXProperty().get(), is(2.0));
        assertThat(segmentationTag.background.getWidth(), is(200.0));
    }

    @Test
    public void shouldExtendWidth_LongToken() {
        setUpSegmentationTag("AVeryLongTokenText");
        waitForFxEvents(ATTEMPTS_COUNT);

        assertThat(segmentationTag.scaleXProperty().get(), is(2.0));
        assertThat(segmentationTag.background.getWidth(), greaterThan(200.0));
    }

    @Test
    public void shouldBeVisible_OnShowToken() {
        setUpSegmentationTag();

        assertThat(segmentationTag, isVisible());
    }

    @Test
    public void shouldNotBeVisible_OnShowToken_TextLengthOne() {
        setUpSegmentationTag("A");

        assertThat(segmentationTag, not(isVisible()));
    }


    @Test
    public void shouldConsumeEvent_OnLessThanTenAttempts_MoveLeft() {
        Token token = mock(Token.class);
        setUpSegmentationTag("AB");

        for (int i = 0; i < 9; i++) {
            interact(() -> segmentationTag.moveLeft());
        }

        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        interact(() -> atomicBoolean.set(segmentationTag.moveLeft()));

        assertTrue(atomicBoolean.get());

    }

    @Test
    public void shouldConsumeEvent_OnLessThanTenAttempts_MoveRight() {
        setUpSegmentationTag();

        for (int i = 0; i < 9; i++) {
            interact(() -> segmentationTag.moveRight());
        }

        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        interact(() -> atomicBoolean.set(segmentationTag.moveRight()));

        assertTrue(atomicBoolean.get());
    }

    @Test
    public void shouldConsumeEvent_NotOnBounds_MoveLeft() {
        setUpSegmentationTag();

        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        interact(() -> atomicBoolean.set(segmentationTag.moveLeft()));

        assertTrue(atomicBoolean.get());
    }

    @Test
    public void shouldConsumeEvent_NotOnBounds_MoveRight() {
        setUpSegmentationTag();

        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        interact(() -> atomicBoolean.set(segmentationTag.moveRight()));

        assertTrue(atomicBoolean.get());
    }

    @Test
    public void shouldNotConsumeEvent_OnTenAttempts_MoveLeft() {
        setUpSegmentationTag("AB");

        for (int i = 0; i <= 9; i++) {
            interact(() -> segmentationTag.moveLeft());
        }

        AtomicBoolean atomicBoolean = new AtomicBoolean(true);
        interact(() -> atomicBoolean.set(segmentationTag.moveLeft()));

        assertFalse(atomicBoolean.get());
    }

    @Test
    public void shouldNotConsumeEvent_OnTenAttempts_MoveRight() {
        setUpSegmentationTag("AB");

        for (int i = 0; i <= 9; i++) {
            interact(() -> segmentationTag.moveRight());
        }

        AtomicBoolean atomicBoolean = new AtomicBoolean(false);
        interact(() -> atomicBoolean.set(segmentationTag.moveRight()));

        assertFalse(atomicBoolean.get());
    }

    @Test
    public void shouldReturnCorrectDisplayedText() {
        setUpSegmentationTag("ABCDEFG");

        assertThat(segmentationTag.getDisplayedTokenText(), is("ABCDEFG"));
    }

    @Test
    public void shouldUpdateSegmentationTag_WhileIsVisible_AndShow() {
        setUpSegmentationTag("AB");

        updateToken("CD");

        assertThat(segmentationTag.getDisplayedTokenText(), is("CD"));
        assertTrue(segmentationTag.isVisible());
    }

    @Test
    public void shouldUpdateSegmentationTag_AndClose_SingleCharacterTokensShouldNotBeDisplayed() {
        setUpSegmentationTag("AB");

        updateToken("C");

        assertThat(segmentationTag.getDisplayedTokenText(), is("C"));
        assertFalse(segmentationTag.isVisible());
    }

    private void setUpSegmentationTag() {
        updateToken("ABC");
        interact(() -> segmentationTag.show());
    }

    private void setUpSegmentationTag(String text) {
        updateToken(text);
        interact(() -> segmentationTag.show());
        waitForFxEvents(ATTEMPTS_COUNT);
    }

    private void updateToken(String text) {
        Token token = mock(Token.class);
        when(token.getText()).thenReturn(text);
        interact(() -> segmentationTag.updateToken(token));
        waitForFxEvents(ATTEMPTS_COUNT);
    }
}
