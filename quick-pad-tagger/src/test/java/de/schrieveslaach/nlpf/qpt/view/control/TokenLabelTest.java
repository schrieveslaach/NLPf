package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import javafx.scene.shape.Path;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;

import static de.schrieveslaach.nlpf.qpt.view.control.TokenLabel.TOKEN_LABEL_HEIGHT;
import static de.schrieveslaach.nlpf.qpt.view.control.adapter.PaginationAdapter.REGION_DISTANCE;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        TokenLabelView.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TokenLabelTest extends GuiTest {

    private TokenLabel tokenLabel;

    private static final double VERTICAL_BAR_WIDTH = 10.0;

    private static final double CIRCLE_WIDTH = TOKEN_LABEL_HEIGHT * 0.15 * 2;

    private static final int ATTEMPTS_COUNT = 50;

    @PostConstruct
    public void constructView() throws Exception {
        init(TokenLabelView.class);
    }

    @Before
    public void setUp() {
        tokenLabel = find("#tokenLabel");
        interact(() -> tokenLabel.setText("ABC123"));
        waitForFxEvents(ATTEMPTS_COUNT);
    }

    @Test
    public void shouldReturnCorrectWidth_OnShowVerticalBar() {
        double tokenLabelWidth = tokenLabel.fullWidthProperty().get();

        interact(() -> tokenLabel.showVerticalBar(true));
        waitForFxEvents(ATTEMPTS_COUNT);

        assertThat(tokenLabel.fullWidthProperty().get(), is(tokenLabelWidth + VERTICAL_BAR_WIDTH + REGION_DISTANCE));
    }

    @Test
    public void shouldReturnCorrectWidth_OnShowCircle() {
        double tokenLabelWidth = tokenLabel.fullWidthProperty().get();

        interact(() -> tokenLabel.showCircle(true));
        waitForFxEvents(ATTEMPTS_COUNT);

        assertThat(tokenLabel.fullWidthProperty().get(), is(tokenLabelWidth + CIRCLE_WIDTH + REGION_DISTANCE));
    }

    @Test
    public void shouldReturnCorrectWidth_OnShowVerticalBarAndCircle() {
        double tokenLabelWidth = tokenLabel.fullWidthProperty().get();

        interact(() -> tokenLabel.showCircle(true));
        interact(() -> tokenLabel.showVerticalBar(true));
        waitForFxEvents(ATTEMPTS_COUNT);

        assertThat(tokenLabel.fullWidthProperty().get(), is(tokenLabelWidth + VERTICAL_BAR_WIDTH + CIRCLE_WIDTH + 2 * REGION_DISTANCE));
    }

    @Test
    public void shouldChangeWidth_WithCircleAndVerticalBarEnabled() {
        interact(() -> {
            tokenLabel.showCircle(true);
            tokenLabel.showVerticalBar(true);
            tokenLabel.labelMinWidthProperty().set(500);
        });
        waitForFxEvents(ATTEMPTS_COUNT);

        assertThat(tokenLabel.fullWidthProperty().get(), is(500 + VERTICAL_BAR_WIDTH + CIRCLE_WIDTH + 2 * REGION_DISTANCE));
    }

    @Test
    public void shouldNotShowSelectionIndicatorByDefault() {
        Path path = find("#selection-indicator");

        assertThat(path.getOpacity(), is(equalTo(0.0)));
    }

    @Test
    public void shouldShowSelectionIndicator() {
        Path path = find("#selection-indicator");

        interact(() -> tokenLabel.showSelectedIndicatorProperty().set(true));

        assertThat(path.getOpacity(), is(equalTo(1.0)));
    }

    @Test
    public void shouldHideSelectionIndicator() {
        Path path = find("#selection-indicator");

        interact(() -> tokenLabel.showSelectedIndicatorProperty().set(true));
        interact(() -> tokenLabel.showSelectedIndicatorProperty().set(false));

        assertThat(path.getOpacity(), is(equalTo(0.0)));
    }

}
