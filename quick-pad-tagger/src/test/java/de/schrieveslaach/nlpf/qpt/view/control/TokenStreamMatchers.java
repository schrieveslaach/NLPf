package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.testfx.api.FxRobotContext;

public class TokenStreamMatchers {


    public static Matcher<Token> isSelected() {
        return new TokenLabelColoringMatcher(Coloring.MORE_FOCUS, "is selected");
    }

    public static Matcher<Token> isHighlightedAsCurrentSentence() {
        return new TokenLabelColoringMatcher(Coloring.FOCUS, "is highlighted as part of the current sentence");
    }

    public static Matcher<Token> isOutOfFocus() {
        return new TokenLabelColoringMatcher(Coloring.LESS_FOCUS, "is out of focus");
    }

    private static class TokenLabelColoringMatcher extends TypeSafeMatcher<Token> {
        private final FxRobotContext context = new FxRobotContext();

        private final Coloring coloring;

        private final String descriptionText;

        private TokenLabelColoringMatcher(Coloring coloring, String descriptionText) {
            this.coloring = coloring;
            this.descriptionText = descriptionText;
        }

        @Override
        protected boolean matchesSafely(Token token) {
            TokenLabel tokenLabel = getTokenLabel(token);
            if (tokenLabel == null) {
                return false;
            }
            return tokenLabel.getColoring().equals(coloring);
        }

        private TokenLabel getTokenLabel(Token token) {
            return context.getNodeFinder().lookup("#token-" + token.hashCode()).query();
        }

        @Override
        public void describeTo(Description description) {
            description.appendValue(descriptionText);
        }

        @Override
        protected void describeMismatchSafely(Token token, Description mismatchDescription) {
            TokenLabel tokenLabel = getTokenLabel(token);
            if (tokenLabel == null) {
                mismatchDescription.appendText("did not find token label for ").appendValue(token);
            } else {
                mismatchDescription.appendText("has different coloring ").appendValue(tokenLabel.getColoring());
            }
        }
    }
}
