package de.schrieveslaach.nlpf.qpt.view.control;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import de.schrieveslaach.nlpf.qpt.model.Selection;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import de.tudarmstadt.ukp.dkpro.core.tokit.RegexSegmenter;
import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.scene.Node;
import org.apache.uima.analysis_engine.AnalysisEngine;
import org.apache.uima.fit.factory.AggregateBuilder;
import org.apache.uima.jcas.JCas;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.testfx.service.query.EmptyNodeQueryException;

import javax.annotation.PostConstruct;
import java.util.Collection;

import static de.schrieveslaach.nlpf.qpt.view.control.TokenStreamMatchers.isHighlightedAsCurrentSentence;
import static de.schrieveslaach.nlpf.qpt.view.control.TokenStreamMatchers.isOutOfFocus;
import static de.schrieveslaach.nlpf.qpt.view.control.TokenStreamMatchers.isSelected;
import static javafx.collections.FXCollections.observableArrayList;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;
import static org.apache.uima.fit.util.JCasUtil.select;
import static org.junit.Assert.assertThat;
import static org.junit.Assert.fail;
import static org.testfx.matcher.base.NodeMatchers.isNull;
import static org.testfx.matcher.base.NodeMatchers.isVisible;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = {
        TokenstreamView.class
})
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class TokenStreamTest extends GuiTest {
    private ObservableList<Token> tokens = observableArrayList();

    private ObjectProperty<JCas> jCas = new SimpleObjectProperty<>();

    private ObjectProperty<Selection> selection = new SimpleObjectProperty<>(new Selection());

    private TokenStream tokenStream;

    @PostConstruct
    public void constructView() throws Exception {
        init(TokenstreamView.class);
    }

    @After
    public void cleanUpTokens() {
        // back to first token
        interact(() -> {
            selection.set(new Selection(jCas.get(), tokens.get(0)));
        });

        tokens = observableArrayList();
        jCas = new SimpleObjectProperty<>();
        tokenStream = null;

    }

    private void loadJCas() {
        interact(() -> {
            try {
                AggregateBuilder builder = new AggregateBuilder();
                builder.add(createEngineDescription(
                        RegexSegmenter.class
                ));
                AnalysisEngine engine = builder.createAggregate();

                JCas jcas = engine.newJCas();
                jcas.setDocumentText("The quick brown fox jumps over the lazy dog.\nThe five boxing wizards jump quickly.");
                jcas.setDocumentLanguage("en");
                DocumentMetaData.create(jcas);

                engine.process(jcas);

                tokenStream = find("#tokenStream");
                tokenStream.bindToJCas(jCas);
                tokenStream.bindToTokens(tokens);
                jCas.set(jcas);

                Collection<Token> newTokens = select(jcas, Token.class);
                tokens.addAll(newTokens);

                tokenStream.bindToSelection(selection, () -> {
                });
                selection.set(new Selection(jCas.get(), getFirstToken()));
            } catch (Exception ex) {
                throw new AssertionError("this should never happen", ex);
            }
        });
        waitForFxEvents(10);
    }

    private void nextToken() {
        interact(() -> {
            int newIndex = tokens.indexOf(selection.get().getSelectedTokens().get(0)) + 1;
            selection.set(new Selection(jCas.get(), tokens.get(newIndex)));
        });
        waitForFxEvents(10);
    }

    private void previousToken() {
        interact(() -> {
            int newIndex = tokens.indexOf(selection.get().getSelectedTokens().get(0)) - 1;
            selection.set(new Selection(jCas.get(), tokens.get(newIndex)));
        });
        waitForFxEvents(10);
    }

    private Token getFirstToken() {
        return getToken(0);
    }

    private Token getSecondToken() {
        return getToken(1);
    }

    private Token getToken(int i) {
        return tokens.get(i);
    }

    @Test
    public void shouldShowCurrentSentenceAfterStart() {
        loadJCas();
        Token secondToken = getSecondToken();
        assertThat(secondToken, isHighlightedAsCurrentSentence());

        nextToken();

        Token firstToken = getFirstToken();
        assertThat(firstToken, isHighlightedAsCurrentSentence());
    }

    @Test
    public void shouldShowCurrentSentence_MoveForward() {
        loadJCas();
        // move to token 'five' in next sentence
        for (int i = 0; i < 10; ++i) {
            nextToken();
        }

        Token dog = getToken(8);
        assertThat(dog, isOutOfFocus());
        Token the = getToken(9);
        assertThat(the, isHighlightedAsCurrentSentence());
        Token five = getToken(10);
        assertThat(five, isSelected());
    }

    @Test
    public void shouldShowCurrentSentence_MoveForwardAndBackward() {
        loadJCas();
        // move to token 'five' in next sentence
        for (int i = 0; i < 10; ++i) {
            nextToken();
        }
        for (int i = 0; i < 2; ++i) {
            previousToken();
        }

        Token dog = getToken(8);
        assertThat(dog, isSelected());
        Token the = getToken(9);
        assertThat(the, isOutOfFocus());
        Token five = getToken(10);
        assertThat(five, isOutOfFocus());
    }

    @Test
    public void shouldSelectFirstTokenAfterStart() {
        loadJCas();
        Token firstToken = getFirstToken();
        assertThat(firstToken, isSelected());
    }

    @Test
    public void shouldKeepTokenSelection_AfterChangingWindowSize() {
        loadJCas();
        nextToken();

        interact(() -> window(0).setWidth(1000));

        Token secondToken = getSecondToken();
        assertThat(secondToken, isSelected());
    }

    @Test
    public void shouldSelectFirstTokenAfterStart_MoveForwardAndBackward() {
        loadJCas();

        nextToken();

        previousToken();

        Token firstToken = getFirstToken();
        assertThat(firstToken, isSelected());
    }

    @Test
    public void shouldShowVerticalBar_OnSentenceAndTokenEnd() {
        loadJCas();

        for (int i = 0; i < 8; i++) {
            nextToken();
        }

        interact(() -> {
            Node rec = find("#sentence-split-indicator");
            assertThat(rec, isVisible());
        });
    }

    @Test
    public void shouldNotShowVerticalBar() {
        loadJCas();

        interact(() -> {
            try {
                find("#sentence-split-indicator");
                fail();
            } catch (EmptyNodeQueryException e) {}
        });
    }

    @Test
    public void shouldShowCircle() {
        loadJCas();

        Token token = tokens.get(0);
        token.removeFromIndexes();
        Token firstToken = new Token(jCas.get(), token.getBegin(), token.getEnd() - 1);
        Token secondToken = new Token(jCas.get(), token.getEnd() - 1, token.getEnd());

        firstToken.addToIndexes();
        secondToken.addToIndexes();

        interact(() -> {
            Collection<Token> newTokens = select(jCas.get(), Token.class);
            tokens.clear();
            tokens.addAll(newTokens);
            Node circle = find("#token-split-indicator");
            interact(() -> assertThat(circle, isVisible()));
        });
    }

    @Test
    public void shouldNotShowCircle() {
        loadJCas();

        interact(() -> {
            try {
                find("#token-split-indicator");
                fail();
            } catch (EmptyNodeQueryException e){}
        });
    }
}
