package de.schrieveslaach.nlpf.qpt.view.control.adapter;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.Label;
import javafx.scene.layout.Region;
import lombok.Getter;

import java.util.function.Function;

public class IntStream extends Region {

    @Getter
    private ObservableList<Integer> ints = FXCollections.observableArrayList();

    PaginationAdapter<Label, Integer> paginationAdapter = new PaginationAdapter<>(
            () -> {
                Label label = new Label();
                label.setMinWidth(200);
                label.setMaxWidth(200);
                return label;
            },
            (node, value) -> {
                if (value != null) {
                    node.setId(idMapper().apply(value));
                    node.setText(textMapper().apply(value));
                } else {
                    node.setId(null);
                    node.setText(null);
                }
            },
            lb -> lb.widthProperty());

    public IntStream() {
        setId("int-stream");
        widthProperty().addListener(paginationAdapter);

        paginationAdapter.regionsProperty().addListener((ListChangeListener<Label>) c -> {
            while (c.next()) {
                getChildren().removeAll(c.getRemoved());
                getChildren().addAll(c.getAddedSubList());
            }
        });

        ints.addListener(paginationAdapter);
    }

    public Function<Integer, String> textMapper() {
        return i -> i.toString();
    }

    public Function<Integer, String> idMapper() {
        return i -> "node-" + i;
    }
}
