package de.schrieveslaach.nlpf.qpt.view.control.adapter;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.roskenet.jfxsupport.test.GuiTest;
import javafx.scene.Node;
import javafx.scene.layout.AnchorPane;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.PostConstruct;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import static com.google.common.primitives.Ints.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.junit.Assert.assertThat;
import static org.testfx.util.WaitForAsyncUtils.waitForFxEvents;

@RunWith(SpringRunner.class)
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
@SpringBootTest(classes = {})
public class PaginationAdapterTest extends GuiTest {

    private IntStream stream;

    @PostConstruct
    public void constructView() throws Exception {
        init(PaginationAdapterView.class);
    }

    @Before
    public void findStream() {
        stream = find("#int-stream");
    }

    @Test
    public void shouldLayoutRegions_WithCorrectTranslate_OnCreate() {
        addAll(0, 1, 2, 3, 4);

        for (int i = 0; i < 4; i++) {
            Node node = find("#node-" + i);
            assertThat(node.getTranslateX(), is(stream.getWidth() / 2 - 100 + i * 220));
        }
    }

    @Test
    public void shouldLayoutRegions_WithCorrectTranslate_OnAddSingle() {
        //Setup
        addAll(0, 1, 2, 3, 4);

        add(1, 11);

        //Check if node has correct translateXProperty for index 1.
        Node newNode = find("#node-11");
        assertThat(newNode.getTranslateX(), is(stream.getWidth() / 2 - 100 + 1 * 220));
    }

    @Test
    public void shouldLayoutRegions_WithCorrectTranslate_OnAddAll() {
        //Setup
        addAll(0, 1, 2, 3, 4);

        List<Integer> values = Arrays.asList(11, 22, 33);
        addAll(1, values);

        //query each added value and check
        // if the corresponding node has the right translateXProperty to its index.
        int i = 1;
        for (Integer value : values) {
            Node node = find("#node-" + value);
            assertThat(node.getTranslateX(), is(stream.getWidth() / 2 - 100 + i * 220));
            ++i;
        }
    }

    @Test
    public void shouldLayoutRegions_OnRemoveSingle() {
        //setup
        addAll(0, 1, 2, 3, 4);

        removeIndex(1);

        Node node = find("#node-" + 2);
        assertThat(node.getTranslateX(), is(stream.getWidth() / 2 - 100 + 1 * 220));
    }

    @Test
    public void shouldLayoutRegions_OnRemoveAll() {
        //setup
        addAll(0, 1, 2, 3, 4);

        removeAll(1, 3);

        Node node = find("#node-" + 3);
        assertThat(node.getTranslateX(), is(stream.getWidth() / 2 - 100 + 1 * 220));
    }

    @Test
    @Ignore // TODO: ignored due to unknown error in Gitlab pipeline (different dimensions)
    public void shouldLayoutRegions_WithCorrectTranslate_MoveToSecondElement() {
        addAll(0, 1, 2, 3, 4);

        scrollToValue(1);

        Node node = find("#node-1");
        assertThat(node.getTranslateX(), is(350.0));
    }

    @Test
    @Ignore // TODO: ignored due to unknown error in Gitlab pipeline (different dimensions)
    public void shouldLayoutRegions_WithCorrectTranslate_MoveToThirdElement() {
        addAll(0, 1, 2, 3, 4);

        scrollToValue(2);

        Node node = find("#node-2");
        assertThat(node.getTranslateX(), is(350.0));
    }

    @Test
    public void shouldLayoutRegionsOfChildPaginationAdapter() {
        addSqrtIntStreamWith(0, 1, 2, 3, 4);

        scrollToValue(1);

        Node node = find("#sqrt-node-1");
        assertThat(node.getTranslateX(), is(350.0));
    }

    @Test
    @Ignore // TODO: ignored due to unknown error in Gitlab pipeline (different dimensions)
    public void shouldLayoutRegionsOfChildPaginationAdapter_MoveToThirdElement() {
        addSqrtIntStreamWith(0, 1, 2, 3, 4);

        scrollToValue(2);

        Node node = find("#sqrt-node-2");
        assertThat(node.getTranslateX(), is(350.0));
    }

    private void addSqrtIntStreamWith(Integer... ints) {
        SquareRootIntStream sris = new SquareRootIntStream();

        AnchorPane.setLeftAnchor(sris, 0.0);
        AnchorPane.setRightAnchor(sris, 0.0);
        AnchorPane.setBottomAnchor(sris, 100.0);

        interact(() -> {
            AnchorPane pane = find("#pane");
            pane.getChildren().add(sris);
            stream.paginationAdapter.addChildPaginationAdapter(sris.paginationAdapter);

            sris.getInts().addAll(ints);
        });

        addAll(ints);
    }

    private void scrollToValue(int value) {
        interact(() -> stream.paginationAdapter.createScrollToAnimation(asList(value)).play());
        waitForFxEvents(100);
    }

    private void addAll(Integer... ints) {
        interact(() -> stream.getInts().addAll(ints));
        waitForFxEvents(10);
    }

    private void add(int index, int value) {
        interact(() -> stream.getInts().add(index, value));
        waitForFxEvents(10);
    }

    private void add(int value) {
        interact(() -> stream.getInts().add(value));
        waitForFxEvents(10);
    }

    private void addAll(int index, Collection<Integer> ints) {
        interact(() -> stream.getInts().addAll(index, ints));
        waitForFxEvents(10);
    }

    private void removeIndex(int index) {
        interact(() -> stream.getInts().remove(index));
        waitForFxEvents(10);
    }

    private void removeAll(int from, int to) {
        interact(() -> stream.getInts().remove(from, to));
        waitForFxEvents(10);
    }
}
