package de.schrieveslaach.nlpf.testing;

/*-
 * ========================LICENSE_START=================================
 * testing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class AnalysisEngineDescriptionMatchers {

    public static Matcher<AnalysisEngineDescription> hasConfigurationParameter(final String parameterName, final Matcher<String> parameterMatcher) {
        return new TypeSafeMatcher<AnalysisEngineDescription>() {
            @Override
            protected boolean matchesSafely(AnalysisEngineDescription description) {
                Object parameterValue = description.getAnalysisEngineMetaData().getConfigurationParameterSettings().getParameterValue(parameterName);
                if (parameterValue == null) {
                    return false;
                }
                return parameterMatcher.matches(parameterValue.toString());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has configuration parameter ")
                        .appendValue(parameterName).appendText(" matching ")
                        .appendDescriptionOf(parameterMatcher);
            }
        };
    }
}
