package de.schrieveslaach.nlpf.testing;

/*-
 * ========================LICENSE_START=================================
 * quick-pad-tagger
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.SneakyThrows;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Enumeration;
import java.util.Properties;

public class CorpusFactory {

    @SneakyThrows(IOException.class)
    public static File createPom(File folder, String groupId, String artifactId, String version) {
        File pom = new File(folder, "pom.xml");
        pom.createNewFile();

        try (FileWriter fw = new FileWriter(pom)) {
            fw.write("<project>");
            fw.write("<modelVersion>4.0.0</modelVersion>");

            fw.write("<groupId>de.company</groupId>");
            fw.write("<artifactId>domain-specific-corpus</artifactId>");
            fw.write("<version>1.0-SNAPSHOT</version>");

            fw.write("<dependencies>");
            fw.write("<dependency>");
            fw.write(String.format("<groupId>%s</groupId><artifactId>%s</artifactId><version>%s</version>", groupId, artifactId, version));
            fw.write("</dependency>");
            fw.write("</dependencies>");

            fw.write("</project>");
        }

        return pom;
    }

    public static File createPomPointingToSnapshotRespository(File folder, String groupId, String artifactId, String version) {
        return createPomPointingToSnapshotRespository(folder, groupId, artifactId, version, new Properties());
    }

    @SneakyThrows(IOException.class)
    public static File createPomPointingToSnapshotRespository(File folder, String groupId, String artifactId, String version, Properties properties) {
        File pom = new File(folder, "pom.xml");
        pom.createNewFile();

        try (FileWriter fw = new FileWriter(pom)) {
            fw.write("<project>");
            fw.write("<modelVersion>4.0.0</modelVersion>");

            fw.write("<groupId>de.company</groupId>");
            fw.write("<artifactId>domain-specific-corpus</artifactId>");
            fw.write("<version>1.0-SNAPSHOT</version>");

            fw.write("<properties>");
            Enumeration<?> propertyNames = properties.propertyNames();
            while (propertyNames.hasMoreElements()) {
                String propertyName = propertyNames.nextElement().toString();

                fw.write("<");
                fw.write(propertyName);
                fw.write(">");
                fw.write(properties.getProperty(propertyName));
                fw.write("</");
                fw.write(propertyName);
                fw.write(">");
            }
            fw.write("</properties>");

            fw.write("<dependencies>");
            fw.write("<dependency>");
            fw.write(String.format("<groupId>%s</groupId><artifactId>%s</artifactId><version>%s</version>", groupId, artifactId, version));
            fw.write("</dependency>");
            fw.write("</dependencies>");

            fw.write("<repositories>");
            fw.write("<repository>");
            fw.write("<id>zoidberg</id>");
            fw.write("<url>http://zoidberg.ukp.informatik.tu-darmstadt.de/artifactory/public-snapshots/</url>");
            fw.write("</repository>");
            fw.write("</repositories>");

            fw.write("</project>");
        }

        return pom;
    }
}
