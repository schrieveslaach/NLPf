package de.schrieveslaach.nlpf.testing;

/*-
 * ========================LICENSE_START=================================
 * testing
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import lombok.SneakyThrows;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;

import java.lang.reflect.Field;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Arrays.stream;

public class EngineDescriptionHashing {

    @SneakyThrows(NoSuchAlgorithmException.class)
    public static String hash(AnalysisEngineDescription engineDescription) {
        MessageDigest digest = MessageDigest.getInstance("SHA-512");
        BigInteger hash = new BigInteger(1, digest.digest(toString(engineDescription).getBytes()));

        return String.format("%032X", hash);
    }

    @SneakyThrows({IllegalArgumentException.class, IllegalAccessException.class})
    private static String toString(AnalysisEngineDescription engineDescription) {
        StringBuilder str = new StringBuilder();

        str.append(engineDescription.getAnnotatorImplementationName())
                .append(System.lineSeparator());

        List<Field> fields = stream(ComponentParameters.class.getFields())
                .sorted(Comparator.comparing(Field::getName))
                .collect(Collectors.toList());
        for (Field f : fields) {
            appendParameter(str, engineDescription, f.get(null).toString());
        }

        return str.toString();
    }

    private static void appendParameter(StringBuilder str, AnalysisEngineDescription engineDescription, String param) {
        str.append(param)
                .append('=')
                .append(engineDescription.getMetaData().getConfigurationParameterSettings().getParameterValue(param))
                .append(System.lineSeparator());
    }

}
