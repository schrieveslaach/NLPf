package de.schrieveslaach.nlpf.testing;

/*-
 * ========================LICENSE_START=================================
 * testing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import org.apache.uima.jcas.JCas;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.CoreMatchers.is;

public class JCasMatchers {

    public static Matcher<JCas> hasDocumentBaseUri(String uri) {
        return hasDocumentBaseUri(is(equalTo(uri)));
    }

    public static Matcher<JCas> hasDocumentBaseUri(Matcher<String> uri) {
        return new TypeSafeMatcher<JCas>() {
            @Override
            protected boolean matchesSafely(JCas jCas) {
                DocumentMetaData metaData = DocumentMetaData.get(jCas);
                return uri.matches(metaData.getDocumentBaseUri());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("document base uri ").appendDescriptionOf(uri);
            }
        };
    }

    public static Matcher<JCas> hasDocumentUri(String uri) {
        return hasDocumentUri(is(equalTo(uri)));
    }

    public static Matcher<JCas> hasDocumentUri(Matcher<String> uri) {
        return new TypeSafeMatcher<JCas>() {
            @Override
            protected boolean matchesSafely(JCas jCas) {
                DocumentMetaData metaData = DocumentMetaData.get(jCas);
                return uri.matches(metaData.getDocumentUri());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("document uri ").appendDescriptionOf(uri);
            }
        };
    }
}
