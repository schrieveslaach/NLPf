package de.schrieveslaach.nlpf.testing;

/*-
 * ========================LICENSE_START=================================
 * nlp-maven-plugin
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS;
import de.tudarmstadt.ukp.dkpro.core.api.metadata.type.DocumentMetaData;
import de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity;
import de.tudarmstadt.ukp.dkpro.core.api.resources.MappingProvider;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Paragraph;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import lombok.SneakyThrows;
import org.apache.uima.UIMAException;
import org.apache.uima.fit.factory.JCasFactory;
import org.apache.uima.jcas.JCas;

import static de.tudarmstadt.ukp.dkpro.core.api.resources.MappingProviderFactory.createPosMappingProvider;
import static org.apache.uima.fit.util.JCasUtil.selectSingleAt;

public class JCasTestFactory {

    @SneakyThrows(UIMAException.class)
    public static JCas createQuickBrownFox() {
        JCas jCas = JCasFactory.createText("The quick brown fox jumps over the lazy dog.", "en");

        DocumentMetaData metaData = DocumentMetaData.create(jCas);
        metaData.setDocumentBaseUri("https://en.wikipedia.org/wiki");
        metaData.setDocumentUri("https://en.wikipedia.org/wiki/The_quick_brown_fox_jumps_over_the_lazy_dog");
        metaData.setDocumentId("The-quick-brown-fox-jumps-over-the-lazy-dog");
        metaData.setDocumentTitle("The_quick_brown_fox_jumps_over_the_lazy_dog");
        metaData.addToIndexes();

        Sentence sentence = new Sentence(jCas, 0, 44);
        sentence.addToIndexes();

        Token token = new Token(jCas, 0, 3);
        token.addToIndexes();

        token = new Token(jCas, 4, 9);
        token.addToIndexes();

        token = new Token(jCas, 10, 15);
        token.addToIndexes();

        token = new Token(jCas, 16, 19);
        token.addToIndexes();

        token = new Token(jCas, 20, 25);
        token.addToIndexes();

        token = new Token(jCas, 26, 30);
        token.addToIndexes();

        token = new Token(jCas, 31, 34);
        token.addToIndexes();

        token = new Token(jCas, 35, 39);
        token.addToIndexes();

        token = new Token(jCas, 40, 43);
        token.addToIndexes();

        token = new Token(jCas, 43, 44);
        token.addToIndexes();

        return jCas;
    }

    @SneakyThrows(UIMAException.class)
    public static JCas createHelloWorld() {
        JCas jCas = JCasFactory.createText("Hello World.");

        DocumentMetaData metaData = DocumentMetaData.create(jCas);
        metaData.setDocumentBaseUri("https://en.wikipedia.org/wiki/");
        metaData.setDocumentUri("https://en.wikipedia.org/wiki/Hello_World");
        metaData.setDocumentId("Hello-World");
        metaData.setDocumentTitle("Hello_World");
        metaData.addToIndexes();

        jCas.setDocumentLanguage("en");

        Sentence sentence = new Sentence(jCas, 0, 12);
        sentence.addToIndexes();

        Paragraph paragraph = new Paragraph(jCas, 0, 12);
        paragraph.addToIndexes();

        MappingProvider mappingProvider = createPosMappingProvider(
                null,
                "ud",
                jCas.getDocumentLanguage());
        mappingProvider.configure(jCas.getCas());

        Token token = new Token(jCas, 0, 5);
        token.addToIndexes();

        POS pos = (POS) jCas.getCas().createAnnotation(mappingProvider.getTagType("UH"), 6, 14);
        pos.setPosValue("UH");
        pos.addToIndexes();

        token = new Token(jCas, 6, 11);
        token.addToIndexes();

        pos = (POS) jCas.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 6, 14);
        pos.setPosValue("NOUN");
        pos.addToIndexes();

        token = new Token(jCas, 11, 12);
        token.addToIndexes();

        pos = (POS) jCas.getCas().createAnnotation(mappingProvider.getTagType("PUNCT"), 6, 14);
        pos.setPosValue("PUNCT");
        pos.addToIndexes();

        return jCas;
    }

    @SneakyThrows(UIMAException.class)
    public static JCas createSatyaNadellaExample() {
        JCas text = JCasFactory.createText(
                "Satya Nadella works for Microsoft. He was appointed as CEO on 4 February 2014, " +
                        "succeeding Steve Ballmer.", "en");

        DocumentMetaData metaData = DocumentMetaData.create(text);
        metaData.setDocumentBaseUri("https://en.wikipedia.org/wiki/");
        metaData.setDocumentUri("https://en.wikipedia.org/wiki/Satya_Nadella");
        metaData.addToIndexes();

        //<editor-fold desc="Satya Nadella works for Microsoft.">
        Sentence sentence = new Sentence(text, 0, 34);
        sentence.addToIndexes();

        Token token = new Token(text, 0, 5);
        token.addToIndexes();

        token = new Token(text, 6, 13);
        token.addToIndexes();

        NamedEntity namedEntity = new NamedEntity(text, 0, 13);
        namedEntity.setValue("person");
        namedEntity.addToIndexes();

        token = new Token(text, 14, 19);
        token.addToIndexes();

        token = new Token(text, 20, 23);
        token.addToIndexes();

        token = new Token(text, 24, 33);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 24, 33);
        namedEntity.setValue("organization");
        namedEntity.addToIndexes();

        token = new Token(text, 33, 34);
        token.addToIndexes();
        //</editor-fold>

        //<editor-fold desc="He was appointed as CEO…">
        sentence = new Sentence(text, 35, 104);
        sentence.addToIndexes();

        token = new Token(text, 35, 37);
        token.addToIndexes();

        token = new Token(text, 38, 41);
        token.addToIndexes();

        token = new Token(text, 42, 51);
        token.addToIndexes();

        token = new Token(text, 52, 54);
        token.addToIndexes();

        token = new Token(text, 55, 58);
        token.addToIndexes();

        token = new Token(text, 59, 61);
        token.addToIndexes();

        token = new Token(text, 62, 63);
        token.addToIndexes();

        token = new Token(text, 64, 72);
        token.addToIndexes();

        token = new Token(text, 73, 77);
        token.addToIndexes();

        token = new Token(text, 77, 78);
        token.addToIndexes();

        token = new Token(text, 79, 89);
        token.addToIndexes();

        token = new Token(text, 90, 95);
        token.addToIndexes();

        token = new Token(text, 96, 103);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 90, 103);
        namedEntity.setValue("person");
        namedEntity.addToIndexes();

        token = new Token(text, 103, 104);
        token.addToIndexes();

        //</editor-fold>
        return text;
    }

    @SneakyThrows(UIMAException.class)
    public static JCas createOpenNlpExample() {
        JCas text = JCasFactory.createText(
                "Pierre Vinken, 61 years old, will join the board as a nonexecutive director on Nov. 29. " +
                        "Mr. Vinken is chairman of Elsevier N.V., the Dutch publishing group. " +
                        "Rudolph Agnew, 55 years old and former chairman of Consolidated Gold Fields PLC, " +
                        "was named a nonexecutive director of this British industrial conglomerate. " +
                        "A form of asbestos once used to make Kent cigarette filters has caused a high " +
                        "percentage of cancer deaths among a group of workers exposed to it more than 30 years ago," +
                        " researchers reported.", "en");

        DocumentMetaData metaData = DocumentMetaData.create(text);
        metaData.setDocumentBaseUri("https://opennlp.apache.org/");
        metaData.setDocumentUri("https://opennlp.apache.org/opennlp");
        metaData.addToIndexes();

        text.setDocumentLanguage("en");

        //<editor-fold desc="Pierre Vinken, 61 years old, …">
        Sentence sentence = new Sentence(text, 0, 87);
        sentence.addToIndexes();

        Token token = new Token(text, 0, 6);
        token.addToIndexes();

        token = new Token(text, 7, 13);
        token.addToIndexes();

        NamedEntity namedEntity = new NamedEntity(text, 0, 13);
        namedEntity.setValue("person");
        namedEntity.addToIndexes();

        token = new Token(text, 13, 14);
        token.addToIndexes();

        token = new Token(text, 15, 17);
        token.addToIndexes();

        token = new Token(text, 18, 23);
        token.addToIndexes();

        token = new Token(text, 24, 27);
        token.addToIndexes();

        token = new Token(text, 27, 28);
        token.addToIndexes();

        token = new Token(text, 29, 33);
        token.addToIndexes();

        token = new Token(text, 34, 38);
        token.addToIndexes();

        token = new Token(text, 39, 42);
        token.addToIndexes();

        token = new Token(text, 43, 48);
        token.addToIndexes();

        token = new Token(text, 49, 51);
        token.addToIndexes();

        token = new Token(text, 52, 53);
        token.addToIndexes();

        token = new Token(text, 54, 66);
        token.addToIndexes();

        token = new Token(text, 67, 75);
        token.addToIndexes();

        token = new Token(text, 76, 78);
        token.addToIndexes();

        token = new Token(text, 79, 83);
        token.addToIndexes();

        token = new Token(text, 84, 86);
        token.addToIndexes();

        token = new Token(text, 86, 87);
        token.addToIndexes();
        //</editor-fold>

        //<editor-fold desc="Mr. Vinken is chairman …">
        sentence = new Sentence(text, 88, 156);
        sentence.addToIndexes();

        token = new Token(text, 88, 91);
        token.addToIndexes();

        token = new Token(text, 92, 98);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 88, 98);
        namedEntity.setValue("person");
        namedEntity.addToIndexes();

        token = new Token(text, 99, 101);
        token.addToIndexes();

        token = new Token(text, 102, 110);
        token.addToIndexes();

        token = new Token(text, 111, 113);
        token.addToIndexes();

        token = new Token(text, 114, 122);
        token.addToIndexes();

        token = new Token(text, 123, 127);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 114, 127);
        namedEntity.setValue("organization");
        namedEntity.addToIndexes();

        token = new Token(text, 127, 128);
        token.addToIndexes();

        token = new Token(text, 129, 132);
        token.addToIndexes();

        token = new Token(text, 133, 138);
        token.addToIndexes();

        token = new Token(text, 139, 149);
        token.addToIndexes();

        token = new Token(text, 150, 155);
        token.addToIndexes();

        token = new Token(text, 155, 156);
        token.addToIndexes();
        //</editor-fold>

        //<editor-fold desc="Rudolph Agnew, 55 years old …">
        sentence = new Sentence(text, 157, 312);
        sentence.addToIndexes();

        token = new Token(text, 157, 164);
        token.addToIndexes();

        token = new Token(text, 165, 170);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 157, 170);
        namedEntity.setValue("person");
        namedEntity.addToIndexes();

        token = new Token(text, 170, 171);
        token.addToIndexes();

        token = new Token(text, 172, 174);
        token.addToIndexes();

        token = new Token(text, 175, 180);
        token.addToIndexes();

        token = new Token(text, 181, 184);
        token.addToIndexes();

        token = new Token(text, 185, 188);
        token.addToIndexes();

        token = new Token(text, 189, 195);
        token.addToIndexes();

        token = new Token(text, 196, 204);
        token.addToIndexes();

        token = new Token(text, 205, 207);
        token.addToIndexes();

        token = new Token(text, 208, 220);
        token.addToIndexes();

        token = new Token(text, 221, 225);
        token.addToIndexes();

        token = new Token(text, 226, 232);
        token.addToIndexes();

        token = new Token(text, 233, 236);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 208, 236);
        namedEntity.setValue("organization");
        namedEntity.addToIndexes();

        token = new Token(text, 236, 237);
        token.addToIndexes();

        token = new Token(text, 238, 241);
        token.addToIndexes();

        token = new Token(text, 242, 247);
        token.addToIndexes();

        token = new Token(text, 248, 249);
        token.addToIndexes();

        token = new Token(text, 250, 262);
        token.addToIndexes();

        token = new Token(text, 263, 271);
        token.addToIndexes();

        token = new Token(text, 272, 274);
        token.addToIndexes();

        token = new Token(text, 275, 279);
        token.addToIndexes();

        token = new Token(text, 280, 287);
        token.addToIndexes();

        token = new Token(text, 288, 298);
        token.addToIndexes();

        token = new Token(text, 299, 311);
        token.addToIndexes();

        token = new Token(text, 311, 312);
        token.addToIndexes();
        //</editor-fold>

        //<editor-fold desc="A form of asbestos once used …">
        sentence = new Sentence(text, 313, 503);
        sentence.addToIndexes();

        token = new Token(text, 313, 314);
        token.addToIndexes();

        token = new Token(text, 315, 319);
        token.addToIndexes();

        token = new Token(text, 320, 322);
        token.addToIndexes();

        token = new Token(text, 323, 331);
        token.addToIndexes();

        token = new Token(text, 332, 336);
        token.addToIndexes();

        token = new Token(text, 337, 341);
        token.addToIndexes();

        token = new Token(text, 342, 344);
        token.addToIndexes();

        token = new Token(text, 345, 349);
        token.addToIndexes();

        token = new Token(text, 350, 354);
        token.addToIndexes();

        token = new Token(text, 355, 364);
        token.addToIndexes();

        token = new Token(text, 365, 372);
        token.addToIndexes();

        token = new Token(text, 373, 376);
        token.addToIndexes();

        token = new Token(text, 377, 383);
        token.addToIndexes();

        token = new Token(text, 384, 385);
        token.addToIndexes();

        token = new Token(text, 386, 390);
        token.addToIndexes();

        token = new Token(text, 391, 401);
        token.addToIndexes();

        token = new Token(text, 402, 404);
        token.addToIndexes();

        token = new Token(text, 405, 411);
        token.addToIndexes();

        token = new Token(text, 412, 418);
        token.addToIndexes();

        token = new Token(text, 419, 424);
        token.addToIndexes();

        token = new Token(text, 425, 426);
        token.addToIndexes();

        token = new Token(text, 427, 432);
        token.addToIndexes();

        token = new Token(text, 433, 435);
        token.addToIndexes();

        token = new Token(text, 436, 443);
        token.addToIndexes();

        token = new Token(text, 444, 451);
        token.addToIndexes();

        token = new Token(text, 452, 454);
        token.addToIndexes();

        token = new Token(text, 455, 457);
        token.addToIndexes();

        token = new Token(text, 458, 462);
        token.addToIndexes();

        token = new Token(text, 463, 467);
        token.addToIndexes();

        token = new Token(text, 468, 470);
        token.addToIndexes();

        token = new Token(text, 471, 476);
        token.addToIndexes();

        token = new Token(text, 477, 480);
        token.addToIndexes();

        token = new Token(text, 480, 481);
        token.addToIndexes();

        token = new Token(text, 482, 493);
        token.addToIndexes();

        token = new Token(text, 494, 502);
        token.addToIndexes();

        token = new Token(text, 502, 503);
        token.addToIndexes();
        //</editor-fold>

        return text;
    }

    @SneakyThrows
    public static JCas createTorwaldsExampleWithPosTags() {
        JCas text = createTorwaldsExample();

        MappingProvider mappingProvider = createPosMappingProvider(
                null,
                "ud",
                text.getDocumentLanguage());
        mappingProvider.configure(text.getCas());

        //<editor-fold desc="Linus Torvalds gives…">
        POS pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 0, 5);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 6, 14);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("VERB"), 15, 20);
        pos.setPosValue("VERB");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 21, 27);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("DET"), 28, 31);
        pos.setPosValue("DET");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 32, 38);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PUNCT"), 38, 39);
        pos.setPosValue("PUNCT");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);
        //</editor-fold>

        //<editor-fold desc="Linux creator Linus Torvalds…">
        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 40, 45);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 46, 53);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 54, 59);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 60, 68);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("VERB"), 69, 71);
        pos.setPosValue("VERB");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("ADV"), 71, 74);
        pos.setPosValue("ADV");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("ADJ"), 75, 80);
        pos.setPosValue("ADJ");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("ADP"), 81, 85);
        pos.setPosValue("ADP");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 86, 92);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PUNCT"), 92, 93);
        pos.setPosValue("PUNCT");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);
        //</editor-fold>

        //<editor-fold desc="And he wants you…">
        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("CONJ"), 94, 97);
        pos.setPosValue("CONJ");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PRON"), 98, 100);
        pos.setPosValue("PRON");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("VERB"), 101, 106);
        pos.setPosValue("VERB");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PRON"), 107, 110);
        pos.setPosValue("PRON");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PART"), 111, 113);
        pos.setPosValue("PART");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("VERB"), 114, 118);
        pos.setPosValue("VERB");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PRON"), 119, 121);
        pos.setPosValue("PRON");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PUNCT"), 121, 122);
        pos.setPosValue("PUNCT");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);
        //</editor-fold>

        //<editor-fold desc="Mr. Torvalds did not…">
        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 123, 126);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 127, 135);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("VERB"), 136, 139);
        pos.setPosValue("VERB");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("ADV"), 140, 143);
        pos.setPosValue("ADV");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("VERB"), 144, 153);
        pos.setPosValue("VERB");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("ADP"), 154, 157);
        pos.setPosValue("ADP");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PRON"), 158, 161);
        pos.setPosValue("PRON");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("NOUN"), 162, 170);
        pos.setPosValue("NOUN");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);

        pos = (POS) text.getCas().createAnnotation(mappingProvider.getTagType("PUNCT"), 170, 171);
        pos.setPosValue("PUNCT");
        pos.addToIndexes();
        selectSingleAt(text, Token.class, pos.getBegin(), pos.getEnd()).setPos(pos);
        //</editor-fold>

        return text;
    }

    @SneakyThrows(UIMAException.class)
    public static JCas createTorwaldsExample() {
        JCas text = JCasFactory.createText(
                "Linus Torvalds gives Nvidia the finger. " +
                        "Linux creator Linus Torvalds isn't happy with Nvidia. " +
                        "And he wants you to know it. " +
                        "Mr. Torvalds did not apologize for his behavior.");

        DocumentMetaData metaData = DocumentMetaData.create(text);
        metaData.setDocumentBaseUri("https://tempuri.org/");
        metaData.setDocumentUri("https://tempuri.org/torwalds");
        metaData.addToIndexes();

        text.setDocumentLanguage("en");

        //<editor-fold desc="Linus Torvalds gives…">
        Sentence sentence = new Sentence(text, 0, 39);
        sentence.addToIndexes();

        Token token = new Token(text, 0, 5);
        token.addToIndexes();

        token = new Token(text, 6, 14);
        token.addToIndexes();

        NamedEntity namedEntity = new NamedEntity(text, 0, 14);
        namedEntity.setValue("person");
        namedEntity.addToIndexes();

        token = new Token(text, 15, 20);
        token.addToIndexes();

        token = new Token(text, 21, 27);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 21, 27);
        namedEntity.setValue("organization");
        namedEntity.addToIndexes();

        token = new Token(text, 28, 31);
        token.addToIndexes();

        token = new Token(text, 32, 38);
        token.addToIndexes();

        token = new Token(text, 38, 39);
        token.addToIndexes();
        //</editor-fold>

        //<editor-fold desc="Linux creator Linus Torvalds…">
        sentence = new Sentence(text, 40, 93);
        sentence.addToIndexes();

        token = new Token(text, 40, 45);
        token.addToIndexes();

        token = new Token(text, 46, 53);
        token.addToIndexes();

        token = new Token(text, 54, 59);
        token.addToIndexes();

        token = new Token(text, 60, 68);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 54, 68);
        namedEntity.setValue("person");
        namedEntity.addToIndexes();

        token = new Token(text, 69, 71);
        token.addToIndexes();

        token = new Token(text, 71, 74);
        token.addToIndexes();

        token = new Token(text, 75, 80);
        token.addToIndexes();

        token = new Token(text, 81, 85);
        token.addToIndexes();

        token = new Token(text, 86, 92);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 86, 92);
        namedEntity.setValue("organization");
        namedEntity.addToIndexes();

        token = new Token(text, 92, 93);
        token.addToIndexes();
        //</editor-fold>

        //<editor-fold desc="And he wants you…">
        sentence = new Sentence(text, 94, 122);
        sentence.addToIndexes();

        token = new Token(text, 94, 97);
        token.addToIndexes();

        token = new Token(text, 98, 100);
        token.addToIndexes();

        token = new Token(text, 101, 106);
        token.addToIndexes();

        token = new Token(text, 107, 110);
        token.addToIndexes();

        token = new Token(text, 111, 113);
        token.addToIndexes();

        token = new Token(text, 114, 118);
        token.addToIndexes();

        token = new Token(text, 119, 121);
        token.addToIndexes();

        token = new Token(text, 121, 122);
        token.addToIndexes();
        //</editor-fold>

        //<editor-fold desc="Mr. Torvalds did not…">
        sentence = new Sentence(text, 123, 171);
        sentence.addToIndexes();

        token = new Token(text, 123, 126);
        token.addToIndexes();

        token = new Token(text, 127, 135);
        token.addToIndexes();

        namedEntity = new NamedEntity(text, 123, 135);
        namedEntity.setValue("person");
        namedEntity.addToIndexes();

        token = new Token(text, 136, 139);
        token.addToIndexes();

        token = new Token(text, 140, 143);
        token.addToIndexes();

        token = new Token(text, 144, 153);
        token.addToIndexes();

        token = new Token(text, 154, 157);
        token.addToIndexes();

        token = new Token(text, 158, 161);
        token.addToIndexes();

        token = new Token(text, 162, 170);
        token.addToIndexes();

        token = new Token(text, 170, 171);
        token.addToIndexes();
        //</editor-fold>

        return text;
    }
}
