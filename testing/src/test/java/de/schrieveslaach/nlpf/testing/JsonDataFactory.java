package de.schrieveslaach.nlpf.testing;

/*-
 * ========================LICENSE_START=================================
 * testing
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.schrieveslaach.nlpf.testing.annotators.MyPosTagger;
import de.schrieveslaach.nlpf.testing.annotators.MySegmenter;
import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpNamedEntityRecognizer;
import de.tudarmstadt.ukp.dkpro.core.opennlp.OpenNlpSegmenter;
import lombok.SneakyThrows;
import org.apache.uima.resource.ResourceInitializationException;
import org.json.JSONObject;

import static de.schrieveslaach.nlpf.testing.EngineDescriptionHashing.hash;
import static java.util.Arrays.asList;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

public class JsonDataFactory {

    public static String createNlpToolTestResultOfMySegmenterAndMyPosTagger() {
        return new JSONObject()
                .put("descriptors", asList(
                        MySegmenter.DEFAULT_HASH_CODE + ".xml",
                        MyPosTagger.DEFAULT_HASH_CODE + ".xml"
                ))
                .put("measures", asList(
                        new JSONObject()
                                .put("analysisEngineName", "MyPosTagger")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.lexmorph.type.pos.POS")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.2857)
                                        .put("precision", 0.3103)
                                        .put("recall", 0.2647)),
                        new JSONObject()
                                .put("analysisEngineName", "MySegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", -1.0)
                                        .put("precision", 0.0)
                                        .put("recall", 0.0)),
                        new JSONObject()
                                .put("analysisEngineName", "MySegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.8057)
                                        .put("precision", 0.8615)
                                        .put("recall", 0.7567))
                ))
                .toString();
    }

    @SneakyThrows(ResourceInitializationException.class)
    public static String createNlpToolTestResultOfOpenNlpSegmenterAndOpenNlpNamedEntityRecognizerWithPersonAndModelVariant() {
        return new JSONObject()
                .put("descriptors", asList(
                        hash(createEngineDescription(OpenNlpSegmenter.class)) + ".xml",
                        hash(createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "person")) + ".xml",
                        hash(createEngineDescription(OpenNlpNamedEntityRecognizer.class, ComponentParameters.PARAM_VARIANT, "organization")) + ".xml"
                ))
                .put("measures", asList(
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpNamedEntityRecognizer")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity")
                                .put("variant", "organization")
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.5714)
                                        .put("precision", 1.0)
                                        .put("recall", 0.4)),
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpNamedEntityRecognizer")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity")
                                .put("variant", "person")
                                .put("measure", new JSONObject()
                                        .put("f-measure", 0.5)
                                        .put("precision", 0.75)
                                        .put("recall", 0.375)),
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpSegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 1.0)
                                        .put("precision", 1.0)
                                        .put("recall", 1.0)),
                        new JSONObject()
                                .put("analysisEngineName", "OpenNlpSegmenter")
                                .put("outputType", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token")
                                .put("variant", JSONObject.NULL)
                                .put("measure", new JSONObject()
                                        .put("f-measure", 1.0)
                                        .put("precision", 1.0)
                                        .put("recall", 1.0))
                ))
                .toString();
    }
}
