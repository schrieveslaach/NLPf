package de.schrieveslaach.nlpf.testing;

/*-
 * ========================LICENSE_START=================================
 * testing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import org.apache.uima.resource.ResourceCreationSpecifier;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;

public class ResourceCreationSpecifierMatchers {

    public static <D extends ResourceCreationSpecifier> Matcher<D> hasImplementationName(Class<?> clazz) {
        return new TypeSafeMatcher<D>() {
            @Override
            protected boolean matchesSafely(D description) {
                return clazz.getName().equals(description.getImplementationName());
            }

            @Override
            public void describeTo(Description description) {
                description.appendText("has implementation name ").appendValue(clazz.getCanonicalName());
            }
        };
    }
}
