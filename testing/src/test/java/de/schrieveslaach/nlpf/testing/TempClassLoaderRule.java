package de.schrieveslaach.nlpf.testing;

/*-
 * ========================LICENSE_START=================================
 * effective-plumbing
 * %%
 * Copyright (C) 2017 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 * 
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import lombok.Getter;
import lombok.NonNull;
import lombok.SneakyThrows;
import org.apache.commons.io.FileUtils;
import org.apache.uima.analysis_component.JCasAnnotator_ImplBase;
import org.apache.uima.analysis_engine.AnalysisEngineDescription;
import org.apache.uima.resource.ResourceInitializationException;
import org.junit.rules.TestRule;
import org.junit.runner.Description;
import org.junit.runners.model.Statement;
import org.xml.sax.SAXException;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Random;

import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

public class TempClassLoaderRule implements TestRule {

    private final ClassLoader baseClassLoader;

    private Random r = new Random(System.currentTimeMillis());

    /**
     * The directory which has been added temporally to the classpath
     */
    private File tempDirectory;

    @Getter
    private ClassLoader tempClassLoader;

    public TempClassLoaderRule(@NonNull ClassLoader baseClassLoader) {
        this.baseClassLoader = baseClassLoader;
    }

    public void addMetaInfEntry(String dir, String file, String content) {
        File metaInfDirectory = getMetaInfDirectory();
        File contentDirectory = new File(metaInfDirectory, dir);
        contentDirectory.mkdirs();

        writeContent(file, content, contentDirectory);
    }

    public void addResource(String dir, String file, String content) {
        File contentDirectory = new File(tempDirectory, dir);
        contentDirectory.mkdirs();

        writeContent(file, content, contentDirectory);
    }

    @SneakyThrows(IOException.class)
    private void writeContent(String file, String content, File contentDirectory) {
        File contentFile = new File(contentDirectory, file);
        try (FileOutputStream fos = new FileOutputStream(contentFile); PrintWriter printWriter = new PrintWriter(fos);) {
            printWriter.print(content);
            printWriter.flush();
        }
    }

    private File getMetaInfDirectory() {
        return new File(tempDirectory, "META-INF");
    }

    @Override
    public Statement apply(Statement base, Description description) {
        return new Statement() {
            @Override
            public void evaluate() throws Throwable {
                tempDirectory = new File(FileUtils.getTempDirectory(), "url-classloader-" + r.nextInt());
                tempClassLoader = new TempClassLoader(baseClassLoader);

                try {
                    base.evaluate();
                } finally {
                    FileUtils.deleteDirectory(tempDirectory);
                }
            }
        };
    }

    @SneakyThrows({ResourceInitializationException.class, IOException.class, SAXException.class})
    public void storePipelineConfigurationIntoClasspath(Class<? extends JCasAnnotator_ImplBase>... annotators) {
        StringBuilder pipelinesTxt = new StringBuilder();
        for (Class<? extends JCasAnnotator_ImplBase> cls : annotators) {
            String filename = cls.getSimpleName() + ".xml";

            pipelinesTxt.append("classpath*:")
                    .append(filename)
                    .append("\n");

            AnalysisEngineDescription description = createEngineDescription(cls);
            StringWriter descriptionXml = new StringWriter();
            description.toXML(descriptionXml);

            addResource("", filename, descriptionXml.toString());
        }

        addMetaInfEntry(
                "de.schrieveslaach.effective.nlp.application.development",
                "pipeline.txt",
                pipelinesTxt.toString());
    }


    private class TempClassLoader extends ClassLoader {

        public TempClassLoader(ClassLoader classLoader) {
            super(classLoader);
        }

        @SneakyThrows(MalformedURLException.class)
        @Override
        public URL getResource(String s) {
            File resourceFile = new File(tempDirectory, s);
            if (!resourceFile.isFile()) {
                return super.getResource(s);
            }

            return resourceFile.toURI().toURL();
        }
    }
}
