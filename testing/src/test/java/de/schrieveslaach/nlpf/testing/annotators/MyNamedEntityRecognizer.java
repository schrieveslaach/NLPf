package de.schrieveslaach.nlpf.testing.annotators;

/*-
 * ========================LICENSE_START=================================
 * testing
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.parameter.ComponentParameters;
import lombok.Getter;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.component.JCasAnnotator_ImplBase;
import org.apache.uima.fit.descriptor.ConfigurationParameter;
import org.apache.uima.fit.descriptor.ResourceMetaData;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;

@TypeCapability(
        inputs = {"de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token"},
        outputs = {"de.tudarmstadt.ukp.dkpro.core.api.ner.type.NamedEntity"}
)
@ResourceMetaData(name = "MyNamedEntityRecognizer")
public class MyNamedEntityRecognizer extends JCasAnnotator_ImplBase {

    @Getter
    private static int runs = 0;

    public static void resetRuns() {
        runs = 0;
    }

    public static final String PARAM_VARIANT = ComponentParameters.PARAM_VARIANT;
    @ConfigurationParameter(name = PARAM_VARIANT, mandatory = true, defaultValue = "person")
    protected String variant;

    @Override
    public void process(JCas aJCas) throws AnalysisEngineProcessException {
        // This annotator won't expect to create any named entities. Its purpose is to test
        // the creation and testing of combinations of NLP pipelines
        ++runs;
    }
}
