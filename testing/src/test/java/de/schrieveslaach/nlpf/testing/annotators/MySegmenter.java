package de.schrieveslaach.nlpf.testing.annotators;

/*-
 * ========================LICENSE_START=================================
 * testing
 * %%
 * Copyright (C) 2017 - 2018 Schrieveslaach
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Lesser Public License for more details.
 *
 * You should have received a copy of the GNU General Lesser Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/lgpl-3.0.html>.
 * =========================LICENSE_END==================================
 */

import de.tudarmstadt.ukp.dkpro.core.api.segmentation.SegmenterBase;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence;
import de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token;
import lombok.Getter;
import org.apache.uima.analysis_engine.AnalysisEngineProcessException;
import org.apache.uima.fit.descriptor.ResourceMetaData;
import org.apache.uima.fit.descriptor.TypeCapability;
import org.apache.uima.jcas.JCas;
import org.apache.uima.resource.ResourceInitializationException;

import static de.schrieveslaach.nlpf.testing.EngineDescriptionHashing.hash;
import static org.apache.commons.lang3.StringUtils.isBlank;
import static org.apache.uima.fit.factory.AnalysisEngineFactory.createEngineDescription;

@TypeCapability(
        outputs = {"de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Token", "de.tudarmstadt.ukp.dkpro.core.api.segmentation.type.Sentence"}
)
@ResourceMetaData(name = "MySegmenter")
public class MySegmenter extends SegmenterBase {

    public static final String DEFAULT_HASH_CODE;

    static {
        try {
            DEFAULT_HASH_CODE = hash(createEngineDescription(MySegmenter.class));
        } catch (ResourceInitializationException e) {
            throw new AssertionError(e);
        }
    }

    @Getter
    private static int runs = 0;

    public static void resetRuns() {
        runs = 0;
    }

    @Override
    protected void process(JCas jCas, String s, int i) throws AnalysisEngineProcessException {
        Sentence sentence = new Sentence(jCas, 0, s.length());
        sentence.addToIndexes();

        int begin = 0;
        for (String token : s.split("\\s+")) {
            Token t = new Token(jCas, begin, begin + token.length());
            t.addToIndexes();

            begin = t.getEnd();
            s = s.substring(token.length());

            if (!isBlank(s)) {
                int whitespaceOffset = 0;
                while (Character.isWhitespace(s.charAt(whitespaceOffset))) {
                    ++whitespaceOffset;
                }
                begin += whitespaceOffset;
                s = s.substring(whitespaceOffset);
            }
        }

        ++runs;
    }
}

